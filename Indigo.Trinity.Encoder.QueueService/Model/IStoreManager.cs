﻿using System.Collections.Generic;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Gives a way to store the data
    /// </summary>
    interface IStoreManager {
        /// <summary>
        /// Saves the data to the store
        /// </summary>
        /// <param name="data">Data to be stored</param>
        /// <returns>Success or failure of the operation</returns>
        void Save(byte[] data);
        /// <summary>
        /// Gets the data from the store
        /// </summary>
        /// <returns>Data stored in the store</returns>
        byte[] Get();
        /// <summary>
        /// Configurations for the data store
        /// </summary>
        Dictionary<string, string> Config { set; }
    }
}