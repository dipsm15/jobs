﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
namespace Indigo.Trinity.Encoder.QueueService {
    internal class FileStoreManager : IStoreManager {
        #region Private Variables
        private static string _lockString = "This is a lock string, to make save operation thread safe";
        private Dictionary<string, string> _config = new Dictionary<string, string>();
        #endregion

        #region Constructor
        public FileStoreManager(Dictionary<string, string> config) {
            ValidateConfig(config);
            _config = config;
        }
        #endregion

        #region Public Constants
        public const string CONFIG_CONNECTION_STRING = "ConnectionString";
        #endregion

        #region IStoreManager Implementation
        /// <summary>
        /// Saves the data to the store
        /// </summary>
        /// <param name="data">Data to be stored</param>
        public void Save(byte[] data) {
            //Check if directory exists or not
            string directory = Path.GetDirectoryName(_config[CONFIG_CONNECTION_STRING]);
            if (Directory.Exists(directory) == false) Directory.CreateDirectory(directory);

            //Save the data
            Monitor.Enter(_lockString);
            try {
                File.WriteAllBytes(_config[CONFIG_CONNECTION_STRING], data);
            } finally { Monitor.Exit(_lockString); }
        }

        /// <summary>
        /// Gets the data from the store
        /// </summary>
        /// <returns>Data stored in the store</returns>
        public byte[] Get() {
            //Check if file exists or not
            if (File.Exists(_config[CONFIG_CONNECTION_STRING]) == false) throw new FileNotFoundException("Store doesn't have the file requested.");

            //Read the data
            Monitor.Enter(_lockString);
            try {
                return File.ReadAllBytes(_config[CONFIG_CONNECTION_STRING]);
            } finally { Monitor.Exit(_lockString); }
        }

        /// <summary>
        /// Configurations for the data store
        /// </summary>
        public Dictionary<string, string> Config { set { ValidateConfig(value); _config = value; } }
        #endregion

        #region Private Helper Functions
        private static void ValidateConfig(Dictionary<string, string> config) {
            if (config == null) throw new ArgumentNullException("Config is NULL.");
            if (config.ContainsKey(CONFIG_CONNECTION_STRING) == false) throw new ArgumentException("Connection String is not set");
        }
        #endregion
    }
}