﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Provides an interface to manipulate the persistent job queue 
    /// </summary>
    internal class QueueManager {
        #region Private Variables
        private const string _queueLocation = "~/App_Data/JobQueue.dat";
        IStoreManager storeManager = null;
        #endregion

        #region Public Constructor
        /// <summary>
        /// Constructs QueueManager
        /// </summary>
        public QueueManager() {
            var config = new Dictionary<string, string>();
            config.Add(FileStoreManager.CONFIG_CONNECTION_STRING, Helper.GetAbsolutePath(_queueLocation));
            storeManager = new FileStoreManager(config);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds an object to the end of the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be added to Queue</param>
        public void Enqueue(Job job) {
            //Get the queue
            var queue = GetQueue();
            //Check if the job is already queued or not
            if (queue.ToList().Where(qJob => string.Equals(qJob.Id, job.Id, StringComparison.OrdinalIgnoreCase)).Count() != 0) throw new Exception("Job with similar id is already queued.");
            //Enqueue the job and save the queue
            queue.Enqueue(job);
            SaveQueue(queue);
        }

        /// <summary>
        /// Removes and returns the object at the beginning of the Queue
        /// </summary>
        /// <returns>The object that is removed from the beginning of the Queue</returns>
        public Job Dequeue() {
            //Get the queue
            var queue = GetQueue();
            
            //Queue is empty
            if (queue.Count == 0) return null;
            
            //Dequeue the job and save the queue
            var job = queue.Dequeue();
            SaveQueue(queue);
            return job;
        }

        /// <summary>
        /// Removes the object from the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be removed from Queue</param>
        public void Remove(Job job) {
            //Get the queue
            var queue = GetQueue();
            var jobTobeRemoved = queue.ToList().Where(qJob => string.Equals(qJob.Id, job.Id, StringComparison.OrdinalIgnoreCase)).SingleOrDefault();
            if (jobTobeRemoved == null) return;

            //Create another queue and while Enqueue is going skip the given job
            var newQueue = new Queue<Job>();
            while (queue.Count != 0) {
                var dequeuedJob = queue.Dequeue();
                if (string.Equals(dequeuedJob.Id, job.Id, StringComparison.OrdinalIgnoreCase)) continue;
                newQueue.Enqueue(dequeuedJob);
            }
            SaveQueue(newQueue);
            return;
        }
        #endregion

        #region Private Helper Functions
        private void SaveQueue(Queue<Job> queue) { storeManager.Save(Serializer.Serialize(queue)); }
        private Queue<Job> GetQueue() {
            Queue<Job> queue = null;
            try {
                queue = Serializer.DeSerialize<Queue<Job>>(storeManager.Get());
            } catch (System.IO.FileNotFoundException) { /* Don't do anything about this exception */}
            if (queue == null) queue = new Queue<Job>();
            return queue;
        }
        #endregion
    }
}