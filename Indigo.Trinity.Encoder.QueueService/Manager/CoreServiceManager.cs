﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Indigo.Trinity.Core.Clients;
using Indigo.Trinity.Core.Clients.AspNet;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Manages calls to the Core Service
    /// </summary>
    public class CoreServiceManager {
        private static string _serviceEndPoint = Helper.GetConnectionStringItem(ConfigurationManager.AppSettings["Indigo.Trinity.Services"], "BaseUrl").TrimEnd('/') + "/Core";
        /// <summary>
        /// Save the Content Details
        /// </summary>
        /// <param name="jobStatus">Job Status</param>
        public static void SaveContentDetails(JobStatus jobStatus) {
            var contentClient = ContentClient();
            var contentDetails = new ContentDetails();

            Indigo.Trinity.Core.Clients.AspNet.Content content = null;
            try { content = contentClient.FindContent(new ContentCriteria() { ContentIdList = new List<string>() { jobStatus.Input.Id } }).Items.FirstOrDefault(); } catch { }
            if (content == null) {
                LoggingManager.Log(string.Format("Content not found for Id {0}", jobStatus.Input.Id));
                return;
            }
            switch (content.Type) {
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.Video: contentDetails = new VideoDetails { ContentId = content.Id }; break;
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.Audio: contentDetails = new AudioDetails { ContentId = content.Id }; break;
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.Image: contentDetails = new ImageDetails { ContentId = content.Id }; break;
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.DVD: contentDetails = new DVDDetails { ContentId = content.Id }; break;
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.EBook: contentDetails = new EBookDetails { ContentId = content.Id }; break;
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.HandsOnLab: contentDetails = new HandsOnLabDetails { ContentId = content.Id }; break;
                case Indigo.Trinity.Core.Clients.AspNet.ContentType.Other: contentDetails = new OtherContentDetails { ContentId = content.Id }; break;
            }

            var thumbNail = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.Thumbnail).SingleOrDefault();
            var thumbNailUrl = thumbNail != null && thumbNail.FileDetails != null && thumbNail.FileDetails.Count > 0 ? thumbNail.FileDetails[0].Url : "";

            var screenShot = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.ScreenShot).SingleOrDefault();
            var screenShotUrl = screenShot != null && screenShot.FileDetails != null && screenShot.FileDetails.Count > 0 ? screenShot.FileDetails[0].Url : "";


            #region VideoDetails
            if (contentDetails is VideoDetails) {
                var videoDetails = contentDetails as VideoDetails;
                var smoothStreaming = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.SmoothStreaming).SingleOrDefault();
                var originalContent = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.WindowsMedia).SingleOrDefault();
                if (originalContent == null) originalContent = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.MP4).SingleOrDefault();

                if (smoothStreaming != null) {
                    videoDetails.Created = smoothStreaming.FileDetails != null && smoothStreaming.FileDetails.Count > 0 ? smoothStreaming.FileDetails[0].Created : DateTime.MinValue;
                    videoDetails.StreamUrl = smoothStreaming.FileDetails != null && smoothStreaming.FileDetails.Count > 0 ? smoothStreaming.FileDetails[0].Url : "";
                    videoDetails.ContentSize = smoothStreaming.FileDetails != null && smoothStreaming.FileDetails.Count > 0 ? smoothStreaming.FileDetails[0].Size : 0;
                    videoDetails.Duration = smoothStreaming.PlaybackDuration;
                }
                if (originalContent != null) {
                    if (videoDetails.Duration.Ticks == 0) videoDetails.Duration = originalContent.PlaybackDuration;
                    videoDetails.DownloadUrl = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Url : "";
                    if (videoDetails.Created == DateTime.MinValue)
                        videoDetails.Created = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Created : DateTime.MinValue;
                    if (string.IsNullOrWhiteSpace(videoDetails.StreamUrl)) videoDetails.StreamUrl = videoDetails.DownloadUrl;
                    if (videoDetails.ContentSize == 0) videoDetails.ContentSize = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Size : 0;
                }
                videoDetails.SnapshotUrl = screenShotUrl;
                videoDetails.ThumbnailUrl = thumbNailUrl;
                SaveContentDetails<VideoDetails>(contentClient, videoDetails);
            }
            #endregion

            #region AudioDetails
            if (contentDetails is AudioDetails) {
                var audioDetails = contentDetails as AudioDetails;
                var originalContent = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.Resource).SingleOrDefault();

                if (originalContent != null) {
                    audioDetails.Duration = originalContent.PlaybackDuration;
                    audioDetails.DownloadUrl = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Url : "";
                    if (audioDetails.Created == DateTime.MinValue)
                        audioDetails.Created = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Created : DateTime.MinValue;
                    if (string.IsNullOrWhiteSpace(audioDetails.StreamUrl)) audioDetails.StreamUrl = audioDetails.DownloadUrl;
                }
                SaveContentDetails<AudioDetails>(contentClient, audioDetails);
            }
            #endregion

            #region ImageDetails
            if (contentDetails is ImageDetails) {
                var imageDetails = contentDetails as ImageDetails;
                var originalContent = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.Thumbnail).SingleOrDefault();
                if (originalContent == null) originalContent = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.ScreenShot).SingleOrDefault();
                imageDetails.DownloadUrl = imageDetails.ThumbnailUrl = string.IsNullOrWhiteSpace(thumbNailUrl) ? screenShotUrl : thumbNailUrl;
                if (originalContent != null) {
                    imageDetails.Created = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Created : DateTime.MinValue;
                    imageDetails.ContentSize = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0].Size : 0;
                }
                SaveContentDetails<ImageDetails>(contentClient, imageDetails);
            }
            #endregion

            #region Other Type of Content
            if (contentDetails is DVDDetails || contentDetails is EBookDetails || contentDetails is HandsOnLabDetails || contentDetails is OtherContentDetails) {
                var originalContent = jobStatus.Output.Where(c => c.OutputFormat == OutputFormat.Resource).SingleOrDefault();
                if (originalContent != null) {
                    var fileDetails = originalContent.FileDetails != null && originalContent.FileDetails.Count > 0 ? originalContent.FileDetails[0] : null;
                    if (fileDetails != null) {
                        contentDetails.ContentSize = fileDetails.Size;
                        contentDetails.DownloadUrl = fileDetails.Url;
                        contentDetails.Created = fileDetails.Created;
                    }
                }
                if (contentDetails is DVDDetails) SaveContentDetails<DVDDetails>(contentClient, contentDetails as DVDDetails);
                if (contentDetails is EBookDetails) SaveContentDetails<EBookDetails>(contentClient, contentDetails as EBookDetails);
                if (contentDetails is HandsOnLabDetails) SaveContentDetails<HandsOnLabDetails>(contentClient, contentDetails as HandsOnLabDetails);
                if (contentDetails is OtherContentDetails) SaveContentDetails<OtherContentDetails>(contentClient, contentDetails as OtherContentDetails);
            }
            #endregion
        }
        private static void SaveContentDetails<T>(ContentProviderClient client, T contentDetails) where T : ContentDetails {
            try {
                client.SaveContentDetails(contentDetails);
            } catch (Exception ex) {
                LoggingManager.Log("Exception at SaveContentDetails", ex);
            }
        }
        private static ContentProviderClient ContentClient() { return AspNetClient.ContentClient(String.Format("{0}/ContentProvider.svc", _serviceEndPoint), LoadAdminHeaders()); }
        private static Dictionary<string, string> LoadAdminHeaders() {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("SessionId", DateTime.Now.ToString("yyyyMMdd").Replace("0", "O").Replace("1", "I").Replace("5", "S").Replace("6", "9"));
            return dictionary;
        }
    }
}