﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Web.Hosting;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Logs Exception, Information, Warnings etc to event
    /// </summary>
    internal class LoggingManager {
        #region Enum
        /// <summary>
        /// Type of Message
        /// </summary>
        public enum MessageType {
            /// <summary>
            /// Information Message
            /// </summary>
            Information = 0,
            /// <summary>
            /// Warning Message
            /// </summary>
            Warning = 1,
            /// <summary>
            /// Error Message
            /// </summary>
            Error = 2
        }
        #endregion

        #region Log Message
        /// <summary>
        /// Logs Message as Information
        /// </summary>
        /// <param name="message">Information Message</param>
        public static void Log(string message) {
            Log(message, MessageType.Information);
        }
        /// <summary>
        /// Logs Exception as Error
        /// </summary>
        /// <param name="message">Exception Message</param>
        /// <param name="ex">Exception</param>
        public static void Log(string message, Exception ex) {
            //Get the Exception Details
            ExceptionFormatter formatter = null;
            if (string.IsNullOrWhiteSpace(message)) formatter = new ExceptionFormatter();
            else formatter = new ExceptionFormatter(new NameValueCollection() { { "Custom Message", message } }, "Indigo.Trinity.Encoder.QueueService");
            if (ex == null) ex = new Exception("Exception is NULL");
            string errMessage = formatter.GetMessage(ex);

            //Log error
            Log(errMessage, MessageType.Error);
        }
        /// <summary>
        /// Logs Message as type of message
        /// </summary>
        /// <param name="message">Message to be Logged</param>
        /// <param name="messageType">Type of Message</param>
        public static void Log(string message, MessageType messageType) {
            try {
                string source = "Indigo.Trinity.Encoder.QueueService.svc";
                string log = "Application";
                //Register Source if not registered
                if (EventLog.SourceExists(source) == false) EventLog.CreateEventSource(source, log);
                //Log Information
                switch (messageType) {
                    case MessageType.Error:
                        EventLog.WriteEntry(source, message, EventLogEntryType.Error);
                        break;
                    case MessageType.Warning:
                        EventLog.WriteEntry(source, message, EventLogEntryType.Warning);
                        break;
                    case MessageType.Information:
                    default:
                        EventLog.WriteEntry(source, message, EventLogEntryType.Information);
                        break;
                }
            } catch (Exception ex) {
                //If some thing goes wrong while logging exception to event, log in file
                try {
                    using (StreamWriter writer = new StreamWriter(HostingEnvironment.MapPath("~/App_Data/Exception.log"), true)) {
                        writer.WriteLine(string.Format("{0}|{1}|{2}", message, ex.Message, DateTime.UtcNow));
                    }
                } catch { }
            }
        }
        #endregion
    }
}