﻿using System.Collections.Generic;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Provides an interface to save the state of the job
    /// </summary>
    internal class StateManager {
        #region Public Methods
        /// <summary>
        /// Gets the state of the job
        /// </summary>
        /// <param name="jobId">Id of the job</param>
        /// <returns>Status of the job</returns>
        public JobStatus GetState(string jobId) {
            IStoreManager storeManager = new FileStoreManager(GetStoreConfig(jobId));
            JobStatus status = null;
            try {
                status = Serializer.DeSerialize<JobStatus>(storeManager.Get());
            } catch (System.IO.FileNotFoundException) { /* Don't do anything about this exception */}
            return status;
        }
        /// <summary>
        /// Saves the state of the job
        /// </summary>
        /// <param name="status">Status of the Job</param>
        public void SaveState(JobStatus status) {
            IStoreManager storeManager = new FileStoreManager(GetStoreConfig(status.Input.Id));
            storeManager.Save(Serializer.Serialize(status));
        }
        #endregion

        #region Private Helper Functions
        //Get the config for the store manager
        private Dictionary<string, string> GetStoreConfig(string jobId) {
            var config = new Dictionary<string, string>();
            config.Add(FileStoreManager.CONFIG_CONNECTION_STRING, Helper.GetAbsolutePath(string.Format("~/App_Data/{0}.dat", jobId)));
            return config;
        }
        #endregion
    }
}