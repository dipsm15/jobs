﻿namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// 
    /// </summary>
    internal class Constants {
        /// <summary>
        /// Name Space for the Data Contract
        /// </summary>
        public const string NAME_SPACE_DATA_CONTRACT = "Indigo.Trinity.Encoder.QueueService";
        /// <summary>
        /// Name Space for the Service Contract
        /// </summary>
        public const string NAME_SPACE_SERVICE_CONTRACT = "Indigo.Trinity.Encoder.QueueService";
        /// <summary>
        /// Folder Path of Content
        /// </summary>
        public static string CONTENT_FOLDER_PATH { get { return System.Configuration.ConfigurationManager.AppSettings["ContentFolderPath"]; } }
    }
}