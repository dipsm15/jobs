﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Helper class providing helper functionalities
    /// </summary>
    internal class Helper {
        /// <summary>
        /// Returns the absolute path for a given relative path with respect to the bin folder
        /// </summary>
        /// <param name="relativePath">Relative Path</param>
        /// <returns>Absolute Path</returns>
        public static string GetAbsolutePath(string relativePath) {
            return HostingEnvironment.MapPath(relativePath);
        }

		public static string GetConnectionStringItem(string connectionString, string key)
		{
			if (String.IsNullOrEmpty(connectionString)) return null;
			string[] connectionParts = connectionString.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
			foreach (string part in connectionParts)
			{
				string[] keyValue = part.Split('=');
				if (keyValue.Length != 2) continue;
				if (keyValue[0].Trim().Equals(key, StringComparison.OrdinalIgnoreCase)) return keyValue[1].Trim();
			}
			return string.Empty;
		}
    }
}