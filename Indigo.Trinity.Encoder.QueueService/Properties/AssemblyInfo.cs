﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Indigo.Trinity.Encoder.QueueService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Indigo Architects")]
[assembly: AssemblyProduct("Indigo.Trinity.Encoder.QueueService")]
[assembly: AssemblyCopyright("Copyright © Indigo Architects 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("4d790a54-f623-4a9b-b817-9696764223e1")]
[assembly: AssemblyFileVersion("2.5.0.4")]
