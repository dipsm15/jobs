﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.IO;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// 
    /// </summary>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.Single)]
    public class Service : IQueue {
        /// <summary>
        /// Adds an object to the end of the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be added to Queue</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        public JobStatus Enqueue(Job job) {
            JobStatus jobStatus = new JobStatus();
            try {
                Validate(job);
                //Check if there is any other job with same id
                var stateMgr = new StateManager();
                var oldJobStatus = stateMgr.GetState(job.Id);

                //Remove the job
                if (oldJobStatus != null) Remove(job);

                //Enqueue the job
                var queueMgr = new QueueManager();
                queueMgr.Enqueue(job);

                //Create a default state
                jobStatus = new JobStatus { Input = job, Status = Status.Queued, Completion = 0 };
                stateMgr.SaveState(jobStatus);
            } catch (Exception ex) { HandlerException(ex, ref jobStatus, "Enqueue"); }
            return jobStatus;
        }

        /// <summary>
        /// Removes the object from the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be removed from Queue</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        public JobStatus Remove(Job job) {
            JobStatus jobStatus = new JobStatus();
            try {
                //Validate
                if (job == null || string.IsNullOrWhiteSpace(job.Id)) throw new ArgumentException("Job is null or Id is not set.");

                //Check if there is any other job with same id
                var stateMgr = new StateManager();
                var oldJobStatus = stateMgr.GetState(job.Id);
                if (oldJobStatus == null) throw new ArgumentException("Job with the given Id doesn't exists.");

                //Remove the job from Queue
                var queueMgr = new QueueManager();
                queueMgr.Remove(job);

                //Create a default state
                jobStatus = new JobStatus { Input = job, Status = Status.Cancelled };
                stateMgr.SaveState(jobStatus);
            } catch (Exception ex) { HandlerException(ex, ref jobStatus, "Remove"); }
            return jobStatus;
        }

        /// <summary>
        /// Removes and returns the object at the beginning of the Queue
        /// </summary>
        /// <returns>The object that is removed from the beginning of the Queue</returns>
        public DequeueResult Dequeue() {
            DequeueResult dequeuedResult = new DequeueResult();
            try {
                //Remove the job
                var queueMgr = new QueueManager();
                dequeuedResult.Job = queueMgr.Dequeue();

                //Queue is Empty
                if (dequeuedResult.Job != null) {
                    //Create a state manager
                    var status = new JobStatus { Input = dequeuedResult.Job, Status = Status.Dequeued };
                    var stateMgr = new StateManager();
                    stateMgr.SaveState(status);
                } else {
                    dequeuedResult.IsSuccessful = false;
                    dequeuedResult.ErrorMessage = "Queue is Empty";
                }
            } catch (Exception ex) { HandlerException(ex, ref dequeuedResult, "Dequeue"); }
            return dequeuedResult;
        }

        /// <summary>
        /// Returns the status of the job
        /// </summary>
        /// <param name="jobId">Job for which status is required</param>
        /// <returns>Result indicating where the operation is successful or not with the status of the job</returns>
        public JobStatus GetStatus(string jobId) {
            JobStatus jobStatus = new JobStatus();
            try {
                //Validate
                if (string.IsNullOrWhiteSpace(jobId)) throw new ArgumentException("Job Id is not set.");

                //Create a StateManager
                var stateMgr = new StateManager();
                jobStatus = stateMgr.GetState(jobId);
            } catch (Exception ex) { HandlerException(ex, ref jobStatus, "GetStatus"); }
            return jobStatus;
        }

        /// <summary>
        /// Saves the status of the job
        /// </summary>
        /// <param name="jobStatus">Status of the job to be saved</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        public Result SaveStatus(JobStatus jobStatus) {
            Result result = new Result();
            try {
                //Create a state manager
                var stateMgr = new StateManager();
                //Check if the status is available for the job id
                var savedStatus = stateMgr.GetState(jobStatus.Input.Id);
                if (savedStatus == null) throw new Exception("Status is not available for the provided job.");
                if (jobStatus.Status != Status.Cancelled && (savedStatus.Status == Status.Queued || savedStatus.Status == Status.Cancelled))
                    throw new Exception(string.Format("The status of the job is {0}, this state can not be overridden.", savedStatus.Status));
                //If job completion is 100%, post info to Core Service
                if (jobStatus.Completion == 100 && jobStatus.UpdatedAtCore == false) {
                    CoreServiceManager.SaveContentDetails(jobStatus);
                    jobStatus.UpdatedAtCore = true;
                }
                //Update the status
                jobStatus.Input = savedStatus.Input;
                stateMgr.SaveState(jobStatus);

            } catch (Exception ex) { HandlerException(ex, ref result, "SaveStatus"); }
            return result;
        }

        #region Private Helper Functions
        private void Validate(Job job) {
            if (job == null) throw new ArgumentNullException("Job is set to NULL");
            if (job.Action == Action.Delete) return;
            if (job.OutputFormat == OutputFormat.None) throw new ArgumentException("OutputFormat is not set.");
            if (job.Content == null || job.Content.Count == 0) throw new ArgumentException("Content is not set.");
            if (job.Content.Where(c => c.IsOverlay == false).Count() == 0) throw new ArgumentException("There is no base content, all are overlay.");
            //If there is only one item in content and that to be video, then skip the time line validation
            bool skipTimeLineValidation = job.Content.Where(c => c.IsOverlay == false).Count() == 1 &&
                                             (job.Content.Where(c => c.IsOverlay == false).SingleOrDefault().Type == ContentType.Video ||
                                                    job.Content.Where(c => c.IsOverlay == false).SingleOrDefault().Type == ContentType.Audio ||
                                                    job.Content.Where(c => c.IsOverlay == false).SingleOrDefault().Type == ContentType.Image ||
                                                    job.Content.Where(c => c.IsOverlay == false).SingleOrDefault().Type == ContentType.Resource)
                                              ? true : false;

            foreach (var content in job.Content) Validate(job, content, skipTimeLineValidation);
            //Validate the time line
            if (skipTimeLineValidation == false) ValidateTimeLine(job.Content.Where(c => c.IsOverlay == false).OrderBy(c => c.TimeLine.StartTime).ToList());
            //If Id is not set, set the Id
            if (string.IsNullOrWhiteSpace(job.Id)) job.Id = Guid.NewGuid().ToString();
        }
        private void ValidateTimeLine(List<Content> contentList) {
            if (contentList.Count == 1) return;
            for (int count = 0; count < contentList.Count - 1; count++)
                if (Math.Abs(contentList[count].TimeLine.EndTime - contentList[count + 1].TimeLine.StartTime) > 0.1) throw new ArgumentException("Start Time of Content is not equal to End Time of the previous Content.");
        }
        private void Validate(Job job, Content content, bool skipTimeLineValidation) {
            if (content == null) throw new ArgumentNullException("Content in the content list is NULL.");
            if (content.Type == ContentType.None) throw new ArgumentException("Content Type is not set for the content.");
            if (skipTimeLineValidation == false || content.TimeLine != null) {
                if (content.TimeLine == null) throw new ArgumentException("TimeLine is not set for the Content.");
                if (content.TimeLine.EndTime <= content.TimeLine.StartTime) throw new ArgumentException("The EndTime is smaller than the StartTime.");
            }
            switch (content.Type) {
                case ContentType.Video: Validate(content as Video, skipTimeLineValidation); break;
                case ContentType.Audio: Validate(content as Audio, skipTimeLineValidation); break;
                case ContentType.Image: Validate(content as Image); break;
                case ContentType.SubTitles: Validate(content as SubTitles); break;
                case ContentType.LiveStream: Validate(content as LiveStream); break;
                case ContentType.Resource: Validate(content as Resource); break;
                default: throw new ArgumentException("Set the type of the content.");
            }
            //Hack to create the thumbnail, if the content is of video type, user doesn't need to provide OutputFormat.Thumbnail
            if (content.Type == ContentType.Video && ((job.OutputFormat & OutputFormat.Thumbnail) != OutputFormat.Thumbnail))
                job.OutputFormat = job.OutputFormat | OutputFormat.Thumbnail;
            if (content.Type == ContentType.Video && ((job.OutputFormat & OutputFormat.ScreenShot) != OutputFormat.ScreenShot))
                job.OutputFormat = job.OutputFormat | OutputFormat.ScreenShot;
        }

        private void Validate(Resource resource) {
            if (resource == null) throw new ArgumentNullException("ContentType (Resource) and Type of Content are different");
            if (string.IsNullOrWhiteSpace(resource.SourcePath)) throw new ArgumentException("SourcePath for the resource is invalid.");
            resource.SourcePath = resource.SourcePath.Trim('\\');
            if (Path.IsPathRooted(resource.SourcePath) == false) resource.SourcePath = Path.Combine(Constants.CONTENT_FOLDER_PATH, resource.SourcePath);
            if (System.IO.File.Exists(resource.SourcePath) == false) throw new ArgumentException("SourcePath for the image is invalid.");
        }
        private void Validate(Video video, bool skipTimeLineValidation) {
            if (video == null) throw new ArgumentNullException("ContentType (Video) and Type of Content are different");
            if (string.IsNullOrWhiteSpace(video.SourcePath)) throw new ArgumentException("SourcePath for the video is invalid.");
            video.SourcePath = video.SourcePath.Trim('\\');
            if (Path.IsPathRooted(video.SourcePath) == false) video.SourcePath = Path.Combine(Constants.CONTENT_FOLDER_PATH, video.SourcePath);
            if (System.IO.File.Exists(video.SourcePath) == false) throw new ArgumentException("SourcePath for the video is invalid.");
            if (skipTimeLineValidation == false) {
                if (video.Clip != null) {
                    if (video.Clip.EndTime < video.Clip.StartTime) throw new ArgumentException("The EndTime is smaller than the StartTime for the Video Clip.");
                    //Clip duration should be equal to the time line duration
                    if (Math.Abs((video.Clip.EndTime - video.Clip.StartTime) - (video.TimeLine.EndTime - video.TimeLine.StartTime)) > 0.1) throw new ArgumentException("The duration for the Time Line and duration for Clip are different for the Video.");
                }
            }
        }
        private void Validate(Audio audio, bool skipTimeLineValidation) {
            if (audio == null) throw new ArgumentNullException("ContentType (Audio) and Type of Content are different");
            if (string.IsNullOrWhiteSpace(audio.SourcePath)) throw new ArgumentException("SourcePath for the video is invalid.");
            audio.SourcePath = audio.SourcePath.Trim('\\');
            if (Path.IsPathRooted(audio.SourcePath) == false) audio.SourcePath = Path.Combine(Constants.CONTENT_FOLDER_PATH, audio.SourcePath);
            if (System.IO.File.Exists(audio.SourcePath) == false) throw new ArgumentException("SourcePath for the audio is invalid.");
            if (skipTimeLineValidation == false) {
                if (audio.Clip != null) {
                    if (audio.Clip.EndTime < audio.Clip.StartTime) throw new ArgumentException("The EndTime is smaller than the StartTime for the Audio Clip.");
                    //Clip duration should be equal to the time line duration
                    if ((audio.Clip.EndTime - audio.Clip.StartTime) != (audio.TimeLine.EndTime - audio.TimeLine.StartTime)) throw new ArgumentException("The duration for the Time Line and duration for Clip are different for the Audio.");
                }
            }
            //TODO: Needs Validation
        }
        private void Validate(Image image) {
            if (image == null) throw new ArgumentNullException("ContentType (Image) and Type of Content are different");
            if (string.IsNullOrWhiteSpace(image.SourcePath)) throw new ArgumentException("SourcePath for the video is invalid.");
            image.SourcePath = image.SourcePath.Trim('\\');
            if (Path.IsPathRooted(image.SourcePath) == false) image.SourcePath = Path.Combine(Constants.CONTENT_FOLDER_PATH, image.SourcePath);
            if (System.IO.File.Exists(image.SourcePath) == false) throw new ArgumentException("SourcePath for the image is invalid.");
            //TODO: Needs Validation
        }
        private void Validate(SubTitles subTitles) {
            if (subTitles == null) throw new ArgumentNullException("ContentType (SubTitles) and Type of Content are different.");
            if (string.IsNullOrWhiteSpace(subTitles.Text)) throw new ArgumentException("Text is Blank or Empty for the Subtitle.");
            //TODO: Needs Validation
        }
        private void Validate(LiveStream liveStream) {
            if (liveStream == null) throw new ArgumentNullException("ContentType (LiveStream) and Type of Content are different");
            //TODO: Needs Validation
        }

        #region Exception Handling and Logging
        private void HandlerException(Exception ex, ref JobStatus jobResult, string functionCalled) {
            LoggingManager.Log(string.Format("Exception at {0}", functionCalled), ex);
            jobResult.IsSuccessful = false;
            jobResult.Status = Status.Failed;
            jobResult.ErrorMessage = ex.Message;
        }
        private void HandlerException(Exception ex, ref DequeueResult dequeuedResult, string functionCalled) {
            LoggingManager.Log(string.Format("Exception at {0}", functionCalled), ex);
            dequeuedResult.IsSuccessful = false;
            dequeuedResult.Job = null;
            dequeuedResult.ErrorMessage = ex.Message;
        }
        private void HandlerException(Exception ex, ref Result result, string functionCalled) {
            LoggingManager.Log(string.Format("Exception at {0}", functionCalled), ex);
            result.IsSuccessful = false;
            result.ErrorMessage = ex.Message;
        }
        #endregion
        #endregion
    }
}