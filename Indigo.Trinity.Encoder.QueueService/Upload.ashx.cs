﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Indigo.Trinity.Core.Clients;
using Indigo.Trinity.Core.Clients.AspNet;
using System.Collections;

namespace Indigo.Trinity.Encoder.QueueService {
    public class FileUpload : IHttpHandler {
        private HttpContext _ctx;

        /// <summary>
        /// Http Request entry point. Handler logic starts here before the request is forwarded to asp.Net
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context) {
            _ctx = context;
            string uploadPath = Helper.GetConnectionStringItem(ConfigurationManager.AppSettings["Indigo.Trinity.Data.Store"], "Upload").TrimEnd('\\');
            FileUploadProcess fileUpload = new FileUploadProcess();
            fileUpload.FileUploadCompleted += new EventHandler<FileUploadCompletedEventArgs>(_FileUploaded);
            fileUpload.ProcessRequest(context, uploadPath);
        }

        /// <summary>
        /// Required by IHttpModule: Not implemented in this case
        /// </summary>
        public bool IsReusable { get { return false; } }

        void _FileUploaded(object sender, FileUploadCompletedEventArgs args) {
            Content content = null;
            OutputFormat format = OutputFormat.None;
            switch (args.Content.Type) {
                case Core.Clients.AspNet.ContentType.Video:
                    content = new Video() { SourcePath = args.UploadPath };
                    format = OutputFormat.SmoothStreaming;
                    break;
                case Core.Clients.AspNet.ContentType.Audio:
                    content = new Audio() { SourcePath = args.UploadPath };
                    format = OutputFormat.SmoothStreaming;
                    break;
                case Core.Clients.AspNet.ContentType.Image:
                    content = new Image() { SourcePath = args.UploadPath };
                    format = OutputFormat.Resource;
                    break;
                default:
                    content = new Resource() { SourcePath = args.UploadPath };
                    format = OutputFormat.Resource;
                    break;
            }
            Job job = new Job() {
                Id = args.Content.Id,
                OutputFormat = format,
                Content = new List<Content>() { content },
                Action = Action.Add
            };
            Service svc = new Service();
            svc.Enqueue(job);
        }
    }

    #region File Upload Process

    /// <summary>
    /// File Upload completed event arguments containing the Content information and the uploaded content path
    /// </summary>
    public class FileUploadCompletedEventArgs : EventArgs {
        /// <summary>
        /// Content whose data has been uploaded
        /// </summary>
        public Core.Clients.AspNet.Content Content { get; set; }

        /// <summary>
        /// Path where the uploaded content is placed with full name
        /// </summary>
        public string UploadPath { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public FileUploadCompletedEventArgs() { }
    }

    /// <summary>
    /// Main component handling the file upload process and saving the file in the upload store
    /// </summary>
    public class FileUploadProcess {
        /// <summary>
        /// File upload completed event raised by the component for the caller
        /// </summary>
        public event EventHandler<FileUploadCompletedEventArgs> FileUploadCompleted;

        /// <summary>
        /// Constructor
        /// </summary>
        public FileUploadProcess() { }

        /// <summary>
        /// Processes the request and responds back with an appropriate response
        /// </summary>
        /// <param name="context"></param>
        /// <param name="uploadPath"></param>
        public void ProcessRequest(HttpContext context, string uploadPath) {
            string fileName = context.Request.QueryString["filename"];
            string sessionId = context.Request.QueryString["sid"];
            string contentId = context.Request.QueryString["cid"];

            // verify inputs: valid session and content Id. SessionId is automatically validated while finding the content for the given Id
            if (String.IsNullOrEmpty(sessionId) || String.IsNullOrEmpty(contentId) || String.IsNullOrEmpty(fileName)) {
                context.Response.Write("No session or content information provided.");
                context.Response.Flush();
                return;
            }
            Core.Clients.AspNet.Content content = null;
            string app = Helper.GetConnectionStringItem(ConfigurationManager.AppSettings["Indigo.Trinity.Services"], "BaseUrl").TrimEnd('/') + "/core";
            var contentClient = AspNetClient.ContentClient(app + "/" + Helper.GetConnectionStringItem(ConfigurationManager.AppSettings["Indigo.Trinity.Services"], "Content"),
                new Dictionary<string, string>() { { "SessionId", sessionId } });

            try { content = contentClient.FindContent(new ContentCriteria() { ContentIdList = new List<string>() { contentId } }).Items.FirstOrDefault(); } catch { }

            if (content == null) {
                context.Response.Write("Invalid Session or Content");
                context.Response.Flush();
                return;
            }
            contentClient.Close();

            // Get the action for this request
            bool getBytes = string.IsNullOrEmpty(context.Request.QueryString["GetBytes"]) ? false : bool.Parse(context.Request.QueryString["GetBytes"]);
            long startByte = string.IsNullOrEmpty(context.Request.QueryString["StartByte"]) ? 0 : long.Parse(context.Request.QueryString["StartByte"]);
            bool cancel = string.IsNullOrEmpty(context.Request.QueryString["Cancel"]) ? false : bool.Parse(context.Request.QueryString["Cancel"]);
            bool complete = string.IsNullOrEmpty(context.Request.QueryString["Complete"]) ? false : bool.Parse(context.Request.QueryString["Complete"]);

            // Start the upload process
            string fname = GetMD5Hash(contentId + "_" + fileName) + (fileName.IndexOf(".") >= 0 ? fileName.Substring(fileName.LastIndexOf(".")) : string.Empty);
            string contentPath = Path.Combine(uploadPath, fname);
            if (getBytes) {
                FileInfo fi = new FileInfo(contentPath);
                if (!fi.Exists) context.Response.Write("0");
                else context.Response.Write(fi.Length.ToString());
                context.Response.Flush();
                return;
            } else if (cancel) {
                context.Response.Write("Cancelled");
                context.Response.Flush();
            } else {
                if (startByte > 0 && File.Exists(contentPath)) {
                    using (FileStream fs = File.Open(contentPath, FileMode.Append)) {
                        SaveFile(context.Request.InputStream, fs);
                        fs.Close();
                    }
                } else {
                    FileInfo fi = new FileInfo(contentPath);
                    if (!Directory.Exists(fi.DirectoryName)) Directory.CreateDirectory(fi.DirectoryName);
                    using (FileStream fs = File.Create(contentPath)) {
                        SaveFile(context.Request.InputStream, fs);
                        fs.Close();
                    }
                }
                if (complete && FileUploadCompleted != null) {
                    FileUploadCompletedEventArgs args = new FileUploadCompletedEventArgs() {
                        Content = content,
                        UploadPath = contentPath
                    };
                    FileUploadCompleted(this, args);
                }
            }
        }

        private void SaveFile(Stream stream, FileStream fs) {
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0) {
                fs.Write(buffer, 0, bytesRead);
            }
        }

        private string GetMD5Hash(string item) {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(item));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++) sb.Append(result[i].ToString("X2"));
            return sb.ToString();
        }
    }

    #endregion
}
