﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Transition helps in merging of two content
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public enum Transition {
        /// <summary>
        /// Default Transition
        /// </summary>
        [EnumMember]
        None = 0,
        /// <summary>
        /// First Content fades in, decreases slowly
        /// </summary>
        [EnumMember]
        FadeIn = 1,
        /// <summary>
        /// First Content fades out, increases slowly
        /// </summary>
        [EnumMember]
        FadeOut = 2
        //TODO: Add other transitions
    }
}
