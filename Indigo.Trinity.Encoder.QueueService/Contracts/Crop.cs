﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Defines a rectangle to crop given content
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Crop {
        /// <summary>
        /// Top Left point of the Crop Rectangle
        /// </summary>
        [DataMember]
        public Point TopLeft { get; set; }
        /// <summary>
        /// Bottom Right point of the Crop Rectangle
        /// </summary>
        [DataMember]
        public Point BottomRight { get; set; }
    }
}