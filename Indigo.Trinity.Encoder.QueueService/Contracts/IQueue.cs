﻿using System.ServiceModel;
using System.ServiceModel.Web;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Represents a first-in, first-out collection of job object.
    /// </summary>
    [ServiceContract(Namespace = Constants.NAME_SPACE_SERVICE_CONTRACT)]
    public interface IQueue {
        /// <summary>
        /// Adds an object to the end of the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be added to Queue</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        [OperationContract]
        JobStatus Enqueue(Job job);

        /// <summary>
        /// Removes the object from the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be removed from Queue</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        [OperationContract]
        JobStatus Remove(Job job);

        /// <summary>
        /// Removes and returns the object at the beginning of the Queue
        /// </summary>
        /// <returns>The object that is removed from the beginning of the Queue</returns>
        [OperationContract]
        DequeueResult Dequeue();

        /// <summary>
        /// Returns the status of the job
        /// </summary>
        /// <param name="jobId">Job Id for which status is required</param>
        /// <returns>Result indicating where the operation is successful or not with the status of the job</returns>
        [OperationContract]
        [WebGet(UriTemplate = "/{jobId}", ResponseFormat = WebMessageFormat.Xml)]
        JobStatus GetStatus(string jobId);

        /// <summary>
        /// Saves the status of the job
        /// </summary>
        /// <param name="jobStatus">Status of the job to be saved</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        [OperationContract]
        Result SaveStatus(JobStatus jobStatus);
    }
}