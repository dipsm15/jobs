﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Content of Resource  type i.e. pdf, doc etc
    /// </summary>
    [Serializable]
    [KnownType(typeof(Content))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Resource : Content {
        public Resource() { Type = ContentType.Resource; }
        /// <summary>
        /// File Location of the content
        /// </summary>
        [DataMember]
        public string SourcePath { get; set; }
    }
}