﻿using System.Runtime.Serialization;
using System;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Object which provides vital information to the consumer regarding status of operation
    /// </summary>
    [Serializable]
    [KnownType(typeof(JobStatus))]
    [KnownType(typeof(DequeueResult))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Result {
        /// <summary>
        /// Constructs Result
        /// </summary>
        public Result() { IsSuccessful = true; }
        /// <summary>
        /// Is the operation successful or not
        /// </summary>
        [DataMember]
        public bool IsSuccessful { get; set; }
        /// <summary>
        /// Message to the client if the operation fails
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }
    }
}