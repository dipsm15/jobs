﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Point with X and Y Co-ordinate
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Point {
        /// <summary>
        /// X co-ordinate
        /// </summary>
        [DataMember]
        public double X { get; set; }
        /// <summary>
        /// Y co-ordinate
        /// </summary>
        [DataMember]
        public double Y { get; set; }
    }
}