﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Base class for all the Content
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(Video))]
    [KnownType(typeof(Image))]
    [KnownType(typeof(Audio))]
    [KnownType(typeof(SubTitles))]
    [KnownType(typeof(LiveStream))]
    [KnownType(typeof(Resource))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Content {
        /// <summary>
        /// Type of Content
        /// </summary>
        [DataMember]
        public ContentType Type { get; set; }
        /// <summary>
        /// Time line for the Content, point at which content should be inserted in main stream
        /// </summary>
        [DataMember]
        public TimeLine TimeLine { get; set; }
        /// <summary>
        /// The effects which should be applied while merging current content
        /// </summary>
        [DataMember]
        public MergeEffect MergeEffect { get; set; }
        /// <summary>
        /// To Confirm whether the given item should act as an overlay
        /// </summary>
        [DataMember]
        public bool IsOverlay { get; set; }
    }
}