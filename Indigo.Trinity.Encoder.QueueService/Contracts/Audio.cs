﻿using System;
using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Content of Audio type i.e. mp3, wma etc
    /// </summary>
    [Serializable]
    [KnownType(typeof(Content))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Audio : Content {
        /// <summary>
        /// Constructs Audio Object
        /// </summary>
        public Audio() { this.Type = ContentType.Audio; }
        /// <summary>
        /// Volume Level for the video
        /// </summary>
        [DataMember]
        public Nullable<int> Volume { get; set; }
        /// <summary>
        /// Portion of the Video which need to be merge with main stream content
        /// </summary>
        [DataMember]
        public TimeLine Clip { get; set; }
        /// <summary>
        /// File Location of the content
        /// </summary>
        [DataMember]
        public string SourcePath { get; set; }
        /// <summary>
        /// Should delete the source file
        /// </summary>
        [DataMember]
        public bool DeleteSource { get; set; }
    }
}