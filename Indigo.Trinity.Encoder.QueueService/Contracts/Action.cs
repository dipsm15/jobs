﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Indigo.Trinity.Encoder.QueueService {
    public enum Action {
        [EnumMember]        Add = 0,
        [EnumMember]        Delete = 1
    }
}