﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Stores the details of output file from encoder
    /// </summary>
    [Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class OutputFileLocation {
        /// <summary>
        /// Format of the file
        /// </summary>
        [DataMember]
        public OutputFormat OutputFormat { get; set; }
        /// <summary>
        /// Location where files are stored
        /// </summary>
        [DataMember]
        public List<OutputFileDetails> FileDetails { get; set; }
        /// <summary>
        /// Playback Duration of the Content
        /// </summary>
        [DataMember]
        public TimeSpan PlaybackDuration { get; set; }
    }
}