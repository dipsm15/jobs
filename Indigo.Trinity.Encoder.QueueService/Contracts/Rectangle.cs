﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Rectangle for placing the overlay
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Rectangle {
        /// <summary>
        /// Top Left corner of the rectangle
        /// </summary>
        [DataMember]
        public Point TopLeft { get; set; }
        /// <summary>
        /// Relative length and width of the rectangle with respect to top left corner
        /// </summary>
        [DataMember]
        public Point RightBottom { get; set; }
    }
}