﻿using System.Collections.Generic;
using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Encapsulates the contents and provides an unique job id
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Job {
        /// <summary>
        /// Unique Id for the current job
        /// </summary>
        [DataMember]
        public string Id { get; set; }
        /// <summary>
        /// The encoded out put format for the provided content
        /// </summary>
        [DataMember]
        public OutputFormat OutputFormat { get; set; }
        /// <summary>
        /// Content needs to be encoded
        /// </summary>
        [DataMember]
        public List<Content> Content { get; set; }
        /// <summary>
        /// Action to be taken in this job
        /// </summary>
        [DataMember]
        public Action Action { get; set; }
    }
}