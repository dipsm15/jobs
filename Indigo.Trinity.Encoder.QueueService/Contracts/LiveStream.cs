﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Content is of Live Stream
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(Content))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class LiveStream : Content {
        /// <summary>
        /// Constructs Live Stream Object
        /// </summary>
        public LiveStream() { this.Type = ContentType.LiveStream; }
    }
}