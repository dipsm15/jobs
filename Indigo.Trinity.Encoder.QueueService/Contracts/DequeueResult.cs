﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Object which provides vital information to the consumer regarding status of operation
    /// </summary>
    [KnownType(typeof(Result))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class DequeueResult : Result {
        /// <summary>
        /// Job which is dequeued from the Queue
        /// </summary>
        [DataMember]
        public Job Job { get; set; }
    }
}
