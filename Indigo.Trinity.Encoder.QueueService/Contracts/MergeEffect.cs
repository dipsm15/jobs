﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// The effect which will be used while merging of two content
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class MergeEffect {
        /// <summary>
        /// Transition at the start of the merge. Merging of current content with previous content (optional).
        /// </summary>
        [DataMember]
        public Transition Start { get; set; }
        /// <summary>
        /// Transition at the end of the merge. Merging of current content with next content (optional).
        /// </summary>
        [DataMember]
        public Transition End { get; set; }
    }
}
