﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Time line for which content needs to be inserted in main stream
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class TimeLine {
        /// <summary>
        /// Start Time with respect to beginning of main stream, represented as Ticks
        /// </summary>
        [DataMember]
        public double StartTime { get; set; }
        /// <summary>
        /// End Time with respect to beginning of main stream, represented as Ticks
        /// </summary>
        [DataMember]
        public double EndTime { get; set; }
    }
}