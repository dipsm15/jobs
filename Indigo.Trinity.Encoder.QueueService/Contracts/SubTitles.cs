﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Content of Text type, useful for showing subtitle
    /// </summary>
    [System.Serializable]
    [KnownType(typeof(Content))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class SubTitles : Content {
        /// <summary>
        /// Constructs Text Object
        /// </summary>
        public SubTitles() { this.Type = ContentType.SubTitles; }
        /// <summary>
        /// Text to be added to main stream
        /// </summary>
        [DataMember]
        public string Text { get; set; }
    }
}