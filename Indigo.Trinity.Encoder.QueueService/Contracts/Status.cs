﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Provides information about the status of the job
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public enum Status {
        /// <summary>
        /// Status Unavailable
        /// </summary>
        [EnumMember]
        None = 0,
        /// <summary>
        /// Job is Queued
        /// </summary>
        [EnumMember]
        Queued = 1,
        /// <summary>
        /// Job is peeked out of queue and in-progress
        /// </summary>
        [EnumMember]
        Dequeued = 2,
        /// <summary>
        /// Job is Encoding
        /// </summary>
        [EnumMember]
        Encoding = 3,
        /// <summary>
        /// Job is complete
        /// </summary>
        [EnumMember]
        Complete = 4,
        /// <summary>
        /// Job is cancelled
        /// </summary>
        [EnumMember]
        Cancelled = 5,
        /// <summary>
        /// Job failed
        /// </summary>
        [EnumMember]
        Failed = 6
    }
}