﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Stores the details of output file from encoder
    /// </summary>
    [Serializable]
    [DataContract]
    public class OutputFileDetails {
        /// <summary>
        /// URL for the file
        /// </summary>
        [DataMember]
        public string Url { get; set; }
        /// <summary>
        /// Size of the File in KB
        /// </summary>
        [DataMember]
        public long Size { get; set; }
        /// <summary>
        /// Date on which file is created
        /// </summary>
        [DataMember]
        public DateTime Created { get; set; }
    }
}