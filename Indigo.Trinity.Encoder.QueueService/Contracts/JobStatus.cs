﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Object which provides vital information to the consumer regarding status of operations done on job
    /// </summary>
    [Serializable]
    [KnownType(typeof(Result))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class JobStatus : Result {
        /// <summary>
        /// The JobId, which is required to retrieve the progress of the encoding
        /// </summary>
        [DataMember]
        public Job Input { get; set; }
        /// <summary>
        /// The locations where the requested format files  will be stored
        /// </summary>
        [DataMember]
        public List<OutputFileLocation> Output { get; set; }
        /// <summary>
        /// Percentage completion of the encoding
        /// </summary>
        [DataMember]
        public double Completion { get; set; }
        /// <summary>
        /// Time at which encoding started
        /// </summary>
        [DataMember]
        public Nullable<DateTime> StartTime { get; set; }
        /// <summary>
        /// Estimated time at which encoding will be complete
        /// </summary>
        [DataMember]
        public Nullable<DateTime> EstimatedEndTime { get; set; }
        /// <summary>
        /// Provides information about the status of the job
        /// </summary>
        [DataMember]
        public Status Status { get; set; }
        /// <summary>
        /// Stores if the status is updated at Core service or not
        /// </summary>
        public bool UpdatedAtCore { get; set; }
    }
}