﻿using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Type of the Content
    /// </summary>
   [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public enum ContentType {
        /// <summary>
        /// Content type is not defined
        /// </summary>
        [EnumMember]
        None,
        /// <summary>
        /// Content of Video type
        /// </summary>
        [EnumMember]
        Video,
        /// <summary>
        /// Content of Image type
        /// </summary>
        [EnumMember]
        Image,
        /// <summary>
        /// Content of Audio type
        /// </summary>
        [EnumMember]
        Audio,
        /// <summary>
        /// Content of SubTitle type
        /// </summary>
        [EnumMember]
        SubTitles,
        /// <summary>
        /// Content is a Live Stream
        /// </summary>
        [EnumMember]
        LiveStream,
       /// <summary>
       /// Resource like hands on lab, pdf, doc etc
       /// </summary>
       [EnumMember]
       Resource
    }
}
