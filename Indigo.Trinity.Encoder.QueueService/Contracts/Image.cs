﻿using System;
using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Content of Image type
    /// </summary>
    [Serializable]
    [KnownType(typeof(Content))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Image : Content {
        /// <summary>
        /// Constructs Image Object
        /// </summary>
        public Image() { this.Type = ContentType.Image; }
        /// <summary>
        /// File Location of the content
        /// </summary>
        [DataMember]
        public string SourcePath { get; set; }
        /// <summary>
        /// Crops the image
        /// </summary>
        [DataMember]
        public Crop Crop { get; set; }
        /// <summary>
        /// Top Left corner of main stream from where image's top left corner will match
        /// </summary>
        [DataMember]
        public Point TopLeft { get; set; }
        /// <summary>
        /// Opacity of the Image .Only used if Item is overlay
        /// </summary>
        [DataMember]
        public Nullable<double> Opacity { get; set; }
        /// <summary>
        /// Rectangle to hold the overlay structure
        /// </summary>
        [DataMember]
        public Rectangle Rectangle { get; set; }
        /// <summary>
        /// Should delete the source file
        /// </summary>
        [DataMember]
        public bool DeleteSource { get; set; }
    }
}