﻿using System;
using System.Runtime.Serialization;
namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Content of Video type i.e. WMV etc
    /// </summary>
    [Serializable]
    [KnownType(typeof(Content))]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    public class Video : Content {
        /// <summary>
        /// Constructs Video Object
        /// </summary>
        public Video() { this.Type = ContentType.Video; }
        /// <summary>
        /// File Location of the content
        /// </summary>
        [DataMember]
        public string SourcePath { get; set; }
        /// <summary>
        /// Portion of the Video which need to be merge with main stream content
        /// </summary>
        [DataMember]
        public TimeLine Clip { get; set; }
        /// <summary>
        /// Volume Level for the video
        /// </summary>
        [DataMember]
        public Nullable<int> Volume { get; set; }
        /// <summary>
        /// Angle in which video needs to be rotated
        /// </summary>
        [DataMember]
        public Nullable<int> RotateAngle { get; set; }
        /// <summary>
        /// Brightness of the Video
        /// </summary>
        [DataMember]
        public Nullable<int> Brightness { get; set; }
        /// <summary>
        /// Opacity of the Video.Only used if Item is overlay
        /// </summary>
        [DataMember]
        public Nullable<double> Opacity { get; set; }
        /// <summary>
        /// Rectangle to hold the overlay structure
        /// </summary>
        [DataMember]
        public Rectangle Rectangle { get; set; }
        /// <summary>
        /// Should delete the source file
        /// </summary>
        [DataMember]
        public bool DeleteSource { get; set; }
    }
}