﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Indigo.Trinity.Encoder.QueueService {
    /// <summary>
    /// Provides the out put formats
    /// </summary>
    [System.Serializable]
    [DataContract(Namespace = Constants.NAME_SPACE_DATA_CONTRACT)]
    [Flags]
    public enum OutputFormat {
        /// <summary>
        /// If set to None, output format will be windows media
        /// </summary>
        [EnumMember]
        None = 0,
        /// <summary>
        /// Encoded content will be an IIS Smooth Streaming
        /// </summary>
        [EnumMember]
        SmoothStreaming = 2,
        /// <summary>
        /// Encoded content will be Windows Media
        /// </summary>
        [EnumMember]
        WindowsMedia = 4,
        /// <summary>
        /// Encoded content will be MP4
        /// </summary>
        [EnumMember]
        MP4 = 8,
        /// <summary>
        /// Thumbnail
        /// </summary>
        [EnumMember]
        Thumbnail = 16,
        /// <summary>
        /// Screen Shot
        /// </summary>
        [EnumMember]
        ScreenShot = 32,
        /// <summary>
        /// Resource
        /// </summary>
        [EnumMember]
        Resource = 64
    }
}
