﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Indigo.Trinity.Encoder.QueueService.Client")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Indigo Architects")]
[assembly: AssemblyProduct("Indigo.Trinity.Encoder.QueueService.Client")]
[assembly: AssemblyCopyright("Copyright © Indigo Architects 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("50f93e95-49c5-4357-b4a0-4285f7f7d5cc")]
[assembly: AssemblyFileVersion("2.5.0.0")]
