﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Xml;

namespace Indigo.Trinity.Encoder.QueueService.Client {
    /// <summary>
    /// Proxy Manager is abstract class. Provides functionality of creating Proxy with all WCF Settings
    /// </summary>
    public abstract class ProxyManager {
        /// <summary>
        /// Creates a Channel Factory for given Service Contract
        /// </summary>
        /// <typeparam name="T">Service Contract</typeparam>
        /// <param name="address">Endpoint Address where Service is Hosted</param>
        /// <returns>Proxy pointing to Address provided</returns>
        public virtual ChannelFactory<T> Proxy<T>(string address) {
            return Proxy<T>(address, null);
        }
        /// <summary>
        /// Creates a Channel Factory for given Service Contract
        /// </summary>
        /// <typeparam name="T">Service Contract</typeparam>
        /// <param name="address">Endpoint Address where Service is Hosted</param>
        /// <param name="headers">Address Headers</param>
        /// <returns>Proxy pointing to Address provided with Address Headers</returns>
        public virtual ChannelFactory<T> Proxy<T>(string address, Dictionary<string, string> headers) {
            //Validate Address
            if (string.IsNullOrEmpty(address)) throw new ArgumentNullException("Address can not be null or empty.");
            //Address
            EndpointAddress endpointAddress = new EndpointAddress(address);

            //Binding
            WSHttpBinding wsHttpBinding = new WSHttpBinding(SecurityMode.None, false);
            wsHttpBinding.OpenTimeout = wsHttpBinding.CloseTimeout = new TimeSpan(0, 1, 0);
            wsHttpBinding.ReceiveTimeout = wsHttpBinding.SendTimeout = new TimeSpan(0, 10, 0);
            wsHttpBinding.MaxReceivedMessageSize = wsHttpBinding.MaxBufferPoolSize = 2147483647;
            wsHttpBinding.BypassProxyOnLocal = wsHttpBinding.AllowCookies = wsHttpBinding.TransactionFlow = false;
            wsHttpBinding.MessageEncoding = WSMessageEncoding.Text;
            wsHttpBinding.TextEncoding = Encoding.UTF8;
            wsHttpBinding.UseDefaultWebProxy = true;
            wsHttpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            wsHttpBinding.ReaderQuotas = new XmlDictionaryReaderQuotas(); //ReaderQuotas, setting to Max
            wsHttpBinding.ReaderQuotas.MaxArrayLength = wsHttpBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxStringContentLength = wsHttpBinding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
            wsHttpBinding.ReaderQuotas.MaxDepth = 2147483647;

            //Create the Proxy
            ChannelFactory<T> proxy = new ChannelFactory<T>(wsHttpBinding, endpointAddress);

            //Add Headers, if any
            if (headers != null && headers.Count > 0) {
                EndpointAddressBuilder endpointAddressBuilder = new EndpointAddressBuilder(proxy.Endpoint.Address);
                foreach (var item in headers)
                    endpointAddressBuilder.Headers.Add(AddressHeader.CreateAddressHeader(item.Key, "indigo", item.Value));
                proxy.Endpoint.Address = endpointAddressBuilder.ToEndpointAddress();
            }

            //Sets the MaxItemsInObjectGraph, so that client can receive large objects
            foreach (var operation in proxy.Endpoint.Contract.Operations) {
                DataContractSerializerOperationBehavior operationBehavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
                //If DataContractSerializerOperationBehavior is not present in the Behavior, then add
                if (operationBehavior == null) {
                    operationBehavior = new DataContractSerializerOperationBehavior(operation);
                    operation.Behaviors.Add(operationBehavior);
                }
                //IMPORTANT: As 'operationBehavior' is a reference, changing anything here will automatically update the value in list, so no need to add this behavior to behaviorlist
                operationBehavior.MaxItemsInObjectGraph = 2147483647;
            }
            return proxy;
        }
    }
}
