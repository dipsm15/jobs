﻿using System;
using System.Collections.Generic;
namespace Indigo.Trinity.Encoder.QueueService.Client {
    /// <summary>
    /// Provides an interface using which client can access functionalities provided by IQueue service
    /// </summary>
    public class JobQueueServiceClient : ProxyManager, IQueue {
        #region Private Variables
        private string _serviceEndPoint = string.Empty;
        private Dictionary<string, string> _headers = null;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructs JobQueueServiceClient using the given input
        /// </summary>
        /// <param name="serviceEndPoint">End point at which service is listening</param>
        /// <param name="headers">Any soap headers</param>
        public JobQueueServiceClient(string serviceEndPoint, Dictionary<string, string> headers) {
            _serviceEndPoint = serviceEndPoint;
            _headers = headers;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds an object to the end of the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be added to Queue</param>
        /// <returns>Status of the job</returns>
        public JobStatus Enqueue(Job job) {
            using (var proxy = base.Proxy<IQueue>(_serviceEndPoint, _headers)) {
                return proxy.CreateChannel().Enqueue(job);
            }
        }

        /// <summary>
        /// Removes the object from the Queue
        /// </summary>
        /// <param name="job">Job object which needs to be removed from Queue</param>
        /// <returns>Status of the job</returns>
        public JobStatus Remove(Job job) {
            using (var proxy = base.Proxy<IQueue>(_serviceEndPoint, _headers)) {
                return proxy.CreateChannel().Remove(job);
            }
        }

        /// <summary>
        /// Removes and returns the object at the beginning of the Queue
        /// </summary>
        /// <returns>The object that is removed from the beginning of the Queue</returns>
        public DequeueResult Dequeue() {
            using (var proxy = base.Proxy<IQueue>(_serviceEndPoint, _headers)) {
                return proxy.CreateChannel().Dequeue();
            }
        }

        /// <summary>
        /// Returns the status of the job
        /// </summary>
        /// <param name="job">Job for which status is required</param>
        /// <returns>Status of the job</returns>
        public JobStatus GetStatus(Job job) {
            using (var proxy = base.Proxy<IQueue>(_serviceEndPoint, _headers)) {
                return proxy.CreateChannel().GetStatus(job.Id);
            }
        }

        /// <summary>
        /// Saves the status of the job
        /// </summary>
        /// <param name="jobStatus">Status of the job to be saved</param>
        /// <returns>Result indicating where the operation is successful or not</returns>
        public Result SaveStatus(JobStatus jobStatus) {
            using (var proxy = base.Proxy<IQueue>(_serviceEndPoint, _headers)) {
                return proxy.CreateChannel().SaveStatus(jobStatus);
            }
        }
        #endregion


        public JobStatus GetStatus(string jobId) { return GetStatus(new Job { Id = jobId }); }
    }
}
