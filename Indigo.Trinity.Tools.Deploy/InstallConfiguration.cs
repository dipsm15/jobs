using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Data.SqlClient;


namespace Athena.Deploy
{
  internal class InstallConfiguration
  {
    #region Constants

    public class BackwardCompatibilityConfigProps
    {
      // "Apllication" mispelled on purpose to match original mispelling released
      public const string RequireDeploymentToCentralAdminWebApllication = "RequireDeploymentToCentralAdminWebApllication";
      // Require="MOSS" = RequireMoss="true" 
      public const string Require = "Require";
      // FarmFeatureId = FeatureId with FeatureScope = Farm
      public const string FarmFeatureId = "FarmFeatureId";
    }

    public class ConfigProps
    {
      public const string BannerImage = "BannerImage";
      public const string LogoImage = "LogoImage";            
      public const string SolutionId = "SolutionId";      
      public const string SolutionTitle = "SolutionTitle";       
    }

    #endregion

    #region Internal Static Properties

    internal static string BannerImage
    {
      get { return ConfigurationManager.AppSettings[ConfigProps.BannerImage]; }
    }

    internal static string LogoImage
    {
      get { return ConfigurationManager.AppSettings[ConfigProps.LogoImage]; }
    }    

    internal static Guid SolutionId
    {
      get { return new Guid(ConfigurationManager.AppSettings[ConfigProps.SolutionId]); }
    }    

    internal static string SolutionTitle
    {
      get { return ConfigurationManager.AppSettings[ConfigProps.SolutionTitle]; }
    }
    #endregion

    #region Internal Static Methods

    internal static string FormatString(string str)
    {
      return FormatString(str, null);
    }

    internal static string FormatString(string str, params object[] args)
    {
      string formattedStr = str;
      string solutionTitle = SolutionTitle;
      if (!String.IsNullOrEmpty(solutionTitle))
      {
        formattedStr = formattedStr.Replace("{SolutionTitle}", solutionTitle);
      }
      if (args != null)
      {
        formattedStr = String.Format(formattedStr, args);
      }
      return formattedStr;
    }

    #endregion
  }

  public enum InstallOperation { Install, Upgrade, Repair, Uninstall }
}
