﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Diagnostics;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

/// <summary>
/// Summary description for Utility
/// </summary>
public static class Utilities
{
    #region Enums
    public enum MessageType { Information = 0, Warning, Error };
    public enum LogTarget { File, EventLog };
    #endregion

    public static LogTarget Target { get; set; }
    private static string _logFile = string.Empty;
    public static string LogFile
    {
        get { return _logFile; }
        set { _logFile = value; }
    }

    #region Log Exception
    public static void Log(string message)
    {
        Log(message, MessageType.Information);
    }

    public static void Log(string message, Exception ex)
    {
        string errMessage = message;
        while (ex != null)
        {
            errMessage += System.Environment.NewLine + ex.Message +
                        System.Environment.NewLine + ex.StackTrace +
                         System.Environment.NewLine;
            ex = ex.InnerException;
        }

        Log(errMessage, MessageType.Error);
    }

    public static void Log(string message, MessageType messageType)
    {
        if (Target == LogTarget.EventLog)
            _WriteToEventLog(message, messageType);
        else
            _WriteToFile(message, messageType);
    }
    #endregion

    #region Zip/UnZip
    public static void UnZip(string zipFile, string extractionPath)
    {
        _UnZip(zipFile, extractionPath);
    }
    #endregion

    #region File System
    public static void CopyDirectory(string sourceDir, string destinationDir)
    {
        if (!Directory.Exists(destinationDir))
            Directory.CreateDirectory(destinationDir);

        string[] items = Directory.GetFileSystemEntries(sourceDir);
        foreach (string item in items)
        {
            if (string.Compare(item, sourceDir, true) == 0) continue;

            string itemName = item.TrimEnd(Path.DirectorySeparatorChar);
            itemName = itemName.Split(Path.DirectorySeparatorChar)[itemName.Split(Path.DirectorySeparatorChar).Length - 1];
            if (Directory.Exists(item))
                CopyDirectory(item, Path.Combine(destinationDir, itemName));
            else
                File.Copy(item, Path.Combine(destinationDir, itemName));
        }
    }
    #endregion

    #region Helper Functions
    private static void _UnZip(string zipFile, string extractionPath)
    {
        if (!Directory.Exists(extractionPath))
        {
            Directory.CreateDirectory(extractionPath);
        }
        FileStream fr = File.OpenRead(zipFile);
        ZipInputStream ins = new ZipInputStream(fr);
        ZipEntry ze = ins.GetNextEntry();
        while (ze != null)
        {
            if (ze.IsDirectory)
            {
                Directory.CreateDirectory(Path.Combine(extractionPath, ze.Name));
            }
            else if (ze.IsFile)
            {
                if (!Directory.Exists(Path.Combine(extractionPath, Path.GetDirectoryName(ze.Name))))
                {
                    Directory.CreateDirectory(Path.Combine(extractionPath, Path.GetDirectoryName(ze.Name)));
                }

                FileStream fs = File.Create(Path.Combine(extractionPath, ze.Name));

                byte[] writeData = new byte[ze.Size];
                int iteration = 0;
                while (true)
                {
                    int size = 2048;
                    size = ins.Read(writeData, (int)Math.Min(ze.Size, (iteration * 2048)), (int)Math.Min(ze.Size - (int)Math.Min(ze.Size, (iteration * 2048)), 2048));
                    if (size > 0)
                    {
                        fs.Write(writeData, (int)Math.Min(ze.Size, (iteration * 2048)), size);
                    }
                    else
                    {
                        break;
                    }
                    iteration++;
                }
                fs.Close();
            }
            ze = ins.GetNextEntry();
        }
        ins.Close();
        fr.Close();
    }

    private static void _WriteToFile(string message, MessageType messageType)
    {
        if (_logFile == string.Empty)
        {
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            _logFile = Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path) + ".log");
        }

        message = string.Format("{0}: {1}: {2}\r\n", DateTime.Now, messageType, message);
        File.AppendAllText(_logFile, message);
    }

    private static void _WriteToEventLog(string message, MessageType messageType)
    {
        try
        {
            string source = "Indigo.Athena.dll";
            string log = "Application";
            if (!EventLog.SourceExists(source))
                EventLog.CreateEventSource(source, log);

            switch (messageType)
            {
                case MessageType.Error:
                    EventLog.WriteEntry(source, message, EventLogEntryType.Error);
                    break;
                case MessageType.Warning:
                    EventLog.WriteEntry(source, message, EventLogEntryType.Warning);
                    break;
                case MessageType.Information:
                default:
                    EventLog.WriteEntry(source, message, EventLogEntryType.Information);
                    break;
            }
        }
        catch { ;}
    }
    #endregion
}
