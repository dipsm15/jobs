using System;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
/// -------------------------------------------------------------------------------------------------------------
/// IMPORTANT: DO NOT ADD REFERENCES to SharePoint or any other assembilies that is NOT part of setup or payload
///             BEFORE pre-requisites are checked.
/// -------------------------------------------------------------------------------------------------------------

namespace Athena.Deploy
{
    static class Program
    {
        /// <summary>Use this path for wrting </summary>
        public static string WorkingFolder { get; private set; }
        public static string RootFolder { get; private set; }
        public static PayloadManager PayloadManager { get; private set; }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            // 1. UI Initializations
            InstallerForm form = new InstallerForm();
            form.Text = InstallConfiguration.FormatString("{SolutionTitle}");
            string InstallationType = Properties.Settings.Default.InstallationType;

            // 2. Application Initializations
            Program.WorkingFolder = _GetWorkingFolder();
            Program.RootFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Program.PayloadManager = new PayloadManager(RootFolder, WorkingFolder, Path.Combine(RootFolder, "payload.xml"));

            Utilities.Target = Utilities.LogTarget.File;
            Utilities.LogFile = Path.Combine(WorkingFolder, "Setup.log");

            Utilities.Log("------------------");
            Utilities.Log("Starting setup...");
            Utilities.Log("Command:" + Environment.CommandLine);

            
            form.ContentControls.Add(CreateWelcomeControl());
            form.ContentControls.Add(CreatePrerequisiteControl());
            form.ContentControls.Add(CreatePartnerInfoControl());
            form.ContentControls.Add(CreateProxyManagerControl());
            form.ContentControls.Add(CreateMiscSettingsControl());
            form.ContentControls.Add(CreateDownloadControl());
            form.ContentControls.Add(CreateSetupReadyControl());
            form.ContentControls.Add(CreateProcessControl());
            form.ContentControls.Add(CreateReportControl());

            Application.Run(form);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            try
            {
                if (e.Exception is System.Net.BITS.BITSException)
                {
                    Utilities.Log("BITSException:" + (e.Exception as System.Net.BITS.BITSException).Message, Utilities.MessageType.Warning);
                    return;
                }
                    
                Utilities.Log("Fatal Exception:" + e.Exception);
                _ShowReport();
            }
            catch { }
            finally { Application.Exit(); }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                if (e.ExceptionObject is System.Net.BITS.BITSException)
                {
                    Utilities.Log("BITSException:" + (e.ExceptionObject as System.Net.BITS.BITSException).Message, Utilities.MessageType.Warning);
                    return;
                }
                    
                Utilities.Log("Fatal Exception:" + e.ExceptionObject);
                _ShowReport();

            }
            catch { }
            finally { Application.Exit(); }
        }

        #region Create Installer Controls
        private static InstallerControl CreatePrerequisiteControl()
        {
            PrerequisiteControl control = new PrerequisiteControl();
            control.Title = "Pre-requisites check";
            control.SubTitle = "Checking for pre-requisites…";
            return control;
        }

      
        private static InstallerControl CreatePartnerInfoControl()
        {
            PartnerInfoControl control = new PartnerInfoControl();
            control.Title = "Partner Information";
            control.SubTitle = "Provide information about your company.";
            return control;
        }

        private static InstallerControl CreateDownloadControl()
        {
            DownloadPayload control = new DownloadPayload();
            control.Title = "Downloading Payload";
            control.SubTitle = "Downloading files required for setup";
            return control;
        }
        private static InstallerControl CreateProxyManagerControl()
        {
            ProxyManagerControl control = new ProxyManagerControl();
            control.Title = "Network settings";
            control.SubTitle = "Provide proxy settings, if required, to access internet.";
            return control;
        }

        private static InstallerControl CreateMiscSettingsControl()
        {
            MiscSettingsControl control = new MiscSettingsControl();
            control.Title = "Other settings";
            control.SubTitle = "Provide other deployment settings for Microsoft Community Portal.";
            return control;
        }

        private static InstallerControl CreateWelcomeControl()
        {
            WelcomeControl control = new WelcomeControl();
            control.Title = "Welcome";
            control.SubTitle = "This setup will guide through the installation of Microsoft Community Portal.";
            return control;
        }

        private static InstallerControl CreateSetupReadyControl()
        {
            SetupReadyControl control = new SetupReadyControl();
            control.Title = "We are ready!";
            control.SubTitle = "Required information has been obtained.";
            return control;
        }
        private static InstallerControl CreateReportControl()
        {
            ReportControl control = new ReportControl();
            control.Title = "Setup completed";
            control.SubTitle = "";
            return control;
        }

        private static InstallProcessControl CreateProcessControl()
        {
            InstallProcessControl control = new InstallProcessControl();
            control.Title = "Installing";
            control.SubTitle = "Please wait while Microsoft Community Portal is installed";
            return control;
        }
        #endregion

        #region Helper Functions
        private static string _GetWorkingFolder()
        {
            string path = Environment.CurrentDirectory;
            
            /// TODO: Make sure that this path has write access.

            return path;
            
        }

        private static void _ShowReport()
        {
            try
            {
                if (File.Exists(Utilities.LogFile))
                {
                    string fileName = "mcp-setup-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".log";
                    string targetPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName);
                    File.Copy(Utilities.LogFile, targetPath);

                    MessageBox.Show("An unexpected exception has occurred. " +
                        "\r\nPlease refer to the 'Setup.log' file on your desktop for details." +
                        "\r\nSetup will now close.",
                        "Fatal Exception", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                    Process.Start("notepad.exe", targetPath);
                }
            }
            catch { /* SUPRESS */ }
        }
        #endregion
    }
}