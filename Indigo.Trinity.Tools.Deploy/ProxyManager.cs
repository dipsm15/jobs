﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Xml;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace Indigo.Athena.Deploy
{
    internal class ProxyManager
    {
        public ChannelFactory<T> Proxy<T>(string address, Dictionary<string, string> headers)
        {
            //Validate Address
            if (string.IsNullOrEmpty(address)) throw new ArgumentNullException("Address can not be null or empty.");
            //Address
            EndpointAddress endpointAddress = new EndpointAddress(address);

            //Binding
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            basicHttpBinding.OpenTimeout = basicHttpBinding.CloseTimeout = new TimeSpan(0, 1, 0);
            basicHttpBinding.ReceiveTimeout = basicHttpBinding.SendTimeout = new TimeSpan(0, 10, 0);
            basicHttpBinding.MaxReceivedMessageSize = basicHttpBinding.MaxBufferPoolSize = 2147483647;
            basicHttpBinding.BypassProxyOnLocal = basicHttpBinding.AllowCookies = false;
            basicHttpBinding.MessageEncoding = WSMessageEncoding.Text;
            basicHttpBinding.TextEncoding = Encoding.UTF8;
            basicHttpBinding.UseDefaultWebProxy = true;
            basicHttpBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            basicHttpBinding.ReaderQuotas = new XmlDictionaryReaderQuotas(); //ReaderQuotas, setting to Max
            basicHttpBinding.ReaderQuotas.MaxArrayLength = basicHttpBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
            basicHttpBinding.ReaderQuotas.MaxStringContentLength = basicHttpBinding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
            basicHttpBinding.ReaderQuotas.MaxDepth = 2147483647;

            //Create the Proxy
            ChannelFactory<T> proxy = new ChannelFactory<T>(basicHttpBinding, endpointAddress);

            //Add Headers, if any
            if (headers != null && headers.Count > 0)
            {
                EndpointAddressBuilder endpointAddressBuilder = new EndpointAddressBuilder(proxy.Endpoint.Address);
                foreach (var item in headers)
                    endpointAddressBuilder.Headers.Add(AddressHeader.CreateAddressHeader(item.Key, "indigo", item.Value));
                proxy.Endpoint.Address = endpointAddressBuilder.ToEndpointAddress();
            }

            //Sets the MaxItemsInObjectGraph, so that client can receive large objects
            foreach (var operation in proxy.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior operationBehavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
                //If DataContractSerializerOperationBehavior is not present in the Behavior, then add
                if (operationBehavior == null)
                {
                    operationBehavior = new DataContractSerializerOperationBehavior(operation);
                    operation.Behaviors.Add(operationBehavior);
                }
                //IMPORTANT: As 'operationBehavior' is a reference, changing anything here will automatically update the value in list, so no need to add this behavior to behaviorlist
                operationBehavior.MaxItemsInObjectGraph = 2147483647;
            }
            return proxy;
        }
    }
}
