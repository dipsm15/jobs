﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
//using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Athena.Deploy
{
    public partial class PartnerInfoControl : InstallerControl
    {
        public PartnerInfoControl()
        {
            InitializeComponent();
        }

        protected internal override void Open(InstallOptions options)
        {
            this.Form.SetupState = InstallationState.GatherPartnerInfo;
            txtPartnerName.Text = options.PartnerName;
            txtPartnerName.Focus();
            this.Form.NextButton.Enabled = (txtPartnerName.Text.Length > 0&&txtEmail.Text.Contains("@") &&txtEmail.Text==txtConfirmEmail.Text) ? true : false;
        }

        protected internal override void Close(InstallOptions options)
        {
            options.PartnerName = txtPartnerName.Text;
            options.AdminEmail = txtEmail.Text;
            //Update options for storing admin email Id .
            Utilities.Log("Partner Name: " + txtPartnerName.Text);
            Utilities.Log("Partner Location: " + txtPartnerLocation.Text);
        }

        #region Event Handlers
        private void txtPartnerName_TextChanged(object sender, EventArgs e)
        {
            this.Form.NextButton.Enabled = (txtPartnerName.Text.Length > 0) ? true : false;
        }
        #endregion
    }
}
