namespace Athena.Deploy
{
  partial class WelcomeControl
  {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
          if (disposing && (components != null))
          {
              components.Dispose();
          }
          base.Dispose(disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WelcomeControl));
          this.label3 = new System.Windows.Forms.Label();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.lblImageText = new System.Windows.Forms.Label();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.SuspendLayout();
          // 
          // label3
          // 
          this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.label3.Location = new System.Drawing.Point(13, 20);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(410, 241);
          this.label3.TabIndex = 2;
          this.label3.Text = resources.GetString("label3.Text");
          this.label3.Click += new System.EventHandler(this.label3_Click);
          // 
          // pictureBox1
          // 
          this.pictureBox1.Image = global::Athena.Deploy.Properties.Resources.Logo_Indigo;
          this.pictureBox1.Location = new System.Drawing.Point(280, 185);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(116, 57);
          this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
          this.pictureBox1.TabIndex = 3;
          this.pictureBox1.TabStop = false;
          // 
          // lblImageText
          // 
          this.lblImageText.AutoSize = true;
          this.lblImageText.Location = new System.Drawing.Point(283, 176);
          this.lblImageText.Name = "lblImageText";
          this.lblImageText.Size = new System.Drawing.Size(66, 13);
          this.lblImageText.TabIndex = 4;
          this.lblImageText.Text = "Powered by:";
          // 
          // WelcomeControl
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.BackColor = System.Drawing.Color.Transparent;
          this.Controls.Add(this.lblImageText);
          this.Controls.Add(this.pictureBox1);
          this.Controls.Add(this.label3);
          this.Name = "WelcomeControl";
          this.Size = new System.Drawing.Size(450, 275);
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.Label lblImageText;
  }
}
