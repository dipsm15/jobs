using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Athena.Deploy
{
    public class InstallerControl : UserControl
    {
        protected InstallerControl() { }

        public string Title { get; set; }
        public string SubTitle { get; set; }

        protected InstallerForm Form { get { return (InstallerForm)this.ParentForm; } }
        protected internal virtual void Open(InstallOptions options) { }
        protected internal virtual void Close(InstallOptions options) { }
        protected internal virtual void RequestCancel() { Application.Exit(); }
        protected internal virtual bool ValidateForm() { return true; }
    }

    public class InstallerControlList : List<InstallerControl> { }
    public static class ControlExtensions
    {
        public static void Invoke(this Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new MethodInvoker(action), null);
            }
            else
            {
                action.Invoke();
            }
        }
    }
    internal interface IDoAsync
    {
        void DoWorkAsync();
    }
}
