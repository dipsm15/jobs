namespace Athena.Deploy
{
    partial class ProxyManagerControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.panelResults = new System.Windows.Forms.Panel();
            this.lnkFileUpload = new System.Windows.Forms.LinkLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.imgFileUpload = new System.Windows.Forms.PictureBox();
            this.lnkFileDownload = new System.Windows.Forms.LinkLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.imgFileDownload = new System.Windows.Forms.PictureBox();
            this.lnkSvcRequest = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.imgServiceRequest = new System.Windows.Forms.PictureBox();
            this.lnkWebRequest = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.imgWebRequest = new System.Windows.Forms.PictureBox();
            this.ToolTipMessage = new System.Windows.Forms.ToolTip(this.components);
            this.btnTestProxy = new System.Windows.Forms.Button();
            this.panelProxySettings = new System.Windows.Forms.Panel();
            this.panelProxyDetails = new System.Windows.Forms.Panel();
            this.panelAuthScheme = new System.Windows.Forms.Panel();
            this.ddlAuthScheme = new System.Windows.Forms.ComboBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtProxyAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAuthScheme = new System.Windows.Forms.Label();
            this.chkUseDefaultCredentials = new System.Windows.Forms.CheckBox();
            this.chkUseProxy = new System.Windows.Forms.CheckBox();
            this.panelResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFileUpload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFileDownload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgServiceRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWebRequest)).BeginInit();
            this.panelProxySettings.SuspendLayout();
            this.panelProxyDetails.SuspendLayout();
            this.panelAuthScheme.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 16);
            this.label2.TabIndex = 24;
            this.label2.Text = "Network Settings:";
            // 
            // panelResults
            // 
            this.panelResults.Controls.Add(this.lnkFileUpload);
            this.panelResults.Controls.Add(this.label8);
            this.panelResults.Controls.Add(this.imgFileUpload);
            this.panelResults.Controls.Add(this.lnkFileDownload);
            this.panelResults.Controls.Add(this.label7);
            this.panelResults.Controls.Add(this.imgFileDownload);
            this.panelResults.Controls.Add(this.lnkSvcRequest);
            this.panelResults.Controls.Add(this.label6);
            this.panelResults.Controls.Add(this.imgServiceRequest);
            this.panelResults.Controls.Add(this.lnkWebRequest);
            this.panelResults.Controls.Add(this.label5);
            this.panelResults.Controls.Add(this.imgWebRequest);
            this.panelResults.Location = new System.Drawing.Point(16, 29);
            this.panelResults.Name = "panelResults";
            this.panelResults.Size = new System.Drawing.Size(269, 93);
            this.panelResults.TabIndex = 35;
            this.panelResults.Visible = false;
            // 
            // lnkFileUpload
            // 
            this.lnkFileUpload.AutoSize = true;
            this.lnkFileUpload.Location = new System.Drawing.Point(160, 71);
            this.lnkFileUpload.Name = "lnkFileUpload";
            this.lnkFileUpload.Size = new System.Drawing.Size(48, 13);
            this.lnkFileUpload.TabIndex = 43;
            this.lnkFileUpload.TabStop = true;
            this.lnkFileUpload.Text = "Success";
            this.lnkFileUpload.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Test file upload";
            // 
            // imgFileUpload
            // 
            this.imgFileUpload.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgFileUpload.Location = new System.Drawing.Point(13, 71);
            this.imgFileUpload.Margin = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.imgFileUpload.Name = "imgFileUpload";
            this.imgFileUpload.Size = new System.Drawing.Size(16, 16);
            this.imgFileUpload.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgFileUpload.TabIndex = 44;
            this.imgFileUpload.TabStop = false;
            this.imgFileUpload.Visible = false;
            // 
            // lnkFileDownload
            // 
            this.lnkFileDownload.AutoSize = true;
            this.lnkFileDownload.Location = new System.Drawing.Point(160, 49);
            this.lnkFileDownload.Name = "lnkFileDownload";
            this.lnkFileDownload.Size = new System.Drawing.Size(48, 13);
            this.lnkFileDownload.TabIndex = 2;
            this.lnkFileDownload.TabStop = true;
            this.lnkFileDownload.Text = "Success";
            this.lnkFileDownload.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Test file download";
            // 
            // imgFileDownload
            // 
            this.imgFileDownload.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgFileDownload.Location = new System.Drawing.Point(13, 49);
            this.imgFileDownload.Margin = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.imgFileDownload.Name = "imgFileDownload";
            this.imgFileDownload.Size = new System.Drawing.Size(16, 16);
            this.imgFileDownload.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgFileDownload.TabIndex = 41;
            this.imgFileDownload.TabStop = false;
            this.imgFileDownload.Visible = false;
            // 
            // lnkSvcRequest
            // 
            this.lnkSvcRequest.AutoSize = true;
            this.lnkSvcRequest.Location = new System.Drawing.Point(160, 27);
            this.lnkSvcRequest.Name = "lnkSvcRequest";
            this.lnkSvcRequest.Size = new System.Drawing.Size(48, 13);
            this.lnkSvcRequest.TabIndex = 1;
            this.lnkSvcRequest.TabStop = true;
            this.lnkSvcRequest.Text = "Success";
            this.lnkSvcRequest.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Test Service request";
            // 
            // imgServiceRequest
            // 
            this.imgServiceRequest.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgServiceRequest.Location = new System.Drawing.Point(13, 27);
            this.imgServiceRequest.Margin = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.imgServiceRequest.Name = "imgServiceRequest";
            this.imgServiceRequest.Size = new System.Drawing.Size(16, 16);
            this.imgServiceRequest.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgServiceRequest.TabIndex = 38;
            this.imgServiceRequest.TabStop = false;
            this.imgServiceRequest.Visible = false;
            // 
            // lnkWebRequest
            // 
            this.lnkWebRequest.AutoSize = true;
            this.lnkWebRequest.Location = new System.Drawing.Point(160, 5);
            this.lnkWebRequest.Name = "lnkWebRequest";
            this.lnkWebRequest.Size = new System.Drawing.Size(48, 13);
            this.lnkWebRequest.TabIndex = 0;
            this.lnkWebRequest.TabStop = true;
            this.lnkWebRequest.Text = "Success";
            this.lnkWebRequest.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Test Web-request";
            // 
            // imgWebRequest
            // 
            this.imgWebRequest.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgWebRequest.Location = new System.Drawing.Point(13, 5);
            this.imgWebRequest.Margin = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.imgWebRequest.Name = "imgWebRequest";
            this.imgWebRequest.Size = new System.Drawing.Size(16, 16);
            this.imgWebRequest.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgWebRequest.TabIndex = 35;
            this.imgWebRequest.TabStop = false;
            // 
            // btnTestProxy
            // 
            this.btnTestProxy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnTestProxy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTestProxy.Location = new System.Drawing.Point(289, 29);
            this.btnTestProxy.Name = "btnTestProxy";
            this.btnTestProxy.Size = new System.Drawing.Size(115, 24);
            this.btnTestProxy.TabIndex = 1;
            this.btnTestProxy.Text = "&Test Settings";
            this.btnTestProxy.UseVisualStyleBackColor = false;
            this.btnTestProxy.Click += new System.EventHandler(this.btnTestProxy_Click);
            // 
            // panelProxySettings
            // 
            this.panelProxySettings.Controls.Add(this.panelProxyDetails);
            this.panelProxySettings.Controls.Add(this.chkUseDefaultCredentials);
            this.panelProxySettings.Controls.Add(this.chkUseProxy);
            this.panelProxySettings.Location = new System.Drawing.Point(16, 128);
            this.panelProxySettings.Name = "panelProxySettings";
            this.panelProxySettings.Size = new System.Drawing.Size(402, 119);
            this.panelProxySettings.TabIndex = 37;
            this.panelProxySettings.Visible = false;
            // 
            // panelProxyDetails
            // 
            this.panelProxyDetails.Controls.Add(this.panelAuthScheme);
            this.panelProxyDetails.Controls.Add(this.txtPassword);
            this.panelProxyDetails.Controls.Add(this.label4);
            this.panelProxyDetails.Controls.Add(this.txtUsername);
            this.panelProxyDetails.Controls.Add(this.label3);
            this.panelProxyDetails.Controls.Add(this.txtProxyAddress);
            this.panelProxyDetails.Controls.Add(this.label1);
            this.panelProxyDetails.Controls.Add(this.lblAuthScheme);
            this.panelProxyDetails.Location = new System.Drawing.Point(2, 29);
            this.panelProxyDetails.Name = "panelProxyDetails";
            this.panelProxyDetails.Size = new System.Drawing.Size(396, 87);
            this.panelProxyDetails.TabIndex = 39;
            // 
            // panelAuthScheme
            // 
            this.panelAuthScheme.BackColor = System.Drawing.Color.Black;
            this.panelAuthScheme.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAuthScheme.Controls.Add(this.ddlAuthScheme);
            this.panelAuthScheme.Location = new System.Drawing.Point(92, 58);
            this.panelAuthScheme.Name = "panelAuthScheme";
            this.panelAuthScheme.Size = new System.Drawing.Size(122, 22);
            this.panelAuthScheme.TabIndex = 35;
            // 
            // ddlAuthScheme
            // 
            this.ddlAuthScheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlAuthScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAuthScheme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddlAuthScheme.FormattingEnabled = true;
            this.ddlAuthScheme.Items.AddRange(new object[] {
            "Basic",
            "Digest",
            "NTML",
            "Negotiate",
            "Passport"});
            this.ddlAuthScheme.Location = new System.Drawing.Point(0, 0);
            this.ddlAuthScheme.Name = "ddlAuthScheme";
            this.ddlAuthScheme.Size = new System.Drawing.Size(120, 21);
            this.ddlAuthScheme.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Location = new System.Drawing.Point(273, 32);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(115, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(217, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Password";
            // 
            // txtUsername
            // 
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsername.Location = new System.Drawing.Point(92, 32);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(121, 20);
            this.txtUsername.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Username";
            // 
            // txtProxyAddress
            // 
            this.txtProxyAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProxyAddress.Location = new System.Drawing.Point(92, 6);
            this.txtProxyAddress.Name = "txtProxyAddress";
            this.txtProxyAddress.Size = new System.Drawing.Size(295, 20);
            this.txtProxyAddress.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Proxy Address";
            // 
            // lblAuthScheme
            // 
            this.lblAuthScheme.AutoSize = true;
            this.lblAuthScheme.Location = new System.Drawing.Point(10, 62);
            this.lblAuthScheme.Name = "lblAuthScheme";
            this.lblAuthScheme.Size = new System.Drawing.Size(71, 13);
            this.lblAuthScheme.TabIndex = 26;
            this.lblAuthScheme.Text = "Auth Scheme";
            // 
            // chkUseDefaultCredentials
            // 
            this.chkUseDefaultCredentials.AutoSize = true;
            this.chkUseDefaultCredentials.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkUseDefaultCredentials.Location = new System.Drawing.Point(163, 6);
            this.chkUseDefaultCredentials.Name = "chkUseDefaultCredentials";
            this.chkUseDefaultCredentials.Size = new System.Drawing.Size(220, 17);
            this.chkUseDefaultCredentials.TabIndex = 38;
            this.chkUseDefaultCredentials.Text = "Use default credentials (test purpose only)";
            this.chkUseDefaultCredentials.UseVisualStyleBackColor = true;
            this.chkUseDefaultCredentials.CheckedChanged += new System.EventHandler(this.chkUseDefaultCredentials_CheckedChanged);
            // 
            // chkUseProxy
            // 
            this.chkUseProxy.AutoSize = true;
            this.chkUseProxy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkUseProxy.Location = new System.Drawing.Point(5, 6);
            this.chkUseProxy.Name = "chkUseProxy";
            this.chkUseProxy.Size = new System.Drawing.Size(71, 17);
            this.chkUseProxy.TabIndex = 37;
            this.chkUseProxy.Text = "Use Proxy";
            this.chkUseProxy.UseVisualStyleBackColor = true;
            this.chkUseProxy.CheckedChanged += new System.EventHandler(this.chkUseProxy_CheckedChanged);
            // 
            // ProxyManagerControl
            // 
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.panelProxySettings);
            this.Controls.Add(this.btnTestProxy);
            this.Controls.Add(this.panelResults);
            this.Controls.Add(this.label2);
            this.Name = "ProxyManagerControl";
            this.Size = new System.Drawing.Size(430, 250);
            this.panelResults.ResumeLayout(false);
            this.panelResults.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFileUpload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFileDownload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgServiceRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgWebRequest)).EndInit();
            this.panelProxySettings.ResumeLayout(false);
            this.panelProxySettings.PerformLayout();
            this.panelProxyDetails.ResumeLayout(false);
            this.panelProxyDetails.PerformLayout();
            this.panelAuthScheme.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelResults;
        private System.Windows.Forms.LinkLabel lnkFileDownload;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox imgFileDownload;
        private System.Windows.Forms.LinkLabel lnkSvcRequest;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox imgServiceRequest;
        private System.Windows.Forms.LinkLabel lnkWebRequest;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox imgWebRequest;
        private System.Windows.Forms.ToolTip ToolTipMessage;
        private System.Windows.Forms.Button btnTestProxy;
        private System.Windows.Forms.LinkLabel lnkFileUpload;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox imgFileUpload;
        private System.Windows.Forms.Panel panelProxySettings;
        private System.Windows.Forms.Panel panelProxyDetails;
        private System.Windows.Forms.Panel panelAuthScheme;
        private System.Windows.Forms.ComboBox ddlAuthScheme;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtProxyAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAuthScheme;
        private System.Windows.Forms.CheckBox chkUseDefaultCredentials;
        private System.Windows.Forms.CheckBox chkUseProxy;
    }
}
