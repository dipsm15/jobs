namespace Athena.Deploy
{
  partial class InstallProcessControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
        this.label2 = new System.Windows.Forms.Label();
        this.label1 = new System.Windows.Forms.Label();
        this.descriptionLabel = new System.Windows.Forms.Label();
        this.progressTotal = new System.Windows.Forms.ProgressBar();
        this.progressTask = new System.Windows.Forms.ProgressBar();
        this.tableLayoutPanel.SuspendLayout();
        this.SuspendLayout();
        // 
        // tableLayoutPanel
        // 
        this.tableLayoutPanel.ColumnCount = 1;
        this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableLayoutPanel.Controls.Add(this.label2, 0, 3);
        this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
        this.tableLayoutPanel.Controls.Add(this.descriptionLabel, 0, 2);
        this.tableLayoutPanel.Controls.Add(this.progressTotal, 0, 1);
        this.tableLayoutPanel.Controls.Add(this.progressTask, 0, 4);
        this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.tableLayoutPanel.Location = new System.Drawing.Point(10, 10);
        this.tableLayoutPanel.Name = "tableLayoutPanel";
        this.tableLayoutPanel.RowCount = 6;
        this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
        this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
        this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
        this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
        this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
        this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
        this.tableLayoutPanel.Size = new System.Drawing.Size(430, 230);
        this.tableLayoutPanel.TabIndex = 0;
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
        this.label2.Location = new System.Drawing.Point(2, 121);
        this.label2.Margin = new System.Windows.Forms.Padding(2, 5, 5, 0);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(423, 13);
        this.label2.TabIndex = 7;
        this.label2.Text = "Task Progress:";
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
        this.label1.Location = new System.Drawing.Point(2, 31);
        this.label1.Margin = new System.Windows.Forms.Padding(2, 5, 5, 0);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(423, 13);
        this.label1.TabIndex = 6;
        this.label1.Text = "Installation Progress:";
        // 
        // descriptionLabel
        // 
        this.descriptionLabel.AutoSize = true;
        this.descriptionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.descriptionLabel.Location = new System.Drawing.Point(2, 76);
        this.descriptionLabel.Margin = new System.Windows.Forms.Padding(2, 2, 5, 5);
        this.descriptionLabel.Name = "descriptionLabel";
        this.descriptionLabel.Size = new System.Drawing.Size(423, 23);
        this.descriptionLabel.TabIndex = 4;
        this.descriptionLabel.Text = "Working...";
        // 
        // progressTotal
        // 
        this.progressTotal.BackColor = System.Drawing.Color.Gainsboro;
        this.progressTotal.Dock = System.Windows.Forms.DockStyle.Fill;
        this.progressTotal.Location = new System.Drawing.Point(3, 47);
        this.progressTotal.Name = "progressTotal";
        this.progressTotal.Size = new System.Drawing.Size(424, 24);
        this.progressTotal.Step = 1;
        this.progressTotal.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
        this.progressTotal.TabIndex = 0;
        // 
        // progressTask
        // 
        this.progressTask.BackColor = System.Drawing.Color.Gainsboro;
        this.progressTask.Dock = System.Windows.Forms.DockStyle.Fill;
        this.progressTask.Location = new System.Drawing.Point(3, 137);
        this.progressTask.Name = "progressTask";
        this.progressTask.Size = new System.Drawing.Size(424, 24);
        this.progressTask.Step = 1;
        this.progressTask.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
        this.progressTask.TabIndex = 5;
        // 
        // InstallProcessControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BackColor = System.Drawing.Color.WhiteSmoke;
        this.Controls.Add(this.tableLayoutPanel);
        this.Name = "InstallProcessControl";
        this.Padding = new System.Windows.Forms.Padding(10);
        this.Size = new System.Drawing.Size(450, 250);
        this.tableLayoutPanel.ResumeLayout(false);
        this.tableLayoutPanel.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
    private System.Windows.Forms.ProgressBar progressTotal;
    private System.Windows.Forms.Label descriptionLabel;
    private System.Windows.Forms.ProgressBar progressTask;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;

  }
}
