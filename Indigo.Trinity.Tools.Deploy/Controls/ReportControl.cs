﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using Indigo.Athena.Services;

namespace Athena.Deploy
{
    public partial class ReportControl : InstallerControl, IDoAsync
    {
        #region Constructor
        public ReportControl()
        {
            InitializeComponent();
        }
        #endregion

        #region Private Members
        string targetPath = null;
        private string _linkLaunchUrl = string.Empty;
        private bool _isLaunchJobsManager = false;
        private string _siteUrl = string.Empty;
        private InstallOptions _options = null;
        #endregion

        #region Constants
        private const string TEST_PARTNER = "Test Partner";
        #endregion

        # region Public Properties

        public bool IsLaunchJobsManager
        {
            get { return _isLaunchJobsManager; }
            set { _isLaunchJobsManager = value; }
        }
        public string SiteUrl
        {
            get { return _siteUrl; }
            private set { _siteUrl = value; }
        }
        #endregion

        #region Overridden Methods
        protected internal override void Open(InstallOptions options)
        {
            try
            {
                _options = options;
                Thread thread = new Thread(new ThreadStart(DoWorkAsync));
                thread.Start();
            }
            catch { /* Supress Error */ }
        }
        #endregion

        #region Event Handlers
        private void lnkLogFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!File.Exists(Utilities.LogFile)) return;

            try { Process.Start("notepad.exe", Utilities.LogFile); }
            catch (Exception ex)
            {
                Utilities.Log("Error opening log file: ", ex);
                MessageBox.Show("Open log file from: " + Utilities.LogFile, "Error opening log file", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lnkMessage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("iexplore.exe", _linkLaunchUrl);
            }
            catch (Exception ex) { Utilities.Log("Error launching url: " + _linkLaunchUrl, ex); }
        }

        private void lnkCancelSend_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
        #endregion

        #region IDoAsync Functions
        public void DoWorkAsync()
        {
            try
            {
                if (this.Form != null)
                {
                    switch (_options.SetupState)
                    {
                        case InstallationState.Successful:
                            this.Form.SetTitle("Installation Successful!");
                            this.Form.SetSubTitle("Microsoft Community Portal installed successfully.");
                            _siteUrl = _options.SiteUrl;
                            break;
                        case InstallationState.Failed:
                            this.Form.SetTitle("Installation Failed");
                            this.Form.SetSubTitle("Microsoft Community Portal could not be installed.");
                            break;
                        default:
                        case InstallationState.Aborted:
                            this.Form.SetTitle("Installation aborted");
                            this.Form.SetSubTitle("Installation has been aborted.");
                            break;
                    }
                    this.Form.PrevButton.Invoke(() => { this.Form.PrevButton.Visible = false; });
                    this.Form.NextButton.Invoke(() => { this.Form.NextButton.Visible = false; });
                    this.Form.AbortButton.Invoke(() => { this.Form.AbortButton.Enabled = false; });
                    this.Form.AbortButton.Invoke(() => { this.Form.AbortButton.Text = "Finish"; });
                }
                lblReportMessage.Invoke(() => { lblReportMessage.Text = "Please wait, saving installation report..."; });
                panelSendReport.Invoke(() => { panelSendReport.Visible = true; });
                _SaveReport();
                _CompressFile(targetPath);
                _UploadReport(_options);
                //Finish the setup
                lblReportMessage.Invoke(() => { lblReportMessage.Text = "Thank you for sending installation report."; });
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in Upload Report", ex);
                lblReportMessage.Invoke(() => { lblReportMessage.Text = "Failed to send installation report."; });
            }
            if (this.Form != null) this.Form.AbortButton.Invoke(() => { this.Form.AbortButton.Enabled = true; });
        }
        #endregion

        #region Private Helper Methods
        private void _CompressFile(string toZipFile)
        {
            //toZipFile can be a single file or a folder
            string fileName = "mcp-setup-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".zip";
            string zipFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName);
            targetPath = zipFile;
            FileInfo[] files = null;

            if (File.Exists(toZipFile))
            {
                files = new FileInfo[] { new FileInfo(toZipFile) };
            }
            else
            {
                DirectoryInfo dir = new DirectoryInfo(toZipFile);
                files = dir.GetFiles("*.*", SearchOption.AllDirectories);
            }

            using (ZipOutputStream s = new ZipOutputStream(File.Create(zipFile)))
            {
                s.SetLevel(9); // 0-9, 9 being the highest compression
                byte[] buffer = new byte[10000000];
                foreach (FileInfo file in files)
                {
                    ZipEntry entry = new ZipEntry(file.Name);
                    entry.DateTime = DateTime.Now;
                    s.PutNextEntry(entry);
                    using (FileStream fs = File.OpenRead(file.FullName))
                    {
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0,
                            buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }
                s.Finish();
                s.Close();
            }
        }

        private void _UploadReport(InstallOptions options)
        {
            if (!File.Exists(Utilities.LogFile)) return;
            try
            {
                lblReportMessage.Invoke(() => { lblReportMessage.Text = "Please wait, sending installation report to Microsoft... Retrieving location"; });
                // 1. Prepare WebProxy 
                WebProxy proxy = null;
                bool useProxy = false;
                bool.TryParse(options.UseProxy, out useProxy);
                if (useProxy)
                {
                    proxy = new WebProxy(options.ProxyAddressPort);
                    if (string.IsNullOrEmpty(options.ProxyUserName)) proxy.UseDefaultCredentials = true;
                    else proxy.Credentials = new NetworkCredential(options.ProxyUserName, options.ProxyPassword);
                }

                // 2. Get Upload Location
                WebRequest.DefaultWebProxy = proxy;
                string uploadLocation = string.Empty;
                string fileName = string.Format("{0}-setuplog-{1}.zip", System.Web.HttpUtility.UrlEncode(this.Form.options.PartnerName), DateTime.Now.ToString("ddMMyyyyhhmmss"));
                ChannelFactory<ISyncService> proxySvc = new Indigo.Athena.Deploy.ProxyManager().Proxy<ISyncService>(Properties.Settings.Default.ServiceUrl, null);
                var svcClient = proxySvc.CreateChannel();
                uploadLocation = svcClient.GetUploadLocationByPartner(TEST_PARTNER, fileName, "SetupLog");
                proxySvc.Close();
                string serverFilePath = string.Format("{0}/{1}", uploadLocation.TrimEnd('/', '\\'), fileName);
                _UploadFileWebClient(serverFilePath, targetPath);
            }
            catch (Exception ex)
            {
                Utilities.Log("Error sending report.", ex);
                lblReportMessage.Text = "Error sending installation report.";
                if (this.Form != null) this.Form.AbortButton.Enabled = true;
            }
        }

        private void _UploadFileWebClient(string serverPath, string localFilePath)
        {
            lblReportMessage.Invoke(() => { lblReportMessage.Text = "Please wait, sending installation report to Microsoft... Uploading..."; });
            if (!File.Exists(localFilePath))
                throw new FileNotFoundException("File not found:" + localFilePath);
            //WebClient client = new WebClient();
            serverPath = serverPath.Replace(" ", "%20");
            string fileName = Path.GetFileName(localFilePath);
            //filename
            string serverFilePath = serverPath.TrimEnd('/', '\\') + "/" + fileName;
            serverFilePath = serverFilePath.Replace('/', '\\');
            try
            {
                Indigo.Athena.Deploy.ProxyManager proxyMgr = new Indigo.Athena.Deploy.ProxyManager();
                var proxy = proxyMgr.Proxy<ISyncService>(Athena.Deploy.Properties.Settings.Default.ServiceUrl, null);
                var svcClient = proxy.CreateChannel();

                int chunkSize = 65000;

                Result result = new Result();
                int retryCount = 0;
                while (retryCount < 3)
                {
                    using (FileStream fs = new FileStream(localFilePath, FileMode.Open))
                    {
                        byte[] buffer = new byte[chunkSize];
                        int bytesRead = 0;

                        int totalChunkCount = Convert.ToInt32(fs.Length / chunkSize);
                        if (fs.Length % chunkSize != 0) totalChunkCount++;

                        int currentChunkCount = 1;
                        // iterate through the file filling a buffer and sending it to the service
                        while ((bytesRead = fs.Read(buffer, 0, chunkSize)) > 0)
                        {
                            result = svcClient.Upload(new FileData { FileName = serverFilePath, Data = buffer, Count = bytesRead, CurrentChunkCount = currentChunkCount, TotalChunckCount = totalChunkCount });
                            currentChunkCount++;
                            if (!result.IsSuccessful) break;
                        }
                        if (!result.IsSuccessful)
                        {
                            retryCount++;
                        }
                        else break;
                    }
                }
                proxy.Close();
            }
            catch (Exception ex) { throw new Exception("Error in uploading  : " + ex.Message); }
            finally
            {
                WebRequest.DefaultWebProxy = null;
            }
        }

        private void _SaveReport()
        {
            try
            {
                if (File.Exists(Utilities.LogFile))
                {
                    string fileName = "mcp-setup-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".log";
                    targetPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), fileName);
                    File.Copy(Utilities.LogFile, targetPath);
                    lblLogMessage.Invoke(() => { lblLogMessage.Text = "Installation log has been saved on your desktop."; });
                }
            }
            catch { /* SUPRESS */ }
        }
        #endregion
    }
}
