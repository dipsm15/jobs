using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Security;
using System.ServiceModel;
using Indigo.Athena.Services;
using System.Threading;

namespace Athena.Deploy
{
    public partial class ProxyManagerControl : InstallerControl, IDoAsync
    {
        #region Private Variables
        private string _authScheme = "Basic";
        private WebProxy _proxy = null;
        private bool _isSettingValid = false;

        #region Test Result
        enum TestState { InProgress, Success, Error, Warning };
        class TestResult
        {
            public string Message { get; set; }
            public TestState State { get; set; }

            public static TestResult SetError(string message)
            {
                return new TestResult() { State = TestState.Error, Message = message };
            }

            public static TestResult SetSuccess(string message)
            {
                return new TestResult() { State = TestState.Success, Message = message };
            }

            public static TestResult SetWarning(string message)
            {
                return new TestResult() { State = TestState.Warning, Message = message };
            }
        }
        #endregion
        #endregion

        #region Constructor
        public ProxyManagerControl() { InitializeComponent(); }
        #endregion

        #region Constants
        private const string SERVICE_URL = "http://mcp.indigoarchitects.com/Relay/Service/Sync.svc";
        private const string TEST_PACKAGE = "http://mcp.indigoarchitects.com/Relay/export/test.cmp";
        private const string TEST_UPLOAD = "UploadTest.txt";
        private const string TEST_PARTNER = "Test Partner";
        #endregion

        #region Overloaded Methods
        protected internal override void Open(InstallOptions options)
        {
            this.Form.SetupState = InstallationState.GatherProxyInfo;

            if (!_isSettingValid)
            {
                this.Form.SetupState = InstallationState.GatherProxyInfo;

                if (!_isSettingValid)
                {
                    ddlAuthScheme.SelectedIndex = 0;
                    if (_GetSystemProxy(SERVICE_URL) != null)
                    {
                        chkUseProxy.Checked = panelProxySettings.Visible = true;
                        _OnUseProxyCheckedChange();
                    }

                    _OnUseDefaultCredentialChange();
                    _AsyncTestInternetAccess();
                }
            }
        }

        protected internal override bool ValidateForm()
        {
            if (!_isSettingValid)
                MessageBox.Show("Please verify the settings using 'Test Settings' button. \r\nCurrent settings are either invalid or not verified.", "Invalid settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            return _isSettingValid;
        }

        protected internal override void Close(InstallOptions options)
        {
            _SaveProxySettings(options);
        }
        #endregion

        #region Event Handlers
        private void btnTestProxy_Click(object sender, EventArgs e)
        {
            if (_isSettingValid)
            {
                panelProxySettings.Visible = panelProxySettings.Enabled = true;
                btnTestProxy.Text = "&Test Settings";
                _isSettingValid = false;
            }
            else
            {
                _authScheme = ddlAuthScheme.SelectedItem.ToString();
                _AsyncTestInternetAccess();
            }
        }
        private void chkUseProxy_CheckedChanged(object sender, EventArgs e)
        {
            _OnUseProxyCheckedChange();
        }
        private void chkUseDefaultCredentials_CheckedChanged(object sender, EventArgs e)
        {
            _OnUseDefaultCredentialChange();
        }
        #endregion

        #region Helper Functions
        private void _AsyncTestInternetAccess()
        {
            try
            {
                Thread thread = new Thread(new ThreadStart(DoWorkAsync));
                thread.Start();
            }
            catch (Exception ex) { Utilities.Log("Error in _AsyncTestInternetAccess.", ex); }

        }
        public void DoWorkAsync()
        {
            try
            {
                _isSettingValid = false;
                if (chkUseProxy.Checked && txtProxyAddress.Text == string.Empty)
                {
                    MessageBox.Show("Enter valid proxy address");
                    return;
                }

                TestResult result;

                imgWebRequest.Invoke(() => { imgWebRequest.Visible = false; });
                imgServiceRequest.Invoke(() => { imgServiceRequest.Visible = false; });
                imgFileDownload.Invoke(() => { imgFileDownload.Visible = false; });
                imgFileUpload.Invoke(() => { imgFileUpload.Visible = false; });
                lnkWebRequest.Invoke(() => { lnkWebRequest.Visible = false; });
                lnkSvcRequest.Invoke(() => { lnkSvcRequest.Visible = false; });
                lnkFileDownload.Invoke(() => { lnkFileDownload.Visible = false; });
                lnkFileUpload.Invoke(() => { lnkFileUpload.Visible = false; });

                panelResults.Invoke(() => { panelResults.Visible = true; });
                btnTestProxy.Invoke(() => { btnTestProxy.Enabled = false; });
                panelProxySettings.Invoke(() => { panelProxySettings.Enabled = false; });

                // Get Proxy Settings
                _proxy = _GetSelectedProxy();
                string message = "Checking internet access " + ((_proxy != null) ? "(using proxy) " : "") + "...";
                if (this.Form != null) this.Form.SetSubTitle(message);

                // 1. Test Web-request
                _SetIcon(imgWebRequest, TestState.InProgress);
                Thread.Sleep(2000);
                result = _TestUrl();
                _SetIcon(imgWebRequest, result.State);
                _SetMessage(lnkWebRequest, result.State.ToString(), result.Message);
                Utilities.Log(string.Format("Test Url: {0} {1}", result.State, result.Message));
                if (result.State == TestState.Error) { _SkipSteps(0); return; }

                // 2. Test Service request
                _SetIcon(imgServiceRequest, TestState.InProgress);
                Thread.Sleep(2000);
                result = _TestService();
                _SetIcon(imgServiceRequest, result.State);
                _SetMessage(lnkSvcRequest, result.State.ToString(), result.Message);
                Utilities.Log(string.Format("Test Service: {0} {1}", result.State, result.Message));
                if (result.State == TestState.Error) { _SkipSteps(1); return; }

                // 3. Test file download
                _SetIcon(imgFileDownload, TestState.InProgress);
                result = _TestDownload();
                _SetIcon(imgFileDownload, result.State);
                _SetMessage(lnkFileDownload, result.State.ToString(), result.Message);
                Utilities.Log(string.Format("Test Download: {0} {1}", result.State, result.Message));
                if (result.State == TestState.Error) { _SkipSteps(2); return; }

                // 4. Test file upload
                _SetIcon(imgFileUpload, TestState.InProgress);
                result = _TestUpload();
                _SetIcon(imgFileUpload, result.State);
                _SetMessage(lnkFileUpload, result.State.ToString(), result.Message);
                Utilities.Log(string.Format("Test Upload: {0} {1}", result.State, result.Message));
                if (result.State == TestState.Error) { _SkipSteps(3); return; }

                _isSettingValid = true;

                btnTestProxy.Invoke(() => { btnTestProxy.Text = "&Modify Settings"; });
                if (this.Form != null) this.Form.SetSubTitle("Network settings verified successfully. Click Next to proceed.");
            }
            catch (Exception ex) { Utilities.Log("If object ref is occured after quitting setup, ignore error.", ex); }
            finally
            {
                btnTestProxy.Invoke(() => { btnTestProxy.Enabled = true; });
            }
        }

        #region Proxy Helpers
        private WebProxy _GetSelectedProxy()
        {
            WebProxy proxy = null;
            if (chkUseProxy.Checked)
            {
                if (proxy == null)
                {
                    proxy = new WebProxy(txtProxyAddress.Text);
                    proxy.Credentials = new NetworkCredential(txtUsername.Text, txtPassword.Text);
                }
                else
                    throw new ApplicationException("Invalid proxy settings.");

                Utilities.Log("Using proxy: " + proxy.Address.OriginalString + " [" + txtUsername.Text + "]");
                return proxy;
            }

            return null;
        }

        private WebProxy _GetSystemProxy(string url)
        {
            Uri requestUri = new Uri(url);
            IWebProxy proxySettings = WebRequest.GetSystemWebProxy();
            WebProxy proxy = null;
            Uri proxyAddress = proxySettings.GetProxy(requestUri);
            if (proxyAddress == null || proxyAddress == requestUri)
                return null;

            proxy = new WebProxy(proxyAddress);
            Utilities.Log("Auto-detected proxy:" + proxy.Address.OriginalString);
            if (proxySettings.Credentials == null)
                proxy.Credentials = new NetworkCredential(txtUsername.Text, txtPassword.Text);
            else
                proxy.Credentials = proxySettings.Credentials;

            return proxy;
        }

        private void _SaveProxySettings(InstallOptions options)
        {
            Utilities.Log("Saving proxy settings:");
            options.UseProxy = chkUseProxy.Checked.ToString();
            Utilities.Log("UseProxy:" + options.UseProxy.ToString());

            if (chkUseProxy.Checked && _proxy != null)
            {
                options.ProxyAddressPort = _proxy.Address.OriginalString;
                options.ProxyUserName = (_proxy.Credentials != null) ? (_proxy.Credentials as NetworkCredential).UserName : null;
                options.ProxyPassword = (_proxy.Credentials != null) ? (_proxy.Credentials as NetworkCredential).Password : null;
                options.ProxyAuthScheme = ddlAuthScheme.SelectedItem.ToString();

                Utilities.Log("Poxy address:" + options.ProxyAddressPort);
                Utilities.Log("Proxy auth scheme:" + options.ProxyAuthScheme);
                Utilities.Log("Proxy credentials specified:" + (!string.IsNullOrEmpty(options.ProxyUserName)).ToString());
            }
            else
            {
                options.ProxyAddressPort = string.Empty;
                options.ProxyUserName = string.Empty;
                options.ProxyPassword = string.Empty;
                options.ProxyAuthScheme = string.Empty;
            }
        }

        #endregion

        #region Test Cases
        private TestResult _TestUrl()
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(SERVICE_URL);
                request.Timeout = 10 * 1000;
                request.Proxy = _proxy;

                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK && response.ContentLength > 0)
                    return TestResult.SetSuccess(string.Format("Response: {0}. Content Length: {1}", response.StatusCode, response.ContentLength));

                return TestResult.SetError(string.Format("Error in testing web-request. Response: {0}. Content Length: {1}", response.StatusCode, response.ContentLength));
            }
            catch (Exception ex)
            {
                Utilities.Log("_TestUrl Error:", ex);
                return TestResult.SetError(string.Format("{0}: {1}", ex.GetType().Name, ex.Message));
            }
            finally
            {
                request = null;
                if (response != null) response.Close();
            }
        }

        private TestResult _TestService()
        {

            ChannelFactory<ISyncService> proxy = new Indigo.Athena.Deploy.ProxyManager().Proxy<ISyncService>(SERVICE_URL + "/basichttp", null);
            var svcClient = proxy.CreateChannel();

            try
            {
                WebRequest.DefaultWebProxy = _proxy;

                PackageCriteria criteria = new PackageCriteria() { Partner = TEST_PARTNER };
                Package[] packages = svcClient.FindPackages(criteria);
                return TestResult.SetSuccess(packages.Length + " packages found.");
            }
            catch (Exception ex)
            {
                Utilities.Log("_TestService Error:", ex);
                return TestResult.SetError(string.Format("{0}: {1}", ex.GetType().Name, ex.Message));
            }
            finally
            {
                WebRequest.DefaultWebProxy = null;
                proxy.Close();
            }
        }

        private TestResult _TestDownload()
        {
            string destinationFile = string.Empty;

            try { destinationFile = _Download(TEST_PACKAGE, _proxy); }
            catch (Exception ex)
            {
                Utilities.Log("_TestDownload Error:", ex);
                return TestResult.SetError(string.Format("{0}: {1}", ex.GetType().Name, ex.Message));
            }

            System.Threading.Thread.Sleep(5000);
            if (!File.Exists(destinationFile))
                return TestResult.SetError("Downloaded file not found.");
            else
                return TestResult.SetSuccess("Download successful.");
        }

        private TestResult _TestUpload()
        {
            string uploadLocation = string.Empty;
            string fileName = string.Empty;

            ChannelFactory<ISyncService> proxy = new Indigo.Athena.Deploy.ProxyManager().Proxy<ISyncService>(SERVICE_URL + "/basichttp", null);
            var svcClient = proxy.CreateChannel();
            try
            {
                WebRequest.DefaultWebProxy = _proxy;
                fileName = string.Format("{0}-uploadtest-{1}{2}", System.Web.HttpUtility.UrlEncode(this.Form.options.PartnerName), DateTime.Now.ToString("ddMMyyyyhhmmss"),
                    Path.GetExtension(TEST_UPLOAD));

                uploadLocation = svcClient.GetUploadLocationByPartner(TEST_PARTNER, fileName, "TestPackage");
            }
            catch (Exception ex)
            {
                Utilities.Log("_TestUpload Error (Service):", ex);
                return TestResult.SetError(string.Format("Service Error: {0}: {1}", ex.GetType().Name, ex.Message));
            }
            finally
            {
                proxy.Close();
            }

            try
            {
                string serverFilePath = string.Format("{0}/{1}", uploadLocation.TrimEnd('/', '\\'), fileName);
                _Upload(serverFilePath, TEST_UPLOAD, _proxy);
            }
            catch (Exception ex)
            {
                Utilities.Log("_TestUpload Error (Upload file):", ex);
                return TestResult.SetError(string.Format("Upload Error: {0}: {1}", ex.GetType().Name, ex.Message));
            }

            return TestResult.SetSuccess("Upload successful.");
        }
        #endregion

        #region BITS Upload / Download
        private string _Download(string downloadUrl, WebProxy proxy)
        {
            System.Net.BITS.Manager jobManager = new System.Net.BITS.Manager();
            System.Net.BITS.Job job = new System.Net.BITS.Job("Proxy config wizard test job",
                System.Net.BITS.JobType.Download, System.Net.BITS.JobPriority.Foreground);
            if (job == null)
                throw new ApplicationException("BITS job could not be created");

            jobManager.Jobs.Add(job);

            if (proxy != null)
            {
                System.Net.BITS.JobProxySettings settings = new System.Net.BITS.JobProxySettings(System.Net.BITS.ProxyUsage.Override, "http=" + proxy.Address.OriginalString, null);
                job.ProxySettings = settings;

                System.Net.BITS.Credentials cred = new System.Net.BITS.Credentials();
                cred.Target = System.Net.BITS.AuthTarget.Proxy;
                cred.Scheme = GetAuthScheme(_authScheme);

                if (!proxy.UseDefaultCredentials && proxy.Credentials != null)
                {
                    cred.UserName = ConvertStringToSecureString((proxy.Credentials as NetworkCredential).UserName);
                    cred.Password = ConvertStringToSecureString((proxy.Credentials as NetworkCredential).Password);
                }
                else
                    cred.UserName = cred.Password = null;

                job.SetCredentials(cred);
            }

            string basePath = Program.WorkingFolder;
            string destinationFile = Path.Combine(basePath, Path.GetFileName(downloadUrl));
            if (File.Exists(destinationFile)) File.Delete(destinationFile);
            job.Files.Add(downloadUrl, destinationFile);

            job.AutoComplete = true;
            job.AutoCancelOnError = false;
            job.AutoCancelOnTransientError = false;
            jobManager.Jobs.Update();
            string error = string.Empty;
            if (job.Error.ContextDescription != null && job.Error.Description != null)
            {
                error = "Context Description: " + job.Error.ContextDescription + " Error Description: " + job.Error.Description + " Job Owner: " + job.Owner;
                throw new ApplicationException(error);
            }

            job.Resume(); // start the job in action

            while (job.State != System.Net.BITS.JobState.Transferred &&
                    job.State != System.Net.BITS.JobState.Cancelled &&
                    job.State != System.Net.BITS.JobState.Acknowledged &&
                    job.State != System.Net.BITS.JobState.Error)
                System.Threading.Thread.Sleep(5000);

            if (job.State == System.Net.BITS.JobState.Error || job.State == System.Net.BITS.JobState.Cancelled)
            {
                if (job.State == System.Net.BITS.JobState.Error)
                {
                    if (job.Error.ContextDescription != null && job.Error.Description != null)
                        error = "Context Description: " + job.Error.ContextDescription + " Error Description: " + job.Error.Description + " Job Owner: " + job.Owner;
                }
                job.Cancel();
                job.Dispose();
                throw new Exception("Error in BITS download. Job State:" + job.State + " " + error);
            }
            else
            {
                job.Dispose();
            }

            return destinationFile;
        }

        private void _Upload(string serverFilePath, string localFile, WebProxy proxy)
        {
            _UploadFileWebClient(serverFilePath, localFile);

        }
        private void _UploadFileWebClient(string serverPath, string localFilePath)
        {
            if (!File.Exists(localFilePath))
                throw new FileNotFoundException("File not found:" + localFilePath);
            //WebClient client = new WebClient();
            serverPath = serverPath.Replace(" ", "%20");
            string fileName = Path.GetFileName(localFilePath);
            //filename
            string serverFilePath = serverPath.TrimEnd('/', '\\') + "/" + fileName;
            serverFilePath = serverFilePath.Replace('/', '\\');

            Indigo.Athena.Deploy.ProxyManager proxyMgr = new Indigo.Athena.Deploy.ProxyManager();
            var proxy = proxyMgr.Proxy<ISyncService>(Athena.Deploy.Properties.Settings.Default.ServiceUrl, null);
            var svcClient = proxy.CreateChannel();
            try
            {
                int chunkSize = 65000;

                Result result = new Result();
                int retryCount = 0;
                while (retryCount < 3)
                {
                    using (FileStream fs = new FileStream(localFilePath, FileMode.Open))
                    {
                        byte[] buffer = new byte[chunkSize];
                        int bytesRead = 0;

                        int totalChunkCount = Convert.ToInt32(fs.Length / chunkSize);
                        if (fs.Length % chunkSize != 0) totalChunkCount++;

                        int currentChunkCount = 1;
                        // iterate through the file filling a buffer and sending it to the service
                        while ((bytesRead = fs.Read(buffer, 0, chunkSize)) > 0)
                        {
                            result = svcClient.Upload(new FileData { FileName = serverFilePath, Data = buffer, Count = bytesRead, CurrentChunkCount = currentChunkCount, TotalChunckCount = totalChunkCount });
                            currentChunkCount++;
                            if (!result.IsSuccessful) break;
                        }
                        if (!result.IsSuccessful)
                        {
                            retryCount++;
                            //if (result.Message != null && result.Message.Length > 0)
                            //    this.Log("Upload failed: " + result.Message[0] + "\nAttempt: " + (retryCount + 1), false, false);
                            //else
                            //    this.Log("Upload failed.\nAttempt: " + (retryCount + 1), false, false);
                        }
                        else break;
                    }
                }
            }
            catch (Exception ex) { throw new Exception("Error in uploading  : " + ex.Message); }
            finally { proxy.Close(); }
        }

        private System.Net.BITS.AuthScheme GetAuthScheme(string proxyAuthScheme)
        {
            proxyAuthScheme = proxyAuthScheme.ToLower();
            switch (proxyAuthScheme.ToLower())
            {
                case "basic": return System.Net.BITS.AuthScheme.Basic;
                case "digest": return System.Net.BITS.AuthScheme.Digest;
                case "ntlm": return System.Net.BITS.AuthScheme.NTLM;
                case "negotiate": return System.Net.BITS.AuthScheme.Negotiate;
                case "passport": return System.Net.BITS.AuthScheme.Passport;
            }
            return System.Net.BITS.AuthScheme.Negotiate;
        }

        private SecureString ConvertStringToSecureString(string value)
        {
            Char[] input = value.ToCharArray();
            SecureString SecureString = new SecureString();

            for (int idx = 0; idx < input.Length; idx++)
            {
                SecureString.AppendChar(input[idx]);
            }
            return SecureString;
        }
        #endregion

        private void _SkipSteps(int stepCount)
        {
            _isSettingValid = false;
            if (stepCount <= 1)
            {
                _SetIcon(imgServiceRequest, TestState.Warning);
                _SetMessage(lnkSvcRequest, TestState.Warning.ToString(), "Check aborted");
            }
            if (stepCount <= 2)
            {
                _SetIcon(imgFileDownload, TestState.Warning);
                _SetMessage(lnkFileDownload, TestState.Warning.ToString(), "Check aborted");
            }
            if (stepCount <= 3)
            {
                _SetIcon(imgFileUpload, TestState.Warning);
                _SetMessage(lnkFileUpload, TestState.Warning.ToString(), "Check aborted");
            }

            panelProxySettings.Invoke(() => { panelProxySettings.Visible = panelProxySettings.Enabled = true; });
            if (this.Form != null) this.Form.SetSubTitle("Provide network settings.");
        }

        private void _SetIcon(PictureBox image, TestState state)
        {
            switch (state)
            {
                case TestState.InProgress: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckPlay; }); break;
                case TestState.Success: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckOk; }); break;
                case TestState.Error: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckFail; }); break;
                case TestState.Warning: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckWait; }); break;
            }
            image.Invoke(() => { image.Visible = true; });
        }

        private void _SetMessage(LinkLabel lblMessage, string message, string details)
        {
            lblMessage.Invoke(() => { lblMessage.Text = message; ToolTipMessage.SetToolTip(lblMessage, details); });

            lblMessage.Invoke(() => { lblMessage.Visible = true; });
        }

        private void _OnUseProxyCheckedChange()
        {
            panelProxyDetails.Enabled = chkUseProxy.Checked;
            WebProxy proxy = _GetSystemProxy(SERVICE_URL);

            if (proxy != null)
                txtProxyAddress.Text = proxy.Address.OriginalString;

            _isSettingValid = false;
        }

        private void _OnUseDefaultCredentialChange()
        {
            txtUsername.Enabled = txtPassword.Enabled = !chkUseDefaultCredentials.Checked;
            _isSettingValid = false;
        }

        #endregion
    }
}

