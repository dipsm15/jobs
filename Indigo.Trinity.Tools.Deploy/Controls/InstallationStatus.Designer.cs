﻿namespace Athena.Deploy.Controls
{
    partial class InstallationStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imgOSCheck = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.ToolTipMessage = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgOSCheck)).BeginInit();
            this.SuspendLayout();
            // 
            // imgOSCheck
            // 
            this.imgOSCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgOSCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgOSCheck.Location = new System.Drawing.Point(5, 4);
            this.imgOSCheck.Margin = new System.Windows.Forms.Padding(5, 10, 5, 5);
            this.imgOSCheck.Name = "imgOSCheck";
            this.imgOSCheck.Size = new System.Drawing.Size(16, 16);
            this.imgOSCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgOSCheck.TabIndex = 6;
            this.imgOSCheck.TabStop = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(29, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 14);
            this.label1.TabIndex = 7;
            this.label1.Text = "Installation Step";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.Location = new System.Drawing.Point(241, 5);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(79, 13);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Status";
            // 
            // InstallationStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgOSCheck);
            this.Name = "InstallationStatus";
            this.Size = new System.Drawing.Size(323, 25);
            ((System.ComponentModel.ISupportInitialize)(this.imgOSCheck)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgOSCheck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ToolTip ToolTipMessage;
    }
}
