﻿namespace Athena.Deploy
{
    partial class ReportControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblLogMessage = new System.Windows.Forms.Label();
            this.lnkLogFile = new System.Windows.Forms.LinkLabel();
            this.panelLog = new System.Windows.Forms.Panel();
            this.panelSendReport = new System.Windows.Forms.FlowLayoutPanel();
            this.lblReportMessage = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelLog.SuspendLayout();
            this.panelSendReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Athena.Deploy.Properties.Resources.info;
            this.pictureBox1.Location = new System.Drawing.Point(13, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(35, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 15);
            this.label7.TabIndex = 45;
            this.label7.Text = "Installation log:";
            // 
            // lblLogMessage
            // 
            this.lblLogMessage.AutoSize = true;
            this.lblLogMessage.Location = new System.Drawing.Point(35, 31);
            this.lblLogMessage.Name = "lblLogMessage";
            this.lblLogMessage.Size = new System.Drawing.Size(230, 13);
            this.lblLogMessage.TabIndex = 46;
            this.lblLogMessage.Text = "For more details about the setup refer to log file.";
            // 
            // lnkLogFile
            // 
            this.lnkLogFile.AutoSize = true;
            this.lnkLogFile.Location = new System.Drawing.Point(276, 31);
            this.lnkLogFile.Name = "lnkLogFile";
            this.lnkLogFile.Size = new System.Drawing.Size(46, 13);
            this.lnkLogFile.TabIndex = 47;
            this.lnkLogFile.TabStop = true;
            this.lnkLogFile.Text = "view log";
            this.lnkLogFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLogFile_LinkClicked);
            // 
            // panelLog
            // 
            this.panelLog.Controls.Add(this.panelSendReport);
            this.panelLog.Controls.Add(this.lblLogMessage);
            this.panelLog.Controls.Add(this.lnkLogFile);
            this.panelLog.Controls.Add(this.pictureBox1);
            this.panelLog.Controls.Add(this.label7);
            this.panelLog.Location = new System.Drawing.Point(3, 3);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(424, 81);
            this.panelLog.TabIndex = 48;
            // 
            // panelSendReport
            // 
            this.panelSendReport.Controls.Add(this.lblReportMessage);
            this.panelSendReport.Location = new System.Drawing.Point(32, 47);
            this.panelSendReport.Name = "panelSendReport";
            this.panelSendReport.Size = new System.Drawing.Size(370, 22);
            this.panelSendReport.TabIndex = 50;
            this.panelSendReport.Visible = false;
            // 
            // lblReportMessage
            // 
            this.lblReportMessage.AutoSize = true;
            this.lblReportMessage.Location = new System.Drawing.Point(3, 0);
            this.lblReportMessage.Name = "lblReportMessage";
            this.lblReportMessage.Size = new System.Drawing.Size(195, 13);
            this.lblReportMessage.TabIndex = 48;
            this.lblReportMessage.Text = "Sending installation report to Microsoft...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "Click \'Finish\' to exit the setup.";
            // 
            // ReportControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panelLog);
            this.Name = "ReportControl";
            this.Size = new System.Drawing.Size(430, 250);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelLog.ResumeLayout(false);
            this.panelLog.PerformLayout();
            this.panelSendReport.ResumeLayout(false);
            this.panelSendReport.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblLogMessage;
        private System.Windows.Forms.LinkLabel lnkLogFile;
        private System.Windows.Forms.Panel panelLog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel panelSendReport;
        private System.Windows.Forms.Label lblReportMessage;


    }
}
