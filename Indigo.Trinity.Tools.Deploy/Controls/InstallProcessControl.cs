using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Security;
using System.Security.AccessControl;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Office.Server.Search.Administration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using Microsoft.SharePoint.Utilities;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using TaskScheduler;

namespace Athena.Deploy
{
    public partial class InstallProcessControl : InstallerControl, IDoAsync
    {
        #region Private Variables
        private static readonly TimeSpan JobTimeout = TimeSpan.FromMinutes(15);
        private Thread _installerThread = null;
        private List<Command> executeCommands;
        private List<Command> rollbackCommands;
        private int nextCommand;
        private bool requestCancel;
        private bool completed = false;
        private InstallOptions _installOptions;
        private int rollbackErrors;
        #endregion

        #region Public Constructor
        public InstallProcessControl()
        {
            InitializeComponent();
            this.Load += new EventHandler(InstallProcessControl_Load);
        }
        #endregion

        #region Event Handlers
        private void InstallProcessControl_Load(object sender, EventArgs e)
        {
            Form.PrevButton.Enabled = false;
            Form.NextButton.Enabled = false;
        }
        #endregion

        #region Protected Methods
        protected internal override void RequestCancel()
        {
            if (completed)
            {
                base.RequestCancel();
            }
            else
            {
                requestCancel = true;
                Form.AbortButton.Enabled = false;
            }

            this.Form.SetupState = InstallationState.Aborted;
            this.Form.MoveNext();
        }

        protected internal override void Open(InstallOptions options)
        {
            this.Form.SetupState = InstallationState.Installing;

            _installOptions = options;
            executeCommands = new List<Command>();
            nextCommand = 0;
            string InstallationType = Properties.Settings.Default.InstallationType;
            if (options.IsNew) executeCommands.Add(new CreateWebApplication(this));
            executeCommands.Add(new CreateMCPDatastore(this));
            executeCommands.Add(new LoadApplicationSettings(this));
            executeCommands.Add(new InstallCoreCommand(this));
            executeCommands.Add(new InstallShellCommand(this));
            if (InstallationType.ToLower() != "primary")
            {
                executeCommands.Add(new ConfigurationUpdater(this));
                executeCommands.Add(new AddingDefaultJobs(this));
            }
            if (InstallationType.ToLower() != "secondary")
            {
                executeCommands.Add(new AddingDefaultPrimaryJobs(this));
            }
            executeCommands.Add(new ScheduleAutoUpdater(this));
            rollbackCommands = new List<Command>();
            for (int i = executeCommands.Count - 1; i >= 0; i--) rollbackCommands.Add(executeCommands[i]);
            progressTotal.Maximum = executeCommands.Count;
            descriptionLabel.Text = string.Format("Step {0} / {1}: {2}", 1, executeCommands.Count, executeCommands[0].Description);
            _installerThread = new Thread(new ThreadStart(DoWorkAsync));
            _installerThread.Start();
        }

        private void command_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressTask.Invoke(() => { progressTask.Value = e.ProgressPercentage; progressTask.Refresh(); });
        }
        #endregion

        #region Command Classes
        #region Command Base Class
        /// <summary>
        /// The base class of all installation commands.
        /// </summary>
        private abstract class Command
        {
            //Used by Encoder
            const string DefaultUserName = "MCPEncoder";
            const string DefaultUserPassword = "37f21480418f4c66b13d6921a438C#*!";

            private readonly InstallProcessControl _parent;
            protected Command(InstallProcessControl parent) { this._parent = parent; }
            internal InstallProcessControl Parent { get { return _parent; } }
            internal abstract string Description { get; }

            #region Virtual Methods
            protected internal virtual bool Execute(InstallOptions options) { return true; }
            protected internal virtual bool Rollback() { return true; }
            #endregion

            public event ProgressChangedEventHandler ProgressChanged;

            #region Private Helper Methods

            public void _HostI2VidWebService(InstallOptions options) {
                try {
                    string webSiteName = "Microsoft Community Portal - (80)";
                    DirectoryEntry root = new DirectoryEntry("IIS://localhost/W3SVC");
                    string webSiteId = string.Empty;
                    foreach (DirectoryEntry de in root.Children) {
                        if (string.Equals(de.SchemaClassName, "IIsWebServer") && de.Properties["ServerComment"][0] != null &&
                           string.Equals(de.Properties["ServerComment"][0].ToString(), webSiteName))
                            webSiteId = de.Name;
                    }
                    if (string.IsNullOrEmpty(webSiteId)) {
                        Utilities.Log(string.Format("Could not find site: {0} while trying to host encoder web service.", webSiteName));
                    }
                    DirectoryEntry Parent = new DirectoryEntry("IIS://localhost/W3SVC/" + webSiteId + "/Root");
                    DirectoryEntry service_virtual_directory = Parent.Children.Add("i2Videos", "IISWebVirtualDir");

                    string path_to_service = GetIISPath() + @"\I2Videos"; 
                    if (path_to_service.EndsWith("\\")) path_to_service = path_to_service.Substring(0, path_to_service.Length - 1);

                    service_virtual_directory.Properties["Path"][0] = path_to_service;
                    service_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    service_virtual_directory.Properties["AccessExecute"][0] = false;
                    service_virtual_directory.Properties["AccessScript"][0] = 1;
                    service_virtual_directory.Properties["AccessRead"][0] = true;
                    service_virtual_directory.Properties["AccessWrite"][0] = false;
                    service_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    service_virtual_directory.Properties["AuthBasic"][0] = false;
                    service_virtual_directory.Properties["AuthNTLM"][0] = false;
                    service_virtual_directory.Properties["AppFriendlyName"][0] = "i2Videos";
                    service_virtual_directory.CommitChanges();
                    service_virtual_directory.Invoke("AppCreate", true);
                    Parent.CommitChanges();
                    SetPermission(Path.Combine(path_to_service, "App_Data"), "Users");
                    Utilities.Log("i2Videos web service successfully hosted.");


                    //Hosting Content inside qservice
                    DirectoryEntry external_virtual_directory = service_virtual_directory.Children.Add("Content", "IISWebVirtualDir");

                    //virtual_directory.Properties["AppIsolated"][0] = 2;

                    string path_to_external = options.i2VideosDataPath+@"\store\files";

                    if (path_to_external.EndsWith("\\")) {
                        path_to_external = path_to_external.Substring(0, path_to_external.Length - 1);
                    }

                    external_virtual_directory.Properties["Path"][0] = path_to_external;
                    external_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    external_virtual_directory.Properties["AccessExecute"][0] = false;
                    external_virtual_directory.Properties["AccessScript"][0] = 1;
                    external_virtual_directory.Properties["AccessRead"][0] = true;
                    external_virtual_directory.Properties["AccessWrite"][0] = false;
                    external_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    external_virtual_directory.Properties["AuthBasic"][0] = false;
                    external_virtual_directory.Properties["AuthNTLM"][0] = false;
                    external_virtual_directory.Properties["AppFriendlyName"][0] = "Content";
                    external_virtual_directory.CommitChanges();
                    service_virtual_directory.CommitChanges();
                    //external_virtual_directory.Invoke("AppCreate", 1);
                    _AddReplaceScriptMapping(external_virtual_directory);
                    SetPermissionRead(path_to_external, "Everyone");
                    //
                } catch (Exception ex) { Utilities.Log("Exception in hosting encoder web service.", ex);}
            }

            private bool _HostEncoderWebService(InstallOptions options)
            {
                try
                {
                    string webSiteName = "Microsoft Community Portal - (80)";
                    DirectoryEntry root = new DirectoryEntry("IIS://localhost/W3SVC");
                    string webSiteId = string.Empty;
                    foreach (DirectoryEntry de in root.Children)
                    {
                        if (string.Equals(de.SchemaClassName, "IIsWebServer") && de.Properties["ServerComment"][0] != null &&
                           string.Equals(de.Properties["ServerComment"][0].ToString(), webSiteName))
                            webSiteId = de.Name;
                    }
                    if (string.IsNullOrEmpty(webSiteId))
                    {
                        Utilities.Log(string.Format("Could not find site: {0} while trying to host encoder web service.", webSiteName));
                        return false;
                    }
                    DirectoryEntry Parent = new DirectoryEntry("IIS://localhost/W3SVC/" + webSiteId + "/Root");
                    DirectoryEntry service_virtual_directory = Parent.Children.Add("QService", "IISWebVirtualDir");

                    string path_to_service = options.EncoderServicePath;
                    if (path_to_service.EndsWith("\\")) path_to_service = path_to_service.Substring(0, path_to_service.Length - 1);

                    service_virtual_directory.Properties["Path"][0] = path_to_service;
                    service_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    service_virtual_directory.Properties["AccessExecute"][0] = false;
                    service_virtual_directory.Properties["AccessScript"][0] = 1;
                    service_virtual_directory.Properties["AccessRead"][0] = true;
                    service_virtual_directory.Properties["AccessWrite"][0] = false;
                    service_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    service_virtual_directory.Properties["AuthBasic"][0] = false;
                    service_virtual_directory.Properties["AuthNTLM"][0] = false;
                    service_virtual_directory.Properties["AppFriendlyName"][0] = "QService";
                    service_virtual_directory.CommitChanges();
                    service_virtual_directory.Invoke("AppCreate", true);
                    Parent.CommitChanges();
                    SetPermission(Path.Combine(path_to_service, "App_Data"), "Users");
                    Utilities.Log("Encoder web service successfully hosted.");


                    //Hosting Content inside qservice
                    DirectoryEntry external_virtual_directory = service_virtual_directory.Children.Add("Content", "IISWebVirtualDir");

                    //virtual_directory.Properties["AppIsolated"][0] = 2;

                    string path_to_external = options.DataStore;

                    if (path_to_external.EndsWith("\\")) {
                        path_to_external = path_to_external.Substring(0, path_to_external.Length - 1);
                    }

                    external_virtual_directory.Properties["Path"][0] = path_to_external;
                    external_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    external_virtual_directory.Properties["AccessExecute"][0] = false;
                    external_virtual_directory.Properties["AccessScript"][0] = 1;
                    external_virtual_directory.Properties["AccessRead"][0] = true;
                    external_virtual_directory.Properties["AccessWrite"][0] = false;
                    external_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    external_virtual_directory.Properties["AuthBasic"][0] = false;
                    external_virtual_directory.Properties["AuthNTLM"][0] = false;
                    external_virtual_directory.Properties["AppFriendlyName"][0] = "Content";
                    external_virtual_directory.CommitChanges();
                    service_virtual_directory.CommitChanges();
                    //external_virtual_directory.Invoke("AppCreate", 1);
                    _AddReplaceScriptMapping(external_virtual_directory);
                    SetPermissionRead(path_to_external, "Everyone");
                    //
                    return true;
                }
                catch (Exception ex) { Utilities.Log("Exception in hosting encoder web service.", ex); return false; }
            }
            private bool _CreateDefaultUser()
            {
                try
                {
                    DirectoryEntry directoryEntry = new DirectoryEntry("WinNT://" + Environment.MachineName + ",computer");
                    try { directoryEntry.Children.Find(DefaultUserName); }
                    catch
                    {
                        DirectoryEntry defaultUser = directoryEntry.Children.Add(DefaultUserName, "user");
                        defaultUser.Properties["FullName"].Add("MCP Encoder");
                        defaultUser.Invoke("SetPassword", new object[] { DefaultUserPassword });
                        defaultUser.CommitChanges();
                        DirectoryEntry adminGroup = directoryEntry.Children.Find("Administrators", "group");
                        adminGroup.Invoke("Add", new object[] { defaultUser.Path });
                        adminGroup.CommitChanges();
                    }
                    Utilities.Log("Default MCP Encoder user created successfully.");
                    return true;
                }
                catch (Exception ex) { Utilities.Log("Exception in Creating default user.", ex); return false; }
            }
            private bool _InstallCodec()
            {
                try
                {
                    string installerFilePath = Program.PayloadManager.GetPath("klitecodec");
                    return _RunProcess(installerFilePath, string.Empty, true);
                }
                catch (Exception ex) { Utilities.Log("Exception in installing Codec.", ex); return false; }
            }
            private bool _InstallEncoderWindowsService(InstallOptions options)
            {
                string installUtil = Program.PayloadManager.GetPath("installutil");
                string parentFolder_DataStore = Directory.GetParent(options.DataStore).ToString();
                string encoderWorker = Path.Combine(parentFolder_DataStore, @"Indigo.Trinity.Encoder.Worker\Indigo.Trinity.Encoder.Worker.exe");
                try { return _RunProcess(installUtil, encoderWorker, false); }
                catch (Exception ex) { Utilities.Log("Exception in installing Encoder Windows Service.", ex); return false; }
            }
            private bool _UnInstallEncoderWindowsService(InstallOptions options)
            {
                string installUtil = Program.PayloadManager.GetPath("installutil");
                string parentFolder_DataStore = Directory.GetParent(options.DataStore).ToString();
                string encoderFolder = Path.Combine(parentFolder_DataStore, "Indigo.Trinity.Encoder.Worker");
                string encoderWorker = Path.Combine(encoderFolder, "Indigo.Trinity.Encoder.Worker.exe") + @" /u";
                try { _RunProcess(installUtil, encoderWorker, false, false); Thread.Sleep(10000); Directory.Delete(encoderFolder, true); return true; }
                catch (Exception ex) { Utilities.Log("Exception in uninstalling Encoder Windows Service.", ex); return false; }
            }
            private bool _RunProcess(string applicationPath, string arguments, bool impersonate, bool formatPath = true)
            {
                if (formatPath)
                {
                    applicationPath = _FormatString(applicationPath);
                    arguments = _FormatString(arguments);
                }
                ProcessStartInfo processStartInfo = new ProcessStartInfo(applicationPath);
                processStartInfo.Arguments = arguments;
                if (impersonate)
                {
                    processStartInfo.UserName = DefaultUserName;
                    char[] passwordArray = DefaultUserPassword.ToCharArray();
                    SecureString securePassword = new SecureString();
                    foreach (char p in passwordArray) securePassword.AppendChar(p);
                    processStartInfo.Password = securePassword;
                    processStartInfo.Domain = System.Environment.MachineName;
                }
                processStartInfo.CreateNoWindow = true;
                processStartInfo.WorkingDirectory = Environment.CurrentDirectory;
                processStartInfo.RedirectStandardError = true;
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.UseShellExecute = false;
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                using (Process process = Process.Start(processStartInfo))
                {
                    string standardOutput = process.StandardOutput.ReadToEnd();
                    string standardError = process.StandardError.ReadToEnd();
                    process.WaitForExit(10 * 10000);

                    int exitCode = process.ExitCode;
                    if (exitCode != 0)
                    {
                        Utilities.Log(string.Format("Error in RunProcess. Arguments : {0}", arguments));
                        return false;
                    }
                }
                return true;
            }
            private string _FormatString(string toFormat)
            {
                toFormat = toFormat.Trim(' ', '\"', '\'');
                toFormat = "\"" + toFormat + "\"";
                return toFormat;
            }
            private void _AddReplaceScriptMapping(DirectoryEntry site)
            {
                string[] mappingExtensions = new string[] { ".htm", ".html", ".exe", ".zip", ".pdf", ".doc", ".docx", ".ppt", ".pptx", ".xls", ".xlsx", ".rar", ".wmv", ".hta", ".wma", ".chm", ".msi" };
                string newMapping = string.Empty;
                bool found = false;

                // Get current ScriptMaps
                object[] maps = (object[])site.InvokeGet("ScriptMaps");
                List<object> newMappingCollection = new List<object>(maps);

                foreach (object mapping in maps)
                {
                    if (((string)mapping).StartsWith(".aspx" + ","))
                    {
                        newMapping = ((string)mapping).Split(',')[1];
                        break;
                    }
                }

                foreach (string mappingExtension in mappingExtensions)
                {
                    for (int i = 0; i < newMappingCollection.Count; i++)
                    {
                        if (((string)newMappingCollection[i]).StartsWith(mappingExtension + ","))
                        {
                            newMappingCollection[i] = mappingExtension + "," + newMapping + ",5,GET,HEAD,POST,DEBUG";
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        newMappingCollection.Add(mappingExtension + "," + newMapping + ",5,GET,HEAD,POST,DEBUG");
                    else
                        found = false;
                }
                site.Properties["ScriptMaps"].Value = newMappingCollection.ToArray();
                site.CommitChanges();
                Utilities.Log("File extension mapping added for dvd virtual directory.");
            }
            private void _AddListPermissions(SPList list, string groupName, string[] permissionLevel, SPWeb web)
            {
                try
                {
                    SPRoleAssignment roleAssignment = new SPRoleAssignment(web.SiteGroups[groupName]);
                    //SPRoleAssignment roleAssignment = new SPRoleAssignment(web.EnsureUser(""));
                    //Check inheritance
                    if (!list.HasUniqueRoleAssignments)
                    {
                        list.BreakRoleInheritance(false);
                    }
                    foreach (string permission in permissionLevel)
                    {
                        SPRoleDefinition RoleDefinition = web.RoleDefinitions[permission];
                        if (!roleAssignment.RoleDefinitionBindings.Contains(RoleDefinition))
                            roleAssignment.RoleDefinitionBindings.Add(RoleDefinition);
                    }
                    list.RoleAssignments.Add(roleAssignment);
                    list.Update();
                }
                catch (Exception)
                {
                    ;
                }
            }
            private void _AddGroup(string name, SPMember owner, SPUser defaultuser, string description, SPWeb web)
            {
                try { web.SiteGroups.Add(name, owner, defaultuser, description); }
                catch (Exception) { }
            }
            #endregion

            #region Protected Helper Methods
            protected void RaiseProgressChangedEvent(int percentProgress)
            {
                if (ProgressChanged != null)
                    ProgressChanged(this, new ProgressChangedEventArgs(percentProgress, null));
            }
            protected void CreateUIWebsite(string path_to_website_root, InstallOptions options)
            {
                string appPoolName = "Microsoft Community Portal - 80";
                try
                {
                    string inetpubPath = GetIISPath();
                    try
                    {
                        //Remove the app pool if it exists
                        ServerManager serverMgr = new ServerManager();
                        var appPool = serverMgr.ApplicationPools[appPoolName];
                        if (appPool != null)
                        {
                            if (appPool.State == ObjectState.Started) appPool.Stop();
                            serverMgr.ApplicationPools.Remove(appPool);
                            serverMgr.CommitChanges();
                        }
                    }
                    catch { }
                    if (options.UISite != null)
                    {
                        ServerManager serverMgr = new ServerManager();
                        options.UISite.Stop();
                        Site _site = serverMgr.Sites[options.UISite.Name];
                        serverMgr.Sites.Remove(_site);
                        serverMgr.CommitChanges();
                        Utilities.Log(options.UISite.Name + "has been deleted.");
                    }

                    string website_name = "Microsoft Community Portal";
                    string ExportFolderName = options.MCPDataFolder + @"\Export";
                    DirectoryEntry root = new DirectoryEntry("IIS://localhost/W3SVC");
                    foreach (DirectoryEntry de in root.Children)
                    {
                        if (de.SchemaClassName == "IIsWebServer" && de.Properties["ServerComment"][0].ToString() == website_name + String.Format(" - ({0})", options.UIPortNo))
                        {
                            root.Invoke("Delete", new string[] { de.SchemaClassName, de.Name });
                            root.CommitChanges();
                            break;
                        }
                    }

                    // Find unused ID value for new web site
                    bool found_valid_site_id = false;
                    int random_site_id = 1;
                    do
                    {
                        bool regenerate_site_id = false;
                        System.Random random_generator = new Random();
                        random_site_id = random_generator.Next();

                        foreach (DirectoryEntry e in root.Children)
                        {
                            if (e.SchemaClassName == "IIsWebServer")
                            {
                                int current_site_id = Convert.ToInt32(e.Name);
                                if (current_site_id == random_site_id)
                                {
                                    regenerate_site_id = true;
                                    break;
                                }
                            }
                        }
                        found_valid_site_id = !regenerate_site_id;
                    } while (!found_valid_site_id);

                    // Create web site
                    DirectoryEntry site = (DirectoryEntry)root.Invoke("Create", "IIsWebServer", random_site_id);

                    site.Invoke("Put", "ServerComment", website_name + String.Format(" - ({0})", options.UIPortNo));

                    site.Invoke("Put", "KeyType", "IIsWebServer");

                    site.Invoke("Put", "ServerBindings", ":" + options.UIPortNo.ToString() + ":");

                    site.Invoke("Put", "ServerState", 2);

                    site.Invoke("Put", "ServerAutoStart", 1);

                    site.Invoke("Put", "ServerSize", 1);

                    site.Invoke("SetInfo");

                    //create app website directory
                    site.AuthenticationType = AuthenticationTypes.Secure;

                    // Create application virtual directory
                    DirectoryEntry virtual_directory = site.Children.Add("Root", "IISWebVirtualDir");

                    //virtual_directory.Properties["AppIsolated"][0] = 2;

                    if (path_to_website_root.EndsWith("\\"))
                    {
                        path_to_website_root = path_to_website_root.Substring(0, path_to_website_root.Length - 1);
                    }
                    //Making new App Pool
                    ServerManager mgr = new ServerManager();
                    ApplicationPool newAppPool = mgr.ApplicationPools.Add(appPoolName);
                    newAppPool.ProcessModel.IdentityType = ProcessModelIdentityType.ApplicationPoolIdentity;
                    newAppPool.ProcessModel.LoadUserProfile = true;
                    newAppPool.ManagedRuntimeVersion = "v4.0";
                    newAppPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    mgr.CommitChanges();
                    options.AppPoolName = appPoolName;
                    //Setting up virtual directory
                    virtual_directory.Properties["Path"][0] = path_to_website_root;
                    virtual_directory.Properties["EnableDirBrowsing"][0] = false;
                    virtual_directory.Properties["AccessScript"][0] = true;
                    virtual_directory.Properties["AccessExecute"][0] = false;
                    virtual_directory.Properties["AccessRead"][0] = true;
                    virtual_directory.Properties["AccessWrite"][0] = false;
                    virtual_directory.Properties["AuthAnonymous"][0] = true;
                    virtual_directory.Properties["AuthBasic"][0] = false;
                    virtual_directory.Properties["AuthNTLM"][0] = false;
                    virtual_directory.Properties["EnableDefaultDoc"][0] = true;
                    virtual_directory.Properties["DefaultDoc"][0] = "default.htm,default.aspx,default.asp";
                    virtual_directory.Properties["AspEnableParentPaths"][0] = true;
                    virtual_directory.Properties["AppFriendlyName"][0] = website_name;
                    //virtual_directory.Properties["ApplicationPool"][0] = "";                    
                    virtual_directory.Properties["AppRoot"][0] = "LM/W3SVC/" + random_site_id.ToString() + "/Root";
                    virtual_directory.Invoke("AppCreate3", new object[] { 0, options.AppPoolName, true });
                    virtual_directory.CommitChanges();
                    site.CommitChanges();
                    virtual_directory.Invoke("AppCreate", true);

                    // Create application virtual directory

                    DirectoryEntry Parent = new DirectoryEntry(@"IIS://localhost/W3SVC/" + random_site_id.ToString() + "/Root");

                    #region hosting export and import
                    DirectoryEntry exp_virtual_directory = Parent.Children.Add("Export", "IISWebVirtualDir");

                    //virtual_directory.Properties["AppIsolated"][0] = 2;

                    string path_to_exp = ExportFolderName;
                    if (path_to_exp.EndsWith("\\"))
                    {
                        path_to_exp = path_to_exp.Substring(0, path_to_exp.Length - 1);
                    }

                    exp_virtual_directory.Properties["Path"][0] = path_to_exp;
                    exp_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    exp_virtual_directory.Properties["AccessExecute"][0] = false;
                    exp_virtual_directory.Properties["AccessScript"][0] = 1;
                    exp_virtual_directory.Properties["AccessRead"][0] = true;
                    exp_virtual_directory.Properties["AccessWrite"][0] = false;
                    exp_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    exp_virtual_directory.Properties["AuthBasic"][0] = false;
                    exp_virtual_directory.Properties["AuthNTLM"][0] = false;
                    exp_virtual_directory.Properties["AppFriendlyName"][0] = "Export";
                    exp_virtual_directory.CommitChanges();
                    site.CommitChanges();

                    DirectoryEntry imp_virtual_directory = Parent.Children.Add("Import", "IISWebVirtualDir");

                    //virtual_directory.Properties["AppIsolated"][0] = 2;

                    string path_to_imp = options.MCPDataFolder + @"\Import"; ;
                    if (path_to_imp.EndsWith("\\"))
                    {
                        path_to_imp = path_to_imp.Substring(0, path_to_imp.Length - 1);
                    }

                    imp_virtual_directory.Properties["Path"][0] = path_to_imp;
                    imp_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    imp_virtual_directory.Properties["AccessExecute"][0] = false;
                    imp_virtual_directory.Properties["AccessScript"][0] = 1;
                    imp_virtual_directory.Properties["AccessRead"][0] = true;
                    imp_virtual_directory.Properties["AccessWrite"][0] = false;
                    imp_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    imp_virtual_directory.Properties["AuthBasic"][0] = false;
                    imp_virtual_directory.Properties["AuthNTLM"][0] = false;
                    imp_virtual_directory.Properties["AppFriendlyName"][0] = "Export";
                    imp_virtual_directory.CommitChanges();
                    site.CommitChanges();
                    #endregion

                    #region hosting services
                    #region Hosting Client Service
                    DirectoryEntry service_virtual_directory = Parent.Children.Add("Core", "IISWebVirtualDir");
                    string path_to_service = options.ServicePath;
                    if (path_to_service.EndsWith("\\"))
                    {
                        path_to_service = path_to_service.Substring(0, path_to_service.Length - 1);
                    }

                    service_virtual_directory.Properties["Path"][0] = path_to_service;
                    service_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    service_virtual_directory.Properties["AccessExecute"][0] = false;
                    service_virtual_directory.Properties["AccessScript"][0] = 1;
                    service_virtual_directory.Properties["AccessRead"][0] = true;
                    service_virtual_directory.Properties["AccessWrite"][0] = false;
                    service_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    service_virtual_directory.Properties["AuthBasic"][0] = false;
                    service_virtual_directory.Properties["AuthNTLM"][0] = false;
                    service_virtual_directory.Properties["AppFriendlyName"][0] = "Core";
                    service_virtual_directory.CommitChanges();
                    site.CommitChanges();
                    service_virtual_directory.Invoke("AppCreate", 1);
                    DirectoryEntry entryNew = new DirectoryEntry(@"IIS://localhost/W3SVC/" + random_site_id.ToString() + "/Root/Core");
                    DirectoryEntry entry = service_virtual_directory.Children.Add("Accounts", "IISWebVirtualDir");
                    entry.Properties["Path"][0] = Path.Combine(inetpubPath, @"Indigo.Trinity.Core\Accounts");
                    entry.Properties["AuthNTLM"][0] = false;
                    entry.Properties["AuthAnonymous"][0] = true;
                    entry.CommitChanges();
                    //entry.Invoke("AppCreate", 1);
                    entryNew = new DirectoryEntry(@"IIS://localhost/W3SVC/" + random_site_id.ToString() + "/Root/Core/Accounts");
                    entry = entryNew.Children.Add("ADS", "IISWebVirtualDir");
                    entry.Properties["Path"][0] = Path.Combine(inetpubPath, @"Indigo.Trinity.Core\Accounts\ADS");
                    entry.Properties["AuthNTLM"][0] = true;
                    entry.Properties["AuthAnonymous"][0] = false;
                    entry.CommitChanges();
                    //entry.Invoke("AppCreate", 1);
                    site.CommitChanges();
                    _AddReplaceScriptMapping(service_virtual_directory);
                    SetPermissionRead(path_to_service, "Everyone");
                    //Editing config file
                    string webconfigFile = Path.Combine(inetpubPath, @"Indigo.Trinity.Studio\web.config");
                    Utilities.Log("Web Config file path: " + webconfigFile);
                    string searchText = "#ServiceUrl#";
                    if (File.Exists(webconfigFile))
                    {
                        ReplaceInFile(webconfigFile, searchText, options.UISiteUrl + "/Core");
                        searchText = "#UploadUrl#";
                        ReplaceInFile(webconfigFile, searchText, "http://"+Environment.MachineName+"/QService/Upload.ashx");
                        searchText = "#EncoderUrl#";
                        ReplaceInFile(webconfigFile, searchText, "http://" + Environment.MachineName + "/QService/QueueService.svc/web");
                        searchText = "#i2videosUrl#";
                        ReplaceInFile(webconfigFile, searchText, "http://" + Environment.MachineName + "/i2videos");
                    }
                    else
                    {
                        Utilities.Log("MCP UI web config file doesnt exist. Please change SiteUrl manually.");
                    }

                    //changing i2videos config
                    webconfigFile = Path.Combine(inetpubPath, @"i2videos\web.config");
                    searchText = "#DataStore#";
                    ReplaceInFile(webconfigFile, searchText, options.i2VideosDataPath);
                    searchText = "#ContentUrl#";
                    ReplaceInFile(webconfigFile, searchText, "http://" + Environment.MachineName + "/i2videos/content");
                    searchText = "#EncoderUrl#";
                    ReplaceInFile(webconfigFile, searchText, "http://" + Environment.MachineName + "/QService/QueueService.svc/web");
                    #endregion
                    #endregion

                    #region Hosting I2Videos Service

                    #endregion

                    DirectoryEntry external_virtual_directory = service_virtual_directory.Children.Add("Content", "IISWebVirtualDir");

                    //virtual_directory.Properties["AppIsolated"][0] = 2;

                    string path_to_external = options.DataStore;

                    if (path_to_external.EndsWith("\\"))
                    {
                        path_to_external = path_to_external.Substring(0, path_to_external.Length - 1);
                    }

                    external_virtual_directory.Properties["Path"][0] = path_to_external;
                    external_virtual_directory.Properties["EnableDirBrowsing"][0] = true;
                    external_virtual_directory.Properties["AccessExecute"][0] = false;
                    external_virtual_directory.Properties["AccessScript"][0] = 1;
                    external_virtual_directory.Properties["AccessRead"][0] = true;
                    external_virtual_directory.Properties["AccessWrite"][0] = false;
                    external_virtual_directory.Properties["AuthAnonymous"][0] = true;
                    external_virtual_directory.Properties["AuthBasic"][0] = false;
                    external_virtual_directory.Properties["AuthNTLM"][0] = false;
                    external_virtual_directory.Properties["AppFriendlyName"][0] = "Content";
                    external_virtual_directory.CommitChanges();
                    site.CommitChanges();
                    //external_virtual_directory.Invoke("AppCreate", 1);
                    _AddReplaceScriptMapping(external_virtual_directory);
                    SetPermissionRead(path_to_external, "Everyone");
                    Utilities.Log("Virtual Directory created successfully at port no : " + options.UIPortNo.ToString());


                }
                catch (Exception ex)
                {
                    Utilities.Log("An error occurred while trying to create website. ", ex);
                }
            }
            protected void DeployEncoder(InstallOptions options)
            {
                try
                {
                    if (!_HostEncoderWebService(options)) return;
                    if (!_CreateDefaultUser()) return;
                    if (!_InstallCodec()) return;
                    Utilities.Log("K Lite Codec successfully installed.");
                    if (!_InstallEncoderWindowsService(options)) return;
                    Utilities.Log("Encoder worker service successfully installed.");
                }
                catch (Exception ex) { Utilities.Log("Error occurred in deploying MCP Encoder.", ex); }
            }
            protected bool UnInstallEncoder(InstallOptions options) { return _UnInstallEncoderWindowsService(options); }
            protected void CopyConfigFile(string siteUrl)
            {
                try
                {
                    SPSite spSite = new SPSite(siteUrl);
                    SPWeb spWeb = spSite.OpenWeb();
                    string path = spWeb.Site.WebApplication.GetIisSettingsWithFallback(SPUrlZone.Default).Path.ToString();
                    string copyPath = new FileInfo(System.Windows.Forms.Application.ExecutablePath).DirectoryName + @"\" + "web.config";
                    if (!File.Exists(copyPath))
                    {
                        copyPath = "web.config";
                        if (!File.Exists(copyPath))
                            throw new FileNotFoundException("web.config not found");
                    }
                    if (File.Exists(path + @"\" + "backupweb.config"))
                    {
                        File.SetAttributes(path + @"\" + "backupweb.config", FileAttributes.Normal);
                        File.Delete(path + @"\" + "backupweb.config");
                    }
                    if (File.Exists(path + @"\" + "web.config"))
                        File.Move(path + @"\" + "web.config", path + @"\" + "backupweb.config");
                    File.Copy(copyPath, path + @"\" + "web.config", true);
                    Utilities.Log("Web.config copied to virtual directory " + path);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error copying web.config ", ex);
                    throw ex;
                }
            }
            protected void SetPartner(string url, string partner)
            {
                try
                {
                    SPWeb web = new SPSite(url).OpenWeb();
                    SPList partnerList = web.Lists["Partners"];
                    if (partnerList.ItemCount > 0)
                    {
                        SPListItem listitem = partnerList.Items[0];
                        listitem["Title"] = partner;
                        listitem.Update();
                    }
                    else
                    {
                        SPListItem newItem = partnerList.Items.Add();
                        newItem["Title"] = partner;
                        newItem["Partner Description"] = partner;
                        newItem["Partner Manager"] = "MCP Support (Indigo)";
                        newItem["Partner Admin"] = "MCP Support (Indigo)";
                        newItem["Partner Support"] = "MCP Support (Indigo)";
                        newItem["Service Url"] = "N/A";
                        if (File.Exists("logo-MCP-Logo.png"))
                            newItem.Attachments.Add("logo-MCP-Logo.png", File.ReadAllBytes("logo-MCP-Logo.png"));
                        else
                            Utilities.Log("logo-MCP-Logo.png cannot be found.");

                        if (File.Exists("background-bg-header.jpg"))
                            newItem.Attachments.Add("background-bg-header.jpg", File.ReadAllBytes("background-bg-header.jpg"));
                        else
                            Utilities.Log("background-bg-header.jpg cannot be found.");

                        newItem.Update();
                    }
                    Utilities.Log(partner + " value added to Partner List");

                }
                catch (Exception ex)
                {
                    Utilities.Log(partner + " cannot be added ", ex);
                }
            }
            protected void AddConfigurationKey(string url, string key, string value)
            {
                try
                {
                    SPWeb web = new SPSite(url).OpenWeb();
                    SPList configList = web.Lists["Config"];
                    SPListItemCollection listitems = configList.Items;
                    bool exist = false;
                    foreach (SPListItem item in listitems)
                    {
                        if (item["Title"].ToString() == key)
                        {
                            item["Value"] = value;
                            item.Update();
                            exist = true;
                        }
                    }
                    if (exist == false)
                    {
                        SPListItem newItem = listitems.Add();
                        newItem["Title"] = key;
                        newItem["Value"] = value;
                        newItem["Description"] = string.Empty;
                        newItem.Update();
                    }
                    Utilities.Log(key + " value added to config List");
                }
                catch (Exception ex)
                {
                    Utilities.Log(key + " cannot be added ", ex);
                }
            }
            protected string GetAlertMailTemplate()
            {
                string path = @"AlertMail.xslt";
                if (File.Exists(path))
                    return File.ReadAllText(path);

                path = Path.Combine(new FileInfo(System.Windows.Forms.Application.ExecutablePath).DirectoryName, "AlertMail.xslt");
                if (File.Exists(path))
                    return File.ReadAllText(path);

                Utilities.Log("Cannot find AlertMail.xslt. Please check if it exists in current directory");
                throw new FileNotFoundException("Cannot find AlertMail.xslt. Please check if it exists in current directory");
            }
            protected void SetMimeTypeProperty(string newExtension, string newMimeType)
            {
                string metabasePath = "IIS://" + Environment.MachineName + "/MimeMap";
                try
                {
                    DirectoryEntry path = new DirectoryEntry(metabasePath);
                    PropertyValueCollection propValues = path.Properties["MimeMap"];

                    object exists = null;
                    foreach (object value in propValues)
                    {
                        // IISOle requires a reference to the Active DS IIS Namespace Provider in Visual Studio .NET
                        IISOle.IISMimeType mimetypeObj = (IISOle.IISMimeType)value;
                        if (newExtension == mimetypeObj.Extension)
                            exists = value;
                    }
                    if (null != exists)
                        propValues.Remove(exists);


                    IISOle.MimeMapClass newObj = new IISOle.MimeMapClass();
                    newObj.Extension = newExtension;
                    newObj.MimeType = newMimeType;
                    propValues.Add(newObj);
                    path.CommitChanges();
                    //LOG
                    Utilities.Log("MimeTypeProperty has been added for " + newExtension);
                }
                catch (Exception ex)
                {
                    if ("HRESULT 0x80005006" == ex.Message)
                        Utilities.Log("Property MimeMap does not exist at " + metabasePath, ex);
                    else
                        Utilities.Log("Failed in SetMimeTypeProperty with the following exception: \n" + ex.Message, ex);
                }
            }
            protected void SetRegistryRoot(string key, string name, object value)
            {
                Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(key, true);

                if (registryKey == null)
                    registryKey = Microsoft.Win32.Registry.ClassesRoot.CreateSubKey(key, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);

                registryKey.SetValue(name, value);
                Utilities.Log("Registry Entry added for " + key + " in registry Root");

            }
            protected void SetRegistryLocalMachine(string key, string name, object value)
            {
                Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, true);

                if (registryKey == null)
                    registryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);

                registryKey.SetValue(name, value);
                Utilities.Log("Registry Entry added for " + key + " in registry LocalMachine");
            }
            protected void InstallGac(string dll)
            {

                if (!File.Exists(dll))
                {
                    Utilities.Log(dll + " not found in current directory");
                    throw new Exception(dll + " not found in current directory");
                }
                string gacPath = new FileInfo(System.Windows.Forms.Application.ExecutablePath).DirectoryName + @"\" + "gacutil.exe";
                if (!File.Exists(gacPath))
                {
                    gacPath = "gacutil.exe";
                    if (!File.Exists(gacPath))
                        throw new FileNotFoundException("gacutil.exe not found");
                }
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo(gacPath);
                startInfo.Arguments = "/i " + dll;
                startInfo.CreateNoWindow = true;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.UseShellExecute = false;
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //startInfo.WorkingDirectory = Path.GetDirectoryName(executablePath);

                using (System.Diagnostics.Process p = System.Diagnostics.Process.Start(startInfo))
                {
                    string standardOutput = p.StandardOutput.ReadToEnd();
                    string errorOutput = p.StandardError.ReadToEnd();
                    p.WaitForExit(10 * 10000);

                    int exitCode = p.ExitCode;
                    if (exitCode != 0)
                    {
                        Utilities.Log(dll + " cannot be added to GAC.");
                        throw new Exception(dll + "cannot be added to GAC.");
                    }
                }
                //LOG
                Utilities.Log(dll + " has been added to GAC");

            }
            protected void RestartService(string name)
            {
                ServiceController controller = new ServiceController();
                controller.MachineName = ".";
                controller.ServiceName = name;
                try
                {
                    if (controller.Status.ToString().ToLower() == "running")
                        controller.Stop();
                    controller.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 10));
                    controller.Refresh();
                    if (controller.Status.ToString().ToLower() == "stopped")
                        controller.Start();
                    Utilities.Log("Service " + name + " has been restarted");
                }
                catch { ;}   //Supressing the error so it does not affect the Setup execution
            }
            protected bool SiteExists(string siteUrl)
            {
                SPSite spSite = new SPSite(siteUrl);
                SPWeb spWeb = spSite.OpenWeb();
                bool exists = spWeb.Url.Equals(siteUrl, StringComparison.InvariantCultureIgnoreCase) ? true : false;
                spWeb.Dispose();
                spSite.Dispose();
                return exists;
            }
            protected void SetPermission(string path, string userName)
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                DirectorySecurity sd = Directory.GetAccessControl(path);
                FileSystemAccessRule ace = new FileSystemAccessRule(
                                                  new NTAccount(userName),
                                                  FileSystemRights.Modify,
                                                  AccessControlType.Allow);
                sd.AddAccessRule(ace);
                FileSystemAccessRule addInheritance = new FileSystemAccessRule(new NTAccount(userName), FileSystemRights.Modify,
                                                                                InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                                                                                 PropagationFlags.InheritOnly, AccessControlType.Allow);
                bool result;
                sd.ModifyAccessRule(AccessControlModification.Add, addInheritance, out result);
                Directory.SetAccessControl(path, sd);
                Utilities.Log("Permissions set for " + path + " for user " + userName);
            }
            protected void SetPermissionRead(string path, string userName)
            {

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                DirectorySecurity sd = Directory.GetAccessControl(path);
                FileSystemAccessRule ace = new FileSystemAccessRule(
                                                  new NTAccount(userName),
                                                  FileSystemRights.Read,
                                                  AccessControlType.Allow);
                sd.AddAccessRule(ace);
                Directory.SetAccessControl(path, sd);
            }
            protected void SetRolesAndGroupsSecondary(string url)
            {
                SPWeb web = new SPSite(url).OpenWeb();
                web.RoleDefinitions.BreakInheritance(false, false);
                SPRoleDefinition newRole = new SPRoleDefinition();
                newRole.Name = "Add";  //Can add and edit but cannot delete
                newRole.Description = "User can add items to list but cannot Delete it";
                newRole.BasePermissions = SPBasePermissions.AddListItems | SPBasePermissions.ViewListItems | SPBasePermissions.EditListItems |
                                            SPBasePermissions.CreateAlerts | SPBasePermissions.BrowseDirectories | SPBasePermissions.ViewPages |
                                            SPBasePermissions.BrowseUserInfo | SPBasePermissions.OpenItems | SPBasePermissions.UseClientIntegration |
                                            SPBasePermissions.Open | SPBasePermissions.EditMyUserInfo | SPBasePermissions.ViewFormPages;
                web.RoleDefinitions.Add(newRole);

                newRole = new SPRoleDefinition();
                newRole.Name = "Edit";//Can edit but cannot add 
                newRole.Description = "User can edit items to list but cannot Add or Delete it";
                newRole.BasePermissions = SPBasePermissions.ViewListItems | SPBasePermissions.EditListItems | SPBasePermissions.CreateAlerts |
                                            SPBasePermissions.BrowseDirectories | SPBasePermissions.ViewPages | SPBasePermissions.BrowseUserInfo |
                                            SPBasePermissions.OpenItems | SPBasePermissions.UseClientIntegration | SPBasePermissions.Open |
                                            SPBasePermissions.EditMyUserInfo | SPBasePermissions.ViewFormPages;
                web.RoleDefinitions.Add(newRole);
                //LOG
                Utilities.Log("Role's Add and Edit added to the site.");

                ///Adding Groups
                _AddGroup("Partner Admin", web.CurrentUser, null, "Partner Admin", web);
                SPGroup newGroup = web.SiteGroups["Partner Admin"];        //Note: This is done to make administrator the owner of administrator
                newGroup.Owner = web.SiteGroups["Partner Admin"];
                newGroup.Update();
                SPRoleAssignment roleAssignment = new SPRoleAssignment(web.SiteGroups["Partner Admin"]);
                roleAssignment.RoleDefinitionBindings.Add(web.RoleDefinitions["Edit"]);
                web.RoleAssignments.Add(roleAssignment);
                web.Update();

                _AddGroup("Viewer", web.SiteGroups["Partner Admin"], null, "Can view content and add rating,feedback", web);
                roleAssignment = new SPRoleAssignment(web.SiteGroups["Viewer"]);
                roleAssignment.RoleDefinitionBindings.Add(web.RoleDefinitions["Read"]);
                web.RoleAssignments.Add(roleAssignment);
                web.Update();

                Utilities.Log("Groups Partner Admin and Viewer added to site");

            }
            protected void AddPermissionsToAllListSecondary(string url)
            {
                string[] listValues = { "Blogs", "Categories", "Config", "DVD", "EDM", "Packages", "Forums", "Hands on Labs", "Jobs", "News", "Partners", "Podcasts", "Recall", "Contacts", "User Content Rating", "User Feedback", "User Preferences", "Webcasts", "Content Activity", "Page Visit Activity", "Search Activity", "Blog Feeds", "Other Content", "Useful Links", "Events", "Quizzes", "Quiz Answers", "Quiz Questions", "Quiz Status", "Alert Templates" };
                string _fullControl = "Full Control", _read = "Read", _contribute = "Contribute", _add = "Add", _edit = "Edit";
                SPWeb web = new SPSite(url).OpenWeb();
                foreach (string listName in listValues)
                {
                    try
                    {
                        SPList list;
                        list = web.Lists[listName];
                        Utilities.Log("Adding Permissions for " + list.Title);
                        switch (list.Title)
                        {


                            case "Webcasts":
                            case "Hands on Labs":
                            case "Podcasts":
                            case "Useful Links":
                            case "Other Content":
                                _AddListPermissions(list, "Partner Admin", new string[] { _edit }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read, _edit }, web);
                                break;
                            case "EDM":
                            case "Tags":
                            case "Blog Feeds":
                            case "Blogs":
                            case "Categories":
                            case "Events":
                            case "Quizzes":
                            case "Quiz Questions":
                                _AddListPermissions(list, "Partner Admin", new string[] { _read }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "DVD":
                            case "News":
                            case "Forums":
                            case "Config":
                            case "Partners":
                                _AddListPermissions(list, "Partner Admin", new string[] { _edit }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "Contacts":
                                _AddListPermissions(list, "Partner Admin", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "Jobs":
                                _AddListPermissions(list, "Partner Admin", new string[] { _contribute }, web);
                                break;
                            case "Quiz Answers":
                            case "Quiz Status":
                                _AddListPermissions(list, "Partner Admin", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _contribute }, web);
                                break;
                            case "Packages":
                                _AddListPermissions(list, "Partner Admin", new string[] { _read }, web);
                                break;

                            case "Recall":
                                _AddListPermissions(list, "Partner Admin", new string[] { _read }, web);
                                break;
                            case "User Preferences":
                            case "User Content Rating":
                            case "User Feedback":
                            case "Content Activity":
                            case "Page Visit Activity":
                            case "Search Activity":
                                _AddListPermissions(list, "Partner Admin", new string[] { _add }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _add }, web);
                                break;
                            case "Alert Templates":
                                _AddListPermissions(list, "Partner Admin", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            default:
                                Console.WriteLine("Please check the List String Array");
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utilities.Log("Couldnt add permission to list" + listName, ex);
                    }
                }
            }
            protected void SetRolesAndGroupsPrimary(string url)
            {
                SPWeb web = new SPSite(url).OpenWeb();
                web.RoleDefinitions.BreakInheritance(false, false);
                SPRoleDefinition newRole = new SPRoleDefinition();
                newRole.Name = "Add";  //Can add and edit but cannot delete
                newRole.Description = "User can add items to list but cannot Delete it";
                newRole.BasePermissions = SPBasePermissions.AddListItems | SPBasePermissions.ViewListItems | SPBasePermissions.EditListItems |
                                            SPBasePermissions.CreateAlerts | SPBasePermissions.BrowseDirectories | SPBasePermissions.ViewPages |
                                            SPBasePermissions.BrowseUserInfo | SPBasePermissions.OpenItems | SPBasePermissions.UseClientIntegration |
                                            SPBasePermissions.Open | SPBasePermissions.EditMyUserInfo | SPBasePermissions.ViewFormPages;
                web.RoleDefinitions.Add(newRole);

                newRole = new SPRoleDefinition();
                newRole.Name = "Edit";//Can edit but cannot add 
                newRole.Description = "User can edit items to list but cannot Add or Delete it";
                newRole.BasePermissions = SPBasePermissions.ViewListItems | SPBasePermissions.EditListItems | SPBasePermissions.CreateAlerts |
                                            SPBasePermissions.BrowseDirectories | SPBasePermissions.ViewPages | SPBasePermissions.BrowseUserInfo |
                                            SPBasePermissions.OpenItems | SPBasePermissions.UseClientIntegration | SPBasePermissions.Open |
                                            SPBasePermissions.EditMyUserInfo | SPBasePermissions.ViewFormPages;
                web.RoleDefinitions.Add(newRole);
                //LOG
                Utilities.Log("Role's Add and Edit added to the site.");

                ///Adding Groups
                _AddGroup("Administrator", web.CurrentUser, null, "Has Full Control", web);
                SPGroup newGroup = web.SiteGroups["Administrator"];        //Note: This is done to make administrator the owner of administrator
                newGroup.Owner = web.SiteGroups["Administrator"];
                newGroup.Update();

                SPRoleAssignment roleAssignment = new SPRoleAssignment(web.SiteGroups["Administrator"]);
                roleAssignment.RoleDefinitionBindings.Add(web.RoleDefinitions["Full Control"]);
                web.RoleAssignments.Add(roleAssignment);
                web.Update();

                _AddGroup("Partner Manager", web.SiteGroups["Administrator"], null, "Can add EDM,category,DVD and content", web);
                roleAssignment = new SPRoleAssignment(web.SiteGroups["Partner Manager"]);
                roleAssignment.RoleDefinitionBindings.Add(web.RoleDefinitions["Contribute"]);
                web.RoleAssignments.Add(roleAssignment);
                web.Update();

                _AddGroup("Content Manager", web.SiteGroups["Administrator"], null, "Can add category and Tags", web);
                roleAssignment = new SPRoleAssignment(web.SiteGroups["Content Manager"]);
                roleAssignment.RoleDefinitionBindings.Add(web.RoleDefinitions["Read"]);
                web.RoleAssignments.Add(roleAssignment);
                web.Update();

                _AddGroup("Viewer", web.SiteGroups["Administrator"], null, "Can view content and add rating,feedback", web);
                roleAssignment = new SPRoleAssignment(web.SiteGroups["Viewer"]);
                roleAssignment.RoleDefinitionBindings.Add(web.RoleDefinitions["Read"]);
                web.RoleAssignments.Add(roleAssignment);
                web.Update();

                Utilities.Log("Groups Administrator, Content Manager, Partner Manager and Viewer added to site");

            }
            protected void AddPermissionsToAllListPrimary(string url)
            {
                SPWeb web = new SPSite(url).OpenWeb();
                string[] listValues = { "Blogs", "Categories", "Config", "DVD", "EDM", "Packages", "Forums", "Hands on Labs", "Jobs", "News", "Partners", "Podcasts", "Recall", "Contacts", "User Content Rating", "User Feedback", "User Preferences", "Webcasts", "Content Activity", "Page Visit Activity", "Search Activity", "Blog Feeds", "Other Content", "Useful Links", "Upload Log", "Service Log", "Auto Updater Log", "Events", "Quizzes", "Quiz Answers", "Quiz Questions", "Quiz Status", "Job Log", "Content Reports", "Alert Templates", "Content Status" };
                string _fullControl = "Full Control", _read = "Read", _contribute = "Contribute", _add = "Add", _edit = "Edit";
                foreach (string listName in listValues)
                {
                    try
                    {
                        SPList list;
                        list = web.Lists[listName];
                        Utilities.Log("Adding Permissions for " + list.Title);
                        switch (list.Title)
                        {

                            case "Webcasts":
                            case "News":
                            case "Hands on Labs":
                            case "Podcasts":
                            case "Useful Links":
                            case "Other Content":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _edit }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _edit }, web);
                                break;
                            case "Categories":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _add }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "Blog Feeds":
                            case "Blogs":
                            case "Forums":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _edit }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "DVD":
                            case "EDM":
                            case "Tags":
                            case "Contacts":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _read }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "Jobs":
                            case "Packages":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _read }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _read }, web);
                                break;
                            case "Config":
                            case "Partners":
                            case "Content Status":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _read }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _read }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "Recall":
                            case "Upload Log":
                            case "Service Log":
                            case "Auto Updater Log":
                            case "Job Log":
                            case "Content Reports":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _read }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                break;
                            case "User Preferences":
                            case "User Content Rating":
                            case "User Feedback":
                            case "Content Activity":
                            case "Page Visit Activity":
                            case "Search Activity":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _add }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _add }, web);
                                break;
                            case "Events":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _contribute }, web);
                                break;
                            case "Quiz Answers":
                            case "Quiz Status":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _contribute }, web);
                                break;
                            case "Quiz Questions":
                            case "Quizzes":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _contribute }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            case "Alert Templates":
                                _AddListPermissions(list, "Administrator", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Content Manager", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Partner Manager", new string[] { _fullControl }, web);
                                _AddListPermissions(list, "Viewer", new string[] { _read }, web);
                                break;
                            default:
                                Console.WriteLine("Please check the List String Array");
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Utilities.Log("Couldnt add permission to list" + listName, ex);
                    }
                }
            }
            protected void AddAllAuthenticatedUsers(string url)
            {
                SPWeb web = new SPSite(url).OpenWeb();
                try
                {
                    SPGroupCollection groups = web.Groups;
                    string _user = SPUtility.GetAllAuthenticatedUsers(web);
                    SPUser user = web.EnsureUser(_user);

                    foreach (SPGroup group in groups)
                    {
                        if (group.Name.ToLower() == "viewer")
                        {
                            group.AddUser(user);
                            web.Update();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utilities.Log("Couldnt add all authenticated users to site" + url, ex);
                }

            }
            protected void CopyWebsite(string destDir, InstallOptions options)
            {
                //string sourceDir = Path.GetFullPath(Properties.Settings.Default.MCPUIDirectoryName);
                string sourceDir = Program.PayloadManager.GetPath("webui");

                if (Directory.Exists(sourceDir))
                {
                    CopyDir(sourceDir, destDir);
                }
                else
                {
                    Utilities.Log("MCP UI directory doesnt exist");
                    throw new Exception("MCP UI Directory not found");
                }
            }
            protected void CopyServices(string destDir, InstallOptions options)
            {
                string sourceDir = Program.PayloadManager.GetPath("services");
                options.ServicePath = destDir;
                if (Directory.Exists(sourceDir))
                {
                    CopyDir(sourceDir, destDir);
                    string webconfigFile = destDir + @"\web.config";
                    Utilities.Log("Web Config file path: " + webconfigFile);
                    if (File.Exists(webconfigFile))
                    {
                        string searchText = "#DataStore#";
                        ReplaceInFile(webconfigFile, searchText, options.DataStore);
                        searchText = "#AdminEmail#";
                        ReplaceInFile(webconfigFile, searchText, options.AdminEmail);
                        searchText = "#EncoderService#";
                        ReplaceInFile(webconfigFile, searchText, "http://" + Environment.MachineName + "/QService/QueueService.svc/ws");
                        searchText = "#CDN#";
                        ReplaceInFile(webconfigFile, searchText, "http://"+Environment.MachineName+"/QService/ContentService.svc/ws");
                    }
                    else
                    {
                        Utilities.Log("MCP service config file doesnt exist. Please change DataStore manually.");
                    }
                }
                else
                {
                    Utilities.Log("MCP Services directory doesnt exist");
                    throw new Exception("MCP Services Directory not found");
                }
            }
            protected void CopyEncoderServices(string destDir, InstallOptions options)
            {
                string sourceDir = Program.PayloadManager.GetPath("encoderservices");
                options.EncoderServicePath = destDir;
                if (Directory.Exists(sourceDir))
                {
                    CopyDir(sourceDir, destDir);
                    string webconfigFile = destDir + @"\web.config";
                    Utilities.Log("Web Config file path: " + webconfigFile);
                    if (File.Exists(webconfigFile))
                    {
                        string searchText = "#ContentFolderPath#";
                        string contentPath = Path.Combine(options.DataStore, @"store");
                         
                        string.Format("BaseUrl=http://{0}; DataStore={1};", Environment.MachineName, contentPath);
                        ReplaceInFile(webconfigFile, searchText, contentPath);

                        searchText = "#MachineName#";
                        ReplaceInFile(webconfigFile, searchText, Environment.MachineName);
                    }
                    else { Utilities.Log("MCP encoder service config file doesnt exist. Please change 'ContentFolderPath' manually."); }
                }
                else
                {
                    Utilities.Log("MCP Encoder Services directory doesnt exist");
                    throw new Exception("MCP Encoder Services Directory not found");
                }
            }
            protected void EditEcoderConfig(string destDir, InstallOptions options)
            {
                string webconfigFile = destDir + @"\Indigo.Trinity.Encoder.exe.config";
                Utilities.Log("Web Config file path: " + webconfigFile);
                if (File.Exists(webconfigFile))
                {
                    string searchText = "#MachineName#";
                    ReplaceInFile(webconfigFile, searchText, options.SiteUrl + @"/_vti_bin/Lists.asmx");
                    searchText = "#LogFilePath#";
                    string deploymentFolder = Path.Combine(options.MCPFolder, @"MCP\MediaEncoder");
                    ReplaceInFile(webconfigFile, searchText, deploymentFolder + @"\Indigo.Trinity.Encoder.log.txt");
                }
                else
                {
                    Utilities.Log("MCP service config file doesnt exist. Please change DataStore manually.");
                }
            }
            protected void CopySilverlight(string destDir, InstallOptions options)
            {
                string sourceDir = Program.PayloadManager.GetPath("silverlight");

                if (Directory.Exists(sourceDir))
                {
                    Directory.CreateDirectory(destDir + @"\Silverlight");
                    if (!Directory.Exists(destDir)) Directory.CreateDirectory(destDir);
                    DirectoryInfo directory = new DirectoryInfo(sourceDir);
                    FileInfo[] files = directory.GetFiles();
                    foreach (FileInfo f in files)
                    {
                        f.Attributes = FileAttributes.Normal;
                        if (File.Exists((destDir + @"\" + f.Name)))
                            File.SetAttributes(destDir + @"\" + f.Name, FileAttributes.Normal);
                        f.CopyTo(destDir + @"\Silverlight" + @"\" + f.Name, true);
                    }

                }
                else
                {
                    Utilities.Log("Silverlight directory doesnt exist");
                    throw new Exception("Silverlight Directory not found");
                }
            }
            protected void EditAdminCreator(InstallOptions options)
            {
                string sourceDir = Program.PayloadManager.GetPath("admincreator");
                if (Directory.Exists(sourceDir))
                {
                    string webconfigFile = sourceDir + @"\Indigo.Trinity.Setup.exe.config";
                    Utilities.Log("Config file path: " + webconfigFile);
                    if (File.Exists(webconfigFile))
                    {
                        string searchText = "#DataStore#";
                        ReplaceInFile(webconfigFile, searchText, options.DataStore);
                    }
                    else
                    {
                        Utilities.Log("MCP service config file doesnt exist. Please change DataStore manually.");
                    }
                }
                else
                {
                    Utilities.Log("MCP Services directory doesnt exist");
                    throw new Exception("MCP Services Directory not found");
                }
            }
            protected void SetupAdminCreator(InstallOptions options)
            {
                string sourceDir = Program.PayloadManager.GetPath("admincreator");
                if (Directory.Exists(sourceDir))
                {
                    string webconfigFile = sourceDir + @"\web.config";
                    if (File.Exists(webconfigFile))
                    {
                        string searchText = "#DataStore#";
                        ReplaceInFile(webconfigFile, searchText, options.DataStore);
                    }
                    else
                    {
                        Utilities.Log("Admin Creator config file doesnt exist. Please change DataStore manually.");
                    }
                }
                else
                {
                    Utilities.Log("Admin Creator directory doesnt exist");
                    throw new Exception("Admin Creator Directory not found");
                }
            }
            protected void ReplaceInFile(string filePath, string searchText, string replaceText)
            {
                StreamReader reader = new StreamReader(filePath);
                string content = reader.ReadToEnd();
                reader.Close();

                content = Regex.Replace(content, searchText, replaceText);

                StreamWriter writer = new StreamWriter(filePath);
                writer.Write(content);
                writer.Close();
            }
            protected void CopyDir(string sourceDir, string destDir)
            {
                if (!Directory.Exists(destDir)) Directory.CreateDirectory(destDir);
                DirectoryInfo directory = new DirectoryInfo(sourceDir);
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo f in files)
                {
                    f.Attributes = FileAttributes.Normal;
                    if (File.Exists((destDir + @"\" + f.Name)))
                        File.SetAttributes(destDir + @"\" + f.Name, FileAttributes.Normal);
                    f.CopyTo(destDir + @"\" + f.Name, true);
                }

                DirectoryInfo[] dir = directory.GetDirectories();
                foreach (DirectoryInfo d in dir)
                {
                    CopyDir(d.FullName, destDir + @"\" + d.Name);
                }
            }
            protected void AddContentSourceAndScope(string url, string extContentUrl)
            {
                //TODO: Is it required
                SearchContext context;
                using (SPSite site = new SPSite(url))
                    context = SearchContext.GetContext(site);
                Microsoft.Office.Server.Search.Administration.Content content = new Microsoft.Office.Server.Search.Administration.Content(context);
                try
                {
                    if (content.ContentSources.Exists("External"))
                        content.ContentSources["External"].Delete();
                    WebContentSource ContSrc = content.ContentSources.Create(typeof(WebContentSource), "External") as WebContentSource;

                    System.Uri StartAddress = new Uri(extContentUrl);
                    ContSrc.StartAddresses.Add(StartAddress);
                    ContSrc.MaxSiteEnumerationDepth = 0;
                    ContSrc.MaxPageEnumerationDepth = int.MaxValue;

                    DailySchedule daily = new DailySchedule(context);
                    daily.BeginDay = 01; daily.BeginMonth = 8; daily.BeginYear = 2008;
                    daily.StartHour = 5; daily.StartMinute = 00;
                    daily.DaysInterval = 1;
                    ContSrc.IncrementalCrawlSchedule = daily;
                    ContSrc.Update();
                    ContSrc.StartFullCrawl();
                    //LOG
                    Utilities.Log("Content Source Added External added.");
                }
                catch (Exception ex)
                {
                    Utilities.Log("Content Source could not be added.", ex);
                }

                Schema sspSchema = new Schema(context);
                ManagedPropertyCollection properties = sspSchema.AllManagedProperties;
                properties["ContentType"].EnabledForScoping = true;
                properties["ContentType"].Update();
                Scopes scopes = new Scopes(context);
                ScopeCollection sspScopes = scopes.AllScopes;
                Uri siteName = new Uri(url);

                try
                {
                    Scope _external = scopes.GetSharedScope("External");
                    _external.Delete();
                    scopes.Update();
                    Utilities.Log("Scope External deleted");
                }
                catch { ;}
                try
                {
                    Scope _internal = scopes.GetSharedScope("Internal");
                    _internal.Delete();
                    scopes.Update();
                    Utilities.Log("Scope Internal deleted");
                }
                catch { ;}
                try
                {
                    Scope newScope = sspScopes.Create("External", "Description", null, true, null, ScopeCompilationType.AlwaysCompile);
                    newScope.Rules.CreatePropertyQueryRule(ScopeRuleFilterBehavior.Include, properties["ContentSource"], "External");
                    //LOG
                    Utilities.Log("Search Scope External Added ");
                    Scope newScope1 = sspScopes.Create("Internal", "Description", null, true, null, ScopeCompilationType.AlwaysCompile);
                    newScope1.Rules.CreatePropertyQueryRule(ScopeRuleFilterBehavior.Include, properties["ContentType"], "Athena - Media");
                    //LOG
                    Utilities.Log("Search Scope Internal Added ");
                    //Update Scopes
                    scopes.StartCompilation();
                }
                catch (Exception ex)
                {
                    Utilities.Log(ex.Message);
                }
            }
            protected void AddWebcastEventHandler(string url)
            {
                SPSite site = new SPSite(url);
                SPWeb web = site.OpenWeb();
                string assembly = "Indigo.Athena, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cc04cc67034428b7";
                string[] listNames = { "Webcasts", "Podcasts", "Hands on Labs", "Other Content", "EDM", "DVD" };
                foreach (string listname in listNames)
                {
                    SPList list = web.Lists[listname];
                    for (int i = list.EventReceivers.Count - 1; i >= 0; i--)
                    {
                        if (list.EventReceivers[i].Assembly == assembly)
                            list.EventReceivers[i].Delete();
                    }
                    list.EventReceivers.Add(SPEventReceiverType.ItemDeleting, assembly, "Indigo.Athena.RecallItemAdder");
                    list.Update();
                }
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
            protected SPSchedule Weekly(DayOfWeek day, int beginHour, int beginMinute)
            {
                SPWeeklySchedule schedule = new SPWeeklySchedule();
                schedule.BeginHour = beginHour;
                schedule.BeginMinute = beginMinute;
                schedule.BeginDayOfWeek = day;
                schedule.EndHour = beginHour;
                schedule.EndMinute = beginMinute + 10;
                schedule.EndDayOfWeek = day;
                return schedule;
            }
            protected SPSchedule Daily(int beginHour, int beginMinute)
            {
                SPDailySchedule schedule = new SPDailySchedule();
                schedule.BeginHour = beginHour;
                schedule.EndHour = beginHour;
                schedule.BeginMinute = beginMinute;
                schedule.EndMinute = beginMinute + 10;
                return schedule;
            }
            protected string FindSiteDirectory(string websiteName, int portNo)
            {
                DirectoryEntry root = new DirectoryEntry("IIS://localhost/W3SVC");
                foreach (DirectoryEntry de in root.Children)
                {
                    if (de.SchemaClassName == "IIsWebServer" && de.Properties["ServerComment"][0].ToString() == websiteName + String.Format(" - ({0})", portNo))
                    {
                        return de.Children.Find("Root", "IISWebVirtualDir").Properties["Path"][0].ToString();
                    }
                }
                return GetIISPath() + @"\MCPWeb";
            }
            protected string GetIISPath()
            {
                RegistryKey environmentKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, Environment.MachineName).OpenSubKey("SOFTWARE\\Microsoft\\InetStp");
                return environmentKey.GetValue("PathWWWRoot").ToString();
            }
            protected void SetFileAttribute(string source)
            {

                DirectoryInfo dirInfo = new DirectoryInfo(source);
                FileInfo[] files = dirInfo.GetFiles();
                foreach (FileInfo file in files)
                {
                    file.Attributes = FileAttributes.Normal;
                }

            }
            protected string EncryptProxyPassword(string data)
            {
                return Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(data));
            }
            #endregion
        }
        #endregion

        #region CreateWebApplication Command
        private class CreateWebApplication : Command
        {
            internal CreateWebApplication(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Creating sharepoint web application"; } }

            protected internal override bool Execute(InstallOptions options)
            {
                Utilities.Log("Creating Sharepoint web application");
                try
                {
                    //Setting the Web Application and Database Name

                    string strWebAppName = Environment.MachineName + "-" + options.portNo.ToString();
                    //string strWebAppName = "SharePoint Central Administration v4";
                    options.AppPoolName = strWebAppName;
                    string strDBName = "WSS_Content_MCP" + Guid.NewGuid().ToString();
                    Utilities.Log(string.Format("Web application name:{0}", strWebAppName));
                    Utilities.Log(string.Format("Web application database name:{0}", strDBName));
                    //Getting an instance of the Local Farm
                    SPFarm spAdminFarm = SPWebService.AdministrationService.Farm;

                    //Creating an instance of the Web Application Builder
                    SPWebApplicationBuilder spWebBuilder;

                    //Creating an instance of the SPWebApplication
                    SPWebApplication spWebApp;

                    RaiseProgressChangedEvent(15);

                    spWebBuilder = new SPWebApplicationBuilder(spAdminFarm);

                    //Setting the Port Number of the Web Application
                    spWebBuilder.Port = options.portNo;
                    Utilities.Log(string.Format("Web application port no:{0}", spWebBuilder.Port));

                    RaiseProgressChangedEvent(30);

                    //Setting the Database Name
                    spWebBuilder.CreateNewDatabase = true;
                    spWebBuilder.DatabaseName = strDBName;

                    //Setting the Security Secttings
                    spWebBuilder.UseNTLMExclusively = true;
                    spWebBuilder.AllowAnonymousAccess = false;
                    spWebBuilder.UseSecureSocketsLayer = false;

                    //Setting the Application Pool Configuration
                    spWebBuilder.ApplicationPoolId = options.AppPoolName;
                    spWebBuilder.IdentityType = IdentityType.NetworkService;

                    RaiseProgressChangedEvent(45);


                    //Creating the Web Application
                    spWebApp = spWebBuilder.Create();

                    //Setting the Web Application Name
                    spWebApp.Name = strWebAppName;

                    //Updating the Web Application Information and then provisioning it
                    spWebApp.Update();
                    spWebApp.Provision();

                    RaiseProgressChangedEvent(60);

                    //Updating the Appliction Pool Configuration
                    spWebApp.ApplicationPool.Name = options.AppPoolName;
                    spWebApp.ApplicationPool.Update();
                    spWebApp.ApplicationPool.Provision();

                    Utilities.Log(string.Format("Application Pool for web application:{0}", spWebApp.ApplicationPool.Name));

                    //Adding the newly created web application to the
                    //List 'Web Applications List' in Central Administation
                    SPWebService.AdministrationService.WebApplications.Add(spWebApp);

                    RaiseProgressChangedEvent(85);

                    //Creating SiteCollection
                    string siteTitle = "Microsoft Community Portal";
                    string siteDescription = "Microsoft Community Portal";
                    string domainName = Environment.UserDomainName;
                    string username = domainName + @"\" + Environment.UserName;
                    spWebApp.Sites.Add("/", siteTitle, siteDescription, 1033, SPWebTemplate.WebTemplateSTS, username, username, "");

                    Utilities.Log(string.Format("User Domain name:{0}", domainName));
                    Utilities.Log(string.Format("User Name:{0}", username));

                    Utilities.Log("Sharepoint web application created successfully.");

                    /*Utilities.Log("Copying DeploymentManifest.xsd.");

                    Utilities.Log("DeploymentManifest.xsd copied successfully.")*/

                    RaiseProgressChangedEvent(100);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in creating sharepoint web application.", ex);
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region CreateMCPDatastore Command
        private class CreateMCPDatastore : Command
        {
            internal CreateMCPDatastore(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Creating MCP directories"; } }

            protected internal override bool Execute(InstallOptions options)
            {
                Utilities.Log("Creating MCP Datastore");
                string RootPath = string.Empty;
                string RootPathClone = string.Empty;
                RootPath = (!string.IsNullOrEmpty(options.MCPFolder)) ? Path.Combine(options.MCPFolder, "MCP") : string.Empty;
                RootPathClone = RootPath;
                try
                {
                    if (Directory.Exists(RootPath))
                    {
                        try
                        {
                            ServiceController controller = new ServiceController();
                            controller.MachineName = ".";
                            controller.ServiceName = "IISADMIN";
                            try
                            {
                                if (controller.Status.ToString().ToLower() == "running")
                                    controller.Stop();
                                controller.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 10));
                                controller.Refresh();

                                SetFileAttribute(RootPath);
                                foreach (string str in Directory.GetDirectories(RootPath))
                                {
                                    SetFileAttribute(str);
                                }
                                Directory.Delete(RootPath, true);

                                if (controller.Status.ToString().ToLower() == "stopped")
                                    controller.Start();

                                Utilities.Log("Existing datastore deleted successfully.");

                            }
                            catch { ;}
                        }
                        catch (Exception ex)
                        {
                            Utilities.Log("Error encountered while deleting existing datastore." + ex + "Setup will retry deleting after 10 seconds.");
                            Thread.Sleep(10);
                            Directory.Delete(RootPath, true);
                            Utilities.Log("Existing datastore deleted successfully.");
                        }
                    }
                    RaiseProgressChangedEvent(25);

                    Utilities.Log(string.Format("Creating MCP Datastore at location:{0}", RootPath));
                    Directory.CreateDirectory(RootPath);

                    RaiseProgressChangedEvent(50);

                    RootPath = RootPathClone;
                    //DataStore path
                    Directory.CreateDirectory(Path.Combine(RootPath, "DataStore"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();
                    options.DataStore = Path.Combine(RootPath, "DataStore");
                    Utilities.Log(string.Format("MCPData Path:{0}", options.DataStore));
                    Directory.CreateDirectory(Path.Combine(options.DataStore, "store/content"));
                    RootPath = RootPathClone;
                    Directory.CreateDirectory(Path.Combine(RootPath, "SPData"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();
                    options.MCPDataFolder = Path.Combine(RootPath, "SPData");
                    Utilities.Log(string.Format("MCPPackage Path:{0}", options.MCPPackage));
                    RootPath = Path.Combine(RootPath, "SPData");
                    Directory.CreateDirectory(Path.Combine(RootPath, "Export"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();
                    Directory.CreateDirectory(Path.Combine(RootPath, "Import"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();

                    RootPath = RootPathClone;
                    Directory.CreateDirectory(Path.Combine(RootPath, "i2VideosDataStore"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();
                    options.i2VideosDataPath = Path.Combine(RootPath, "i2VideosDataStore");
                    RootPath = options.i2VideosDataPath;
                    //
                    Directory.CreateDirectory(Path.Combine(RootPath, "store"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();
                    RootPath = Path.Combine(RootPath, "store");
                    //
                    Directory.CreateDirectory(Path.Combine(RootPath, "files"));
                    if (!Directory.Exists(Path.GetFullPath(RootPath))) throw new FileNotFoundException();


                    //content path
                    RaiseProgressChangedEvent(75);
                    string InstallType = Properties.Settings.Default.InstallationType;
                    Utilities.Log("MCP Datastore created successfully");
                    RaiseProgressChangedEvent(100);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in creating MCP Datastore.", ex);
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region LoadApplicationSettings Command
        private class LoadApplicationSettings : Command
        {
            internal LoadApplicationSettings(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Loading Application Settings.."; } }

            protected internal override bool Execute(InstallOptions options)
            {

                try
                {
                    Utilities.Log("Loading application settings");
                    if (Properties.Settings.Default.InstallationType.ToLower() != "primary")
                        options.SiteShellFile = (!string.IsNullOrEmpty(Properties.Settings.Default.SecondaryShell)) ? Properties.Settings.Default.SecondaryShell : "";
                    else
                        options.SiteShellFile = (!string.IsNullOrEmpty(Properties.Settings.Default.PrimaryShell)) ? Properties.Settings.Default.PrimaryShell : "";
                    Utilities.Log(string.Format("Shell file to be imported:{0}", options.SiteShellFile));
                    // Utilities.Log(string.Format("External content url:{0}", options.ExtContentSiteUrl));
                    //_RaiseProgressChangedEvent(50);
                    //options.ContentUrl = @"/Content/";
                    //Utilities.Log(string.Format("Content url:{0}", options.ContentUrl));
                    //_RaiseProgressChangedEvent(100);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in loading application settings", ex);
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region InstallCoreCommand Command
        private class InstallCoreCommand : Command
        {
            private InstallOptions _options;
            internal InstallCoreCommand(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Installing core application"; } }

            protected internal override bool Execute(InstallOptions options)
            {
                try
                {
                    _options = options;
                    Utilities.Log("Installing core mcp commands");
                    // 1. Copy Indigo.Athena.Data.dll to gac 
                    InstallGac("System.Net.BITS.dll");
                    InstallGac("Indigo.Athena.dll");
                    InstallGac("AjaxControlToolkit.dll");
                    InstallGac("Indigo.Athena.UI.dll");
                    //CSharpdll for compression decompression
                    InstallGac("ICSharpCode.SharpZipLib.dll");
                    InstallGac("Indigo.Trinity.Core.Clients.AspNet.dll");
                    InstallGac("Indigo.Trinity.CDN.Client.dll");
                    //Set registry values
                    SetRegistryLocalMachine(@"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\14.0\WSS", "MCPSiteUrl", options.SiteUrl);
                    if (Properties.Settings.Default.InstallationType.ToLower() == "primary")
                        InstallGac("Interop.DexterLib.dll");
                    RaiseProgressChangedEvent(10);
                    try
                    {
                        Utilities.Log("Installing AJAX Extensions 1.0");
                        string installerPath = Program.PayloadManager.GetPath("ajaxextensions");
                        Process installer = Process.Start(installerPath, "/passive /lie ajaxextensions.log");
                        installer.WaitForExit();
                        Utilities.Log("AJAX Extensions install completed with exit code: " + installer.ExitCode);
                    }
                    catch (Exception ex) { Utilities.Log("Error installing AJAX Extensions:", ex); }
                    RaiseProgressChangedEvent(25);

                    try
                    {
                        Utilities.Log("Installing IIS Media Services 3.0");
                        string installerPath = (OSInfo.Bits == 64) ? Program.PayloadManager.GetPath("iismedia64") : Program.PayloadManager.GetPath("iismedia32");
                        Process installer = Process.Start(installerPath, "/passive /lie " + Path.GetFileNameWithoutExtension(installerPath) + ".log");
                        installer.WaitForExit();
                        Utilities.Log("IIS Media Services install completed with exit code: " + installer.ExitCode);
                    }
                    catch (Exception ex) { Utilities.Log("Error installing IIS Media Services:", ex); }

                    RaiseProgressChangedEvent(25);
                    // 2. Store to registry
                    SetRegistryLocalMachine(@"SYSTEM\CurrentControlSet\Services\Eventlog\Application\Indigo.Athena.dll", null, string.Empty);
                    string UIdirectoryName = GetIISPath() + @"\Indigo.Trinity.Studio";
                    string ServiceDirectoryName = GetIISPath() + @"\Indigo.Trinity.Core";
                    Utilities.Log("UI Directory name: " + UIdirectoryName);
                    if (!Directory.Exists(UIdirectoryName)) Directory.CreateDirectory(UIdirectoryName);
                    CopyServices(ServiceDirectoryName, options);
                    CopyWebsite(UIdirectoryName, options);
                    CopySilverlight(options.DataStore, options);
                    RaiseProgressChangedEvent(50);
                    //Code added for encoder service
                    //1. Copy encoder web service to IIS root
                    string EncoderServiceDirectoryName = GetIISPath() + @"\Indigo.Trinity.Encoder.QueueService";
                    CopyEncoderServices(EncoderServiceDirectoryName, options);
                    //2. Copy encoder windows service to MCP folder
                    string encoderworkerSource = Program.PayloadManager.GetPath("encoderworker");
                    string parentFolder_DataStore = Directory.GetParent(options.DataStore).ToString();
                    string encoderWorkerDestination = Path.Combine(parentFolder_DataStore, @"Indigo.Trinity.Encoder.Worker");
                    if (Directory.Exists(encoderWorkerDestination)) { UnInstallEncoder(options); }
                    CopyDir(encoderworkerSource, encoderWorkerDestination);
                    //Copying i2videos service
                    string i2vidSource = Program.PayloadManager.GetPath("i2videos");
                    string i2vidDest = GetIISPath() + @"\I2Videos";
                    if (Directory.Exists(i2vidDest)) Directory.Delete(i2vidDest);
                    Directory.CreateDirectory(i2vidDest);
                    CopyDir(i2vidSource, i2vidDest);
                    //end 
                    RaiseProgressChangedEvent(55);
                    CreateUIWebsite(UIdirectoryName, options);
                    _HostI2VidWebService(options);
                    //Edit encoder worker config 
                    string configFile = Path.Combine(parentFolder_DataStore, @"Indigo.Trinity.Encoder.Worker") + @"\Indigo.Trinity.Encoder.Worker.exe.config";
                    Utilities.Log("Encoder worker Config file path: " + configFile);
                    if (File.Exists(configFile))
                    {
                        string searchText = "#DestinationFolder#";
                        string contentPath = Path.Combine(Path.GetFullPath(options.DataStore), @"store\Content");
                        ReplaceInFile(configFile, searchText, contentPath);
                        searchText = "#LoggingFile#";
                        ReplaceInFile(configFile, searchText, Path.Combine(Path.Combine(parentFolder_DataStore, @"Indigo.Trinity.Encoder.Worker"), "WorkerRole.txt"));
                        searchText = "#ServiceEndpoint#";
                        ReplaceInFile(configFile, searchText, "http://"+Environment.MachineName+"/QService/QueueService.svc/ws");
                        searchText = "#MachineName#";
                        ReplaceInFile(configFile, searchText, Environment.MachineName);
                    }
                    else { Utilities.Log("MCP encoder worker config file doesnt exist. Please change 'DestinationFolder', 'ServiceEndpoint', and 'LoggingFile' manually."); }
                    DeployEncoder(options);
                    RaiseProgressChangedEvent(75);
                    try
                    {
                        EditAdminCreator(options);
                        Utilities.Log("Creating Application Administrative Account");
                        string installerPath = Program.PayloadManager.GetPath("admincreator");
                        Process installer = Process.Start(installerPath + @"/Indigo.Trinity.Setup.exe", "/passive /lie admincreatorx64.log");
                        installer.WaitForExit();
                        Utilities.Log("IIS Media Services install completed with exit code: " + installer.ExitCode);
                    }
                    catch (Exception ex) { Utilities.Log("Error in admin creator :", ex); }
                    Utilities.Log("Application Administrative Account creation completed successfully");

                    //setting the remote administrator access denied to false.
                    SPWebService.ContentService.RemoteAdministratorAccessDenied = false;
                    SPWebService.ContentService.Update();
                    //4.Restarting IIS and TIMER Service
                    RestartService("IISADMIN");
                    RestartService("W3SVC");
                    RestartService("SPTIMERV3");
                    RestartService("BITS");
                    RaiseProgressChangedEvent(100);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in installing Core command.", ex);
                    return false;
                }
                return true;
            }

            protected internal override bool Rollback()
            {
                if (_options != null) UnInstallEncoder(_options);
                return base.Rollback();
            }
        }
        #endregion

        #region InstallShellCommand Command
        private class InstallShellCommand : Command
        {
            internal InstallShellCommand(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Installing Microsoft Community Portal Site shell"; } }

            protected internal override bool Execute(InstallOptions options)
            {
                try
                {
                    if (string.IsNullOrEmpty(options.SiteShellFile)) return true;

                    if (options.setupType != InstallType.Update)
                    {
                        // Delete old Shell
                        SPSite spSite = new SPSite(options.SiteUrl);
                        SPWeb spWeb = spSite.OpenWeb();
                        if (spWeb.Url.Equals(options.SiteUrl, StringComparison.InvariantCultureIgnoreCase))
                            spWeb.Delete();
                        spWeb.Dispose();
                        spSite.Dispose();
                        Utilities.Log("Deleting Old Shell successful.");
                    }

                    //Copy Web.Config to virtual directory
                    CopyConfigFile(options.SiteUrl);
                    //_CopyConfigFile(options.SiteUrl);
                    RaiseProgressChangedEvent(10);

                    /// Step 2. Import
                    Utilities.Log("Creating import settings ");
                    SPImportSettings settings = new SPImportSettings();
                    settings.SiteUrl = options.SiteUrl;
                    settings.RetainObjectIdentity = true;
                    settings.CommandLineVerbose = true;
                    settings.LogFilePath = Utilities.LogFile;
                    settings.BaseFileName = Path.GetFileName(options.SiteShellFile);
                    settings.FileLocation = Path.GetDirectoryName(options.SiteShellFile);
                    settings.FileCompression = true;

                    SPImport importer = new SPImport(settings);
                    importer.ProgressUpdated += new EventHandler<SPDeploymentEventArgs>(importer_ProgressUpdated);
                    importer.Run();
                    Utilities.Log("Imported Shell from " + options.SiteShellFile);
                    RaiseProgressChangedEvent(60);

                    /// Step 3. Increase Attachment Size to 1GB and Remove blocked file types 
                    SPWebApplication spWebApp = SPWebApplication.Lookup(new Uri(options.SiteUrl));
                    string[] myBlockedExtensions = { "chm", "msi", "exe" };
                    Collection<string> blockFileTypes = spWebApp.BlockedFileExtensions;
                    foreach (string extension in myBlockedExtensions)
                    {
                        if (blockFileTypes.Contains(extension))
                            blockFileTypes.Remove(extension);
                    }
                    spWebApp.MaximumFileSize = 1000;
                    spWebApp.Update();
                    Utilities.Log("Attachment size increased to 1GB ");
                    Utilities.Log("Removed Blocked Extensions chm, msi, exe");

                    RaiseProgressChangedEvent(70);

                    // Step 4. TODO: Set Security Roles, Groups
                    string InstallationType = Properties.Settings.Default.InstallationType;
                    if (InstallationType.ToLower() != "primary")
                    {
                        SetRolesAndGroupsSecondary(options.SiteUrl);
                        AddPermissionsToAllListSecondary(options.SiteUrl);
                        AddAllAuthenticatedUsers(options.SiteUrl);
                    }
                    else
                    {
                        SetRolesAndGroupsPrimary(options.SiteUrl);
                        AddPermissionsToAllListPrimary(options.SiteUrl);

                    }

                    if (Properties.Settings.Default.InstallationType.ToLower() == "primary")
                    {
                        AddWebcastEventHandler(options.SiteUrl);
                    }
                    RaiseProgressChangedEvent(80);

                    if (options.setupType != InstallType.Update)
                    {
                        // Step 5. Adding External Site Url to Config List
                        AddConfigurationKey(options.SiteUrl, "Lists.DVD.MediaBaseUrl", options.ExtContentSiteUrl);
                        AddConfigurationKey(options.SiteUrl, "Lists.DVD.MediaBaseDir", options.ExternalContentDir);
                        AddConfigurationKey(options.SiteUrl, "WebUrl", options.UISiteUrl);
                        AddConfigurationKey(options.SiteUrl, "ContentUrl", options.ContentUrl);
                        //_AddConfigurationKey(options.SiteUrl, "ContentPath", options.MCPContent);
                        AddConfigurationKey(options.SiteUrl, "DataStore", options.DataStore);
                        AddConfigurationKey(options.SiteUrl, "EncoderServiceUrl", "http://" + Environment.MachineName + "/QService/QueueService.svc/ws");
                        AddConfigurationKey(options.SiteUrl, "PackageFolder", options.MCPDataFolder + @"\Import");
                        AddConfigurationKey(options.SiteUrl, "UsageExportFolder", options.MCPDataFolder + @"\Export");
                        SetPermission(options.MCPDataFolder, "NETWORK SERVICE");
                        if (Properties.Settings.Default.InstallationType.ToLower() == "primary")
                        {
                            string ExportFolderUrl = options.UISiteUrl.TrimEnd('/') + @"\Export";
                            options.ExportUrl = ExportFolderUrl;
                            AddConfigurationKey(options.SiteUrl, "ExportFolder", options.MCPDataFolder + @"\Export");
                            AddConfigurationKey(options.SiteUrl, "ExportFolderURL", options.ExportUrl);
                            AddConfigurationKey(options.SiteUrl, "UsageUploadDir", options.MCPUpload);
                            AddConfigurationKey(options.SiteUrl, "UsageUploadUrl", options.UploadUrl);
                            AddConfigurationKey(options.SiteUrl, "ContentPackagePath", options.MCPPackage);
                        }
                        RaiseProgressChangedEvent(90);

                        // Step 6. TODO: Set Search Scope + Crawl Rules
                        AddContentSourceAndScope(options.SiteUrl, options.ExtContentSiteUrl);
                        RaiseProgressChangedEvent(100);
                    }
                    Utilities.Log("Installation of shell command completed successfully.");
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in installing shell command", ex);
                    return false;
                }
                return true;
            }

            private void importer_ProgressUpdated(object sender, SPDeploymentEventArgs e)
            {
                try
                {
                    /// WORKAROUND: SPImport does not seem to provide correct value for 'e.ObjectsTotal'. Hence assumed 1000.
                    long totalObjects = (e.ObjectsTotal > 0) ? e.ObjectsTotal : 1000;
                    RaiseProgressChangedEvent(10 + (int)(((double)(e.ObjectsProcessed % totalObjects) / totalObjects) * 50));
                }
                catch { Utilities.Log("Error updating shell import progress.", Utilities.MessageType.Warning); }
            }
        }
        #endregion

        #region AddingDefaultJobs Command
        private class AddingDefaultJobs : Command
        {
            internal AddingDefaultJobs(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Adding Default Jobs to site ..."; } }

            protected internal override bool Execute(InstallOptions options)
            {
                try
                {
                    Utilities.Log("Adding default jobs to site.");
                    //Default Jobs  
                    if (!SiteExists(options.SiteUrl))
                        throw new Exception("Setup Microsoft Community Site prior to Content Import");

                    // 1. Setup Import Job
                    SPSite spSite = new SPSite(options.SiteUrl);
                    SPWeb spWeb = spSite.OpenWeb();

                    Indigo.Athena.Jobs.JobManager jobMgr = new Indigo.Athena.Jobs.JobManager(spWeb);

                    //Content Validator
                    Indigo.Athena.Jobs.ContentValidatorJob contentjob = (Indigo.Athena.Jobs.ContentValidatorJob)jobMgr.New("DefaultContentValidator", Indigo.Athena.Jobs.JobType.ContentValidator);

                    // Common Job Properties
                    contentjob.WebUrl = options.SiteUrl;
                    contentjob.Title = "Microsoft Community Portal - Default Content Validator";
                    contentjob.Description = "Validates the MCP content.";
                    SPHourlySchedule validatorSchedule = new SPHourlySchedule();
                    validatorSchedule.BeginMinute = 0;
                    validatorSchedule.EndMinute = 59;
                    contentjob.Schedule = validatorSchedule;
                    contentjob.TempFolder = options.TempFolder;
                    jobMgr.Save(contentjob);
                    Utilities.Log("Submitted job " + contentjob.Title);

                    RaiseProgressChangedEvent(10);
                    //Service Content Import Job
                    Indigo.Athena.Jobs.ServiceContentImportJob contentImportjob = (Indigo.Athena.Jobs.ServiceContentImportJob)jobMgr.New("DefaultServiceContentImportJob", Indigo.Athena.Jobs.JobType.ServiceContentImport);
                    contentImportjob.WebUrl = options.SiteUrl;
                    contentImportjob.Title = "Microsoft Community Portal - Default Service Content Import";
                    contentImportjob.Description = "Enqueues new content to encoder service and imports the encoded content to client service.";
                    validatorSchedule = new SPHourlySchedule();
                    validatorSchedule.BeginMinute = 30;
                    validatorSchedule.EndMinute = 59;
                    contentImportjob.Schedule = validatorSchedule;
                    contentImportjob.TempFolder = options.TempFolder;
                    jobMgr.Save(contentImportjob);
                    Utilities.Log("Submitted job " + contentImportjob.Title);
                    RaiseProgressChangedEvent(20);
                    //Service Usage Import
                    Indigo.Athena.Jobs.ServiceUsageImport serviceUsageImportjob = (Indigo.Athena.Jobs.ServiceUsageImport)jobMgr.New("DefaultServiceUsageImportJob", Indigo.Athena.Jobs.JobType.ServiceUsageImport);
                    serviceUsageImportjob.WebUrl = options.SiteUrl;
                    serviceUsageImportjob.Title = "Microsoft Community Portal - Default Service Usage Import";
                    serviceUsageImportjob.Description = "Imports the usage from client service.";
                    SPDailySchedule validatorDailySchedule = new SPDailySchedule();
                    validatorDailySchedule.BeginHour = 04;
                    validatorDailySchedule.BeginMinute = 30;
                    serviceUsageImportjob.Schedule = validatorDailySchedule;
                    serviceUsageImportjob.TempFolder = options.TempFolder;
                    jobMgr.Save(serviceUsageImportjob);
                    Utilities.Log("Submitted job " + contentImportjob.Title);
                    RaiseProgressChangedEvent(30);
                    //Usage Uploader Job
                    Indigo.Athena.Jobs.UsageUploadJob usageuploadjob = (Indigo.Athena.Jobs.UsageUploadJob)jobMgr.New("DefaultUsageUploadJob", Indigo.Athena.Jobs.JobType.UsageUpload);
                    // Common Job Properties
                    usageuploadjob.WebUrl = options.SiteUrl;
                    usageuploadjob.Title = "Microsoft Community Portal - Default Usage Uploader Job";
                    usageuploadjob.Description = "Uploads the usage lists to primary.";
                    usageuploadjob.Schedule = Daily(6, 0);
                    usageuploadjob.LogFile = options.Logfile;
                    usageuploadjob.TempFolder = options.LogFolder;
                    usageuploadjob.ExportFromDate = DateTime.Now;
                    jobMgr.Save(usageuploadjob);
                    Utilities.Log("Submitted job " + usageuploadjob.Title);
                    RaiseProgressChangedEvent(40);
                    //Download
                    Indigo.Athena.Jobs.PackageDownloaderJob downloadjob = (Indigo.Athena.Jobs.PackageDownloaderJob)jobMgr.New("DefaultPackageDownloaderJob", Indigo.Athena.Jobs.JobType.Download);
                    // Common Job Properties
                    downloadjob.WebUrl = options.SiteUrl;
                    downloadjob.Title = "Microsoft Community Portal - Package Downloader Job";
                    downloadjob.Description = "Downloads the content packages.";

                    //if (!options.WeeklyDownload)
                    downloadjob.Schedule = Daily(23, 0);

                    //else
                    //downloadjob.Schedule = Weekly(options.DownloaddaySchedule, options.DownloadhourSchedule, options.DownloadminSchedule);
                    downloadjob.LogFile = options.Logfile;
                    downloadjob.TempFolder = options.LogFolder;
                    downloadjob.LastSyncDate = DateTime.Now;
                    jobMgr.Save(downloadjob);
                    Utilities.Log("Submitted job " + downloadjob.Title);
                    RaiseProgressChangedEvent(50);
                    //Import
                    Indigo.Athena.Jobs.ContentImporterJob importjob = (Indigo.Athena.Jobs.ContentImporterJob)jobMgr.New("DefaultContentImporterJob", Indigo.Athena.Jobs.JobType.Import);

                    // Common Job Properties
                    importjob.WebUrl = options.SiteUrl;
                    importjob.Title = "Microsoft Community Portal - Content Importer Job";
                    importjob.Description = "Imports the downloaded packages.";

                    //if (!options.WeeklyImport)
                    importjob.Schedule = Daily(23, 30);
                    //else
                    //importjob.Schedule = Weekly(options.ImportdaySchedule, options.ImporthourSchedule, options.ImportminSchedule);

                    importjob.LogFile = options.Logfile;
                    importjob.TempFolder = options.TempFolder;
                    jobMgr.Save(importjob);
                    Utilities.Log("Submitted job " + importjob.Title);

                    RaiseProgressChangedEvent(60);
                    //Recall (Changed to automatic schedule)
                    Indigo.Athena.Jobs.ContentRecallJob recalljob = (Indigo.Athena.Jobs.ContentRecallJob)jobMgr.New("DefaultContentRecallJob", Indigo.Athena.Jobs.JobType.Recall);

                    // Common Job Properties
                    recalljob.WebUrl = options.SiteUrl;
                    recalljob.Title = "Microsoft Community Portal - Content Recall Job";
                    recalljob.Description = "Default Content Recall Job";
                    recalljob.Schedule = Daily(6, 30); //recallSchedule;
                    recalljob.LogFile = options.Logfile;
                    recalljob.TempFolder = options.LogFolder;
                    recalljob.LastSyncDate = DateTime.Now;
                    jobMgr.Save(recalljob);
                    Utilities.Log("Submitted job " + recalljob.Title);
                    RaiseProgressChangedEvent(70);
                    //MediaDownloader - Smooth Streaming Related
                    Indigo.Athena.Jobs.MediaDownloaderJob mediadownloaderjob = (Indigo.Athena.Jobs.MediaDownloaderJob)jobMgr.New("DefaultMediaDownloaderJob", Indigo.Athena.Jobs.JobType.MediaDownloader);

                    mediadownloaderjob.WebUrl = options.SiteUrl;
                    mediadownloaderjob.Title = "Microsoft Community Portal - Media Downloader Job";
                    mediadownloaderjob.Description = "Default Media Downloader Job";
                    mediadownloaderjob.Schedule = Daily(0, 0);
                    mediadownloaderjob.LogFile = options.Logfile;
                    mediadownloaderjob.TempFolder = options.LogFolder;
                    jobMgr.Save(mediadownloaderjob);
                    Utilities.Log("Submitted job " + mediadownloaderjob.Title);
                    RaiseProgressChangedEvent(80);
                    //Alert Job -Daily

                    Indigo.Athena.Jobs.AlertsJob alertjob = (Indigo.Athena.Jobs.AlertsJob)jobMgr.New("DailyAlertJob", Indigo.Athena.Jobs.JobType.Alert);
                    alertjob.WebUrl = options.SiteUrl;
                    alertjob.Title = "Microsoft Community Portal-Daily Alert Job";
                    alertjob.Description = "Default Daily Alert Job";
                    alertjob.Schedule = Daily(8, 0);
                    alertjob.LogFile = options.Logfile;
                    alertjob.TempFolder = options.LogFolder;
                    alertjob.LastAlertDate = DateTime.UtcNow;
                    jobMgr.Save(alertjob);
                    Utilities.Log("Submitted Job " + alertjob.Title);
                    RaiseProgressChangedEvent(85);
                    //Alert Job -Weekly
                    Indigo.Athena.Jobs.AlertsJob alertjob1 = (Indigo.Athena.Jobs.AlertsJob)jobMgr.New("WeeklyAlertJob", Indigo.Athena.Jobs.JobType.Alert);
                    alertjob1.WebUrl = options.SiteUrl;
                    alertjob1.Title = "Microsoft Community Portal-Weekly Alert Job";
                    alertjob1.Description = "Default Weekly Alert Job";
                    alertjob1.Schedule = Weekly(DayOfWeek.Monday, 8, 30);
                    alertjob1.LogFile = options.Logfile;
                    alertjob1.TempFolder = options.LogFolder;
                    alertjob1.LastAlertDate = DateTime.UtcNow;
                    jobMgr.Save(alertjob1);
                    Utilities.Log("Submitted Job " + alertjob1.Title);
                    RaiseProgressChangedEvent(90);
                    //Archive Job
                    Indigo.Athena.Jobs.ArchiveJob archiveJob = (Indigo.Athena.Jobs.ArchiveJob)jobMgr.New("DefaultArchiveJob", Indigo.Athena.Jobs.JobType.Archive);
                    archiveJob.Title = "Microsoft Community Portal- Daily Archive Job";
                    archiveJob.Description = "Archives the application items.";
                    archiveJob.Schedule = Daily(7, 0);
                    archiveJob.LogFile = options.Logfile;
                    archiveJob.TempFolder = options.LogFolder;
                    jobMgr.Save(archiveJob);
                    Utilities.Log("Submitted Job " + archiveJob.Title);
                    //Dispose SPWeb
                    if (spWeb != null) spWeb.Dispose();
                    if (spSite != null) spSite.Dispose();

                    Utilities.Log("Default jobs added to site successfully.");
                    RaiseProgressChangedEvent(100);

                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in adding default jobs to site.", ex);
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region AddingDefaultPrimaryJobs Command
        private class AddingDefaultPrimaryJobs : Command
        {
            internal AddingDefaultPrimaryJobs(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Adding Default Jobs to site ..."; } }

            protected internal override bool Execute(InstallOptions options)
            {
                try
                {
                    Utilities.Log("Adding default primary jobs");
                    //Default Jobs  
                    if (!SiteExists(options.SiteUrl))
                        throw new Exception("Setup Microsoft Community Site prior to Content Import");
                    // 1. Setup Import Job
                    SPSite spSite = new SPSite(options.SiteUrl);
                    SPWeb spWeb = spSite.OpenWeb();

                    Indigo.Athena.Jobs.JobManager jobMgr = new Indigo.Athena.Jobs.JobManager(spWeb);

                    //Content Validator
                    Indigo.Athena.Jobs.ContentValidatorJob contentjob = (Indigo.Athena.Jobs.ContentValidatorJob)jobMgr.New("DefaultContentValidator", Indigo.Athena.Jobs.JobType.ContentValidator);

                    // Common Job Properties
                    contentjob.WebUrl = options.SiteUrl;
                    contentjob.Title = "Microsoft Community Portal - Default Content Validator";
                    contentjob.Description = "Default Content Validator";
                    contentjob.Schedule = Daily(23, 0);
                    contentjob.LogFile = options.Logfile;
                    contentjob.TempFolder = options.TempFolder;
                    jobMgr.Save(contentjob);
                    Utilities.Log("Submitted job " + contentjob.Title);

                    RaiseProgressChangedEvent(35);

                    Indigo.Athena.Jobs.ContentUpdater contentupdaterjob = (Indigo.Athena.Jobs.ContentUpdater)jobMgr.New("DefaultContentUpdater", Indigo.Athena.Jobs.JobType.ContentUpdater);
                    contentupdaterjob.WebUrl = options.SiteUrl;
                    contentupdaterjob.Title = "Microsoft Community Portal - Default Content Updater";
                    contentupdaterjob.Description = "Default Content Updater";
                    contentupdaterjob.Schedule = Daily(21, 0);
                    contentupdaterjob.LogFile = options.Logfile;
                    contentupdaterjob.TempFolder = options.LogFolder;
                    jobMgr.Save(contentupdaterjob);
                    Utilities.Log("Submitted job " + contentupdaterjob.Title);

                    RaiseProgressChangedEvent(70);

                    Indigo.Athena.Jobs.MediaExportJob mediaexportjob = (Indigo.Athena.Jobs.MediaExportJob)jobMgr.New("DefaultMediaExporter", Indigo.Athena.Jobs.JobType.MediaExport);
                    mediaexportjob.WebUrl = options.SiteUrl;
                    mediaexportjob.Title = "Microsoft Community Portal - Default Media Export Job";
                    mediaexportjob.Description = "Default Media Export Job";
                    mediaexportjob.Schedule = Daily(22, 0);
                    mediaexportjob.LogFile = options.Logfile;
                    mediaexportjob.TempFolder = options.LogFolder;
                    jobMgr.Save(mediaexportjob);
                    Utilities.Log("Submitted job " + mediaexportjob.Title);

                    RaiseProgressChangedEvent(95);

                    Utilities.Log("Default primary jobs added successfully");
                    RaiseProgressChangedEvent(100);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in adding default primary jobs", ex);
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region ConfigurationUpdater Command
        private class ConfigurationUpdater : Command
        {
            internal ConfigurationUpdater(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Configuring the Site ..."; } }

            protected internal override bool Execute(InstallOptions options)
            {
                try
                {
                    Utilities.Log("Starting with configuration updater command");
                    string url = options.SiteUrl;
                    string packageFolder = options.MCPDataFolder + @"\Import";
                    string UsageExportFolder = options.MCPDataFolder + @"\Export";
                    string ServiceUrl = (!string.IsNullOrEmpty(Properties.Settings.Default.ServiceUrl)) ? Properties.Settings.Default.ServiceUrl : "";
                    string downloadUrl = (!string.IsNullOrEmpty(Properties.Settings.Default.DownloadBaseUrl)) ? Properties.Settings.Default.DownloadBaseUrl : "";

                    RaiseProgressChangedEvent(15);

                    AddConfigurationKey(url, "SMTPServer", options.SMTPServer);
                    AddConfigurationKey(url, "PackageFolder", packageFolder);
                    AddConfigurationKey(url, "UsageExportFolder", UsageExportFolder);
                    AddConfigurationKey(url, "UseProxy", options.UseProxy);
                    AddConfigurationKey(url, "ProxyAddressPort", options.ProxyAddressPort);
                    AddConfigurationKey(url, "ProxyUserName", options.ProxyUserName);
                    AddConfigurationKey(url, "ProxyPassword", EncryptProxyPassword(options.ProxyPassword));
                    AddConfigurationKey(url, "ProxyAuthScheme", options.ProxyAuthScheme);
                    AddConfigurationKey(url, "UploadMethod", "HTTP");
                    AddConfigurationKey(url, "DownloadMethod", "BITS");
                    AddConfigurationKey(url, "ContentPath", options.DataStore + @"\store\content");
                    AddConfigurationKey(url, "ContentUrl", options.SiteUrl + @"/services/content");
                    AddConfigurationKey(url, "ShowLocalMediaOnly", Properties.Settings.Default.ShowLocalMediaOnly.ToString());

                    RaiseProgressChangedEvent(50);

                    SetPartner(url, options.PartnerName);

                    RaiseProgressChangedEvent(65);

                    //Setting Default Configurations
                    AddConfigurationKey(url, "ContentList", "Webcasts,Podcasts,Hands on Labs,Other Content,Forums,Blog Feeds,Categories,Blogs,DVD,EDM");
                    AddConfigurationKey(url, "ContentAgingDays", "90");
                    AddConfigurationKey(url, "AlertMailTemplate", GetAlertMailTemplate());

                    RaiseProgressChangedEvent(80);

                    //Giving Network Service Access to specific directories.                
                    SetPermission(packageFolder, "NETWORK SERVICE");
                    SetPermission(UsageExportFolder, "NETWORK SERVICE");
                    SetPermission(options.DataStore, "NETWORK SERVICE");
                    SetPermission(options.DataStore, "EVERYONE");

                    Utilities.Log("Configuration updater command completed successfully.");
                    RaiseProgressChangedEvent(100);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in Configuration Updater command.", ex);
                    return false;
                }
                return true;
            }
        }
        #endregion

        #region ScheduleAutoUpdater Command
        private class ScheduleAutoUpdater : Command
        {
            internal ScheduleAutoUpdater(InstallProcessControl parent) : base(parent) { }

            internal override string Description { get { return "Scheduling AutoUpdater ..."; } }

            protected internal override bool Execute(InstallOptions options)
            {
                try
                {
                    bool isSecondary = string.Equals(Properties.Settings.Default.InstallationType, "secondary", StringComparison.OrdinalIgnoreCase);

                    Utilities.Log(string.Format("Setting scheduled task for installation type : {0}", isSecondary ? "Secondary" : "Primary"));
                    if (isSecondary == false) return true; //No Schedule Task for Primary

                    //If it is Secondary, Schedule Auto Updater
                    //Step 1: Copy Auto updater
                    string payloadFolder = Program.PayloadManager.GetPath("autoupdater");
                    string deploymentFolder = Path.Combine(options.MCPFolder, @"MCP\AutoUpdater");
                    Utilities.CopyDirectory(payloadFolder, deploymentFolder);

                    if (Directory.Exists(deploymentFolder))
                    {
                        //Step 2: Update the Config of Auto Updater
                        string configPath = Path.Combine(deploymentFolder, "Indigo.Trinity.Deploy.AutoUpdater.exe.config");
                        ReplaceInFile(configPath, "#ProxyAddress#", options.ProxyAddressPort);
                        ReplaceInFile(configPath, "#ProxyUserName#", options.ProxyUserName);
                        ReplaceInFile(configPath, "#ProxyPassword#", EncryptProxyPassword(options.ProxyPassword));
                        ReplaceInFile(configPath, "#ManifestURL#", Properties.Settings.Default.ManifestURL);
                        ReplaceInFile(configPath, "#SyncServiceURL#", Properties.Settings.Default.ServiceUrl);
                        ReplaceInFile(configPath, "#PartnerName#", options.PartnerName);

                        //Step 3: Create Task and Schedule it
                        ScheduledTasks scheduledTasks = new ScheduledTasks();
                        Task task = null;
                        string taskName = "Indigo.Trinity.Deploy.AutoUpdater";
                        try
                        {
                            task = scheduledTasks.CreateTask(taskName);
                            Utilities.Log(string.Format("Scheduled Task {0} created successfully", taskName));
                        }
                        catch
                        {
                            Utilities.Log(taskName + "already exists");
                            scheduledTasks.DeleteTask(taskName);
                            task = scheduledTasks.CreateTask(taskName);
                        }
                        RaiseProgressChangedEvent(35);
                        // Fill in the program info
                        task.ApplicationName = deploymentFolder + @"\Indigo.Trinity.Deploy.AutoUpdater.exe";
                        task.WorkingDirectory = deploymentFolder + @"\Indigo.Trinity.Deploy.AutoUpdater.exe";
                        task.Comment = "Downloads and installs updates for MCP";
                        // Set the account under which the task should run.
                        task.SetAccountInformation(options.AutoupdaterUserName, options.AutoupdaterPassword);
                        // Allow the task to run for no more than 2 hours, 30 minutes.
                        task.MaxRunTime = new TimeSpan(2, 30, 0);
                        // Set priority to only run when system is idle.
                        task.Priority = System.Diagnostics.ProcessPriorityClass.Normal;
                        RaiseProgressChangedEvent(55);
                        // Create a trigger to start the task every Sunday at 6:30 AM.
                        task.Triggers.Add(new WeeklyTrigger(6, 30, DaysOfTheWeek.Monday));
                        // Save the changes that have been made.
                        task.Save();
                        // Close the task to release its COM resources.
                        task.Close();
                        // Dispose the ScheduledTasks to release its COM resources.
                        scheduledTasks.Dispose();
                        Utilities.Log(string.Format("Task: {0} scheduled successfully", taskName));
                    }
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error in scheduling autoupdater" + ex + ".Setup will continue with further commands.");
                    return true;
                }
                return true;
            }
        }
        #endregion
        #endregion

        # region Private Methods
        private void InitiateRollback()
        {
            if (Form != null) Form.Invoke(() => { Form.AbortButton.Enabled = false; });

            progressTotal.Invoke(() => { progressTotal.Maximum = rollbackCommands.Count; });
            progressTotal.Invoke(() => { progressTotal.Value = rollbackCommands.Count; });
            nextCommand = 0;
            rollbackErrors = 0;
            progressTotal.Invoke(() => { progressTotal.Step = -1; });
            progressTask.Invoke(() => { progressTask.Value = 0; });

            Rollback();
        }
        private void Rollback()
        {
            descriptionLabel.Invoke(() => { descriptionLabel.Text = "Error Detected.Rolling back all operrations."; });
            if (nextCommand < rollbackCommands.Count)
            {
                try
                {
                    Command command = rollbackCommands[nextCommand];
                    if (command.Rollback())
                    {
                        descriptionLabel.Invoke(() => { descriptionLabel.Text = "Rolling back command: " + command.Description; });
                        nextCommand++;
                        progressTotal.Invoke(() => { progressTotal.PerformStep(); });
                    }
                }
                catch
                {
                    rollbackErrors++;
                    nextCommand++;
                    progressTotal.Invoke(() => { progressTotal.PerformStep(); });
                }
                Rollback();
            }
            else
            {
                if (rollbackErrors == 0)
                {
                    descriptionLabel.Invoke(() => { descriptionLabel.Text = "Rollback successfully completed."; });
                    Utilities.Log("Rollback successfully completed.");
                }
                else
                {
                    descriptionLabel.Invoke(() => { descriptionLabel.Text = "Rollback completed with " + rollbackErrors + " errors. Please see log file for details."; });
                }
                if (Form != null)
                {
                    Form.Invoke(() => { Form.AbortButton.Text = "Exit"; });
                    Form.Invoke(() => { Form.AbortButton.Enabled = true; });
                    this.Form.SetupState = InstallationState.Failed;
                    this.Form.MoveNext();
                }
            }
        }
        #endregion

        #region IDoAsync Region
        public void DoWorkAsync()
        {
            if (requestCancel)
            {
                descriptionLabel.Invoke(() => { descriptionLabel.Text = "Operation canceled! Rolling back all operations."; });
                Utilities.Log("Operation canceled! Rolling back all operations.");
                InitiateRollback();
                return;
            }

            else if (nextCommand < executeCommands.Count)
            {
                Command command = executeCommands[nextCommand];
                command.ProgressChanged += new ProgressChangedEventHandler(command_ProgressChanged);
                progressTask.Invoke(() => { progressTask.Value = 0; });
                if (command.Execute(_installOptions))
                {
                    nextCommand++;
                    progressTotal.Invoke(() => { progressTotal.PerformStep(); });
                    if (nextCommand < executeCommands.Count)
                    {
                        descriptionLabel.Invoke(() => { descriptionLabel.Text = string.Format("Step {0} / {1}: {2}", nextCommand + 1, executeCommands.Count, executeCommands[nextCommand].Description); });
                    }
                }
                else
                {
                    InitiateRollback();
                    return;
                }
                DoWorkAsync();
            }
            else
            {
                descriptionLabel.Invoke(() => { descriptionLabel.Text = "All operations successfully completed."; });
                Utilities.Log("All operations successfully completed.");
                if (this.Form != null) this.Form.SetupState = InstallationState.Successful;
                if (this.Form != null) this.Form.MoveNext();
            }
        }
        #endregion
    }
}
