﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Athena.Deploy
{
    public partial class MiscSettingsControl : InstallerControl
    {
        public MiscSettingsControl()
        {
            InitializeComponent();
           // browseContent.ShowNewFolderButton = false;
        }

        protected internal override void Open(InstallOptions options)
        {
            this.Form.SetupState = InstallationState.GatherMiscInfo;
            string currentUser = string.Format("{0}\\{1}", Environment.UserDomainName, Environment.UserName);
            txtUsername.Text = string.IsNullOrEmpty(options.AutoupdaterUserName) ? currentUser : options.AutoupdaterUserName;
            txtPassword.Text = string.IsNullOrEmpty(options.AutoupdaterUserName) ? string.Empty : options.AutoupdaterPassword;
            txtInstallationDir.Text = options.MCPFolder;

        }

        protected internal override bool ValidateForm()
        {
            if (txtUsername.Text == string.Empty || txtPassword.Text == string.Empty)
            {
                MessageBox.Show("Provide valid credentials for scheduled tasks.", "Invalid credentials", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (txtSMTPAddress.Text == string.Empty)
            {
                DialogResult response = MessageBox.Show("You have not specified SMTP server."+
                                    "\r\nEmail notifications will be disabled. Proceed?" +
                                    "\r\n(You can specify the SMTP server later using MCP Admin Portal.)", "SMTP Server not specified",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (response == DialogResult.No)
                    return false;
            }

           /*if (!_ValidateContent(txtContentLocation.Text))
                return false; */

            return true;
        }

        protected internal override void Close(InstallOptions options)
        {
            options.SMTPServer = txtSMTPAddress.Text;
            options.AutoupdaterUserName = txtUsername.Text;
            options.AutoupdaterPassword = txtPassword.Text;
            options.MCPFolder = txtInstallationDir.Text;
            //options.MCPInitialContent = txtContentLocation.Text;

            Utilities.Log("SMTP Server:" + options.SMTPServer);
            Utilities.Log("Scheduled Task User Context:" + options.AutoupdaterUserName);
            //Utilities.Log("Initial Content Location:" + options.MCPInitialContent);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (fbdInstallDir.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtInstallationDir.Text = fbdInstallDir.SelectedPath;
            }
        }

        #region Event Handlers
       /* private void btnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = browseContent.ShowDialog();
            if (result != DialogResult.OK) return;

            txtContentLocation.Text = browseContent.SelectedPath;
            _ValidateContent(browseContent.SelectedPath);
        }
        
        private void txtContentLocation_Leave(object sender, EventArgs e)
        {
            _ValidateContent(txtContentLocation.Text);
        }
       */
        #endregion

        #region Helper Functions
       /* private bool _ValidateContent(string path)
        {
            if (string.IsNullOrEmpty(path)) return true;
            
            if (!Directory.Exists(path))
            {
                MessageBox.Show("Provide valid content location.", "Invalid path", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (Directory.GetFiles(path, "*.cmp").Length == 0)
            {
                MessageBox.Show("No content found at specified location. Provide valid content location.", "Invalid path", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }
        */ 
        #endregion
    }
}
