namespace Athena.Deploy
{
    partial class PrerequisiteControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.imgDesktopExp = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDesktopExp = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.imgMCPCheck = new System.Windows.Forms.PictureBox();
            this.imgSearchServerCheck = new System.Windows.Forms.PictureBox();
            this.imgDotNetCheck = new System.Windows.Forms.PictureBox();
            this.imgIISCheck = new System.Windows.Forms.PictureBox();
            this.imgDiskCheck = new System.Windows.Forms.PictureBox();
            this.imgRAMCheck = new System.Windows.Forms.PictureBox();
            this.imgOSCheck = new System.Windows.Forms.PictureBox();
            this.txtOSStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRAMStatus = new System.Windows.Forms.Label();
            this.txtDiskStatus = new System.Windows.Forms.Label();
            this.txtIISStatus = new System.Windows.Forms.Label();
            this.txtDotNetStatus = new System.Windows.Forms.Label();
            this.txtSearchServerStatus = new System.Windows.Forms.Label();
            this.txtMCPStatus = new System.Windows.Forms.Label();
            this.tableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDesktopExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMCPCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSearchServerCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDotNetCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIISCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDiskCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRAMCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOSCheck)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayout
            // 
            this.tableLayout.ColumnCount = 3;
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayout.Controls.Add(this.imgDesktopExp, 0, 6);
            this.tableLayout.Controls.Add(this.label2, 1, 6);
            this.tableLayout.Controls.Add(this.txtDesktopExp, 2, 6);
            this.tableLayout.Controls.Add(this.label3, 1, 1);
            this.tableLayout.Controls.Add(this.imgMCPCheck, 0, 7);
            this.tableLayout.Controls.Add(this.imgSearchServerCheck, 0, 5);
            this.tableLayout.Controls.Add(this.imgDotNetCheck, 0, 4);
            this.tableLayout.Controls.Add(this.imgIISCheck, 0, 3);
            this.tableLayout.Controls.Add(this.imgDiskCheck, 0, 2);
            this.tableLayout.Controls.Add(this.imgRAMCheck, 0, 1);
            this.tableLayout.Controls.Add(this.imgOSCheck, 0, 0);
            this.tableLayout.Controls.Add(this.txtOSStatus, 2, 0);
            this.tableLayout.Controls.Add(this.label1, 1, 0);
            this.tableLayout.Controls.Add(this.label4, 1, 2);
            this.tableLayout.Controls.Add(this.label5, 1, 3);
            this.tableLayout.Controls.Add(this.label6, 1, 4);
            this.tableLayout.Controls.Add(this.label7, 1, 5);
            this.tableLayout.Controls.Add(this.label9, 1, 7);
            this.tableLayout.Controls.Add(this.txtRAMStatus, 2, 1);
            this.tableLayout.Controls.Add(this.txtDiskStatus, 2, 2);
            this.tableLayout.Controls.Add(this.txtIISStatus, 2, 3);
            this.tableLayout.Controls.Add(this.txtDotNetStatus, 2, 4);
            this.tableLayout.Controls.Add(this.txtSearchServerStatus, 2, 5);
            this.tableLayout.Controls.Add(this.txtMCPStatus, 2, 7);
            this.tableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayout.Location = new System.Drawing.Point(0, 0);
            this.tableLayout.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayout.Name = "tableLayout";
            this.tableLayout.RowCount = 8;
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayout.Size = new System.Drawing.Size(430, 288);
            this.tableLayout.TabIndex = 3;
            // 
            // imgDesktopExp
            // 
            this.imgDesktopExp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgDesktopExp.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgDesktopExp.Location = new System.Drawing.Point(9, 177);
            this.imgDesktopExp.Margin = new System.Windows.Forms.Padding(5);
            this.imgDesktopExp.Name = "imgDesktopExp";
            this.imgDesktopExp.Size = new System.Drawing.Size(16, 16);
            this.imgDesktopExp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgDesktopExp.TabIndex = 27;
            this.imgDesktopExp.TabStop = false;
            this.imgDesktopExp.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 178);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Smooth Streaming Pre-Req";
            // 
            // txtDesktopExp
            // 
            this.txtDesktopExp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesktopExp.AutoSize = true;
            this.txtDesktopExp.Location = new System.Drawing.Point(200, 178);
            this.txtDesktopExp.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtDesktopExp.Name = "txtDesktopExp";
            this.txtDesktopExp.Size = new System.Drawing.Size(220, 17);
            this.txtDesktopExp.TabIndex = 29;
            this.txtDesktopExp.Text = "Checking...";
            this.txtDesktopExp.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 35);
            this.label3.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Physical Memory (RAM)";
            // 
            // imgMCPCheck
            // 
            this.imgMCPCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgMCPCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgMCPCheck.Location = new System.Drawing.Point(9, 205);
            this.imgMCPCheck.Margin = new System.Windows.Forms.Padding(5);
            this.imgMCPCheck.Name = "imgMCPCheck";
            this.imgMCPCheck.Size = new System.Drawing.Size(16, 16);
            this.imgMCPCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgMCPCheck.TabIndex = 12;
            this.imgMCPCheck.TabStop = false;
            this.imgMCPCheck.Visible = false;
            // 
            // imgSearchServerCheck
            // 
            this.imgSearchServerCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgSearchServerCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgSearchServerCheck.Location = new System.Drawing.Point(9, 148);
            this.imgSearchServerCheck.Margin = new System.Windows.Forms.Padding(5, 6, 5, 5);
            this.imgSearchServerCheck.Name = "imgSearchServerCheck";
            this.imgSearchServerCheck.Size = new System.Drawing.Size(16, 16);
            this.imgSearchServerCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSearchServerCheck.TabIndex = 10;
            this.imgSearchServerCheck.TabStop = false;
            this.imgSearchServerCheck.Visible = false;
            // 
            // imgDotNetCheck
            // 
            this.imgDotNetCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgDotNetCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgDotNetCheck.Location = new System.Drawing.Point(9, 118);
            this.imgDotNetCheck.Margin = new System.Windows.Forms.Padding(5);
            this.imgDotNetCheck.Name = "imgDotNetCheck";
            this.imgDotNetCheck.Size = new System.Drawing.Size(16, 16);
            this.imgDotNetCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgDotNetCheck.TabIndex = 9;
            this.imgDotNetCheck.TabStop = false;
            this.imgDotNetCheck.Visible = false;
            // 
            // imgIISCheck
            // 
            this.imgIISCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgIISCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgIISCheck.Location = new System.Drawing.Point(9, 90);
            this.imgIISCheck.Margin = new System.Windows.Forms.Padding(5);
            this.imgIISCheck.Name = "imgIISCheck";
            this.imgIISCheck.Size = new System.Drawing.Size(16, 16);
            this.imgIISCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgIISCheck.TabIndex = 8;
            this.imgIISCheck.TabStop = false;
            this.imgIISCheck.Visible = false;
            // 
            // imgDiskCheck
            // 
            this.imgDiskCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgDiskCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgDiskCheck.Location = new System.Drawing.Point(9, 62);
            this.imgDiskCheck.Margin = new System.Windows.Forms.Padding(5);
            this.imgDiskCheck.Name = "imgDiskCheck";
            this.imgDiskCheck.Size = new System.Drawing.Size(16, 16);
            this.imgDiskCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgDiskCheck.TabIndex = 7;
            this.imgDiskCheck.TabStop = false;
            this.imgDiskCheck.Visible = false;
            // 
            // imgRAMCheck
            // 
            this.imgRAMCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgRAMCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgRAMCheck.Location = new System.Drawing.Point(9, 34);
            this.imgRAMCheck.Margin = new System.Windows.Forms.Padding(5);
            this.imgRAMCheck.Name = "imgRAMCheck";
            this.imgRAMCheck.Size = new System.Drawing.Size(16, 16);
            this.imgRAMCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgRAMCheck.TabIndex = 6;
            this.imgRAMCheck.TabStop = false;
            this.imgRAMCheck.Visible = false;
            // 
            // imgOSCheck
            // 
            this.imgOSCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.imgOSCheck.Image = global::Athena.Deploy.Properties.Resources.CheckPlay;
            this.imgOSCheck.Location = new System.Drawing.Point(9, 7);
            this.imgOSCheck.Margin = new System.Windows.Forms.Padding(5, 7, 5, 5);
            this.imgOSCheck.Name = "imgOSCheck";
            this.imgOSCheck.Size = new System.Drawing.Size(16, 16);
            this.imgOSCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgOSCheck.TabIndex = 5;
            this.imgOSCheck.TabStop = false;
            // 
            // txtOSStatus
            // 
            this.txtOSStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOSStatus.AutoSize = true;
            this.txtOSStatus.Location = new System.Drawing.Point(200, 8);
            this.txtOSStatus.Margin = new System.Windows.Forms.Padding(10, 8, 10, 5);
            this.txtOSStatus.Name = "txtOSStatus";
            this.txtOSStatus.Size = new System.Drawing.Size(220, 16);
            this.txtOSStatus.TabIndex = 4;
            this.txtOSStatus.Text = "Checking...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 8, 5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Operating System";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 63);
            this.label4.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Disk space";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "IIS 7 Server";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = ".Net Framework 4.0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 148);
            this.label7.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Search Server 2010 Express";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 206);
            this.label9.Margin = new System.Windows.Forms.Padding(10, 6, 5, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Existing Installation";
            // 
            // txtRAMStatus
            // 
            this.txtRAMStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRAMStatus.AutoSize = true;
            this.txtRAMStatus.Location = new System.Drawing.Point(200, 35);
            this.txtRAMStatus.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtRAMStatus.Name = "txtRAMStatus";
            this.txtRAMStatus.Size = new System.Drawing.Size(220, 17);
            this.txtRAMStatus.TabIndex = 20;
            this.txtRAMStatus.Text = "Checking...";
            this.txtRAMStatus.Visible = false;
            // 
            // txtDiskStatus
            // 
            this.txtDiskStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiskStatus.AutoSize = true;
            this.txtDiskStatus.Location = new System.Drawing.Point(200, 63);
            this.txtDiskStatus.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtDiskStatus.Name = "txtDiskStatus";
            this.txtDiskStatus.Size = new System.Drawing.Size(220, 17);
            this.txtDiskStatus.TabIndex = 21;
            this.txtDiskStatus.Text = "Checking...";
            this.txtDiskStatus.Visible = false;
            // 
            // txtIISStatus
            // 
            this.txtIISStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIISStatus.AutoSize = true;
            this.txtIISStatus.Location = new System.Drawing.Point(200, 91);
            this.txtIISStatus.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtIISStatus.Name = "txtIISStatus";
            this.txtIISStatus.Size = new System.Drawing.Size(220, 17);
            this.txtIISStatus.TabIndex = 22;
            this.txtIISStatus.Text = "Checking...";
            this.txtIISStatus.Visible = false;
            // 
            // txtDotNetStatus
            // 
            this.txtDotNetStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDotNetStatus.AutoSize = true;
            this.txtDotNetStatus.Location = new System.Drawing.Point(200, 119);
            this.txtDotNetStatus.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtDotNetStatus.Name = "txtDotNetStatus";
            this.txtDotNetStatus.Size = new System.Drawing.Size(220, 18);
            this.txtDotNetStatus.TabIndex = 23;
            this.txtDotNetStatus.Text = "Checking...";
            this.txtDotNetStatus.Visible = false;
            // 
            // txtSearchServerStatus
            // 
            this.txtSearchServerStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchServerStatus.AutoSize = true;
            this.txtSearchServerStatus.Location = new System.Drawing.Point(200, 148);
            this.txtSearchServerStatus.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtSearchServerStatus.Name = "txtSearchServerStatus";
            this.txtSearchServerStatus.Size = new System.Drawing.Size(220, 19);
            this.txtSearchServerStatus.TabIndex = 24;
            this.txtSearchServerStatus.Text = "Checking...";
            this.txtSearchServerStatus.Visible = false;
            // 
            // txtMCPStatus
            // 
            this.txtMCPStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMCPStatus.AutoSize = true;
            this.txtMCPStatus.Location = new System.Drawing.Point(200, 206);
            this.txtMCPStatus.Margin = new System.Windows.Forms.Padding(10, 6, 10, 5);
            this.txtMCPStatus.Name = "txtMCPStatus";
            this.txtMCPStatus.Size = new System.Drawing.Size(220, 77);
            this.txtMCPStatus.TabIndex = 26;
            this.txtMCPStatus.Text = "Checking...";
            this.txtMCPStatus.Visible = false;
            // 
            // PrerequisiteControl
            // 
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.tableLayout);
            this.Name = "PrerequisiteControl";
            this.Size = new System.Drawing.Size(430, 288);
            this.tableLayout.ResumeLayout(false);
            this.tableLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDesktopExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMCPCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSearchServerCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDotNetCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgIISCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgDiskCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRAMCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOSCheck)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayout;
        private System.Windows.Forms.PictureBox imgOSCheck;
        private System.Windows.Forms.Label txtOSStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox imgMCPCheck;
        private System.Windows.Forms.PictureBox imgSearchServerCheck;
        private System.Windows.Forms.PictureBox imgDotNetCheck;
        private System.Windows.Forms.PictureBox imgIISCheck;
        private System.Windows.Forms.PictureBox imgDiskCheck;
        private System.Windows.Forms.PictureBox imgRAMCheck;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label txtRAMStatus;
        private System.Windows.Forms.Label txtDiskStatus;
        private System.Windows.Forms.Label txtIISStatus;
        private System.Windows.Forms.Label txtDotNetStatus;
        private System.Windows.Forms.Label txtSearchServerStatus;
        private System.Windows.Forms.Label txtMCPStatus;
        private System.Windows.Forms.PictureBox imgDesktopExp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtDesktopExp;


    }
}

