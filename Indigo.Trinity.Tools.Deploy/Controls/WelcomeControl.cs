using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Diagnostics;

/// -------------------------------------------------------------------------------------------------------------
/// IMPORTANT: DO NOT ADD REFERENCES to SharePoint or any other assembilies that is NOT part of setup or payload
///             BEFORE pre-requisites are checked.
/// -------------------------------------------------------------------------------------------------------------

namespace Athena.Deploy
{
    public partial class WelcomeControl : InstallerControl
    {
        public WelcomeControl()
        {
            InitializeComponent();
        }

        protected internal override void Open(InstallOptions options)
        {
            
            this.Form.SetupState = InstallationState.Ready;
            Utilities.Log("Environment: User: " + Environment.UserDomainName + "\\" + Environment.UserName);
            Utilities.Log("Environment: Machine: " + Environment.MachineName);
            Utilities.Log("Environment: CPUs: " + Environment.ProcessorCount);
            Utilities.Log("Environment: Platform: " + ((PlatformHelper.Is64BitOperatingSystem) ? "64-bit" : "32-bit"));
            Utilities.Log("Environment: OS Version: " + Environment.OSVersion.Version + " SP: " + Environment.OSVersion.ServicePack);
            Utilities.Log("Setup Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        protected internal override void Close(InstallOptions options)
        {

        }

        #region Helper Functions
        class PlatformHelper
        {
            public static bool Is64BitProcess = (IntPtr.Size == 8);
            public static bool Is64BitOperatingSystem
            {
                get { return Is64BitProcess || InternalCheckIsWow64(); }
            }

            [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
            [return: MarshalAs(UnmanagedType.Bool)]
            private static extern bool IsWow64Process(
                [In] IntPtr hProcess,
                [Out] out bool wow64Process
            );

            [MethodImpl(MethodImplOptions.NoInlining)]
            private static bool InternalCheckIsWow64()
            {
                try
                {
                    if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                        Environment.OSVersion.Version.Major >= 6)
                    {
                        using (Process p = Process.GetCurrentProcess())
                        {
                            bool retVal;
                            if (!IsWow64Process(p.Handle, out retVal))
                            {
                                return false;
                            }
                            return retVal;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch { return false; }
            }
        }
        #endregion

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
