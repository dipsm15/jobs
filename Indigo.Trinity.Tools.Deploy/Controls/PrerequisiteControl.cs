using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Management;
using System.Reflection;
using System.DirectoryServices;
using System.IO;

using Microsoft.Web.Administration;
using System.Diagnostics;
using System.Runtime.InteropServices;

/// -------------------------------------------------------------------------------------------------------------
/// IMPORTANT: DO NOT ADD REFERENCES to SharePoint or any other assembilies that is NOT part of setup or payload
///             BEFORE pre-requisites are checked.
/// -------------------------------------------------------------------------------------------------------------

namespace Athena.Deploy
{
    public partial class PrerequisiteControl : InstallerControl, IDoAsync
    {
        #region Component State
        enum ComponentState { InProgress, Success, Error, Warning };
        class ComponentStatus
        {
            public string Message { get; set; }
            public ComponentState State { get; set; }

            public ComponentStatus SetError(string message)
            {
                this.State = ComponentState.Error;
                this.Message = message;
                return this;
            }

            public ComponentStatus SetSuccess(string message)
            {
                this.State = ComponentState.Success;
                this.Message = message;
                return this;
            }

            public ComponentStatus SetWarning(string message)
            {
                this.State = ComponentState.Warning;
                this.Message = message;
                return this;
            }
        }
        #endregion

        #region Private Members
        private int _port = 8000;
        private int _minRAM = 2 * 1024; //2 GB Minimum //TODO: Before Publishing increase to 4 GB
        private InstallOptions _options = null;
        #endregion

        #region Constructor
        public PrerequisiteControl() { InitializeComponent(); }
        #endregion

        #region Overridden Methods
        protected internal override void Open(InstallOptions options)
        {
            this.Form.SetupState = InstallationState.PreRequisitesCheck;
            this.Form.NextButton.Enabled = false;
            this.Form.Refresh();
            _options = options;
            Thread thread = new Thread(new ThreadStart(DoWorkAsync));
            thread.Start();
        }
        #endregion

        #region Private Helper Functions
        private ComponentStatus CheckMCP(InstallOptions options)
        {
            ComponentStatus status = new ComponentStatus();
            List<int> ports = new List<int>();
            string message = string.Empty;

            bool result_1 = false;
            try
            {
                Utilities.Log("Detecting existing installation...");
                ServerManager serverMgr = new ServerManager();
                SiteCollection sites = serverMgr.Sites;
                bool result = false;
                foreach (Site _site in sites)
                {
                    Microsoft.Web.Administration.BindingCollection bindings = _site.Bindings;
                    foreach (Microsoft.Web.Administration.Binding binding in bindings)
                    {
                        //Utilities.Log("Existing site: " + _site.Name + ":" + binding.EndPoint.Port.ToString());
                        if (binding.EndPoint != null && binding.EndPoint.Port == 80)
                            if (_site.State.ToString().ToLower() != "stopped")
                            {
                                /* _site.Stop();
                                  serverMgr.CommitChanges();*/
                                options.UISite = _site;
                                message = string.Format("{0} site will be deleted. ", _site.Name);
                                result = true;
                            }
                            else
                                message = "MCP UI site will be deployed at port 80. ";

                        if (binding.EndPoint != null && binding.EndPoint.Port == _port)
                        {
                            string url = string.Format("http://{0}:{1}", Environment.MachineName, _port);
                            Microsoft.SharePoint.SPSite site;
                            try { site = new Microsoft.SharePoint.SPSite(url); }
                            catch { site = null; }
                            if (site != null && site.OpenWeb("/mcp").Exists)
                            {
                                options.ServicePort = string.Format("8888");
                                options.SiteUrl = string.Format("http://{0}:{1}/mcp", Environment.MachineName, _port);
                                options.portNo = (new Uri(options.SiteUrl)).Port;
                                options.UISiteUrl = string.Format("http://{0}", Environment.MachineName);
                                options.UIPortNo = (new Uri(options.UISiteUrl)).Port;
                                options.IsNew = false;
                                options.AppPoolName = Environment.MachineName + "-" + options.portNo.ToString();
                                result_1 = true;
                                return status.SetWarning(string.Format("SharePoint Web-Application at port {0} will be re-installed.", _port));
                            }
                            else
                                result_1 = false;
                        }
                        if (binding.EndPoint != null && binding.EndPoint.Port > 1023)
                        {
                            ports.Add(binding.EndPoint.Port);
                        }
                    }
                }

                bool _result = ports.Contains(_port);
                do
                {
                    if (_result)
                        _port += 1000;
                    _result = ports.Contains(_port);
                } while (_result != false);
                message += (string.Format("Sharepoint site will be installed at port:{0}", _port));
                if (!result_1)
                {
                    options.SiteUrl = string.Format("http://{0}:{1}/mcp", Environment.MachineName, _port);
                    options.portNo = (new Uri(options.SiteUrl)).Port;
                }
                options.UISiteUrl = string.Format("http://{0}", Environment.MachineName);
                options.UIPortNo = (new Uri(options.UISiteUrl)).Port;

                if (!result)
                    return status.SetSuccess(message);

                return status.SetWarning(message);
            }
            catch (Exception ex)
            {
                Utilities.Log("Status of existing MCP installation could not be determined.", ex);
                return status.SetWarning("Status of existing MCP installation could not be determined.");
            }
        }

        private ComponentStatus CheckDisk(InstallOptions options)
        {
            ComponentStatus status = new ComponentStatus();

            try
            {
                DriveInfo[] drives = DriveInfo.GetDrives();
                DriveInfo selectedDrive = null;
                foreach (var drive in drives)
                {
                    if (drive.IsReady)
                        Utilities.Log(string.Format("Disk-space: {0} {1} {2}/{3} free", drive.Name, drive.DriveType, drive.AvailableFreeSpace, drive.TotalSize));

                    if (drive.DriveType == DriveType.Fixed && drive.IsReady &&
                        (selectedDrive == null || selectedDrive.AvailableFreeSpace < drive.AvailableFreeSpace))
                        selectedDrive = drive;
                }

                double diskSpace = selectedDrive.AvailableFreeSpace / 1073741824;
                if (diskSpace > 1)
                {
                    options.MCPFolder = selectedDrive.Name;
                    Utilities.Log("Selected drive: " + selectedDrive.Name);
                    return status.SetSuccess(string.Format("{0} {1} GB free", selectedDrive.Name, diskSpace.ToString("F")));
                }
                else if (diskSpace > 20)
                {
                    options.MCPFolder = selectedDrive.Name;
                    Utilities.Log("Selected drive (with size warning): " + selectedDrive.Name, Utilities.MessageType.Warning);
                    return status.SetWarning(string.Format("{0} {1} GB free (Minimum: 100 GB)", selectedDrive.Name, diskSpace.ToString("F")));
                }
                else
                    return status.SetError(string.Format("{0} {1} GB free (Minimum: 100 GB)", selectedDrive.Name, diskSpace.ToString("F")));
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in checking disk-space: ", ex);
                return status.SetWarning("Could not be determined.");
            }
        }

        #region Get RAM for the Machine
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private class MEMORYSTATUSEX
        {
            public uint dwLength;
            public uint dwMemoryLoad;
            public ulong ullTotalPhys;
            public ulong ullAvailPhys;
            public ulong ullTotalPageFile;
            public ulong ullAvailPageFile;
            public ulong ullTotalVirtual;
            public ulong ullAvailVirtual;
            public ulong ullAvailExtendedVirtual;
            public MEMORYSTATUSEX()
            {
                this.dwLength = (uint)Marshal.SizeOf(typeof(MEMORYSTATUSEX));
            }
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool GlobalMemoryStatusEx([In, Out] MEMORYSTATUSEX lpBuffer);
        private ComponentStatus CheckRam()
        {
            ComponentStatus status = new ComponentStatus();
            try
            {
                double totalCapacity = 0;

                MEMORYSTATUSEX memStatus = new MEMORYSTATUSEX();
                if (GlobalMemoryStatusEx(memStatus)) totalCapacity = memStatus.ullTotalPhys;

                totalCapacity /= (1024.0 * 1024.0);
                totalCapacity = Math.Round(totalCapacity);
                if (totalCapacity >= _minRAM - 5) //Substracting 5, some times the total capacity falls short by 1 or 2 MB
                    return status.SetSuccess(string.Format("{0} MB", totalCapacity));

                Utilities.Log("Total RAM (in MB): " + totalCapacity.ToString());
                return status.SetError(string.Format("{0} MB (Minimum Recommended: {1} MB)", totalCapacity, _minRAM));
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in checking RAM: ", ex);
                return status.SetWarning("Could not be determined.");
            }
        }
        #endregion

        private ComponentStatus CheckNetFrameworks()
        {
            string netKey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP";

            ComponentStatus status = new ComponentStatus();

            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey(netKey);
                if (key != null)
                {
                    List<string> frameworks = new List<string>(key.GetSubKeyNames());
                    Utilities.Log(".Net Frameworks found: " + string.Join(",", frameworks.ToArray()));

                    if (!frameworks.Contains("v2.0.50727"))
                        return status.SetError(".Net Framework 2.0 is not installed.");
                    else if (!frameworks.Contains("v3.0"))
                        return status.SetError(".Net Framework 3.0 is not installed.");
                    else if (!frameworks.Contains("v3.5"))
                        return status.SetError(".Net Framework 3.5 is not installed.");
                    else if (!frameworks.Contains("v4.0"))
                        return status.SetError(".Net Framework 4.0 is not installed.");
                    return status.SetSuccess("Installed.");
                }
                else
                {
                    Utilities.Log(".Net Framework not installed.", Utilities.MessageType.Warning);
                    return status.SetError(".Net Framework is not installed.");
                }
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in checking .Net Framework: ", ex);
                return status.SetError("Could not be determined.");
            }
        }

        private ComponentStatus CheckSearch()
        {
            string name32 = @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\14.0\WSS\InstalledProducts\90140000-1014-0000-1000-0000000FF1CE";
            string name64 = @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\14.0\WSS\InstalledProducts\95140000-1137-0000-1000-0000000FF1CE";
            string wssKey = @"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\14.0";
            ComponentStatus status = new ComponentStatus();

            try
            {
                AppDomain.CurrentDomain.Load("Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c");
            }
            catch
            {
                Utilities.Log("SharePoint is not installed.");
                return status.SetError("SharePoint is not installed.");
            }

            try
            {
                try
                {
                    RegistryKey wssVersion = Registry.LocalMachine.OpenSubKey(wssKey);
                    if (wssVersion != null && wssVersion.GetValue("location") != null)
                    {
                        string owssvrPath = Path.Combine(wssVersion.GetValue("location").ToString(), "ISAPI\\owssvr.dll");
                        if (!File.Exists(owssvrPath))
                            return status.SetError("WSS 3.0 SP2 is not installed.");
                    }
                    else
                        return status.SetError("WSS 3.0 SP2 is not installed.");
                }
                catch (Exception ex)
                {
                    Utilities.Log("WSS 3.0 version could not be determined.", ex);
                    return status.SetWarning("WSS 3.0 version could not be determined.");
                }


                RegistryKey key = Registry.LocalMachine.OpenSubKey(name32);

                if (key != null)
                {
                    Utilities.Log("Search Server 2008 Express (32-bit) is installed");
                    return status.SetSuccess("Installed.");
                }

                key = Registry.LocalMachine.OpenSubKey(name64);
                if (key != null)
                {
                    Utilities.Log("Search Server 2008 Express (64-bit) is installed");
                    return status.SetSuccess("Installed.");
                }

                Utilities.Log("Search Server 2008 Express is not installed.", Utilities.MessageType.Warning);
                return status.SetError("Search Server 2008 Express is not installed.");
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in checking Search Server 2008 Express: ", ex);
                return status.SetError("Could not be determined.");
            }
        }

        private ComponentStatus CheckOS()
        {
            ComponentStatus status = new ComponentStatus();
            try
            {
                Utilities.Log(string.Format("OS: {0} {1} ({2})", OSInfo.Name, OSInfo.Edition, OSInfo.VersionString));
                if (OSInfo.Name.ToLower().Equals("windows server 2008"))
                    status.SetSuccess(OSInfo.Name);
                else
                    status.SetError("Required OS: Windows Server 2008. \r\n Current OS: " + OSInfo.Name);
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in checking operating system: ", ex);
                status.SetError("Could not be determined.");
            }

            return status;
        }

        private ComponentStatus CheckIIS()
        {
            ComponentStatus status = new ComponentStatus();
            status.SetSuccess("Installed.");
            try
            {
                string version = @"Software\Microsoft\InetStp";
                string IISPath = Environment.GetEnvironmentVariable("windir") + @"\system32\inetsrv";
                if (Directory.Exists(IISPath) && File.Exists(IISPath + @"\InetMgr.exe"))
                {
                    RegistryKey key = Registry.LocalMachine.OpenSubKey(version);
                    if (key == null || !key.GetValue("MajorVersion").Equals(7))
                        status.SetError("IIS 7 is not installed.");
                }
                else
                    status.SetError("IIS Server is not installed.");
            }
            catch
            {
                status.SetError("Could not be determined.");
            }

            if (status.State == ComponentState.Success)
                Utilities.Log("IIS 7 is installed.");
            else
                Utilities.Log("IIS 7 Check: " + status.Message, Utilities.MessageType.Warning);

            return status;
        }

        //On windows server 2008, Windows Media Player is using Desktop Experience
        //So to check is desktop experience is installed, checking Windows Media Player exists or not
        //Same is done for Encoder
        private ComponentStatus CheckDesktopExperience()
        {
            ComponentStatus status = new ComponentStatus();
            try
            {
                string programFilesPath = Environment.GetEnvironmentVariable("ProgramFiles(x86)");
                string wmPlayerPath = Path.Combine(programFilesPath, "Windows Media Player\\wmplayer.exe");
                string encoderPath = Path.Combine(programFilesPath, "Microsoft Expression\\Encoder 4\\Encoder.exe");

                if (File.Exists(wmPlayerPath) && File.Exists(encoderPath)) return status.SetSuccess("Installed.");
                else if (File.Exists(wmPlayerPath) && File.Exists(encoderPath) == false) return status.SetWarning("Encoder not installed.");
                else if (File.Exists(wmPlayerPath) == false && File.Exists(encoderPath)) return status.SetWarning("Desktop Experience not installed.");
                else return status.SetWarning("None of the component installed.");
            }
            catch (Exception ex) { Utilities.Log("Error in CheckDesktopExperience.", ex); }
            return status.SetError("Could not be determined.");
        }

        protected internal override void Close(InstallOptions options)
        {

        }

        private void _SetIcon(PictureBox image, ComponentState state)
        {
            switch (state)
            {
                case ComponentState.InProgress: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckPlay; }); break;
                case ComponentState.Success: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckOk; }); break;
                case ComponentState.Error: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckFail; }); break;
                case ComponentState.Warning: image.Invoke(() => { image.Image = global::Athena.Deploy.Properties.Resources.CheckWait; }); break;
            }
            image.Invoke(() => { image.Visible = true; });
        }

        private void _SetMessage(Label lblMessage, string message)
        {
            lblMessage.Invoke(() => { lblMessage.Text = message; });
            lblMessage.Invoke(() => { lblMessage.Visible = true; });
        }
        #endregion

        #region IDoAsync
        public void DoWorkAsync()
        {
            Thread.Sleep(100);
            ComponentStatus status;
            ComponentState setupState = ComponentState.Success;

            // 1. Check OS
            _SetIcon(imgOSCheck, ComponentState.InProgress);
            _SetMessage(txtOSStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckOS();
            _SetIcon(imgOSCheck, status.State);
            _SetMessage(txtOSStatus, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check OS: {0} {1}", status.State, status.Message));

            // 2. Check RAM
            _SetIcon(imgRAMCheck, ComponentState.InProgress);
            _SetMessage(txtRAMStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckRam();
            _SetIcon(imgRAMCheck, status.State);
            _SetMessage(txtRAMStatus, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check RAM: {0} {1}", status.State, status.Message));

            // 3. Check Disk space
            _SetIcon(imgDiskCheck, ComponentState.InProgress);
            _SetMessage(txtDiskStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckDisk(_options);
            _SetIcon(imgDiskCheck, status.State);
            _SetMessage(txtDiskStatus, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check Disk space: {0} {1}", status.State, status.Message));

            // 4. Check IIS
            _SetIcon(imgIISCheck, ComponentState.InProgress);
            _SetMessage(txtIISStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckIIS();
            _SetIcon(imgIISCheck, status.State);
            _SetMessage(txtIISStatus, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check IIS: {0} {1}", status.State, status.Message));

            // 5. Check .Net Frameworks
            _SetIcon(imgDotNetCheck, ComponentState.InProgress);
            _SetMessage(txtDotNetStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckNetFrameworks();
            _SetIcon(imgDotNetCheck, status.State);
            _SetMessage(txtDotNetStatus, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check .Net Frameworks: {0} {1}", status.State, status.Message));

            // 6. Check Search Server
            _SetIcon(imgSearchServerCheck, ComponentState.InProgress);
            _SetMessage(txtSearchServerStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckSearch();
            _SetIcon(imgSearchServerCheck, status.State);
            _SetMessage(txtSearchServerStatus, status.Message);
            Utilities.Log(string.Format("Check Search Server 2008 Express: {0} {1}", status.State, status.Message));
            if (status.State == ComponentState.Error)
            {
                setupState = ComponentState.Error;
                _SetIcon(imgMCPCheck, ComponentState.Warning);
                _SetMessage(txtMCPStatus, "Check aborted");
                _SetIcon(imgDesktopExp, ComponentState.Warning);
                _SetMessage(txtDesktopExp, "Check aborted");
                return;
            }

            // 7. Check MCP
            _SetIcon(imgDesktopExp, ComponentState.InProgress);
            _SetMessage(txtDesktopExp, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckDesktopExperience();
            _SetIcon(imgDesktopExp, status.State);
            _SetMessage(txtDesktopExp, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check Smooth Streaming Pre-Requisites: {0} {1}", status.State, status.Message));
            this.Invoke(() => { this.Refresh(); });

            // 8. Check MCP
            _SetIcon(imgMCPCheck, ComponentState.InProgress);
            _SetMessage(txtMCPStatus, "Checking...");
            this.Invoke(() => { this.Refresh(); });
            status = CheckMCP(_options);
            _SetIcon(imgMCPCheck, status.State);
            _SetMessage(txtMCPStatus, status.Message);
            if (status.State == ComponentState.Error) setupState = ComponentState.Error;
            Utilities.Log(string.Format("Check Existing installation: {0} {1}", status.State, status.Message));

            this.Invoke(() => { this.Refresh(); });
            if (Form != null)
            {
                Form.Invoke(() => { Form.NextButton.Enabled = (setupState == ComponentState.Error) ? false : true; });
                Form.SetSubTitle("Pre-requistes check completed.");
            }
        }
        #endregion
    }
}

