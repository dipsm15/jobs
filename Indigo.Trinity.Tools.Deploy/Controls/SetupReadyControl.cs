﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Athena.Deploy
{
    public partial class SetupReadyControl : InstallerControl
    {
        public SetupReadyControl()
        {
            InitializeComponent();
        }

        protected internal override void Open(InstallOptions options)
        {
            this.Form.SetupState = InstallationState.ReadyToInstall;
            Utilities.Log(options.ToString());
        }
    }
}
