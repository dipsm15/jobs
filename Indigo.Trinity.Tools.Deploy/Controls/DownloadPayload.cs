﻿ using System;
using System.Drawing;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace Athena.Deploy
{
    public partial class DownloadPayload : InstallerControl, IDoAsync
    {
        #region Private Variables
        private InstallOptions _options = null;
        #endregion

        #region Constructor
        public DownloadPayload()
        {
            InitializeComponent();
        }
        #endregion

        #region Internal Methods
        protected internal void ProgressChanged() { progressDownload.Invoke(() => { progressDownload.Value++; }); }
        protected internal override void Open(InstallOptions options)
        {
            try
            {
                if (Form != null) Form.NextButton.Invoke(() => { Form.NextButton.Enabled = false; });
                _options = options;
                Thread thread = new Thread(new ThreadStart(DoWorkAsync));
                thread.Start();
            }
            catch (Exception ex) { Utilities.Log("DownloadPayload Open : ", ex); }
        }
        #endregion

        #region Event Handlers
        private void buttonTryAgain_Click(object sender, EventArgs e) { Open(_options); }
        #endregion

        #region IDoAsync Methods
        public void DoWorkAsync()
        {
            progressDownload.Invoke(() => { progressDownload.Value = 0; });
            labelStatus.Invoke(() => { labelStatus.Text = "Downloading Payload"; });
            labelStatus.Invoke(() => { labelStatus.ForeColor = Color.Green; });
            buttonTryAgain.Invoke(() => { buttonTryAgain.Visible = false; });
            try
            {
                WebProxy proxy = null;
                bool useProxy = false;
                bool.TryParse(_options.UseProxy, out useProxy);
                if (useProxy)
                {
                    proxy = new WebProxy(_options.ProxyAddressPort);
                    if (string.IsNullOrEmpty(_options.ProxyUserName)) proxy.UseDefaultCredentials = true;
                    else proxy.Credentials = new NetworkCredential(_options.ProxyUserName, _options.ProxyPassword);
                }
                Program.PayloadManager.DownloadFiles(proxy, this);
                progressDownload.Invoke(() => { progressDownload.Value = progressDownload.Maximum; });
                labelStatus.Invoke(() => { labelStatus.Text = "Successfully Downloaded"; });
                if (Form != null) Form.NextButton.Invoke(() => { Form.NextButton.Enabled = true; });
            }
            catch (Exception ex)
            {
                Utilities.Log("DownloadPayload Command: ", ex);
                progressDownload.Invoke(() => { progressDownload.Value = 0; });
                labelStatus.Invoke(() => { labelStatus.Text = "Error in Download"; });
                labelStatus.Invoke(() => { labelStatus.ForeColor = Color.Red; });
                labelErrorMsg.Invoke(() => { labelErrorMsg.Text = "If you are getting frequent error status then you might need to check your proxy settings and network configuration"; });
                buttonTryAgain.Invoke(() => { buttonTryAgain.Visible = true; });
            }
        }
        #endregion
    }
}
