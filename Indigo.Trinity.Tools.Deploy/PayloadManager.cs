﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Xml;
using System.Threading;

namespace Athena.Deploy
{
    public class PayloadManager
    {
        public PayloadManager(string baseFolder, string workingFolder, string manifestFile)
        {
            if (!Directory.Exists(baseFolder))
                throw new DirectoryNotFoundException(baseFolder);

            if (!File.Exists(manifestFile))
                throw new FileNotFoundException(manifestFile);

            _baseFolder = baseFolder;
            _workingFolder = workingFolder;
            _LoadManifest(manifestFile);
        }
        private string _baseFolder = string.Empty;
        private string _workingFolder = string.Empty;
        //private string _manifestFile = string.Empty;
        private Dictionary<string, PayloadItem> _items = new Dictionary<string, PayloadItem>();
        public void DownloadFiles(WebProxy proxy, DownloadPayload downloadPayload)
        {
            // 1. Get files to be downloaded
            List<string> downloadUrls = new List<string>();
            foreach (var item in _items.Values)
            {
                string downloadUrl = _GetDownloadUrl(item);
                if (!string.IsNullOrEmpty(downloadUrl) && !downloadUrls.Contains(downloadUrl.ToLower()))
                    downloadUrls.Add(downloadUrl.ToLower());
            }

            // 2. Download files 
            List<string> files = new List<string>();
            WebClient webClient = null;
            foreach (string url in downloadUrls)
            {
                string targetPath = Path.Combine(_workingFolder, Path.GetFileName(url));
                try
                {
                    if (!File.Exists(targetPath))
                    {
                        try
                        {
                            webClient = new WebClient();
                            webClient.Proxy = proxy;
                            webClient.DownloadFile(url, targetPath);
                        }
                        catch (WebException ex) { Utilities.Log("Error downloading file: " + url, ex); _DeleteFile(targetPath); }
                        finally { webClient.Proxy = null; webClient.Dispose(); }
                    }
                    else Utilities.Log("Skipping already downloaded file: " + targetPath);
                }
                catch (Exception ex) { Utilities.Log("Error in DownloadFiles: " + url, ex); }
                files.Add(targetPath);
                downloadPayload.ProgressChanged();
            }

            // 3. Extract Zip files, if any
            UnZipFiles(downloadPayload);
        }

        public void UnZipFiles(DownloadPayload downloadPayload)
        {
            string zipFile = string.Empty;
            foreach (var item in _items.Values)
            {
                zipFile = _GetZipPath(item);
                if (string.IsNullOrEmpty(zipFile) || !File.Exists(zipFile)) continue;

                // Avoid un-necessary unzipping
                if (item.IsDirectory && Directory.Exists(_GetLocalPath(item))) continue;
                else if (!item.IsDirectory && File.Exists(_GetLocalPath(item))) continue;

                try
                {
                    Utilities.Log("Unzipping file: '" + zipFile + "' To: " + _workingFolder);
                    Utilities.UnZip(zipFile, _workingFolder);
                }
                catch (Exception ex)
                {
                    Utilities.Log("Error unzipping file: " + zipFile, ex);
                    throw;
                }
                downloadPayload.ProgressChanged();
            }
        }

        /// <summary>Gets the local path for specified payload</summary>
        public string GetPath(string payloadKey)
        {
            if (!_items.ContainsKey(payloadKey))
                throw new KeyNotFoundException(payloadKey);

            PayloadItem item = _items[payloadKey];
            string localPath = _GetLocalPath(item);

            if (item.IsDirectory && !Directory.Exists(localPath)) throw new DirectoryNotFoundException(_items[payloadKey].RelativePath);
            else if (!item.IsDirectory && !File.Exists(localPath)) throw new FileNotFoundException(_items[payloadKey].RelativePath);

            return localPath;
        }

        #region Helper Functions
        private void _LoadManifest(string manifestFile)
        {
            #region .Net 3.5: XDcoument implementation
            //XmlDocument xDoc = XDocument.Load(manifestFile);
            //foreach (var item in xDoc.Root.Elements("Item"))
            //{
            //    string key = item.Attribute(XName.Get("Key")).Value;
            //    XAttribute attrib = null;
            //    _items.Add(key, new PayloadItem()
            //    {
            //        Key = key,
            //        DownloadUrl = ((attrib = item.Attribute(XName.Get("DownloadUrl"))) != null) ? attrib.Value : string.Empty,
            //        RelativePath = ((attrib = item.Attribute(XName.Get("RelativePath"))) != null) ? attrib.Value : string.Empty,
            //        ZipFile = ((attrib = item.Attribute(XName.Get("ZipFile"))) != null) ? attrib.Value : string.Empty,
            //        IsDirectory = ((attrib = item.Attribute(XName.Get("IsDirectory"))) != null) ? bool.Parse(attrib.Value) : false,
            //    });
            //}
            #endregion

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(manifestFile);
            foreach (XmlNode item in xDoc.SelectNodes("//Item"))
            {
                string key = item.Attributes["Key"].Value;
                XmlAttribute attrib = null;
                _items.Add(key, new PayloadItem()
                {
                    Key = key,
                    DownloadUrl = ((attrib = item.Attributes["DownloadUrl"]) != null) ? attrib.Value : string.Empty,
                    RelativePath = ((attrib = item.Attributes["RelativePath"]) != null) ? attrib.Value : string.Empty,
                    ZipFile = ((attrib = item.Attributes["ZipFile"]) != null) ? attrib.Value : string.Empty,
                    IsDirectory = ((attrib = item.Attributes["IsDirectory"]) != null) ? bool.Parse(attrib.Value) : false,
                });
            }
        }

        private string _GetLocalPath(PayloadItem item)
        {
            string localFolder = (string.IsNullOrEmpty(item.ZipFile)) ? _baseFolder : _workingFolder;
            return Path.Combine(localFolder, item.RelativePath);
        }

        private string _GetDownloadUrl(PayloadItem item)
        {
            string downloadFile = (string.IsNullOrEmpty(item.ZipFile)) ? item.RelativePath.TrimStart('/', '\\') : item.ZipFile;
            return (string.IsNullOrEmpty(item.DownloadUrl)) ? "" : item.DownloadUrl.TrimEnd('/', '\\') + "/" + downloadFile;
        }

        private string _GetZipPath(PayloadItem item)
        {
            string localFolder = (string.IsNullOrEmpty(item.DownloadUrl)) ? _baseFolder : _workingFolder;
            return (string.IsNullOrEmpty(item.ZipFile)) ? "" : Path.Combine(localFolder, item.ZipFile);
        }
        private void _DeleteFile(string path) { try { if (File.Exists(path)) File.Delete(path); } catch { } }
        #endregion
    }

    public class PayloadItem
    {
        public string Key { get; set; }
        public string DownloadUrl { get; set; }
        public string RelativePath { get; set; }
        public string ZipFile { get; set; }
        public bool IsDirectory { get; set; }

        public PayloadItem()
        {
            this.IsDirectory = false;
        }
    }

    /// <summary>This exception is thrown when required file is not yet downloaded.</summary>
    public class DownloadRequiredException : ApplicationException
    {
        public string DownloadUrl { get; private set; }
        public DownloadRequiredException(string downloadUrl)
        {
            this.DownloadUrl = downloadUrl;
        }
    }
}
