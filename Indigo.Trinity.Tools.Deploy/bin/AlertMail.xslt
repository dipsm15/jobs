<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <html>
      <head>
        <title>Alerts mail</title>
        <style type="text/css">
          body { font-family:verdana; font-size:8pt; margin:0px; background-color: #CDE5FD;}
        </style>
      </head>
      <body>
        <div id="background" style="background-color: #CDE5FD; padding: 5px; margin: 0px">

          <div style="background-color: White; border: silver solid 1px; padding: 0px;">
            <table style="width: 100%; background-color: White; padding: 0px;">
              <tr>
                <td>
                  <div id="mailTitle" style="clear: both; height: 25px; font-family: Trebuchet MS;">
                    <table style="width: 100%">
                      <tr>
                        <td style="font-size: 14pt;">
                          Microsoft Community Portal Alerts
                        </td>
                        <td style="font-size: 10pt; color: Gray; width: 200px; vertical-align: bottom; text-align: right">
                          $Date$
                        </td>
                      </tr>
                    </table>
                  </div>
                  <hr style="border: black solid 1px;" />

                  <xsl:for-each select="/Alerts/Category">
                    <div id="categorySection" style="clear: both; margin-bottom: 10px;">

                      <div id="categoryTitle" style="border: silver solid 1px; background-color: #f2f2f2; 
                    padding: 2px 5px; font-size: 10pt;">
                        <xsl:value-of select="./@Name"/>
                      </div>


                      <xsl:for-each select="./Item">
                        <div id="item">

                          <table>
                            <tr>
                              <td class="thumbnail" rowspan="2" valign="top">

                                <xsl:if test="@Type='Webcasts'">
                                  <img src="cid:webcasticon" width="50" height="50" />
                                </xsl:if>
                                <xsl:if test="@Type='Podcasts'">
                                  <img src="cid:podcasticon" width="50" height="50" />
                                </xsl:if>
                                <xsl:if test="@Type='Hands on Labs'">
                                  <img src="cid:hands_on_labsicon" width="50" height="50" />
                                </xsl:if>
                                <xsl:if test="@Type='Other Content'">
                                  <img src="cid:othericon" width="50" height="50" />
                                </xsl:if>

                              </td>
                              <td style="font-size:9pt;">
                                <a>
                                  <xsl:attribute name="href">
                                    <xsl:value-of select="./Url" disable-output-escaping="yes"/>
                                  </xsl:attribute>
                                  <xsl:value-of select="./Title"/>
                                </a>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div style="overflow: hidden; height: 25px; font-size:8pt;">
                                  <xsl:value-of select="./Description"/>
                                </div>
                              </td>
                            </tr>
                          </table>
                          <hr style="border: black solid 1px;" />
                        </div>
                      </xsl:for-each>
                    </div>
                  </xsl:for-each>
                  <div style="clear:both; margin-top:15px; border-top:silver solid 1px; padding:5px 5px 0px 5px;">
                    <a href="$SiteUrl$?changealerts">Change Subscription</a>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>