using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Athena.Deploy
{
  public static class LogManager
  {
    private static readonly ILog defaultLogger = new FileLogger();

    public static ILog GetLogger()
    {
      return defaultLogger;
    }

    private class FileLogger : ILog
    {
        private StreamWriter _logStream;
        private void _Write(string type, object message, Exception ex)
        {
            if (_logStream != null)
            {
                _logStream.Write(DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss") + " " + type);
                DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                if (message != null) _logStream.WriteLine(message);
                if (ex != null) _logStream.WriteLine(ex);
            }
        }

        public FileLogger()
        {
            try
            {
                string fileName = "InstallLog" + DateTime.Now.ToString("yyyy.MMM.dd") + ".log";
                _logStream = new StreamWriter(@fileName, true);
                _logStream.AutoFlush = true;
            }
            catch
            {
                _logStream = null;
            }
        }
      public void Info(object message)
      {
          _Write("Info: ", message, null);
      }

      public void Info(object message, Exception t)
      {
          _Write("Info: ", message, t);
      }

      public void Warn(object message)
      {
          _Write("Warning: ", message, null);
      }

      public void Warn(object message, Exception t)
      {
          _Write("Warning: ", message, t);
      }

      public void Error(object message)
      {
          _Write("Error: ", message, null);
      }

      public void Error(object message, Exception t)
      {
          _Write("Error: ", message, t);
      }

      public void Fatal(object message)
      {
          _Write("Fatal: ", message, null);
      }

      public void Fatal(object message, Exception t)
      {
          _Write("Fatal: ", message, t);
      }
    }
  }
}
