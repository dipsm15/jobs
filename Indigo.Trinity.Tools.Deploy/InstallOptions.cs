/******************************************************************/
/*                                                                */
/*                SharePoint Solution Installer                   */
/*                                                                */
/*    Copyright 2007 Lars Fastrup Nielsen. All rights reserved.   */
/*    http://www.fastrup.dk                                       */
/*                                                                */
/*    This program contains the confidential trade secret         */
/*    information of Lars Fastrup Nielsen.  Use, disclosure, or   */
/*    copying without written consent is strictly prohibited.     */
/*                                                                */
/* KML: Added WebApplicationTargets and SitecollectionTargets     */
/*                                                                */
/******************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Microsoft.Web.Administration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.IO;


namespace Athena.Deploy
{
    public enum InstallType
    {
        Install, Update, ReInstall
    }

    public enum InstallationState
    {
        Unknown, Successful, Failed, Aborted, PreRequisitesCheck, GatherPartnerInfo, GatherProxyInfo, GatherMiscInfo, Installing, Ready, ReadyToInstall
    }

    public class InstallOptions
    {
        public InstallOptions()
        {
        }

        public InstallationState SetupState = InstallationState.Ready;
        public int portNo;
        public string DefaultUserName = string.Empty;
        public string DefaultPassword = string.Empty;
        public string ExternalContentDir;
        public string SiteUrl;
        public string ExtContentSiteUrl;
        public string ContentUrl;
        public string SiteShellFile;
        public string EBSFolder;
        public string UISiteUrl;
        public int UIPortNo;
        public string RSSFeedFile;
        public string ContentImportFile;
        public string UploadUrl;
        public string ExportUrl;
        public string DownloadBaseUrl;
        public string Logfile;
        public string MCPInitialContent;
        public string AppPoolName;
        public Site UISite;
        public string DataStore;
        public string ServicePath;
        public string ServicePort;
        public string EncoderServicePath;
        public string i2VideosDataPath;
        //Config
        public string MCPDataFolder;
        public string MCPContent;
        public string MCPPackage;
        public string MCPUpload;
        //Admin
        public string AdminEmail;


        public string PartnerName;
        public string SMTPServer;
        public string AutoupdaterUserName;
        public string AutoupdaterPassword;

        //Schedule Jobs
        public int DownloadhourSchedule;
        public int DownloadminSchedule;
        public DayOfWeek DownloaddaySchedule;
        public bool WeeklyDownload;

        public int ImporthourSchedule;
        public int ImportminSchedule;
        public DayOfWeek ImportdaySchedule;
        public bool WeeklyImport;

        //proxy options
        public string UseProxy;
        public string ProxyAddressPort;
        public string ProxyUserName;
        public string ProxyPassword;
        public string ProxyAuthScheme;

        //Temp Folder
        public string LogFolder =  Environment.GetEnvironmentVariable("windir")+@"\temp";
        public string TempFolder = Environment.GetEnvironmentVariable("windir")+@"\temp";
        public string MCPFolder = string.Empty;

        //Validate Install
        public bool IsNew = true;
        public bool IsLaunchJobsManager = false;

        public InstallType setupType;

        public override string ToString()
        {
            StringBuilder log = new StringBuilder();
            log.AppendLine("Installation Settings:");
            log.AppendFormat("SetupState: {0}|", this.SetupState);
            log.AppendFormat("portNo: {0}|",portNo);
            log.AppendFormat("ExternalContentDir: {0}|",ExternalContentDir);
            log.AppendFormat("SiteUrl: {0}|",SiteUrl);
            log.AppendFormat("ExtContentSiteUrl: {0}|",ExtContentSiteUrl);
            log.AppendFormat("ContentUrl: {0}|",ContentUrl);
            log.AppendFormat("SiteShellFile: {0}|",SiteShellFile);
            log.AppendFormat("EBSFolder: {0}|",EBSFolder);
            log.AppendFormat("UISiteUrl: {0}|",UISiteUrl);
            log.AppendFormat("UIPortNo: {0}|",UIPortNo);
            log.AppendFormat("RSSFeedFile: {0}|",RSSFeedFile);
            log.AppendFormat("ContentImportFile: {0}|",ContentImportFile);
            log.AppendFormat("UploadUrl: {0}|",UploadUrl);
            log.AppendFormat("ExportUrl: {0}|",ExportUrl);
            log.AppendFormat("DownloadBaseUrl: {0}|",DownloadBaseUrl);
            log.AppendFormat("Logfile: {0}|",Logfile);
            log.AppendFormat("MCPInitialContent: {0}|",MCPInitialContent);
            log.AppendFormat("MCPDataFolder: {0}|",MCPDataFolder);
            log.AppendFormat("MCPContent: {0}|",MCPContent);
            log.AppendFormat("MCPPackage: {0}|",MCPPackage);
            log.AppendFormat("MCPUpload: {0}|",MCPUpload);
            log.AppendFormat("PartnerName: {0}|",PartnerName);
            log.AppendFormat("SMTPServer: {0}|",SMTPServer);
            log.AppendFormat("AutoupdaterUserName: {0}|",AutoupdaterUserName);
            log.AppendFormat("DownloadhourSchedule: {0}|",DownloadhourSchedule);
            log.AppendFormat("DownloadminSchedule: {0}|",DownloadminSchedule);
            log.AppendFormat("DownloaddaySchedule: {0}|",DownloaddaySchedule);
            log.AppendFormat("WeeklyDownload: {0}|",WeeklyDownload);
            log.AppendFormat("ImporthourSchedule: {0}|",ImporthourSchedule);
            log.AppendFormat("ImportminSchedule: {0}|",ImportminSchedule);
            log.AppendFormat("ImportdaySchedule: {0}|",ImportdaySchedule);
            log.AppendFormat("WeeklyImport: {0}|",WeeklyImport);
            log.AppendFormat("UseProxy: {0}|",UseProxy);
            log.AppendFormat("ProxyAddressPort: {0}|",ProxyAddressPort);
            log.AppendFormat("ProxyUserName: {0}|",ProxyUserName);
            log.AppendFormat("ProxyAuthScheme: {0}|",ProxyAuthScheme);
            log.AppendFormat("LogFolder: {0}|",LogFolder);
            log.AppendFormat("TempFolder: {0}|",TempFolder);
            log.AppendFormat("MCPFolder: {0}|",MCPFolder);
            log.AppendFormat("IsNew: {0}|",IsNew);
            log.AppendFormat("setupType: {0}|",setupType);

            return log.ToString();
        }
    }
}
