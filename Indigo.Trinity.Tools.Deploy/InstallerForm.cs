﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Athena.Deploy
{
    public partial class InstallerForm : Form
    {
        private readonly InstallerControlList contentControls;
        public readonly InstallOptions options;
        private InstallerControl currentContentControl;
        private int currentContentControlIndex = 0;

        public InstallerForm()
        {
            this.contentControls = new InstallerControlList();

            this.options = new InstallOptions();
            this.options.Logfile = "InstallLog" + DateTime.Now.ToString("yyyymmdd");
            InitializeComponent();

            this.Load += new EventHandler(InstallerForm_Load);
        }

        #region Event Handlers

        private void InstallerForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = "Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ReplaceContentControl(0);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            currentContentControlIndex = ReplaceContentControl(currentContentControlIndex + 1);
        }

        private string GetSiteType(string siteType)
        {
            switch (siteType.ToLower())
            {
                case "primary":
                    return "Primary";
                case "secondary":
                    return "Partner";
            }
            return "Partner";
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            currentContentControlIndex = ReplaceContentControl(currentContentControlIndex - 1);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (currentContentControl != contentControls[contentControls.Count - 1])
                currentContentControl.RequestCancel();
            else
            {
                if (currentContentControl.GetType() == typeof(ReportControl))
                {
                    if ((currentContentControl as ReportControl).IsLaunchJobsManager)
                        System.Diagnostics.Process.Start(Path.Combine(Program.PayloadManager.GetPath("importutility"), "Indigo.Athena.Jobs.exe"), "-" + (currentContentControl as ReportControl).SiteUrl);
                    Application.Exit();
                }
            }
        }

        #endregion

        #region Public Properties

        public InstallerControlList ContentControls { get { return contentControls; } }
        public Button AbortButton { get { return btnCancel; } }
        public Button PrevButton { get { return btnPrevious; } }
        public Button NextButton { get { return btnNext; } }

        public InstallOperation Operation;
        public InstallationState SetupState
        {
            get { return options.SetupState; }
            set { options.SetupState = value; _CurrentStepChanged(); }
        }

        #endregion

        #region Public Methods

        public void SetTitle(string title)
        {
            lblTitle.Invoke(() => { lblTitle.Text = title; });
        }

        public void SetSubTitle(string title)
        {
            lblDescription.Invoke(() => { lblDescription.Text = title; });
        }

        public void MovePrevious()
        {
            currentContentControlIndex = ReplaceContentControl(currentContentControlIndex - 1);
        }

        public void MoveNext()
        {
            currentContentControlIndex = ReplaceContentControl(currentContentControlIndex + 1);
        }
        #endregion

        #region Private Methods

        private int ReplaceContentControl(int index)
        {
            // Validate
            if (index < 0 || index >= contentControls.Count)
                return currentContentControlIndex;

            // 1. Close current control
            if (currentContentControl != null)
            {
                if (currentContentControlIndex < index && !currentContentControl.ValidateForm()) return currentContentControlIndex;
                currentContentControl.Close(options);
            }

            // 2. Set Button State
            btnPrevious.Invoke(() => { btnPrevious.Enabled = (index == 0) ? false : true; });
            btnNext.Invoke(() => { btnNext.Enabled = (index == contentControls.Count - 1) ? false : true; });

            // 3. Load new control
            InstallerControl newContentControl = contentControls[index];
            newContentControl.Invoke(() => { newContentControl.Dock = DockStyle.Fill; });

            // 4. Initialize control (Title, SubTitle, Container (Panel)
            lblTitle.Invoke(() => { lblTitle.Text = newContentControl.Title; });
            lblDescription.Invoke(() => { lblDescription.Text = newContentControl.SubTitle; });

            installerPane.Invoke(() => { installerPane.Controls.Clear(); });
            installerPane.Invoke(() => { installerPane.Controls.Add(newContentControl); });
            newContentControl.Invoke(() => { newContentControl.Open(options); });
            currentContentControl = newContentControl;
            if (currentContentControl != contentControls[contentControls.Count - 1])
                btnCancel.Invoke(() => { btnCancel.Text = "Exit"; });

            _CurrentStepChanged();

            return index;
        }

        private Image LoadImage(string filename)
        {
            try { return Image.FromFile(filename); }
            catch (IOException) { return null; }
        }

        private void _CurrentStepChanged()
        {
            lblPreRequsite.Invoke(() => { lblPreRequsite.Font = new Font(lblPreRequsite.Font, FontStyle.Regular); });
            lblGatherInfo.Invoke(() => { lblGatherInfo.Font = new Font(lblGatherInfo.Font, FontStyle.Regular); });
            lblInstall.Invoke(() => { lblInstall.Font = new Font(lblInstall.Font, FontStyle.Regular); });
            lblReport.Invoke(() => { lblReport.Font = new Font(lblReport.Font, FontStyle.Regular); });

            switch (options.SetupState)
            {
                case InstallationState.Ready: break;
                case InstallationState.PreRequisitesCheck:
                    lblPreRequsite.Invoke(() => { lblPreRequsite.Font = new Font(lblPreRequsite.Font, FontStyle.Bold); });
                    break;
                case InstallationState.GatherPartnerInfo:
                case InstallationState.GatherProxyInfo:
                case InstallationState.GatherMiscInfo:
                case InstallationState.ReadyToInstall:
                    lblGatherInfo.Invoke(() => { lblGatherInfo.Font = new Font(lblGatherInfo.Font, FontStyle.Bold); });
                    break;
                case InstallationState.Installing:
                    lblInstall.Invoke(() => { lblInstall.Font = new Font(lblInstall.Font, FontStyle.Bold); });
                    break;
                case InstallationState.Successful:
                case InstallationState.Failed:
                case InstallationState.Aborted:
                    lblReport.Invoke(() => { lblReport.Font = new Font(lblReport.Font, FontStyle.Bold); });
                    break;
            }
        }
        #endregion
    }
}