using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Specialized;
using Indigo.Athena.Synchronization;
using System.Reflection;
using ICSharpCode.SharpZipLib.Zip;


namespace Indigo.Athena.Jobs
{
    public class ContentExporterJob : Job
    {
        public ContentExporterJob() { }      // IMP: Default Constructor for Serialization

        public ContentExporterJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Export, spWeb)
        {
        }

        #region Properties
        // 1. Content (Site, Lists, Partner)
        // 2. Destination (Folder)

        [Persisted]
        public string Partner;
        [Persisted]
        public DateTime ExportFromDate = DateTime.MinValue;
        [Persisted]
        public string ProcessorArchitecture = "x64";

        private string[] ExportLists;
        private string ExportLocalFolder;
        private string ExportRemoteFolder;                       // For remote downloads
        private int MaxFileSize = 50;                            // Max. file size of single cmp file in MB
        
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            // IMP: PackageName = BaseFileName = Package.Title
            // IMP: PackageFolder should match VirtualDirectory for Downloads
            string packageName = "Exp" + DateTime.Now.ToString("yyyyMMddHHmmss");
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Exporting";
                _LoadConfig();

                string _ExportFolder = Guid.NewGuid().ToString();
                // Step 1. Export Metadata
                SPExportSettings settings = new SPExportSettings();
                settings.CommandLineVerbose = true;
                settings.OverwriteExistingDataFile = true;
                settings.ExcludeDependencies = false;
                settings.FileCompression = false;
                settings.ExportMethod = SPExportMethodType.ExportAll;

                string packageLocalFolder = Path.Combine(this.ExportLocalFolder, packageName);
                string packageTempFolder = Path.Combine(this.ExportLocalFolder, _ExportFolder);
                //Create Directory
                if (!Directory.Exists(packageLocalFolder)) Directory.CreateDirectory(packageLocalFolder);
                if (!Directory.Exists(packageTempFolder))
                {
                    Directory.CreateDirectory(packageTempFolder);
                }
                else
                {
                    Directory.Delete(packageTempFolder, true);
                    Directory.CreateDirectory(packageTempFolder);
                }
                settings.SiteUrl = this.WebUrl;
                settings.FileLocation = packageTempFolder;
                settings.FileMaxSize = 1000;                  // 25 MB size files
                settings.BaseFileName = _ExportFolder;
                settings.LogFilePath = this.LogFile;
                settings.Validate();

                /// Step 2. Add objects to be exported
                SPSite site = new SPSite(this.WebUrl);
                SPWeb web = site.OpenWeb();

                SPExportObject oExport;
                DateTime fromDate = this.ExportFromDate;
                DateTime toDate = DateTime.Now;
                foreach (string listName in this.ExportLists)
                {
                    /// Export only those items, selected by the specified criteria
                    SPList list;
                    try { list = web.Lists[listName]; }
                    catch { continue; }

                    List<SPListItem> items = _Find(list, fromDate, toDate, this.Partner);
                    Console.WriteLine("{0} items will be exported from '{1}'.", items.Count, listName);
                    int count = 0;
                    foreach (SPListItem item in items)
                    {
                        if (!isValidItem(item, this.ProcessorArchitecture)) continue;
                        oExport = new SPExportObject();
                        oExport.Id = item.UniqueId;
                        oExport.Type = SPDeploymentObjectType.ListItem;
                        oExport.ParentId = list.ID;
                        settings.ExportObjects.Add(oExport);
                        count++;
                    }
                    if (count != 0)
                        this.Log(string.Format("List name : {0},objects Exported : {1}", listName, count), false, false);
                }

                if (settings.ExportObjects.Count == 0)
                {
                    this.Log("No data found for export...Job Completed...", true, true);
                    this.Status = "Completed";
                    return;
                }
                /// Step 3. Export
                SPExport exporter = new SPExport(settings);
                exporter.Run();
                this.Log("Data Exported...", false, true);
                //Dispose exporter
                exporter.Dispose();


                this.Log("Compressing Data...", false, true);
                string zipFile = Path.Combine(packageLocalFolder, packageName);
                CompressFolder(packageTempFolder, zipFile);

                //Split Files
                FileSplitter splitter = new FileSplitter();
                splitter.SegmentSize = (this.MaxFileSize * 1024 * 1024);
                int segments = splitter.SplitFile(zipFile);
                File.Delete(zipFile);
                Directory.Delete(packageTempFolder, true);

                // Step 4: Update Package List
                PackageManager packageMgr = new PackageManager(web);
                Package package = packageMgr.New();
                package.Title = packageName;
                package.DatePackaged = toDate;
                package.Partner = this.Partner;
                package.ProcessorArchitecture = (Processor)Enum.Parse(typeof(Processor), this.ProcessorArchitecture);
                package.PackageType = PackageModeType.Data.ToString();
                string[] filePaths = Directory.GetFiles(packageLocalFolder, packageName + ".*");
                List<string> fileNames = new List<string>();
                foreach (string f in filePaths)
                {
                    string filename = f;
                    filename += ".cmp";
                    File.Move(f, filename);
                    fileNames.Add(filename);
                }
                package.Files = fileNames.ToArray();
                package.LocalFolder = packageLocalFolder;

                // IMP: Cannot use Path.Combine() for Urls
                if (Path.GetFileName(this.ExportRemoteFolder) != "")
                    this.ExportRemoteFolder = this.ExportRemoteFolder + "/";
                package.RemoteFolder = this.ExportRemoteFolder + packageName + "/";
                package.Status = PackageStatus.Exported.ToString();
                package.Save();
                this.Log("Package list updated...", false, true);

                this.ExportFromDate = toDate;
                this.Update();
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
                this.Log("Job Successfully Completed...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log(string.Format("Error in exporting {0}...", packageName), ex, true, true);
                this.Status = "Failed";
            }
        }


        #endregion

        #region Helper Function's
        private List<SPListItem> _Find(SPList list, DateTime fromDate, DateTime toDate, string partner)
        {
            List<SPListItem> listItems = new List<SPListItem>();

            foreach (SPListItem item in list.Items)
            {
                // 1. Filter by Modified timestamp
                DateTime modifiedDate = (DateTime)item["Modified"];
                if (!(modifiedDate > fromDate && modifiedDate <= toDate)) continue;

                try
                {
                    if (_IsUnknownCategory((string)item["ContentCategory"])) continue;
                    SPFieldLookupValueCollection partners = (SPFieldLookupValueCollection)item["Partners"];
                    if (partners.Count != 0 && string.IsNullOrEmpty(partner)) continue;
                    if (!string.IsNullOrEmpty(partner) && !(_HasPartner(partners, partner))) continue;
                }
                catch (Exception)
                {
                    if (partner != string.Empty && partner != null) continue;
                }
                listItems.Add(item);
            }
            return listItems;
        }

        private bool _HasPartner(SPFieldLookupValueCollection partners, string partner)
        {
            foreach (SPFieldLookupValue p in partners)
            {
                if (partner.Equals(p.LookupValue, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }       

        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load Export Lists
            List<string> contentLists = new List<string>(configMgr.Settings["ContentList"].Split(','));
            for (int i = 0; i < contentLists.Count; i++)
                contentLists[i] = contentLists[i].Trim();
            this.ExportLists = contentLists.ToArray();
            if (ExportLists.Length == 0) throw new Exception("Configuration Error: 'ContentList' can not be empty.");

            // 2. Load ExportLocalFolder
            this.ExportLocalFolder = configMgr.Settings["ExportFolder"];
            if (string.IsNullOrEmpty(ExportLocalFolder)) throw new Exception("Configuration Error: 'ExportFolder' can not be empty.");

            // 3. ExportLocalFolder
            this.ExportRemoteFolder = configMgr.Settings["ExportFolderURL"];
            if (string.IsNullOrEmpty(ExportRemoteFolder)) throw new Exception("Configuration Error: 'ExportFolderURL' can not be empty.");
        }

        private bool isValidItem(SPListItem item, string architecture)
        {
            SPSite site = new SPSite(this.WebUrl);
            SPWeb web = site.OpenWeb();
            long size = 0;

            foreach (string s in item.Attachments)
            {
                SPFile file = web.GetFile(item.Attachments.UrlPrefix + s);
                if (file.Length > size) size = file.Length;
            }

            if (size > 150000000 && string.Compare(architecture, "x86", true) == 0) return false;
            if (size <= 150000000 && string.Compare(architecture, "x64", true) == 0) return false;
            return true;
        }

        private bool _IsUnknownCategory(string category)
        {

            SPFieldLookupValue lookupCategory = new SPFieldLookupValue(category);
            return lookupCategory.LookupValue.Equals("unknown", StringComparison.InvariantCultureIgnoreCase);
        }

        private void CompressFolder(string directory, string zipFile)
        {
            DirectoryInfo dir = new DirectoryInfo(directory);
            FileInfo[] files = dir.GetFiles("*.*", SearchOption.AllDirectories);
            using (ZipOutputStream s = new ZipOutputStream(File.Create(zipFile)))
            {
                s.SetLevel(9); // 0-9, 9 being the highest compression
                byte[] buffer = new byte[10000000];
                foreach (FileInfo file in files)
                {
                    ZipEntry entry = new ZipEntry(file.Name);
                    entry.DateTime = DateTime.Now;
                    s.PutNextEntry(entry);
                    using (FileStream fs = File.OpenRead(file.FullName))
                    {
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0,
                            buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }
                s.Finish();
                s.Close();
            }
        }

        #endregion
    }
}
