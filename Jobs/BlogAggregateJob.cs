using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Indigo.Athena.Acquisition;
using Indigo.Athena.Acquisition.Extractors;

namespace Indigo.Athena.Jobs
{
    public class BlogAggregateJob : Job
    {
        public BlogAggregateJob() : base() { }      // IMP: Default Constructor for Serialization
        public BlogAggregateJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.BlogAggregate, spWeb)
        { }

        SPSite site = null;
        SPWeb web = null;

        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Status = "Extracting";
                this.Log("Job started...", false, true);
                MediaType sourceType = MediaType.Blogs;
                RSSExtractor extractor = new RSSExtractor();

                // Get a ref to SPWeb
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();

                //Initial clean up
                //Used to remove entries from blog list which have empty "FeedTitle" column
                DeleteOldContent(null);
                this.Log("Removed content with empty Feed Title", false, false);

                SPListItemCollection blogFeedList = web.Lists["Blog Feeds"].Items;
                foreach (SPListItem blogFeed in blogFeedList)
                {
                    string temp = Convert.ToString(blogFeed["Max. Count"]);
                    string feedUrl = Convert.ToString(blogFeed["FeedUrl"]);
                    int maxCount = (string.IsNullOrEmpty(temp) || int.Parse(temp) < 0) ? 0 : int.Parse(temp);

                    if (string.IsNullOrEmpty(feedUrl)) continue;
                    extractor.Initalize(feedUrl, sourceType, null);
                    ContentInfo[] allExtractedItems;
                    try { allExtractedItems = extractor.GetMetadata(); }
                    catch (Exception ex)
                    {
                        this.Log(string.Format("Error getting data for {0} from : {1}",
                            blogFeed.Title, feedUrl), ex, false, true);
                        continue;
                    }
                    if (allExtractedItems.Length == 0 && maxCount != 0) continue;

                    DeleteOldContent(blogFeed.Title);

                    for (int i = 0; i < maxCount && i < allExtractedItems.Length; i++)
                    {
                        //Set the category and feed title value. Required from "Blogs" list for "Blog Feeds".
                        allExtractedItems[i].Category = (new SPFieldLookupValue(Convert.ToString(blogFeed["ContentCategory"]))).LookupValue;
                        allExtractedItems[i].FeedTitle = blogFeed.Title;
                        try 
                        { allExtractedItems[i].Save(web); }
                        catch (Exception e)
                        { this.Log(string.Format("Error saving : {0},Exception : {1}", 
                            allExtractedItems[i].Title, e.Message), false, false); }
                    }
                }
                this.Status = "Completed";
                this.Log("Job Completed Successfully...", true, true);
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                // Release resources
                if (site != null) site.Dispose();
                if (web != null) web.Dispose();
            }
        }
        #region Helper Function's
        private void DeleteOldContent(string title)
        {
            SPListItemCollection items = web.Lists["Blogs"].Items;
            for (int i = items.Count - 1; i >= 0; i--)
            {
                if ((new SPFieldLookupValue(Convert.ToString(items[i]["FeedTitle"]))).LookupValue == title)
                    items[i].Delete();
            }
        }
        #endregion
    }
}
