using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using Indigo.Athena.Synchronization;
using ICSharpCode.SharpZipLib.Zip;
using System.Diagnostics;

namespace Indigo.Athena.Jobs
{
    public class UpdaterJob : Job
    {
        public UpdaterJob() { }      // IMP: Default Constructor for Serialization

        public UpdaterJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Updater, spWeb)
        {
        }

        public override void Execute(Guid targetInstanceId)
        {
            SPSite site = null;
            SPWeb web = null;
            try
            {
                this.Status = "Updating";
                this.Log("Updater Job started...", false, true);
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();
                PackageManager pkgManager = new PackageManager(web);
                PackageCriteria criteria = new PackageCriteria();
                criteria.Status = PackageStatus.Downloaded.ToString();
                criteria.PackageType = PackageModeType.Update.ToString();
                Package[] packages = pkgManager.Find(criteria);
                this.Status = "Starting Updater";
                if (packages.Length >= 1)
                {
                    Package latestUpdate = GetLatestUpdate(packages);
                    string[] files = latestUpdate.Files;
                    string executableFile = files[0];
                    this.Status = "Executing";
                    this.Log("Executing File ...", false, true);
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = executableFile;
                    proc.StartInfo.WorkingDirectory = latestUpdate.LocalFolder;
                    proc.EnableRaisingEvents = false;
                    proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.StartInfo.RedirectStandardOutput = false;
                    proc.StartInfo.RedirectStandardError = false;
                    proc.StartInfo.RedirectStandardInput = false;
                    proc.StartInfo.UseShellExecute = true;
                    proc.Start();
                }
                this.Log("Updater Job completed ...", false, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Status = "Error";
                this.Log("Updater Job failed with " + ex.Message, false, true);
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }

        #region Helper Function
        private Package GetLatestUpdate(Package[] packages)
        {
            Package result = packages[0];

            foreach (Package p in packages)
            {
                if (result.DatePackaged > p.DatePackaged)
                    result = p;
            }
            return result;
        }
        #endregion
    }
}
