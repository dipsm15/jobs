using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using Indigo.Athena.Synchronization;
using ICSharpCode.SharpZipLib.Zip;

namespace Indigo.Athena.Jobs
{
    public class ContentImporterJob : Job
    {
        public ContentImporterJob() { }      // IMP: Default Constructor for Serialization

        public ContentImporterJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Import, spWeb)
        {
        }

        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Importing";
                DateTime startTime = DateTime.Now;  
                // Get a ref to SPWeb
                SPSite site = new SPSite(this.WebUrl);
                SPWeb web = site.OpenWeb();

                PackageManager pkgManager = new PackageManager(web);
                PackageCriteria criteria = new PackageCriteria();
                criteria.Status = PackageStatus.Downloaded.ToString();
                criteria.PackageType = PackageModeType.Data.ToString();
                Package[] packages = pkgManager.Find(criteria);

                int count = 0;
                this.Log(string.Format("Packages to be imported : {0}", packages.Length), false, true);
                foreach (Package p in packages)
                {
                    try
                    {
                        this.Status = string.Format("Importing ({0}/{1})", ++count, packages.Length);
                        string zipFile = Path.Combine(p.LocalFolder, p.Title + ".cmp");
                        if (File.Exists(zipFile)) File.Delete(zipFile);

                        //Join Files                    
                        string[] files = p.Files;
                        FileJoiner joiner = new FileJoiner();
                        joiner.JoinFile(files, zipFile);

                        //Uncompress                    
                        string DecompressFolder = Path.Combine(p.LocalFolder, p.Title);
                        DecompressFile(zipFile, DecompressFolder);

                        //Exporting                   

                        /// Step 1. Create import settings
                        SPImportSettings settings = new SPImportSettings();
                        settings.RetainObjectIdentity = true;
                        settings.CommandLineVerbose = true;
                        settings.FileCompression = false;
                        settings.SiteUrl = this.WebUrl;
                        settings.LogFilePath = this.LogFile;
                        settings.BaseFileName = p.Title;
                        settings.FileLocation = DecompressFolder;
                        settings.Validate();

                        /// Step 2. Import
                        SPImport importer = new SPImport(settings);
                        importer.Run();

                        p.Status = PackageStatus.Imported.ToString();
                        p.Save();
                        //CleanUp
                        importer.Dispose();
                        Directory.Delete(DecompressFolder, true);
                        Directory.Delete(p.LocalFolder,true);
                        this.Log(string.Format("Imported package ({0}/{1}) : {2} ", count, packages.Length, p.Title), false, false);
                    }
                    catch (Exception ex) { this.Log("Error in Package ",ex, true, true); continue; }
                }
               
                //Clean up function
                if (packages.Length != 0)
                    DeleteOldContent(startTime);

                if(web != null) web.Dispose();
                if( site != null) site.Dispose();
                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
        }

        
        #region HELPER FUNCTION's
        private void DecompressFile(string zipFile, string DecompressFolder)
        {
            if (!Directory.Exists(DecompressFolder))
            {
                Directory.CreateDirectory(DecompressFolder);
            }
            else
            {
                Directory.Delete(DecompressFolder, true);
                Directory.CreateDirectory(DecompressFolder);
            }
            ZipInputStream zipIn = new ZipInputStream(File.OpenRead(zipFile));
            ZipEntry entry ;
            while ((entry = zipIn.GetNextEntry()) != null)
            {
                string name = GetFileName(entry.Name);
                FileStream streamWriter = File.Create(DecompressFolder + @"\" + name);
                long size = entry.Size;
                byte[] data = new byte[size];
                while (true)
                {
                    size = zipIn.Read(data, 0, data.Length);
                    if (size > 0) streamWriter.Write(data, 0, (int)size);
                    else break;
                }
                streamWriter.Close();
            }

            //Delete ZipFile
            File.Delete(zipFile);
        }

        private string GetFileName(string filename)
        {
            try
            {
                string[] Split = filename.Split('\\');
                string file = Split[Split.Length - 1];
                return file;
            }
            catch
            {
                return filename;
            }
        }

        //Used to delete items that were not imported in last run
        private void DeleteOldContent(DateTime startTime)
        {
            SPSite _site = new SPSite(this.WebUrl);
            SPWeb _web = _site.OpenWeb();
            string[] listnames ={ "Blogs", "Blog Feeds" };
            foreach (string listname in listnames)
            {
                SPListItemCollection items = _web.Lists[listname].Items;
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    if (DateTime.Compare(startTime, Convert.ToDateTime(items[i]["Modified"])) == 1)
                        items[i].Delete();
                }
            }
            if(_web != null) _web.Dispose();
            if (_site != null) _site.Dispose();
        }
        #endregion

    }
}
