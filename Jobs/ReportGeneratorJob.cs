using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using System.IO;
using System.Net.Mail;

namespace Indigo.Athena.Jobs
{
    public class ReportGeneratorJob : Job
    {
        public ReportGeneratorJob() { }

        public ReportGeneratorJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.ReportGenerator, spWeb)
        { }

        #region Properties
        [Persisted]
        public string ReceiverAddress = string.Empty;
        [Persisted]
        public string SenderAddress = string.Empty;

        private string SmtpServer = string.Empty;
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            SPSite site = null;
            SPWeb web = null;
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Generating Report";
                _LoadConfig();
                // 1. Get SPWeb Context
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();
                string seperator = ",";
                string[] listName = { "Webcasts", "Podcasts", "Hands On Labs", "Other Content" };
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("LIST NAME" + seperator + "CATEGORY" + seperator + "TITLE" + seperator + "DESCRIPTION" + seperator + "AUTHOR" + seperator + "DURATION" + seperator + "SIZE" + seperator + "SOURCE" + seperator + "TAGS" + seperator + "CREATED ON" + seperator + "MODIFIED ON"+seperator+"NAMING CHECK" + seperator + "CATEGORY CHECK" + seperator + "TAGS CHECK" + seperator + "THUMBNAIL CHECK" + seperator + "");
                foreach (string list in listName)
                {
                    SPListItemCollection items = web.Lists[list].Items;
                    foreach (SPListItem item in items)
                    {
                        //GetValues
                        string title = CleanString(GetFieldValue(item, "Title"));
                        string description = CleanString(GetFieldValue(item, "ContentDescription"));
                        string category = CleanString(GetFieldLookupValue(item, "ContentCategory"));
                        string author = CleanString(GetFieldValue(item, "_Author"));
                        string duration = GetFieldValue(item, "MediaDuration");
                        string size = CleanString(GetFieldValue(item, "MediaSize"));
                        string source = CleanString(GetFieldValue(item, "Source"));
                        string tags = CleanString(GetFieldValue(item, "Tags"));
                        string dateCreated = CleanString(GetFieldValue(item, "Created"));
                        string dateModified = CleanString(GetFieldValue(item, "Modified"));

                        bool thumbnail = false;
                        bool screenshot = false;
                        try
                        {
                            foreach (string s in item.Attachments)
                            {
                                SPFile file = web.GetFile(item.Attachments.UrlPrefix + s);
                                if (file.Name == "Thumbnail")
                                    thumbnail = true;
                                if (file.Name == "Screenshot")
                                    screenshot = true;
                            }
                        }
                        catch { }

                        //Start Appending
                        sb.Append(list + seperator);
                        sb.Append(category + seperator);
                        sb.Append(title + seperator);
                        sb.Append(description + seperator);
                        sb.Append(author + seperator);
                        sb.Append(duration + seperator);
                        sb.Append(size + seperator);
                        sb.Append(source + seperator);
                        sb.Append(tags + seperator);
                        sb.Append(dateCreated + seperator);
                        sb.Append(dateModified + seperator);

                        //Check
                        if (!string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(description))
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        if (!string.IsNullOrEmpty(category) && category != "Unknown")
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        if (!string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(description) && !string.IsNullOrEmpty(tags) && !string.IsNullOrEmpty(author) && !string.IsNullOrEmpty(size) && !string.IsNullOrEmpty(duration))
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        if (thumbnail && screenshot)
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        sb.AppendLine("");
                    }
                }

                string pathPrefix = Path.Combine(Path.GetTempPath(), "ContentQA.csv");
                File.WriteAllText(pathPrefix, sb.ToString());

                this.Status = "Sending Mail";
                MailMessage MyMessage = new MailMessage();
                MyMessage.From = new MailAddress(SenderAddress);
                MyMessage.Subject = "Content QA Report " + DateTime.Now.ToShortDateString();
                MyMessage.Body = "Attached is the Content QA Report for " + DateTime.Now.ToShortDateString() + ".";

                string[] addresses = ReceiverAddress.Split(';');
                foreach (string curAddress in addresses)
                {
                    if (!string.IsNullOrEmpty(curAddress))
                        MyMessage.To.Add(new MailAddress(curAddress));
                }

                MyMessage.IsBodyHtml = false;
                MyMessage.Attachments.Add(new Attachment(pathPrefix));

                SmtpClient mailClient = new SmtpClient(this.SmtpServer);
                mailClient.Send(MyMessage);

                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                // Release resources
                if (site != null) site.Dispose();
                if (web != null) web.Dispose();
            }
        }

        #endregion

        #region Helper function

        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load MailTemplate
            this.SmtpServer = configMgr.Settings["SMTPServer"];
            if (string.IsNullOrEmpty(SmtpServer)) throw new Exception("Configuration Error: 'SMTPServer' can not be empty.");

        }

        private string CleanString(string description)
        {
            if (description == null)
                return string.Empty;
            try
            {
                byte[] encodedBytes = ASCIIEncoding.ASCII.GetBytes(description);
                description = ASCIIEncoding.ASCII.GetString(encodedBytes);
                string seperator = ",";
                description = description.Replace("?", "");
                description = description.TrimEnd('�');
                description = description.Replace(seperator, " ");
                string result = "";
                for (int i = 0; i < description.Length; i++)
                {
                    if (char.IsLetterOrDigit(description[i]) || char.IsPunctuation(description[i]) || char.IsSeparator(description[i]))
                        result += description[i];
                }
                return result;
            }
            catch (Exception ex)
            {
                this.Log(ex.Message + " for this string " + description,false,false);
                return description;
            }
        }

        public static string GetFieldValue(SPListItem item, string key)
        {
            try
            {
                if (!key.Equals("LinkTitle", StringComparison.InvariantCultureIgnoreCase) && !key.Equals("_Author", StringComparison.InvariantCultureIgnoreCase) && (item.Fields[key].Type == SPFieldType.Lookup || item.Fields[key].Type == SPFieldType.User))
                    return GetFieldLookupValue(item, key);
                else
                    return item[key].ToString();
            }
            catch { return string.Empty; }
        }

        private static string GetFieldLookupValue(SPListItem item, string key)
        {
            try
            {
                SPFieldLookupValue lookupVal = new SPFieldLookupValue(item[key].ToString());
                return lookupVal.LookupValue;
            }
            catch { return string.Empty; }
        }
        #endregion
    }
}
