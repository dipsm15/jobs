using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.ServiceModel;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.Net;
using System.Security;
using Indigo.Athena.Services;

namespace Indigo.Athena.Jobs
{
    public enum PackageType { UsageLog, JobLog, ContentReport };

    public class UsageUploadJob : Job
    {
        #region Constructors
        public UsageUploadJob() { }

        public UsageUploadJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.UsageUpload, spWeb)
        {
        }
        #endregion

        #region Properties
        [Persisted]
        public DateTime ExportFromDate = DateTime.MinValue;
        private string[] UsageLists;
        private string ExportLocalFolder;
        private string ServiceUrl;
        #endregion

        private SPWeb web;
        private SPSite site;
        private UploadType uploadType;
        enum UploadType { BITS, HTTP };
        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            // IMP: PackageName = BaseFileName
            string packageName = "UsgExp" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string jobPackageName = "JobExp" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string contentReportName = "ContentReport" + DateTime.Now.ToString("yyyyMMddHHmmss");

            string packageFile = string.Empty;
            string jobPackageFile = string.Empty;
            string contentReportFile = string.Empty;

            bool isSuccessful = true;

            string packageLocalFolder = string.Empty;
            try
            {
                this.Log("Job started...", false, true);

                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();

                _LoadConfig();

                //Proxy Settings
                ConfigManager configMgr = new ConfigManager(web);
                string useProxy = "false";
                string proxyAddressAndPort = string.Empty;
                string proxyUserName = string.Empty;
                string proxyPassword = string.Empty;

                try
                {
                    useProxy = configMgr.Settings["UseProxy"];
                    if (string.IsNullOrEmpty(useProxy)) throw new Exception("Configuration Error: 'UseProxy' can not be empty.");
                    proxyAddressAndPort = configMgr.Settings["ProxyAddressPort"];
                    proxyUserName = configMgr.Settings["ProxyUserName"];
                    proxyPassword = configMgr.Settings["ProxyPassword"];
                }
                catch (Exception)
                {
                    useProxy = "false";
                }

                if (useProxy == "true")
                {
                    WebProxy p = null;
                    string password = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(proxyPassword));
                    if (proxyAddressAndPort != "")
                    {
                        if (proxyUserName != "" && proxyPassword != "")
                        {
                            ICredentials cred;
                            cred = new NetworkCredential(proxyUserName, password);
                            p = new WebProxy(proxyAddressAndPort, true, null, cred);
                            WebRequest.DefaultWebProxy = p;
                        }
                        else
                        {
                            p = new WebProxy(proxyAddressAndPort, true, null);
                            WebRequest.DefaultWebProxy = p;
                            WebRequest.DefaultWebProxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                        }
                    }
                    else
                    {
                        if (proxyUserName != "" && proxyPassword != "")
                        {
                            ICredentials cred;
                            cred = new NetworkCredential(proxyUserName, password);
                            WebRequest.DefaultWebProxy.Credentials = cred;
                        }
                        else
                            WebRequest.DefaultWebProxy.Credentials = CredentialCache.DefaultNetworkCredentials;
                    }
                }

                ChannelFactory<ISyncService> proxy = ProxyManager.Proxy<ISyncService>(this.ServiceUrl, null);
                var svcClient = proxy.CreateChannel();

                packageLocalFolder = Path.Combine(this.ExportLocalFolder, DateTime.Now.Ticks.ToString());
                if (!Directory.Exists(packageLocalFolder)) Directory.CreateDirectory(packageLocalFolder);

                SPListItem partner;
                try { partner = SPListLib.GetAllItems("Partners", string.Empty, web)[0]; }
                catch { throw new Exception("Partner not found."); }

                /// Step 1 Upload Usage Export
                string uploadLocation = string.Empty;
                try
                {
                    /// Step 1.1 Export Usage objects.
                    this.Status = "Exporting Usage";
                    SPExportSettings settings = _GetSPExportSettings(packageLocalFolder, packageName);
                    SPExportObject oExport;
                    DateTime fromDate = this.ExportFromDate;
                    DateTime toDate = DateTime.Now;
                    foreach (string listName in this.UsageLists)
                    {
                        /// Export only those items, selected by the specified criteria
                        SPList list;
                        try { list = web.Lists[listName]; }
                        catch { continue; }

                        List<SPListItem> items = _Find(list, fromDate, toDate);
                        Console.WriteLine("{0} items will be exported from '{1}'.", items.Count, listName);

                        foreach (SPListItem item in items)
                        {
                            oExport = new SPExportObject();
                            oExport.Id = item.UniqueId;
                            oExport.Type = SPDeploymentObjectType.ListItem;
                            oExport.ParentId = list.ID;
                            settings.ExportObjects.Add(oExport);
                        }
                    }
                    if (settings.ExportObjects.Count != 0)
                    {
                        SPExport exporter = new SPExport(settings);
                        exporter.Run();
                        packageFile = Path.Combine(packageLocalFolder, packageName + ".cmp");
                        if (!File.Exists(packageFile))
                            throw new Exception("Usage package file not created at path : " + packageFile);
                        this.Log("Usage Export created...", false, false);

                        ///Step 1.2 Upload Package
                        uploadLocation = svcClient.GetUploadLocationByPartner(_GetValue(partner, "LinkTitle"), packageFile, PackageType.UsageLog.ToString());
                        this.Log("Upload location obtained for usage export...Uploading package...", false, false);
                        _UploadFile(uploadLocation, packageFile);
                        svcClient.UpdateUploadStatus(_GetValue(partner, "LinkTitle"), packageFile, true);
                        this.ExportFromDate = toDate;
                        this.Update();
                    }
                    else
                        this.Log("No Usage found for export.", false, false);
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(uploadLocation))
                        svcClient.UpdateUploadStatus(_GetValue(partner, "LinkTitle"), packageFile, false);
                    this.Log("Error in Usage package upload.", ex, false, false);
                    isSuccessful = false;
                }

                /// Step 2 Upload Job Export
                string uploadLocationJobs = string.Empty;
                try
                {
                    /// Step 2.1 Export Job objects
                    SPExportSettings jobSettings = _GetSPExportSettings(packageLocalFolder, jobPackageName);
                    SPListItemCollection jobItems = web.Lists["Jobs"].Items;
                    SPExportObject oExport;
                    foreach (SPListItem item in jobItems)
                    {
                        oExport = new SPExportObject();
                        oExport.Id = item.UniqueId;
                        oExport.Type = SPDeploymentObjectType.ListItem;
                        oExport.ParentId = item.ParentList.ID;
                        jobSettings.ExportObjects.Add(oExport);
                    }
                    if (jobSettings.ExportObjects.Count != 0)
                    {
                        SPExport exporter = new SPExport(jobSettings);
                        exporter.Run();
                        jobPackageFile = Path.Combine(packageLocalFolder, jobPackageName + ".cmp");
                        if (!File.Exists(jobPackageFile))
                            throw new Exception("Job package file not created at path : ");

                        this.Log("Job Export created...", false, false);
                        ///Step 2.2 Upload Package
                        uploadLocationJobs = svcClient.GetUploadLocationByPartner(_GetValue(partner, "LinkTitle"), jobPackageFile, PackageType.JobLog.ToString());
                        this.Log("Upload location obtained for Job export...Uploading packages...", false, false);
                        _UploadFile(uploadLocationJobs, jobPackageFile);
                        svcClient.UpdateUploadStatus(_GetValue(partner, "LinkTitle"), jobPackageFile, true);
                    }
                    else
                        this.Log("No Job found for export.", false, false);
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(uploadLocationJobs))
                        svcClient.UpdateUploadStatus(_GetValue(partner, "LinkTitle"), jobPackageFile, false);
                    this.Log("Error in Job package upload.", ex, false, false);
                    isSuccessful = false;
                }

                ///Step 3: Upload Content Report
                string uploadLocationContentReport = string.Empty;
                string contentReport = _GetContentReport();
                if (!string.IsNullOrEmpty(contentReport))
                {
                    try
                    {
                        contentReportFile = Path.Combine(packageLocalFolder, contentReportName + ".csv");
                        File.WriteAllText(contentReportFile, contentReport);
                        if (!File.Exists(contentReportFile))
                            throw new Exception("Content Report file does not exist.");
                        uploadLocationContentReport = svcClient.GetUploadLocationByPartner(_GetValue(partner, "LinkTitle"), contentReportFile, PackageType.ContentReport.ToString());
                        this.Log("Upload location obtained for Content Report...Uploading file...", false, false);
                        _UploadFile(uploadLocationContentReport, contentReportFile);
                        svcClient.UpdateUploadStatus(_GetValue(partner, "LinkTitle"), contentReportFile, true);
                        svcClient.AddContentReport(_GetValue(partner, "LinkTitle"), contentReportFile);
                        this.Log("ContentReport Added successfully.", false, false);
                    }
                    catch (Exception ex)
                    {
                        if (!string.IsNullOrEmpty(uploadLocationContentReport))
                            svcClient.UpdateUploadStatus(_GetValue(partner, "LinkTitle"), contentReportFile, false);
                        this.Log("Error in metadata upload.", ex, false, false);
                        isSuccessful = false;
                    }
                }
                else
                    this.Log("No content metadata available.", false, false);

                if (isSuccessful)
                {
                    this.Status = "Completed";
                    this.Log("Job Completed Successfully...", true, true);
                }
                else
                {
                    this.Status = "Failed";
                    this.Log("Job Completed with errors...", true, true);
                }
            }
            catch (Exception ex)
            {
                this.Log(string.Format("Error in Job..."), ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                if (!string.IsNullOrEmpty(packageLocalFolder) && Directory.Exists(packageLocalFolder))
                    Directory.Delete(packageLocalFolder, true);
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }

        private List<SPListItem> _Find(SPList list, DateTime fromDate, DateTime toDate)
        {
            List<SPListItem> listItems = new List<SPListItem>();

            foreach (SPListItem item in list.Items)
            {
                // 1. Filter by Modified timestamp
                DateTime modifiedDate = (DateTime)item["Modified"];
                if (!(modifiedDate > fromDate && modifiedDate <= toDate)) continue;

                listItems.Add(item);
            }
            return listItems;
        }

        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load Export Lists
            List<string> usageLists = new List<string>(configMgr.Settings["UsageLists"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            for (int i = 0; i < usageLists.Count; i++)
                usageLists[i] = usageLists[i].Trim();
            this.UsageLists = usageLists.ToArray();
            if (UsageLists.Length == 0) throw new Exception("Configuration Error: 'UsageLists' can not be empty.");

            // 2. Load ExportLocalFolder
            this.ExportLocalFolder = configMgr.Settings["UsageExportFolder"];
            if (string.IsNullOrEmpty(ExportLocalFolder)) throw new Exception("Configuration Error: 'UsageExportFolder' can not be empty.");

            // 3. Load ServiceUrl
            this.ServiceUrl = configMgr.Settings["ServiceUrl"];
            if (string.IsNullOrEmpty(ServiceUrl)) throw new Exception("Configuration Error: 'ServiceUrl' can not be empty.");

            try
            {
                string _uploadType = configMgr.Settings["UploadMethod"];
                if (string.Compare(_uploadType, "HTTP", true) == 0)
                    this.uploadType = UploadType.HTTP;
                else
                    this.uploadType = UploadType.BITS;

                if (string.IsNullOrEmpty(_uploadType))
                    this.uploadType = UploadType.BITS;
            }
            catch
            {
                this.uploadType = UploadType.BITS;
            }
        }

        private void _UploadFile(string serverPath, string localFilePath)
        {
            _UploadFileWebClient(serverPath, localFilePath);
        }

        private void _UploadBITS(string serverPath, string localFilePath)
        {
            if (!File.Exists(localFilePath))
                throw new FileNotFoundException("File not found:" + localFilePath);

            serverPath = serverPath.Replace(" ", "%20");
            string fileName = Path.GetFileName(localFilePath);
            string serverFilePath = serverPath.TrimEnd('/', '\\') + "/" + fileName;

            System.Net.BITS.Manager jobManager = new System.Net.BITS.Manager();
            System.Net.BITS.Job job = new System.Net.BITS.Job(fileName,
                System.Net.BITS.JobType.Upload, System.Net.BITS.JobPriority.Normal);
            jobManager.Jobs.Add(job);

            //Proxy Settings
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());
            string useProxy = "false";
            string proxyAddressAndPort = string.Empty;
            string proxyUserName = string.Empty;
            string proxyPassword = string.Empty;
            string proxyAuthScheme = string.Empty;
            try
            {
                useProxy = configMgr.Settings["UseProxy"];
                if (string.IsNullOrEmpty(useProxy)) throw new Exception("Configuration Error: 'UseProxy' can not be empty.");
                proxyAddressAndPort = configMgr.Settings["ProxyAddressPort"];
                proxyUserName = configMgr.Settings["ProxyUserName"];
                proxyPassword = configMgr.Settings["ProxyPassword"];
                proxyAuthScheme = configMgr.Settings["ProxyAuthScheme"];
            }
            catch (Exception)
            {
                useProxy = "false";
            }

            if (useProxy == "true")
            {
                if (proxyAddressAndPort != "")
                {
                    System.Net.BITS.JobProxySettings settings = new System.Net.BITS.JobProxySettings(System.Net.BITS.ProxyUsage.Override, "http=" + proxyAddressAndPort, null);
                    job.ProxySettings = settings;
                }

                System.Net.BITS.Credentials cred = new System.Net.BITS.Credentials();
                cred.Target = System.Net.BITS.AuthTarget.Proxy;
                //Get Auth Scheme
                if (proxyAuthScheme != "")
                    cred.Scheme = GetAuthScheme(proxyAuthScheme);

                if (proxyUserName != "")
                    cred.UserName = ConvertStringToSecureString(proxyUserName);
                else
                    cred.UserName = null;

                if (proxyPassword != "")
                    cred.Password = ConvertStringToSecureString(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(proxyPassword)));
                else
                    cred.Password = null;
                //Set Credentials
                job.SetCredentials(cred);
            }

            job.Files.Add(serverFilePath, localFilePath);
            job.AutoCancelOnError = false;
            job.AutoCancelOnTransientError = false;
            job.AutoComplete = true;
            jobManager.Jobs.Update();
            string error = string.Empty;
            if (job.Error.ContextDescription != null && job.Error.Description != null)
                error = "Context Description: " + job.Error.ContextDescription + " Error Description: " + job.Error.Description + " Job Owner: " + job.Owner;
            job.Resume();               // start the job in action

            while (job.State != System.Net.BITS.JobState.Transferred &&
                    job.State != System.Net.BITS.JobState.Cancelled &&
                    job.State != System.Net.BITS.JobState.Acknowledged &&
                    job.State != System.Net.BITS.JobState.Error)
                System.Threading.Thread.Sleep(5000);

            if (job.Progress.BytesTotal > job.Progress.BytesTransferred)
            {

                if (job.State == System.Net.BITS.JobState.Error)
                {
                    if (job.Error.ContextDescription != null && job.Error.Description != null)
                        error = "Context Description: " + job.Error.ContextDescription + " Error Description: " + job.Error.Description + " Job Owner: " + job.Owner;
                }
                job.Cancel();
                job.Dispose();
                throw new Exception("Error in BITS download. Job State:" + job.State + " " + error);
            }

            job.Dispose();
        }

        private void _UploadFileWebClient(string serverPath, string localFilePath)
        {
            if (!File.Exists(localFilePath))
                throw new FileNotFoundException("File not found:" + localFilePath);
            //WebClient client = new WebClient();
            serverPath = serverPath.Replace(" ", "%20");
            string fileName = Path.GetFileName(localFilePath);
            //filename
            string serverFilePath = serverPath.TrimEnd('/', '\\') + "/" + fileName;
            serverFilePath = serverFilePath.Replace('/', '\\');
            try
            {
                ChannelFactory<ISyncService> proxy = ProxyManager.Proxy<ISyncService>(this.ServiceUrl, null);
                var svcClient = proxy.CreateChannel();

                int chunkSize = 65000;
                Result result = new Result();
                int retryCount = 0;
                while (retryCount < 3)
                {
                    using (FileStream fs = new FileStream(localFilePath, FileMode.Open))
                    {
                        byte[] buffer = new byte[chunkSize];
                        int bytesRead = 0;

                        int totalChunkCount = Convert.ToInt32(fs.Length / chunkSize);
                        if (fs.Length % chunkSize != 0) totalChunkCount++;

                        int currentChunkCount = 1;
                        // iterate through the file filling a buffer and sending it to the service
                        while ((bytesRead = fs.Read(buffer, 0, chunkSize)) > 0)
                        {
                            result = svcClient.Upload(new FileData { FileName = serverFilePath, Data = buffer, Count = bytesRead, CurrentChunkCount = currentChunkCount, TotalChunckCount = totalChunkCount });
                            currentChunkCount++;
                            if (!result.IsSuccessful) break;
                        }
                        if (!result.IsSuccessful)
                        {
                            retryCount++;
                            if (result.Message != null && result.Message.Count > 0)
                                this.Log("Upload failed: " + result.Message[0] + "\nAttempt: " + (retryCount + 1), false, false);
                            else
                                this.Log("Upload failed.\nAttempt: " + (retryCount + 1), false, false);
                        }
                        else break;
                    }
                }
                proxy.Close();
            }
            catch (Exception ex) { throw new Exception("Error in uploading  : " + ex.Message); }
        }

        private System.Net.BITS.AuthScheme GetAuthScheme(string proxyAuthScheme)
        {
            proxyAuthScheme = proxyAuthScheme.ToLower();
            switch (proxyAuthScheme)
            {
                case "basic": return System.Net.BITS.AuthScheme.Basic;
                    break;
                case "digest": return System.Net.BITS.AuthScheme.Digest;
                    break;
                case "ntlm": return System.Net.BITS.AuthScheme.NTLM;
                    break;
                case "negotiate": return System.Net.BITS.AuthScheme.Negotiate;
                    break;
                case "passport": return System.Net.BITS.AuthScheme.Passport;
                    break;
            }
            return System.Net.BITS.AuthScheme.Negotiate;
        }

        private SecureString ConvertStringToSecureString(string value)
        {
            Char[] input = value.ToCharArray();
            SecureString SecureString = new SecureString();

            for (int idx = 0; idx < input.Length; idx++)
            {
                SecureString.AppendChar(input[idx]);
            }
            return SecureString;
        }

        private string _GetValue(SPListItem item, string key)
        {
            try
            {
                if (!key.Equals("LinkTitle", StringComparison.InvariantCultureIgnoreCase) && !key.Equals("_Author", StringComparison.InvariantCultureIgnoreCase) && (item.Fields[key].Type == SPFieldType.Lookup || item.Fields[key].Type == SPFieldType.User))
                    return GetFieldLookupValue(item, key);
                else
                    return item[key].ToString();
            }
            catch { return string.Empty; }
        }

        private static string GetFieldLookupValue(SPListItem item, string key)
        {
            try
            {
                SPFieldLookupValue lookupVal = new SPFieldLookupValue(item[key].ToString());
                return lookupVal.LookupValue;
            }
            catch { return string.Empty; }
        }

        private SPExportSettings _GetSPExportSettings(string fileLocation, string baseFileName)
        {
            SPExportSettings settings = new SPExportSettings();
            settings.CommandLineVerbose = true;
            settings.OverwriteExistingDataFile = true;
            settings.ExcludeDependencies = false;
            settings.FileCompression = true;
            settings.ExportMethod = SPExportMethodType.ExportAll;
            settings.SiteUrl = this.WebUrl;
            settings.FileLocation = fileLocation;
            settings.FileMaxSize = int.MaxValue;                  // 25 MB size files
            settings.BaseFileName = baseFileName;
            settings.LogFilePath = this.LogFile;
            settings.Validate();
            return settings;
        }

        private string _GetContentReport()
        {
            try
            {
                string seperator = ",";
                string[] listName = { "Webcasts", "Podcasts", "Hands On Labs", "Other Content" };
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("LIST NAME" + seperator + "GUID" + seperator + "ID" + seperator + "CATEGORY" + seperator + "TITLE" + seperator + "DESCRIPTION" + seperator + "AUTHOR" + seperator + "DURATION" + seperator + "SIZE" + seperator + "SOURCE" + seperator + "TAGS" + seperator + "CREATED ON" + seperator + "MODIFIED ON" + seperator + "NAMING CHECK" + seperator + "CATEGORY CHECK" + seperator + "ATTRIBUTE CHECK" + seperator + "THUMBNAIL CHECK" + seperator + "");
                foreach (string list in listName)
                {
                    SPListItemCollection items = web.Lists[list].Items;
                    foreach (SPListItem item in items)
                    {
                        //GetValues
                        string guid = item.UniqueId.ToString();
                        string id = _GetValue(item, "ID");
                        string title = CleanString(_GetValue(item, "Title"));
                        string description = CleanString(_GetValue(item, "ContentDescription"));
                        string category = CleanString(GetFieldLookupValue(item, "ContentCategory"));
                        string author = CleanString(_GetValue(item, "_Author"));
                        string duration = CleanString(_GetValue(item, "MediaDuration"));
                        string size = CleanString(_GetValue(item, "MediaSize"));
                        string source = CleanString(_GetValue(item, "Source"));
                        string tags = CleanString(_GetValue(item, "Tags"));
                        string dateCreated = CleanString(_GetValue(item, "Created"));
                        string dateModified = CleanString(_GetValue(item, "Modified"));

                        bool thumbnail = false;
                        bool screenshot = false;
                        try
                        {
                            foreach (string s in item.Attachments)
                            {
                                SPFile file = web.GetFile(item.Attachments.UrlPrefix + s);
                                if (file.Name.ToLower() == "thumbnail")
                                    thumbnail = true;
                                if (file.Name.ToLower() == "screenshot")
                                    screenshot = true;
                            }
                        }
                        catch { }

                        //Start Appending
                        sb.Append(list + seperator);
                        sb.Append(guid + seperator);
                        sb.Append(id + seperator);
                        sb.Append(category + seperator);
                        sb.Append(title + seperator);
                        sb.Append(description + seperator);
                        sb.Append(author + seperator);
                        sb.Append(duration + seperator);
                        sb.Append(size + seperator);
                        sb.Append(source + seperator);
                        sb.Append(tags + seperator);
                        sb.Append(dateCreated + seperator);
                        sb.Append(dateModified + seperator);

                        //Check
                        if (!string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(description) && !string.IsNullOrEmpty(tags) && !string.IsNullOrEmpty(author))
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        if (!string.IsNullOrEmpty(category) && category != "Unknown")
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        if (!string.IsNullOrEmpty(size) && !string.IsNullOrEmpty(duration))
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        if (thumbnail && screenshot)
                            sb.Append("Yes" + seperator);
                        else
                            sb.Append("No" + seperator);

                        sb.AppendLine("");
                    }
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                this.Log("Error getting content metadata : ", ex, false, false);
                return string.Empty;
            }
        }

        private string CleanString(string description)
        {
            if (description == null)
                return string.Empty;
            byte[] encodedBytes = ASCIIEncoding.ASCII.GetBytes(description);
            description = ASCIIEncoding.ASCII.GetString(encodedBytes);
            description = description.Replace(",", " ");
            string result = "";
            for (int i = 0; i < description.Length; i++)
            {
                if (char.IsLetterOrDigit(description[i]) || char.IsPunctuation(description[i]) || char.IsSeparator(description[i]))
                    result += description[i];
            }
            return result;
        }
        #endregion
    }
}
