﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using Indigo.Athena.Synchronization;

namespace Indigo.Athena.Jobs
{
    class UserProfileUploadJob : Job
    {
        #region Constructors
        public UserProfileUploadJob() { }

        public UserProfileUploadJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.UserProfileUploadJob, spWeb)
        {
        }
        #endregion

        #region properties
        SPWeb _Web;
        SPSite _Site;
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            try
            {

                this.Log("Job started...", false, true);
                this.Status = "Extracting";
                string validate = _Validate();
                string partnerName = _GetPartner();
                if (validate != null)
                    if (validate.ToLower() == "true")
                    {
                        _Site = new SPSite(this.WebUrl);
                        _Web = _Site.OpenWeb();
                        SPUserCollection users = _Web.AllUsers;
                        this.Log("Count of users is : " + users.Count, false, false);
                        foreach (SPUser user in users)
                        {
                            UserProfile userProfile = new UserProfile();
                            if (partnerName != null)
                                userProfile.PartnerName = partnerName;
                            else
                                userProfile.PartnerName = string.Empty;
                            if (user.Name != null)
                                userProfile.Name = user.Name;
                            else
                                userProfile.Name = string.Empty;
                            if (user.Email != null&&user.Email!=string.Empty)
                                userProfile.EmailId = user.Email;
                            else
                                continue;
                                _Save(userProfile);
                        }
                        this.Status = "Completed";
                        this.Log("Job Completed Successfully...\n.....................................", false, false);
                        if (_Web != null) _Web.Dispose();
                        if (_Site != null) _Site.Dispose();
                    }
            }
            catch (Exception e) { this.Log("Error: " + e, false, true); }
        }
        #endregion

        #region HelperFunctions

        private string _GetPartner()
        {
            string val=null;
            SPSite oSite = new SPSite(this.WebUrl);
            SPWeb oWeb = oSite.OpenWeb();
            SPList oList = oWeb.Lists["Partners"];
            SPListItem oListItem = oList.Items[0];
            val = oListItem["Title"].ToString();
            oSite.Dispose();
            oWeb.Dispose();
            return val;
        }

        private string _Validate()
        {
            try
            {
                string val = null;
                SPSite oSite = new SPSite(this.WebUrl);
                SPWeb oWeb = oSite.OpenWeb();
                SPList oList = oWeb.Lists["Config"];
                SPListItemCollection collListItems = oList.Items;
                this.Log("Looking for List", false, false);
                foreach (SPListItem oListItem in collListItems)
                {
                    if (oListItem.DisplayName.ToLower().Contains("uploaduserprofile"))
                    {
                        val = oListItem["Value"].ToString();
                        break;
                    }
                }
                return val;
            }
            catch (Exception e) { this.Log("Cannot locate lists  " + e, false, false); return null; }
        }

        
        private void _Save(UserProfile userProfile)
        {
            try
            {
                SPList oList = _Web.Lists["UserProfile"];
                if (!_IsNew(oList, userProfile.EmailId))
                {
                    SPListItem item = oList.Items.Add();
                    item["Name"] = userProfile.Name;
                    item["EmailId"] = userProfile.EmailId;
                    item["CompanyName"] = userProfile.PartnerName;
                    item.Update();
                }
            }
            catch (Exception e) { this.Log("Cannot save entries to list  "+e,false,false);} 
        }

        private bool _IsNew(SPList list, string item)
        {
            bool isExists = false;
            foreach (SPListItem currentItem in list.Items)
            {
                if (currentItem["EmailId"].ToString().ToLower() == item.ToLower())
                {
                    isExists = true;
                    break;
                }
            }
            return isExists;
        }

        #endregion
    }
}
