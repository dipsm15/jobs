using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.BITS;
using System.Security;
using System.Text;
using Indigo.Athena.Synchronization;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Indigo.Athena.Jobs
{
    public class PackageDownloaderJob : Job
    {
        public PackageDownloaderJob() { }      // IMP: Default Constructor for Serialization

        public PackageDownloaderJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Download, spWeb)
        {
        }

        #region Properties
        [Persisted]
        public DateTime LastSyncDate = DateTime.MinValue;

        private string ServiceUrl = string.Empty;
        private string Partner;
        private string PackageFolder;

        //TODO: Create Proxy Setting Object
        private bool _useProxy = false;
        private string _proxyAddress = string.Empty, _proxyUserName = string.Empty, _proxyPassword = string.Empty, _proxyAuthScheme = string.Empty;
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            SPSite site = null;
            SPWeb web = null;
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Downloading";
                LoadConfig();

                DateTime jobStartTime = DateTime.Now;

                // 1. Get Package List from Remote site
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();
                PackageManager packageMgr = new PackageManager(web);

                PackageCriteria criteria = new PackageCriteria();
                criteria.Partner = this.Partner;
                criteria.MinPackageDate = this.LastSyncDate;
                criteria.MaxPackageDate = DateTime.MaxValue;
                Package[] packages = packageMgr.Find(this.ServiceUrl, criteria);

                // 2. Save Package to local Site
                foreach (Package p in packages)
                {
                    if (packageMgr.IsNew(p))
                    {
                        p.Status = PackageStatus.Downloading.ToString();
                        p.LocalFolder = this.PackageFolder;
                        for (int i = 0; i < p.Files.Length; i++)
                            p.Files[i] = Path.Combine(this.PackageFolder, Path.GetFileName(p.Files[i]));
                        p.Save();
                    }
                }
                this.Log("Packages list updated... Data download starting...", false, true);

                // 3. Foreach saved Package item, download data
                criteria.Status = PackageStatus.Downloading.ToString();
                criteria.MinPackageDate = criteria.MaxPackageDate = null;
                packages = packageMgr.Find(criteria);
                int count = 0;
                string[] downloadedFileLocation = null;

                //Gets the Proxy Settings which will be used by Downloaders
                GetProxySettings();

                //Choose the downloader on basis of config entry
                bool useBits = IsUsingBits();
                foreach (Package p in packages)
                {
                    this.Status = string.Format("Downloading ({0}/{1})", ++count, packages.Length);
                    this.Log(string.Format("Downloading ({0}/{1}) {2}", count, packages.Length, p.Title), false, false);
                    p.LocalFolder = Path.Combine(this.PackageFolder, p.Title);
                    try
                    {
                        if (useBits) downloadedFileLocation = Download(p.RemoteFolder, p.LocalFolder, p.Files);
                        else
                        {
                            HttpDownloader downloader = null;
                            if (_useProxy)
                                downloader = new HttpDownloader(new ProxySettings
                                {
                                    ProxyAddress = _proxyAddress,
                                    ProxyUserName = _proxyUserName,
                                    ProxyPassword = _proxyPassword
                                });
                            else downloader = new HttpDownloader();
                            downloader.Start(p.RemoteFolder, p.LocalFolder, p.Files.ToList());
                            downloadedFileLocation = downloader.Files.ToArray();
                        }
                        p.Files = downloadedFileLocation;
                        p.Status = PackageStatus.Downloaded.ToString();
                        p.Save();
                        this.Log(string.Format("Downloaded ({0}/{1}) : {2}", count, packages.Length, p.Title), false, false);
                        downloadedFileLocation = null;
                    }
                    catch (Exception ex)
                    {
                        this.Log(string.Format("Error in Downloading Package {0}  ", p.Title), ex, false, true);
                    }
                }

                this.LastSyncDate = jobStartTime;
                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";
                this.Update();
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }
        #endregion

        #region Helper Functions
        private void GetProxySettings()
        {
            //Proxy Settings
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());
            _useProxy = false;
            try
            {
                _useProxy = string.Equals(configMgr.Settings["UseProxy"], "true", StringComparison.OrdinalIgnoreCase);
                _proxyAddress = configMgr.Settings["ProxyAddressPort"];
                _proxyUserName = configMgr.Settings["ProxyUserName"];
                _proxyPassword = configMgr.Settings["ProxyPassword"];
                _proxyAuthScheme = configMgr.Settings["ProxyAuthScheme"];
                this.Log(string.Format("UseProxy:{0} AddressAndPort:{1} UserName:{2} Password:{3} AuthScheme:{4}", _useProxy.ToString(), _proxyAddress.ToString(), _proxyUserName.ToString(), _proxyPassword.ToString(), _proxyAuthScheme.ToString()), false, false);
            }
            catch { _useProxy = false; }
        }

        private bool IsUsingBits()
        {
            //Proxy Settings
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());
            bool isUsingBits = true;
            string downloadMethod = "BITS"; //By default
            try
            {
                downloadMethod = string.IsNullOrEmpty(configMgr.Settings["DownloadMethod"]) ? "BITS" : configMgr.Settings["DownloadMethod"];
                isUsingBits = string.Equals(downloadMethod, "BITS", StringComparison.OrdinalIgnoreCase);
            }
            catch { /* Suppress Error */ }
            this.Log(string.Format("DownloadMethod : {0}", downloadMethod), false, false);
            return isUsingBits;
        }

        private string[] Download(string remoteFolder, string localFolder, string[] srcFiles)
        {
            if (!Directory.Exists(localFolder)) Directory.CreateDirectory(localFolder);

            System.Net.BITS.Manager jobManager = new System.Net.BITS.Manager();
            this.Log(jobManager.Version.ToString(), false, false);
            System.Net.BITS.Job job = new System.Net.BITS.Job(this.Title,
                System.Net.BITS.JobType.Download, System.Net.BITS.JobPriority.Foreground);
            jobManager.Jobs.Add(job);

            //Apply Proxy Settings if required
            if (_useProxy)
            {
                if (_proxyAddress != "")
                {
                    System.Net.BITS.JobProxySettings settings = new System.Net.BITS.JobProxySettings(System.Net.BITS.ProxyUsage.Override, "http=" + _proxyAddress, null);
                    job.ProxySettings = settings;
                }

                System.Net.BITS.Credentials cred = new System.Net.BITS.Credentials();
                cred.Target = System.Net.BITS.AuthTarget.Proxy;
                //Get Auth Scheme
                if (_proxyAuthScheme != "") cred.Scheme = GetAuthScheme(_proxyAuthScheme);

                if (_proxyUserName != "") cred.UserName = ConvertStringToSecureString(_proxyUserName);
                else cred.UserName = null;

                if (_proxyPassword != "") cred.Password = ConvertStringToSecureString(Encoding.UTF8.GetString(Convert.FromBase64String(_proxyPassword)));
                else cred.Password = null;
                //Set Credentials
                job.SetCredentials(cred);
            }

            // IMP: Cannot use Path.Combine() for Urls
            if (Path.GetFileName(remoteFolder) != "") remoteFolder = remoteFolder + "/";

            List<string> fileList = new List<string>();
            foreach (string file in srcFiles)
            {
                string remoteFile = remoteFolder + Path.GetFileName(file);
                string tgtFile = Path.Combine(localFolder, Path.GetFileName(file));
                job.Files.Add(remoteFile, tgtFile);
                fileList.Add(tgtFile);
            }
            job.AutoComplete = true;
            job.AutoCancelOnError = false;
            job.AutoCancelOnTransientError = false;
            jobManager.Jobs.Update();
            string error = string.Empty;
            if (job.Error.ContextDescription != null && job.Error.Description != null)
                error = "Context Description: " + job.Error.ContextDescription + " Error Description: " + job.Error.Description + " Job Owner: " + job.Owner;

            job.Resume(); // start the job in action

            while (job.State != System.Net.BITS.JobState.Transferred &&
                    job.State != System.Net.BITS.JobState.Cancelled &&
                    job.State != System.Net.BITS.JobState.Acknowledged &&
                    job.State != JobState.Error)
                System.Threading.Thread.Sleep(5000);

            if (job.State == JobState.Error || job.State == JobState.Cancelled)
            {
                if (job.State == JobState.Error)
                {
                    if (job.Error.ContextDescription != null && job.Error.Description != null)
                        error = "Context Description: " + job.Error.ContextDescription + " Error Description: " + job.Error.Description + " Job Owner: " + job.Owner;
                }
                job.Cancel();
                job.Dispose();
                throw new Exception("Error in BITS download. Job State:" + job.State + " " + error);
            }
            else
            {
                job.Dispose();
                return fileList.ToArray();
            }
        }

        private AuthScheme GetAuthScheme(string proxyAuthScheme)
        {
            proxyAuthScheme = proxyAuthScheme.ToLower();
            switch (proxyAuthScheme)
            {
                case "basic": return AuthScheme.Basic;
                case "digest": return AuthScheme.Digest;
                case "ntlm": return AuthScheme.NTLM;
                case "negotiate": return AuthScheme.Negotiate;
                case "passport": return AuthScheme.Passport;
            }
            return AuthScheme.Negotiate;
        }

        private SecureString ConvertStringToSecureString(string value)
        {
            Char[] input = value.ToCharArray();
            SecureString SecureString = new SecureString();

            for (int idx = 0; idx < input.Length; idx++)
            {
                SecureString.AppendChar(input[idx]);
            }
            return SecureString;
        }

        private void LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load ServiceUrl
            this.ServiceUrl = configMgr.Settings["ServiceUrl"];
            if (string.IsNullOrEmpty(ServiceUrl)) throw new Exception("Configuration Error: 'ServiceUrl' can not be empty.");

            // 2. Load Package Folder
            this.PackageFolder = configMgr.Settings["PackageFolder"];
            if (string.IsNullOrEmpty(PackageFolder)) throw new Exception("Configuration Error: 'PackageFolder' can not be empty.");

            // 3. Load partner
            SetPartner();
        }

        private void SetPartner()
        {
            //Load the partner using partner list.
            SPWeb web = (new SPSite(this.WebUrl)).OpenWeb();
            try
            {
                this.Partner = web.Lists["Partners"].Items[0]["LinkTitle"].ToString();
                if (string.IsNullOrEmpty(this.Partner))
                    throw new Exception("Partner name cannot be empty.");
            }
            catch (Exception ex) { throw new Exception("Configuration Error: " + ex.Message); }
            finally { if (web != null) web.Dispose(); }
        }
        #endregion
    }
}
