﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;
using System.ComponentModel;

namespace Indigo.Athena.Jobs
{
    public class HttpDownloader
    {
        public ProxySettings Settings { get; set; }
        public bool IsProxy { get; set; }
        public List<string> Files { get; set; }
        public Exception Error { get; set; }

        public HttpDownloader(ProxySettings settings)
        {
            Settings = settings;
            IsProxy = true;
        }

        public HttpDownloader()
        {
            IsProxy = false;
        }

        public void Start(string remoteFolder, string localFolder, List<string> files)
        {
            FileDownloader downloader = new FileDownloader();
            //Mandatory
            downloader.LocalDirectory = localFolder;
            downloader.RemoteFolder = remoteFolder;
            downloader.Files.AddRange(files);

            //Optional
            if (IsProxy == true)
                downloader.SetProxy(Settings.ProxyAddress, Settings.ProxyUserName, Settings.ProxyPassword);
            downloader.MAX_ALLOWED_NUMBER_OF_TRIES = 1;
            downloader.PACKAGE_SIZE = 2048;
            downloader.THREAD_SLEEP_BETWEEN_EXCEPTION = 5 * 1000;

            //Starts the Download
            downloader.Start();

            //Loop to check the progress
            while (downloader.IsBusy)
            {
                Thread.Sleep(2000);
            }
            if (downloader.Error != null) throw downloader.Error;
            Files = downloader.TargetFiles;
        }
    }

    #region File Downloader
    public class FileDownloader
    {
        #region Public Members
        /// <summary>
        /// If any exception occurred the downloader will sleep for this time (in milliseconds). Increase time if net connectivity is poor.
        /// </summary>
        public int THREAD_SLEEP_BETWEEN_EXCEPTION = 5 * 1000;
        /// <summary>
        /// Maximum Number of Tries for which downloader will check for exception
        /// </summary>
        public int MAX_ALLOWED_NUMBER_OF_TRIES = 5;
        /// <summary>
        /// Package Size in which file will be downloaded
        /// </summary>
        public int PACKAGE_SIZE = 4096;
        #endregion

        #region Public Properties
        /// <summary>
        /// Remote Folder URL which will added to file names to make an absolute URL
        /// </summary>
        public string RemoteFolder { get; set; }
        /// <summary>
        /// Files which needs to be downloaded
        /// </summary>
        public List<string> Files { get; set; }
        /// <summary>
        /// Local Directory path where files will be downloaded
        /// </summary>
        public string LocalDirectory { get; set; }
        /// <summary>
        /// Indicates Progress of the Download
        /// </summary>
        public int PercentProgress { get { return Convert.ToInt32(_crntProgress); } }
        /// <summary>
        /// Indicates if the download is in progress or not
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            private set
            {
                if (value == true)
                {
                    _bgwDownloader.WorkerSupportsCancellation = true;
                    _bgwDownloader.RunWorkerAsync();
                }
                _isBusy = value;
            }
        }
        /// <summary>
        /// List containing File Paths where Remote Files will be downloaded
        /// </summary>
        public List<string> TargetFiles { get; set; }
        /// <summary>
        /// Gets the Error occured
        /// </summary>
        public Exception Error { get { return _ex; } }
        #endregion

        #region Private Constants
        private const int MAX_PROGRESS_TRIES_BEFORE_ABORT = 3;
        private const int Monitor_Progress_Timer_Interval = 5 * 1000;
        #endregion

        #region Private Members
        // The thread inside which the download happens
        private Thread _thrDownload;
        // The stream of data retrieved from the web server
        private Stream _strResponse;
        // The stream of data that we write to the hard drive
        private Stream _strLocal;
        // The request to the web server for file information
        private HttpWebRequest _webRequest;
        // The response from the web server containing information about the file
        private HttpWebResponse _webResponse;
        // The delegate which we will call from the thread to update the form
        private delegate void UpdateProgessCallback(Int64 BytesRead, Int64 TotalBytes);
        //File Index being downloaded
        private int _crntFile = 0;
        //Number of tries
        private int _triesCount = 0;
        // The download worker
        private BackgroundWorker _bgwDownloader = new BackgroundWorker();
        //Exception occurred
        private Exception _ex;
        private bool _isBusy;
        UpdateProgessCallback progressDelegate = null;
        private decimal _crntProgress = 0.0M;
        private decimal _lstProgress = 0.0M;
        private System.Timers.Timer _monitorProgressTimer = new System.Timers.Timer();
        private int _crntProgressTries = 0;
        // Proxy
        private bool _useProxy;
        private string _proxyAddress, _userName, _password;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructs the File Downloader
        /// </summary>
        public FileDownloader()
        {
            Files = new List<string>();
            TargetFiles = new List<string>();
            _bgwDownloader.DoWork += new DoWorkEventHandler(bgwDownloader_DoWork);
            progressDelegate = new UpdateProgessCallback(UpdateProgress);
            _monitorProgressTimer.Elapsed += new System.Timers.ElapsedEventHandler(monitorProgressTimer_Elapsed);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Start the download, set all mandatory fields before calling Start
        /// </summary>
        public void Start()
        {
            #region Validate
            if (string.IsNullOrEmpty(this.RemoteFolder) || this.RemoteFolder.Trim() == string.Empty) throw new ArgumentException("RemoteFolder is not set.");
            if (string.IsNullOrEmpty(this.LocalDirectory) || this.LocalDirectory.Trim() == string.Empty) throw new ArgumentException("LocalDirectory is not set.");
            if (this.Files.Count == 0) throw new ArgumentException("No files to download.");
            //Create directory if doesn't exists
            if (!Directory.Exists(this.LocalDirectory)) { Directory.CreateDirectory(this.LocalDirectory); }
            #endregion

            //Setting IsBusy to True, starts the back ground thread
            this.IsBusy = true;
            _monitorProgressTimer.Interval = Monitor_Progress_Timer_Interval;
        }
        /// <summary>
        /// Sets the Proxy
        /// </summary>
        /// <param name="address">Proxy Server address with port</param>
        /// <param name="userName">Proxy User Name</param>
        /// <param name="password">Proxy Password</param>
        public void SetProxy(string address, string userName, string password)
        {
            this._useProxy = true;
            this._proxyAddress = address;
            this._userName = userName;
            this._password = password;
        }
        #endregion

        #region Private Helper Methods
        private void bgwDownloader_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                try
                {
                    _thrDownload = new Thread(Download);
                    _thrDownload.Start();
                    _monitorProgressTimer.Start();
                    _thrDownload.Join();
                }
                catch (Exception) { }
                _monitorProgressTimer.Stop();
                //If Max tries are exhausted then abort download 
                if (_triesCount >= MAX_ALLOWED_NUMBER_OF_TRIES)
                {
                    _isBusy = false;
                    _bgwDownloader.CancelAsync();
                    return;
                }

                //If there is exception, sleep for some time
                if (_ex != null)
                {
                    Thread.Sleep(THREAD_SLEEP_BETWEEN_EXCEPTION);
                    _ex = null;
                }

                //If all the files are downloaded then break
                if (_crntFile >= Files.Count) break;
            }
            this.IsBusy = false;
        }
        //Downloads the current file
        private void Download()
        {
            string localFilePath = string.Empty;
            try
            {
                string remoteUrl = RemoteFolder + "/" + Path.GetFileName(Files[_crntFile]);
                localFilePath = Path.Combine(LocalDirectory, Path.GetFileName(Files[_crntFile]));

                // Put the object argument into an int variable
                int startPointInt = 0;
                if (File.Exists(localFilePath)) File.Delete(localFilePath);
                //startPointInt = Convert.ToInt32(new FileInfo(localFilePath).Length);
                // Create a request to the file we are downloading
                _webRequest = (HttpWebRequest)WebRequest.Create(remoteUrl);
                // Set the starting point of the request
                //_webRequest.AddRange(startPointInt);

                // Set default authentication for retrieving the file
                if (_useProxy) _webRequest.Proxy = GetProxy();

                // Retrieve the response from the server
                _webResponse = (HttpWebResponse)_webRequest.GetResponse();

                // Ask the server for the file size and store it
                Int64 fileSize = _webResponse.ContentLength;

                // Open the URL for download 
                _strResponse = _webResponse.GetResponseStream();

                // Create a new file stream where we will be saving the data (local drive)
                if (startPointInt == 0) _strLocal = new FileStream(localFilePath, FileMode.Create, FileAccess.Write, FileShare.None);
                else _strLocal = new FileStream(localFilePath, FileMode.Append, FileAccess.Write, FileShare.None);

                // It will store the current number of bytes we retrieved from the server
                int bytesSize = 0;
                // A buffer for storing and writing the data retrieved from the server
                var downBuffer = new byte[PACKAGE_SIZE];

                // Loop through the buffer until the buffer is empty
                while ((bytesSize = _strResponse.Read(downBuffer, 0, downBuffer.Length)) > 0)
                {
                    // Write the data from the buffer to the local hard drive
                    _strLocal.Write(downBuffer, 0, bytesSize);
                    // Invoke the method that updates the form's label and progress bar
                    progressDelegate.BeginInvoke(_strLocal.Length, fileSize + startPointInt, null, null);
                }
                TargetFiles.Add(localFilePath);
                _crntFile++;
                _triesCount = 0;
            }
            catch (Exception e)
            {
                //Means file is fully downloaded
                if (e != null && string.IsNullOrEmpty(e.Message) == false && e.Message.Contains("(416) Requested Range Not Satisfiable"))
                {
                    TargetFiles.Add(localFilePath);
                    _crntFile++;
                    _triesCount = 0;
                    return;
                }
                _ex = e;
                _triesCount++;
            }
            finally
            {
                // When the above code has ended, close the streams
                if (_strResponse != null) _strResponse.Close();
                if (_strLocal != null) _strLocal.Close();
            }
        }
        //Sets the Proxy Credentials the web request
        private IWebProxy GetProxy()
        {
            var myProxy = new WebProxy();
            // Create a new Uri object.
            Uri newUri = new Uri(_proxyAddress);
            // Associate the newUri object to 'myProxy' object so that new myProxy settings can be set.
            myProxy.Address = newUri;
            // Create a NetworkCredential object and associate it with the 
            // Proxy property of request object.
            myProxy.Credentials = new NetworkCredential(this._userName, this._password);
            return myProxy;
        }
        // Calculate the download progress in percentages
        private void UpdateProgress(Int64 BytesRead, Int64 TotalBytes)
        {
            _crntProgress = (BytesRead * 100.0M / TotalBytes) / Files.Count;
            if (_crntFile != Files.Count) _crntProgress += _crntFile * 100.0M / Files.Count;
        }
        //Monitors progress and if progress is not changed, aborts the download thread and resumes the download in new thread
        private void monitorProgressTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_thrDownload != null && _thrDownload.IsAlive == false) return;
            if (_lstProgress != _crntProgress)
            {
                _lstProgress = _crntProgress;
                _crntProgressTries = 0;
            }
            else
            {
                if (_crntProgressTries >= MAX_PROGRESS_TRIES_BEFORE_ABORT)
                {
                    _crntProgressTries = 0;
                    // Close the web response and the streams
                    if (_webResponse != null) _webResponse.Close();
                    if (_strResponse != null) _strResponse.Close();
                    if (_strLocal != null) _strLocal.Close();
                    _thrDownload.Abort();
                    return;
                }
                _crntProgressTries++;
            }
        }
        #endregion
    }
    #endregion

    public class ProxySettings
    {
        private string _password = string.Empty;
        public bool UseProxy { get; set; }
        public string ProxyAddress { get; set; }
        public string AuthScheme { get; set; }
        public string ProxyUserName { get; set; }
        public string ProxyPassword
        {
            get { return System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(_password)); }
            set { _password = value; }
        }
    }
}
