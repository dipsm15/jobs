using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Indigo.Athena.Jobs
{
    public class PurgeJob : Job
    {
        public PurgeJob() { }      // IMP: Default Constructor for Serialization

        public PurgeJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Purge, spWeb)
        {
        }

        #region Properties
        private int ContentAgingDays = 90;
        private string[] PurgeLists;
        #endregion

        public override void Execute(Guid targetInstanceId)
        {
            SPSite spSite = null;
            SPWeb spWeb = null;
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Deleting Data";
                // 1. Get SPWeb Context
                spSite = new SPSite(this.WebUrl);
                spWeb = spSite.OpenWeb();
                _LoadConfig();

                foreach (string listName in this.PurgeLists)
                {
                    SPListItemCollection items = GetObsoleteContent(spWeb, listName, ContentAgingDays);
                    int itemCount = items.Count;
                    this.Log(string.Format("ListName : {0},Item count to be deleted : {1}", listName, itemCount), false, false);
                    while (itemCount > 0)
                    {
                        items[itemCount - 1].Delete();
                        itemCount = items.Count;
                    }
                }

                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                // Release resources
                if (spSite != null) spSite.Dispose();
                if (spWeb != null) spWeb.Dispose();
            }
        }

        #region Helper Functions
        private SPListItemCollection GetObsoleteContent(SPWeb spWeb, string listName, int daysCreatedBefore)
        {
            #region Build Query

            string queryTemplate = string.Empty;
            queryTemplate = "<Where><Lt><FieldRef Name='Created'/><Value Type='DateTime'>$DateCreated$</Value></Lt></Where>";
            queryTemplate = queryTemplate.Replace("$DateCreated$", DateTime.Today.AddDays(-daysCreatedBefore).ToString("yyyy-MM-ddThh:mm:ssZ"));

            SPQuery query = new SPQuery();
            query.Query = queryTemplate;

            #endregion

            SPList parentList = spWeb.Lists[listName];
            return parentList.GetItems(query);
        }

        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load Purge Lists
            List<string> contentLists = new List<string>(configMgr.Settings["ContentList"].Split(','));
            //2. List Of lists that should not be deleted and other validation.
            List<string> checkListValues = new List<string>();
            checkListValues.Add("Categories"); checkListValues.Add("Config");
            checkListValues.Add("");

            foreach (string value in checkListValues)
            {
                if (contentLists.Contains(value))
                    contentLists.Remove(value);
            }

            for (int i = 0; i < contentLists.Count; i++)
                contentLists[i] = contentLists[i].Trim();
            this.PurgeLists = contentLists.ToArray();
            if (PurgeLists.Length == 0) throw new Exception("Configuration Error: 'ContentList' can not be empty.");

            try { this.ContentAgingDays = int.Parse(configMgr.Settings["ContentAgingDays"]); }
            catch { }
            //Limitation placed that ContentAgingDays can never be less than 90. 
            if (ContentAgingDays < 90)
                ContentAgingDays = 90;
        }
        #endregion
    }
}
