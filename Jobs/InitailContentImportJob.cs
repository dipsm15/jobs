using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using Indigo.Athena.Synchronization;

namespace Indigo.Athena.Jobs
{
    public class InitialContentImporterJob : Job
    {
        public InitialContentImporterJob() { }      // IMP: Default Constructor for Serialization

        public InitialContentImporterJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.InitialImport, spWeb)
        {
        }
        #region Properties
        [Persisted]
        public string ImportFolder;
        #endregion

        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Importing";

                this.Log(string.Format("Importing package from '{0}'", ImportFolder), false, true);
                /// Step 1. Create import settings
                SPImportSettings settings = new SPImportSettings();
                settings.RetainObjectIdentity = true;
                settings.CommandLineVerbose = true;
                settings.FileCompression = false;

                settings.SiteUrl = this.WebUrl;
                settings.LogFilePath = this.LogFile;
                settings.FileLocation = ImportFolder;

                /// Step 2. Import
                SPImport importer = new SPImport(settings);
                importer.Run();
                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
        }
    }
}
