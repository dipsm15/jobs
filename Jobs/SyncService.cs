using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Collections;
using Indigo.Athena;
using Indigo.Athena.Synchronization;
using Microsoft.SharePoint;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel.Channels;

namespace Indigo.Athena.Services
{
    #region Data Contracts
    #region Package Data Contract

    [Serializable]
    [DataContractAttribute(Namespace = "http://Athena.IndigoArchitects.com")]
    public class Package
    {
        public Package() { }

        [DataMember(Name = "Id", IsRequired = true)]
        public Guid Id;
        [DataMember(Name = "Title")]
        public string Title;
        [DataMember(Name = "Partner")]
        public string Partner;
        [DataMember(Name = "Files")]
        public string[] Files;
        [DataMember(Name = "PackageDate")]
        public DateTime? DatePackaged;
        [DataMember(Name = "ExpirationDate")]
        public DateTime? DateExpires;
        [DataMember(Name = "PackageType")]
        public string PackageType;
        [DataMember(Name = "Status")]
        public string Status;
        [DataMember(Name = "LocalFolder")]
        public string LocalFolder;
        [DataMember(Name = "RemoteFolder")]
        public string RemoteFolder;
    }

    [Serializable]
    [DataContractAttribute(Namespace = "http://Athena.IndigoArchitects.com")]
    public class PackageCriteria
    {
        [DataMember(Name = "Id")]
        public Guid? Id;
        [DataMember(Name = "Title")]
        public string Title;
        [DataMember(Name = "Partner")]
        public string Partner;
        [DataMember(Name = "ValidFrom")]
        public DateTime? ValidFrom;
        [DataMember(Name = "ValidTo")]
        public DateTime? ValidTo;
        [DataMember(Name = "Status")]
        public string Status;
        [DataMember(Name = "PackageType")]
        public string PackageType;
    }

    #endregion Package Data Contract
    #region RecallItem Data Contract

    [DataContractAttribute(Namespace = "http://Athena.IndigoArchitects.com")]
    public class RecallItem
    {
        [DataMember(Name = "ItemId", IsRequired = true)]
        public Guid ItemId;
        [DataMember(Name = "ListId", IsRequired = true)]
        public Guid ListId;
        [DataMember(Name = "Title")]
        public string Title;
        [DataMember(Name = "RecallDate")]
        public DateTime RecallDate;
        [DataMember(Name = "Status")]
        public string Status;
    }

    [DataContractAttribute(Namespace = "http://Athena.IndigoArchitects.com")]
    public class RecallItemCriteria
    {
        [DataMember(Name = "ItemId")]
        public Guid? ItemId;
        [DataMember(Name = "ListId")]
        public Guid? ListId;
        [DataMember(Name = "Partner")]
        public string Partner;
        [DataMember(Name = "MinRecallDate")]
        public DateTime? MinRecallDate;
        [DataMember(Name = "MaxRecallDate")]
        public DateTime? MaxRecallDate;
        [DataMember(Name = "Status")]
        public string Status;
    }

    #endregion Package Data Contract
    #region Usage Upload
    [DataContract(Namespace = "http://Athena.IndigoArchitects.com")]
    public class FileData
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public byte[] Data { get; set; }
        [DataMember]
        public int Count { get; set; }
        [DataMember]
        public int CurrentChunkCount { get; set; }
        [DataMember]
        public int TotalChunckCount { get; set; }
        public override string ToString()
        {
            return string.Format("FN-{0}|CCC-{1}|TCC-{2}", this.FileName, this.CurrentChunkCount, this.TotalChunckCount);
        }
    }
    [DataContract(Namespace = "http://Athena.IndigoArchitects.com")]
    public class Result
    {
        public Result() { Message = new List<string>(); }
        [DataMember]
        public bool IsSuccessful { get; set; }
        [DataMember]
        public List<string> Message { get; set; }
    }
    #endregion
    /*
    */
    #endregion Data Contracts


    [ServiceContract(Namespace = "http://Athena.IndigoArchitects.com")]
    public interface ISyncService
    {
        [OperationContract(Name = "FindPackages")]
        Package[] FindPackages(PackageCriteria criteria);

        [OperationContract(Name = "FindRecalledItems")]
        RecallItem[] FindRecalledItems(RecallItemCriteria criteria);

        [OperationContract(Name = "GetUploadLocationByPartner")]
        string GetUploadLocationByPartner(string parnterName, string fileName, string packageType);

        [OperationContract(Name = "UpdateUploadStatus")]
        void UpdateUploadStatus(string parnterName, string fileName, bool uploadCompleted);

        [OperationContract(Name = "ReportUpdateStatus")]
        void ReportUpdateStatus(Guid manifestId, string partner, string status, byte[] logFile, bool success);

        [OperationContract(Name = "AddContentReport")]
        void AddContentReport(string partnerName, string filename);

        [OperationContract(Name = "UploadUsage")]
        Result Upload(FileData fileData);
    }


    #region Faults
    /// <summary>
    /// Fault contract that will send all the unhandled exception to the client.
    /// </summary>
    [DataContract(Namespace = "Athena/Services/V-1.0")]
    public class SystemFault
    {
        private Guid _id;
        private string _message = "An error has occurred while consuming this service. Please contact your system administrator for more information.";

        /// <summary>
        /// Gets the system defined message.
        /// </summary>
        /// <value>The message.</value>
        [DataMember(IsRequired = false, Name = "Message", Order = 0)]
        public String Message
        {
            get { return _message; }
            set { _message = value; }
        }

        /// <summary>
        /// Gets or sets the error id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember(IsRequired = false, Name = "Id", Order = 1)]
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
    }

    #endregion

    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
    public class SyncService : ISyncService
    {

        public enum ServiceCallType { List_Download_Packages, Initiate_Upload, Update_Upload_Status, List_Recall_items, Report_Update_Status, Add_Content_Report };
        public enum UpdateStatus { Success, Error };

        public Package[] FindPackages(PackageCriteria criteria)
        {

            SPSite spSite = null;
            SPWeb spWeb = null;
            try
            {
                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                if (string.IsNullOrEmpty(siteUrl)) throw new Exception("SiteUrl not set");

                spSite = new SPSite(siteUrl);
                spWeb = spSite.OpenWeb();

                Processor currentArchitecture = _FindArchitecture(criteria.Partner);
                PackageManager pkgMgr = new PackageManager(spWeb);
                Indigo.Athena.Synchronization.PackageCriteria pkgCriteria = Convert(criteria);
                pkgCriteria.ProcessorArchitecture = currentArchitecture;
                Indigo.Athena.Synchronization.Package[] pkgs = pkgMgr.Find(pkgCriteria);
                Package[] pkgList = Convert(pkgs);
                ServiceLog(ServiceCallType.List_Download_Packages, true, pkgList.Length.ToString() + " Packages found.", criteria.Partner);
                return pkgList;
            }
            catch (Exception ex)
            {
                ServiceLog(ServiceCallType.List_Download_Packages, false, ex.Message, criteria.Partner);
                Utilities.Log("FindPackages Error: ", ex);
                throw ex;
            }
            finally
            {
                if (spWeb != null) spWeb.Dispose();
                if (spSite != null) spSite.Dispose();
            }
        }

        public RecallItem[] FindRecalledItems(RecallItemCriteria criteria)
        {
            SPSite spSite = null;
            SPWeb spWeb = null;
            if (criteria.Partner == string.Empty || criteria.Partner == null)
                throw new Exception("Partner cannot be empty in criteria. Please check partner value.");

            try
            {
                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                if (string.IsNullOrEmpty(siteUrl)) throw new Exception("SiteUrl not set");

                spSite = new SPSite(siteUrl);
                spWeb = spSite.OpenWeb();
                RecallManager recallMgr = new RecallManager(spWeb);
                Indigo.Athena.RecallItem[] recallItems = recallMgr.Find(Convert(criteria));
                string date = string.Empty;
                if (criteria.MinRecallDate != null)
                    date = criteria.MinRecallDate.ToString();
                ServiceLog(ServiceCallType.List_Recall_items, true, recallItems.Length.ToString() + " Recall Items found for Sync Date " + date, criteria.Partner);
                return Convert(recallItems);
            }
            catch (Exception ex)
            {
                ServiceLog(ServiceCallType.List_Recall_items, false, ex.Message, criteria.Partner);
                Utilities.Log("FindRecalledItems Error: ", ex);
                throw ex;
            }
            finally
            {
                if (spWeb != null) spWeb.Dispose();
                if (spSite != null) spSite.Dispose();
            }
        }

        public string GetUploadLocationByPartner(string partnerName, string fileName, string packageType)
        {
            SPSite site = null;
            SPWeb web = null;
            try
            {
                if (string.IsNullOrEmpty(partnerName) || partnerName.ToLower() == "default")
                    throw new Exception("Invalid partner:" + partnerName);

                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                site = new SPSite(siteUrl);
                web = site.OpenWeb();

                SPListItemCollection partners = SPListLib.GetAllItems("Partners", string.Empty, web);
                SPListItem partner = null;
                foreach (SPListItem currentPartner in partners)
                {
                    string currentPartnerName = currentPartner["LinkTitle"].ToString();
                    if (string.Compare(currentPartnerName, partnerName, true) == 0)
                        partner = currentPartner;
                }

                if (partner == null) throw new Exception("Invalid partner:" + partnerName);

                /// Once the partner is found add the File entry to the "Upload Log" list.
                ConfigManager manager = new ConfigManager(web);
                if (string.IsNullOrEmpty(manager.Settings["UsageUploadUrl"]) ||
                    string.IsNullOrEmpty(manager.Settings["UsageUploadDir"]))
                    throw new Exception("Configuration Error: Usage Upload is not configured.");
                string uploadUrl = partner.UniqueId.ToString(); //manager.Settings["UsageUploadUrl"].TrimEnd('/') + "/" + partner.UniqueId.ToString();
                string uploadDir = manager.Settings["UsageUploadDir"].TrimEnd('\\') + "\\" + partner.UniqueId.ToString();
                if (!Directory.Exists(manager.Settings["UsageUploadDir"]))
                    throw new Exception("Configuration Error: Usage Upload configuration is incorrect.");
                if (!Directory.Exists(uploadDir))
                    Directory.CreateDirectory(uploadDir);

                try
                {
                    string filePath = uploadDir.TrimEnd('/', '\\') + "\\" + System.IO.Path.GetFileName(fileName);
                    SPListItem uploadLog = web.Lists["Upload Log"].Items.Add();
                    SetFieldValue(uploadLog, "Title", "N/A", web);
                    SetFieldValue(uploadLog, "Partner", partnerName, web);
                    SetFieldValue(uploadLog, "Filename", filePath, web);
                    SetFieldValue(uploadLog, "Status", "Uploaded", web);
                    SetFieldValue(uploadLog, "Package Type", packageType, web);
                    uploadLog.Update();
                }
                catch { throw new Exception("Configuration Error: 'Upload Log' list schema"); }
                ServiceLog(ServiceCallType.Initiate_Upload, true, "Upload Initiated for file " + Path.GetFileName(fileName) + " at upload url: " + manager.Settings["UsageUploadUrl"].TrimEnd('/') + "/" + partner.UniqueId.ToString(), partnerName);
                return uploadUrl;
            }
            catch (Exception ex)
            {
                ServiceLog(ServiceCallType.Initiate_Upload, false, "Upload Initiation failed for file " + Path.GetFileName(fileName) + " with following error: " + ex.Message, partnerName);
                Utilities.Log("GetUploadLocation Error: " + ex.Message);
                throw ex;
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }

        public void UpdateUploadStatus(string partnerName, string fileName, bool uploadCompleted)
        {
            SPSite site = null;
            SPWeb web = null;
            try
            {
                if (string.IsNullOrEmpty(partnerName) || partnerName.ToLower() == "default")
                    throw new Exception("Invalid partner:" + partnerName);

                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                site = new SPSite(siteUrl);
                web = site.OpenWeb();

                SPListItemCollection partners = SPListLib.GetAllItems("Partners", string.Empty, web);
                SPListItem partner = null;
                foreach (SPListItem currentPartner in partners)
                {
                    string currentPartnerName = currentPartner["LinkTitle"].ToString();
                    if (string.Compare(currentPartnerName, partnerName, true) == 0)
                        partner = currentPartner;
                }

                if (partner == null) throw new Exception("Invalid partner:" + partnerName);

                /// Once the partner is found add the File entry to the "Upload Log" list.
                ConfigManager manager = new ConfigManager(web);
                if (string.IsNullOrEmpty(manager.Settings["UsageUploadUrl"]) ||
                    string.IsNullOrEmpty(manager.Settings["UsageUploadDir"]))
                    throw new Exception("Configuration Error: Usage Upload is not configured.");
                string uploadUrl = manager.Settings["UsageUploadUrl"].TrimEnd('/') + "/" + partner.UniqueId.ToString();
                string uploadDir = manager.Settings["UsageUploadDir"].TrimEnd('\\') + "\\" + partner.UniqueId.ToString();
                if (!Directory.Exists(manager.Settings["UsageUploadDir"]))
                    throw new Exception("Configuration Error: Usage Upload configuration is incorrect.");
                if (!Directory.Exists(uploadDir))
                    Directory.CreateDirectory(uploadDir);

                try
                {
                    string filePath = uploadDir.TrimEnd('/', '\\') + "\\" + System.IO.Path.GetFileName(fileName);
                    SPListItem uploadLog = GetUploadItem(partnerName, filePath);
                    if (uploadCompleted == true)
                        SetFieldValue(uploadLog, "Status", "Uploaded", web);
                    else
                        SetFieldValue(uploadLog, "Status", "Error", web);
                    uploadLog.Update();
                }
                catch { throw new Exception("Configuration Error: 'Upload Log' list schema"); }
                if (uploadCompleted == true)
                    ServiceLog(ServiceCallType.Update_Upload_Status, true, "Upload Status for file " + Path.GetFileName(fileName) + " has been updated to: " + "Uploaded", partnerName);
                else
                    ServiceLog(ServiceCallType.Update_Upload_Status, true, "Upload Status for file " + Path.GetFileName(fileName) + " has been updated to: " + "Error", partnerName);

            }
            catch (Exception ex)
            {
                ServiceLog(ServiceCallType.Update_Upload_Status, false, "Upload Status for file " + fileName + " has failed with following error: " + ex.Message, partnerName);
                Utilities.Log("GetUploadLocation Error: " + ex.Message);
                throw ex;
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }

        public void ReportUpdateStatus(Guid manifestId, string partner, string details, byte[] logFile, bool success)
        {
            try
            {
                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                SPSite site = new SPSite(siteUrl);
                SPWeb web = site.OpenWeb();

                SPList list = web.Lists["Auto Updater Log"];
                SPListItem item = list.Items.Add();
                //Operation
                NameValueCollection criteria = new NameValueCollection();
                criteria.Add("ManifestID", manifestId.ToString());
                criteria.Add("Partner", partner);
                SPListItemCollection curItems = SPListLib.GetItems(list.Title, string.Empty, criteria, web);

                if (curItems.Count == 0)
                {
                    SetFieldValue(item, "ManifestID", manifestId.ToString(), web);
                    //Status
                    if (success)
                        SetFieldValue(item, "Status", UpdateStatus.Success.ToString(), web);
                    else
                        SetFieldValue(item, "Status", UpdateStatus.Error.ToString(), web);

                    SetFieldValue(item, "Details", details, web);
                    //Partner
                    SetFieldValue(item, "Partner", partner, web);
                    item.Attachments.Add(DateTime.Now.ToString("yyyyMMddHHmmss") + ".log", logFile);
                    item.Update();
                }
                else
                {
                    foreach (SPListItem curItem in curItems)
                    {
                        SetFieldValue(curItem, "ManifestID", manifestId.ToString(), web);
                        //Status
                        if (success)
                            SetFieldValue(curItem, "Status", UpdateStatus.Success.ToString(), web);
                        else
                            SetFieldValue(curItem, "Status", UpdateStatus.Error.ToString(), web);

                        SetFieldValue(curItem, "Details", details, web);
                        //Partner
                        SetFieldValue(curItem, "Partner", partner, web);

                        curItem.Attachments.Add(DateTime.Now.ToString("yyyyMMddHHmmss") + ".log", logFile);
                        curItem.Update();
                    }
                }
                ServiceLog(ServiceCallType.Report_Update_Status, true, "Report Update Status for manifest ID  " + manifestId.ToString() + " has been successful", partner);
            }
            catch (Exception ex)
            {
                ServiceLog(ServiceCallType.Report_Update_Status, false, "Report Update Status for manifest ID  " + manifestId.ToString() + " has failed with following error: " + ex.Message, partner);
                Utilities.Log("ReportUpdateStatus Error: " + ex.Message);
                throw ex;
            }
        }

        public void AddContentReport(string partnerName, string filename)
        {
            SPSite site = null;
            SPWeb web = null;
            SPListItem uploadLogEntry = null;
            try
            {
                if (string.IsNullOrEmpty(partnerName) || partnerName.ToLower() == "default")
                    throw new Exception("Invalid partner:" + partnerName);

                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                site = new SPSite(siteUrl);
                web = site.OpenWeb();

                SPListItemCollection partners = SPListLib.GetAllItems("Partners", string.Empty, web);
                SPListItem partner = null;
                foreach (SPListItem currentPartner in partners)
                {
                    string currentPartnerName = currentPartner["LinkTitle"].ToString();
                    if (string.Compare(currentPartnerName, partnerName, true) == 0)
                        partner = currentPartner;
                }

                if (partner == null) throw new Exception("Invalid partner:" + partnerName);

                ConfigManager manager = new ConfigManager(web);
                if (string.IsNullOrEmpty(manager.Settings["UsageUploadDir"]))
                    throw new Exception("Configuration Error: Usage Upload is not configured.");
                string uploadDir = manager.Settings["UsageUploadDir"].TrimEnd('\\') + "\\" + partner.UniqueId.ToString();
                if (!Directory.Exists(uploadDir))
                    throw new Exception("Usage Upload Directory does not exist.");

                string filePath = uploadDir.TrimEnd('/', '\\') + "\\" + System.IO.Path.GetFileName(filename);
                if (!File.Exists(filePath))
                    throw new Exception(string.Format("File '{0}' does not exist.", System.IO.Path.GetFileName(filename)));

                uploadLogEntry = GetUploadItem(partnerName, filePath);
                if (uploadLogEntry == null)
                    throw new Exception("No entry found in Upload Log list.");

                SPListItemCollection contentReports = SPListLib.GetAllItems("Content Reports", string.Empty, web);
                SPListItem contentReport = contentReports.Add();
                SetFieldValue(contentReport, "Partner", partnerName, web);
                SetFieldValue(contentReport, "Timestamp", DateTime.Now.ToString(), web);
                contentReport.Update();
                contentReport.Attachments.AddNow("ContentReport.csv", File.ReadAllBytes(filePath));
                SetFieldValue(contentReport, "Report", string.Format(@"<a target=""_blank"" href=""{0}"">View log</a>", contentReport.Attachments.UrlPrefix + "ContentReport.csv"), web);
                contentReport.Update();

                SetFieldValue(uploadLogEntry, "Status", "Imported", web);
                uploadLogEntry.Update();
                ServiceLog(ServiceCallType.Add_Content_Report, true, "Content Report Added", partnerName);
            }
            catch (Exception ex)
            {
                ServiceLog(ServiceCallType.Add_Content_Report, false, "Cannot add file " + Path.GetFileName(filename) + " with following error: " + ex.Message, partnerName);
                Utilities.Log("AddContentReport Error: " + ex.Message);
                if (uploadLogEntry != null)
                {
                    SetFieldValue(uploadLogEntry, "Status", "Error", web);
                    uploadLogEntry.Update();
                }
                throw ex;
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }

        public Result Upload(FileData fileData)
        {
            Result result = new Result();
            try
            {
                //Log Request if logging is enable
                if (string.Equals(ConfigurationManager.AppSettings["UseLogging"], "true", StringComparison.OrdinalIgnoreCase)) LogRequest(fileData);

                //Validate the inputs
                Validate(fileData);

                //Get upload folder and Validate the path
                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                SPWeb web = new SPSite(siteUrl).OpenWeb();
                ConfigManager configMgr = new ConfigManager(web);
                string uploadFolder = configMgr.Settings["UsageUploadDir"]; //ConfigurationManager.AppSettings["UploadFolderPath"];
                if (string.IsNullOrEmpty(uploadFolder)) throw new Exception("UploadFolderPath not set in config");
                if (Directory.Exists(uploadFolder) == false) Directory.CreateDirectory(uploadFolder);

                //Check and create directory according to file name provided
                string fileLocation = CreateUploadLocation(uploadFolder, fileData.FileName, fileData.CurrentChunkCount);

                //Write the "data" to file
                using (FileStream fs = new FileStream(fileLocation, FileMode.Append, FileAccess.Write)) fs.Write(fileData.Data, 0, fileData.Count);

                //If the chunk count is equal to total chunk count, then rename the file with provided extension
                if (fileData.CurrentChunkCount == fileData.TotalChunckCount) File.Move(fileLocation, Path.Combine(uploadFolder, fileData.FileName));

                result.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                Utilities.Log("Error in UploadService", ex);
                result.IsSuccessful = false;
                result.Message.Add(ex.Message);
            }
            return result;
        }

        #region Helper Functions
        //Logs Request Details
        private void LogRequest(FileData fileData)
        {
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
            Utilities.Log(string.Format("IP[{0}]|Request[{1}]|TimeStamp[{2}]", endpoint.Address, fileData == null ? "NULL" : fileData.ToString(), DateTime.Now)); //TimeStamp as Date Time Now
        }

        //Validates File Data
        private void Validate(FileData fileData)
        {
            if (fileData == null) throw new ArgumentNullException("File Data is NULL");
            if (string.IsNullOrEmpty(fileData.FileName)) throw new ArgumentNullException("File Name provided is Invalid");
            if (fileData.Data == null) throw new ArgumentNullException("Data is Invalid");
            if (fileData.CurrentChunkCount > fileData.TotalChunckCount) throw new ArgumentException("Current Chunk Count can not be greater than Total Chunck Count.");
        }

        //Creates the Upload Location with folder provided in file name
        private string CreateUploadLocation(string uploadFolder, string fileName, int currentChunkCount)
        {
            string filePath = Path.Combine(uploadFolder, fileName);
            if (currentChunkCount == 1)
            {
                if (File.Exists(filePath)) File.Delete(filePath);
                if (File.Exists(Path.ChangeExtension(filePath, "uip"))) File.Delete(Path.ChangeExtension(filePath, "uip"));
            }
            if (!File.Exists(filePath))
            {
                if (fileName.Contains("\\"))
                {
                    string directoryName = fileName.Substring(0, fileName.LastIndexOf("\\"));
                    Directory.CreateDirectory(Path.Combine(uploadFolder, directoryName));
                }
            }
            return Path.ChangeExtension(filePath, "uip"); //Upload In Progress
        }

        private Processor _FindArchitecture(string partnerName)
        {
            string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            SPWeb web = (new SPSite(siteUrl)).OpenWeb();
            SPListItemCollection partners = SPListLib.GetAllItems("Partners", string.Empty, web);
            SPListItem partner = null;
            foreach (SPListItem currentPartner in partners)
            {
                string currentPartnerName = currentPartner["LinkTitle"].ToString();
                if (string.Compare(currentPartnerName, partnerName, true) == 0)
                    partner = currentPartner;
            }
            if (partner == null) throw new Exception("Invalid partner:" + partnerName);
            if (partner.Fields.ContainsField("Processor Architecture"))
                return (Processor)Enum.Parse(typeof(Processor), partner["Processor Architecture"].ToString());
            else
                throw new Exception("Field doesnt exist: Processor Architecture.");
        }

        private Synchronization.PackageCriteria Convert(PackageCriteria criteria)
        {
            Synchronization.PackageCriteria pkgCriteria = new Synchronization.PackageCriteria();
            pkgCriteria.Id = criteria.Id;
            pkgCriteria.Partner = criteria.Partner;
            pkgCriteria.Status = criteria.Status;
            pkgCriteria.Title = criteria.Title;
            pkgCriteria.PackageType = criteria.PackageType;
            pkgCriteria.MinPackageDate = criteria.ValidFrom;
            pkgCriteria.MaxPackageDate = criteria.ValidTo;
            return pkgCriteria;
        }

        private Package[] Convert(Synchronization.Package[] packageList)
        {
            List<Package> pkgList = new List<Package>();
            foreach (Synchronization.Package package in packageList)
            {
                Package pkg = new Package();
                pkg.PackageType = package.PackageType;
                pkg.DateExpires = package.DateExpires;
                pkg.DatePackaged = package.DatePackaged;
                pkg.Files = package.Files;
                pkg.Id = package.Id;
                pkg.LocalFolder = package.LocalFolder;
                pkg.RemoteFolder = package.RemoteFolder;
                pkg.Partner = package.Partner;
                pkg.Title = package.Title;
                pkgList.Add(pkg);
            }
            return pkgList.ToArray();
        }

        private Indigo.Athena.RecallCriteria Convert(RecallItemCriteria criteria)
        {
            RecallCriteria recallCriteria = new RecallCriteria();
            recallCriteria.ItemId = criteria.ItemId;
            recallCriteria.MaxRecallDate = criteria.MaxRecallDate;
            recallCriteria.MinRecallDate = criteria.MinRecallDate;
            recallCriteria.Partner = criteria.Partner;
            recallCriteria.Status = criteria.Status;
            return recallCriteria;
        }

        private RecallItem[] Convert(Indigo.Athena.RecallItem[] recallList)
        {
            List<RecallItem> recList = new List<RecallItem>();
            foreach (Indigo.Athena.RecallItem recall in recallList)
            {
                RecallItem rec = new RecallItem();
                rec.ItemId = recall.ItemId;
                rec.ListId = (Guid)recall.ListId;

                rec.Title = recall.Title;
                rec.RecallDate = recall.RecallDate;
                rec.Status = recall.Status;
                recList.Add(rec);
            }
            return recList.ToArray();
        }

        private void SetFieldValue(SPListItem item, string key, string value, SPWeb web)
        {
            if (item.Fields[key].Type == SPFieldType.Lookup)
            {
                SPFieldLookup lookupField = (SPFieldLookup)item.Fields[key];

                NameValueCollection criteria = new NameValueCollection();
                criteria.Add(lookupField.LookupField, value);
                SPListItemCollection items = SPListLib.GetItems(lookupField.LookupList, "", criteria, web);

                if (items.Count == 0) throw new Exception("Invalid lookup value:" + value);
                item[key] = new SPFieldLookupValue(items[0].ID, value);
            }
            else
                item[key] = value;
        }

        private string GetFieldValue(SPListItem item, string key)
        {
            try
            {
                if (!key.Equals("LinkTitle", StringComparison.InvariantCultureIgnoreCase) && !key.Equals("_Author", StringComparison.InvariantCultureIgnoreCase) && (item.Fields[key].Type == SPFieldType.Lookup || item.Fields[key].Type == SPFieldType.User))
                    return GetFieldLookupValue(item, key);
                else
                    return item[key].ToString();
            }
            catch { return string.Empty; }
        }

        private string GetFieldLookupValue(SPListItem item, string key)
        {
            try
            {
                SPFieldLookupValue lookupVal = new SPFieldLookupValue(item[key].ToString());
                return lookupVal.LookupValue;
            }
            catch { return string.Empty; }
        }

        private SPListItem GetUploadItem(string partnerName, string filePath)
        {
            string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
            SPWeb web = (new SPSite(siteUrl)).OpenWeb();
            SPListItemCollection items = web.Lists["Upload Log"].Items;
            foreach (SPListItem uploadItem in items)
            {

                string partner = GetFieldValue(uploadItem, "Partner");
                string filename = GetFieldValue(uploadItem, "Filename");

                if (partner == partnerName && filename == filePath)
                    return uploadItem;
            }
            if (web != null) web.Dispose();
            return null;
        }

        private void ServiceLog(ServiceCallType type, bool success, string details, string partner)
        {
            try
            {

                string siteUrl = ConfigurationManager.AppSettings["SiteUrl"];
                SPSite site = new SPSite(siteUrl);
                SPWeb web = site.OpenWeb();

                SPList list = web.Lists["Service Log"];
                SPListItem item = list.Items.Add();
                //Operation
                SetFieldValue(item, "Operation", GetServiceType(type), web);
                //Success
                if (success)
                    SetFieldValue(item, "Result", "Success", web);
                else
                    SetFieldValue(item, "Result", "Failed", web);
                //Details
                SetFieldValue(item, "Details", details, web);
                //Partner
                SetFieldValue(item, "Partner", partner, web);
                //IP Address
                OperationContext context = OperationContext.Current;
                if (context != null)
                {
                    MessageProperties messageProperties = context.IncomingMessageProperties;

                    RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

                    SetFieldValue(item, "IPAddress", endpointProperty.Address, web);
                }
                else
                {
                    SetFieldValue(item, "IPAddress", "", web);
                }

                item.Update();

                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
            catch (Exception ex)
            {
                Utilities.Log("Service Log Error: " + ex.Message);
            }
        }

        private string GetServiceType(ServiceCallType type)
        {
            switch (type)
            {
                case ServiceCallType.List_Download_Packages: return "List Download Packages";
                case ServiceCallType.List_Recall_items: return "List Recall Items";
                case ServiceCallType.Update_Upload_Status: return "Update Upload Status";
                case ServiceCallType.Initiate_Upload: return "Initiate Upload";
                case ServiceCallType.Report_Update_Status: return "Report Update Status";
                case ServiceCallType.Add_Content_Report: return "Add Content Report";
            }
            return string.Empty;
        }

        #endregion
    }
}