using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using System.Collections.Specialized;
using System.IO;
using Microsoft.SharePoint.Deployment;

namespace Indigo.Athena.Jobs
{
    public class UsageImportJob : Job
    {
        #region Constructors
        public UsageImportJob() { }


        public UsageImportJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.UsageImport, spWeb)
        {
        }
        #endregion

        #region Private Variables
        SPWeb web;
        SPSite site;
        UploadedPackage _currentPackage = null;
        private string[] ImportLists;
        #endregion

        #region SecurityViolationException
        private class SecurityViolationException : ApplicationException
        {

            public SecurityViolationException(string message)
                : base(message)
            {
            }
            public SecurityViolationException()
            {
            }
        }
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Importing";
                bool jobStatus = true;
                bool invalidPackage = true;
                _LoadConfig();
                // Get a ref to SPWeb
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();

                /// Step 1 Import Usage log packages
                /// Step 1.1 Get Usage log packages
                UploadedPackage[] uploadedUsagePackages = _GetFilesToImport(web, PackageType.UsageLog);
                this.Log(string.Format("Usage packages found for import : {0}", uploadedUsagePackages.Length), false, false);

                int count = 0;
                foreach (UploadedPackage package in uploadedUsagePackages)
                {
                    try
                    {
                        this.Status = string.Format("Importing Usage Package ({0}/{1})", ++count, uploadedUsagePackages.Length);

                        _currentPackage = package;
                        if (string.IsNullOrEmpty(package.FileName) || !File.Exists(package.FileName))
                        {
                            this.Log(string.Format("Filename is empty or file does not exist : {0}", package.FileName), false, false);
                            package.UpdateStatus("Invalid");
                            jobStatus = false;
                            continue;
                        }

                        /// Step 1.2 Create  Temp import settings for usage log
                        SPImportSettings validationSettings = new SPImportSettings();
                        validationSettings.RetainObjectIdentity = false;
                        validationSettings.CommandLineVerbose = true;
                        validationSettings.FileCompression = true;

                        validationSettings.SiteUrl = this.WebUrl;
                        validationSettings.LogFilePath = this.LogFile;

                        validationSettings.BaseFileName = Path.GetFileName(package.FileName);
                        validationSettings.FileLocation = Path.GetDirectoryName(package.FileName);

                        SPImport validatorImporter = new SPImport(validationSettings);
                        try
                        {
                            validatorImporter.Started += new EventHandler<SPDeploymentEventArgs>(PackageValidator);
                            validatorImporter.Run();
                        }
                        catch (SecurityViolationException ex)
                        {
                            package.UpdateStatus("Invalid");
                            validatorImporter.Cancel();
                            invalidPackage = true;
                            throw ex;
                        }
                        catch
                        {
                            validatorImporter.Cancel();
                            invalidPackage = false;
                        }

                        /// Step 1.3 Create  actutal import settings
                        SPImportSettings settings = _GetSPImportSettings(PackageType.UsageLog.ToString(), package.FileName);

                        /// Step 1.4 Import
                        SPImport importer = new SPImport(settings);
                        importer.ObjectImported += new EventHandler<SPObjectImportedEventArgs>(importer_ObjectImported);
                        importer.Run();
                        package.UpdateStatus("Imported");
                        this.Log(string.Format("Import completed for package '{0}', type '{1}'", package.FileName, package.Type), false, false);
                    }
                    catch (Exception ex)
                    {
                        if (invalidPackage)
                        {
                            this.Log("Import failed for package : " + package.FileName + " ", ex, false, true);
                            package.UpdateStatus("Invalid");
                        }
                        else
                        {
                            jobStatus = false;
                            this.Log("Import failed for package : " + package.FileName, ex, false, true);
                            this.Status = "Failed";
                            package.UpdateStatus("Error");
                        }
                    }
                }

                /// Step 2 Import Job log packages
                /// Step 2.1 Get Job log packages
                UploadedPackage[] uploadedJobPackages = _GetFilesToImport(web, PackageType.JobLog);
                this.Log(string.Format("Job packages found for import : {0}", uploadedJobPackages.Length), false, false);

                count = 0;
                foreach (UploadedPackage package in uploadedJobPackages)
                {
                    try
                    {
                        this.Status = string.Format("Importing Job Package ({0}/{1})", ++count, uploadedJobPackages.Length);

                        _currentPackage = package;
                        if (string.IsNullOrEmpty(package.FileName) || !File.Exists(package.FileName))
                        {
                            this.Log(string.Format("Filename is empty or file does not exist : {0}", package.FileName), false, false);
                            package.UpdateStatus("Invalid");
                            jobStatus = false;
                            continue;
                        }
                        /// Step 2.2 Create  actutal import settings
                        SPImportSettings settings = _GetSPImportSettings(PackageType.JobLog.ToString(), package.FileName);

                        /// Step 2.3 Import
                        SPImport importer = new SPImport(settings);
                        importer.Started += new EventHandler<SPDeploymentEventArgs>(OnJobLogImportStarted);
                        importer.ObjectImported += new EventHandler<SPObjectImportedEventArgs>(OnJobLogObjectImported);
                        importer.Run();
                        package.UpdateStatus("Imported");
                        this.Log(string.Format("Import completed for package '{0}', type '{1}'", package.FileName, package.Type), false, false);
                    }
                    catch (Exception ex)
                    {
                        jobStatus = false;
                        this.Log("Import failed for package : " + package.FileName, ex, false, true);
                        this.Status = "Failed";
                        package.UpdateStatus("Error");
                    }
                }

                if (jobStatus)
                {
                    this.Log("Job Completed Successfully...", true, true);
                    this.Status = "Completed";
                }
                else
                {
                    this.Log("Job Completed with Errors...", true, true);
                    this.Status = "Failed";
                }
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }

        void PackageValidator(object sender, SPDeploymentEventArgs e)
        {

            SPImportObjectCollection _objects = e.RootObjects;
            foreach (SPImportObject curobject in _objects)
            {
                if (curobject.Type == SPDeploymentObjectType.ListItem && _CompareTargetName(curobject.TargetParentUrl))
                    continue;
                else
                    throw new SecurityViolationException("List not allowed to be imported: " + _GetListFromUrl(curobject.TargetParentUrl));
            }
            throw new Exception("Package Verfied");
        }

        string _GetListFromUrl(string targetName)
        {
            targetName = targetName.Replace("/mcp/Lists/", "");
            targetName = targetName.Replace("/", "");
            return targetName;
        }
        bool _CompareTargetName(string targetName)
        {
            try
            {
                foreach (string s in this.ImportLists)
                {
                    if (targetName == ("/mcp/Lists/" + s + "/"))
                        return true;
                }
            }
            catch (Exception exception)
            {
            }
            return false;
        }

        void importer_ObjectImported(object sender, SPObjectImportedEventArgs e)
        {

            if (e.Type != SPDeploymentObjectType.ListItem) return;

            try
            {
                SPListItem importedItem = (SPListItem)web.GetObject(e.TargetUrl);
                _SetFieldValue(importedItem, "Partner", _currentPackage.Partner);
            }
            catch (Exception ex)
            {
                Utilities.Log(string.Format("{0} Job: Error - {1}", this.Name, ex.Message));
            }
        }

        void OnJobLogObjectImported(object sender, SPObjectImportedEventArgs e)
        {
            if (e.Type != SPDeploymentObjectType.ListItem) return;

            try
            {
                SPListItem importedItem = (SPListItem)web.GetObject(e.TargetUrl);
                _SetFieldValue(importedItem, "Partner", _currentPackage.Partner);
                if (importedItem.Attachments.Count != 0)
                    _SetFieldValue(importedItem, "Log", string.Format(@"<a target=""_blank"" href=""{0}"">View log</a>", importedItem.Attachments.UrlPrefix + "Log.txt"));
                else
                    _SetFieldValue(importedItem, "Log", string.Empty);

                NameValueCollection criteria = new NameValueCollection();
                criteria.Add("Job Id", _GetValue(importedItem, "Job Id"));
                SPListItemCollection items = SPListLib.GetItems("Job Log", string.Empty, criteria, web);
                for (int i = items.Count - 1; i >= 0; i--)
                {
                    if (items[i].UniqueId != importedItem.UniqueId)
                        items[i].Delete();
                }
            }
            catch (Exception ex)
            {
                this.Log(string.Format("Error - {0}", ex.Message), false, false);
            }
        }

        void OnJobLogImportStarted(object sender, SPDeploymentEventArgs args)
        {
            SPList list = web.Lists["Job Log"];

            SPImportObjectCollection rootObjects = args.RootObjects;
            foreach (SPImportObject io in rootObjects)
            {
                if (io.Type == SPDeploymentObjectType.ListItem)
                {
                    if (io.TargetParentUrl != "/mcp/Lists/Jobs/")
                        throw new Exception("Invalid list item for import, list name : " + _GetListFromUrl(io.TargetParentUrl));
                    io.TargetParentUrl = list.RootFolder.ServerRelativeUrl;
                    io.ParentId = list.ID;
                }
            }
        }
        #endregion

        #region Helper Functions

        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load Export Lists
            List<string> contentLists = new List<string>(configMgr.Settings["UsageImportList"].Split(','));
            for (int i = 0; i < contentLists.Count; i++)
                contentLists[i] = contentLists[i].Trim();
            this.ImportLists = contentLists.ToArray();
            if (ImportLists.Length == 0) throw new Exception("Configuration Error: 'ContentList' can not be empty.");
        }

        private UploadedPackage[] _GetFilesToImport(SPWeb webContext, PackageType packageType)
        {
            NameValueCollection criteria = new NameValueCollection();
            criteria.Add("Status", "Uploaded");

            SPListItemCollection uploadedItems = SPListLib.GetItems("Upload Log", string.Empty, criteria, webContext);
            List<UploadedPackage> packages = new List<UploadedPackage>();
            foreach (SPListItem item in uploadedItems)
            {
                UploadedPackage package = new UploadedPackage(item);
                if (package.FileName != string.Empty)
                {
                    if (packageType == PackageType.UsageLog && (package.Type == packageType.ToString() || package.Type == string.Empty))
                        packages.Add(package);
                    if (packageType == PackageType.JobLog && package.Type == packageType.ToString())
                        packages.Add(package);
                }
            }
            packages.Sort(delegate(UploadedPackage p1, UploadedPackage p2) { return p1.Created.CompareTo(p2.Created); });
            return packages.ToArray();
        }

        private void _SetFieldValue(SPListItem item, string key, string value)
        {
            if (item.Fields[key].Type == SPFieldType.Lookup)
            {
                SPFieldLookup lookupField = (SPFieldLookup)item.Fields[key];

                NameValueCollection criteria = new NameValueCollection();
                criteria.Add(lookupField.LookupField, value);
                SPListItemCollection items = SPListLib.GetItems(lookupField.LookupList, "", criteria, item.ParentList.ParentWeb);

                if (items.Count == 0) throw new Exception("Invalid lookup value:" + value);
                item[key] = new SPFieldLookupValue(items[0].ID, value);
            }
            else
                item[key] = value;

            item.Update();
        }

        private string _GetValue(SPListItem item, string key)
        {
            try
            {
                if (item.Fields[key].Type == SPFieldType.Lookup)
                {
                    SPFieldLookupValue lookupValue = new SPFieldLookupValue(item[key].ToString());
                    return lookupValue.LookupValue;
                }
                else return item[key].ToString();
            }
            catch { return string.Empty; }
        }

        private SPImportSettings _GetSPImportSettings(string packageType, string fileName)
        {
            SPImportSettings settings = new SPImportSettings();

            if (string.IsNullOrEmpty(packageType) || !(packageType == PackageType.UsageLog.ToString() || packageType == PackageType.JobLog.ToString()))
                throw new Exception("Incorrect Package Type : " + fileName);
            //For importing job logs, RetainObjectIdentity should be false (Reparenting).
            settings.RetainObjectIdentity = (packageType == PackageType.UsageLog.ToString()) ? true : false;

            settings.CommandLineVerbose = true;
            settings.FileCompression = true;

            settings.SiteUrl = this.WebUrl;
            settings.LogFilePath = this.LogFile;

            settings.BaseFileName = Path.GetFileName(fileName);
            settings.FileLocation = Path.GetDirectoryName(fileName);
            return settings;
        }
        #endregion


        #region Uploaded Package
        private class UploadedPackage
        {
            public string FileName = string.Empty;
            public string Partner = string.Empty;
            public string Status = string.Empty;
            public string Type = string.Empty;
            public DateTime Created;
            private SPListItem _listItem;

            public UploadedPackage(SPListItem item)
            {
                _listItem = item;
                this.FileName = _GetValue(item, "Filename");
                this.Partner = _GetValue(item, "Partner");
                this.Status = _GetValue(item, "Status");
                this.Type = _GetValue(item, "Package Type");
                this.Created = DateTime.Parse(_GetValue(item, "Created"));
            }

            public void UpdateStatus(string newStatus)
            {
                try
                {
                    this.Status = newStatus;
                    _listItem["Status"] = newStatus;
                    _listItem.Update();
                }
                catch { }
            }

            private string _GetValue(SPListItem item, string key)
            {
                try
                {
                    if (item.Fields[key].Type == SPFieldType.Lookup)
                    {
                        SPFieldLookupValue lookupValue = new SPFieldLookupValue(item[key].ToString());
                        return lookupValue.LookupValue;
                    }
                    else return item[key].ToString();
                }
                catch { return string.Empty; }
            }
        }
        #endregion
    }
}
