﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using System.Collections.Specialized;

namespace Indigo.Athena.Jobs
{
    public class TagListUpdaterJob : Job
    {
        #region Constructor
        public TagListUpdaterJob() { }

        public TagListUpdaterJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.TagListUpdater, spWeb)
        {
        }
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Status = "Updating";
                this.Log("Job started...", false, true);

                //List<string> tagList = new List<string>();
                Dictionary<int, Tags> tagList = new Dictionary<int, Tags>();    
                SPSite site = new SPSite(this.WebUrl);
                SPWeb web = site.OpenWeb();
                SPListCollection lists = web.Lists;

                //store all tags individually in List 'tagList'
                this.Log("Getting all tags from Content lists", false, false);
                int count = 0;
                foreach (SPList list in lists)
                {
                    if (list.Title == "Webcasts" || list.Title == "Podcasts" || list.Title == "Hands On Labs" || list.Title == "Other Content")
                    {
                        SPListItemCollection items = list.Items;                        
                        foreach (SPListItem item in items)
                        {
                            try
                            {
                                object categoryObj = item["ContentCategory"];
                                if (categoryObj == null)                                                                    
                                    continue;                                
                                string str = categoryObj.ToString().ToLower();
                                if (str.Substring(str.IndexOf('#') + 1).Equals("unknown"))
                                    continue;

                                //bool showLocalMediaOnly = bool.Parse(_LoadConfiguration("ShowLocalMediaOnly").ToLower());
                                if (!bool.Parse(item["MediaDownloaded"].ToString()))
                                    continue;

                                str = string.Empty;
                                object obj = new object();
                                obj = item["Tags"];
                                if (obj == null)
                                    continue;

                                str = string.Empty;
                                str = obj.ToString();
                                string[] tags = str.Split(',');
                                foreach (string tag in tags)
                                {                                   
                                    tagList.Add(count++, new Tags(tag.Trim().ToLower(), (DateTime)item["Modified"]));
                                    //tagList.Add(tag.Trim().ToLower());
                                }
                            }
                            catch (Exception ex)
                            {
                                this.Log(string.Format("Error in reading item: {0}", item.Title), ex, false, false);
                            }
                        }
                    }
                }

                //sort the items as per their 'Modified' timestamp in descending order
                this.Log("Sorting tags as per timestamp", false, false);
                for (int i = 0; i < (tagList.Count - 1); i++)
                    for (int j = i + 1; j < tagList.Count; j++)
                        if (tagList[i].Timestamp < tagList[j].Timestamp)
                        {
                            Tags temp = tagList[i];
                            tagList[i] = tagList[j];
                            tagList[j] = temp;
                        }

                //store unique tags in dictionary 'tagsDictionary' along with the corresponding tags
                this.Log("Finding count of tags", false, false);                
                Dictionary<string, TagDetails> tagsDictionary = new Dictionary<string, TagDetails>();
                for (int i = 0; i < tagList.Count; i++)
                {
                    int j = 0;
                    int tagCount = 0;
                    while (j < tagList.Count)
                    {
                        if (string.Compare(tagList[i].Title, tagList[j].Title, true) == 0)
                            tagCount++;
                        j++;
                    }
                    if (!tagsDictionary.ContainsKey(tagList[i].Title))
                        tagsDictionary.Add(tagList[i].Title, new TagDetails(tagList[i].Timestamp, tagCount));
                }

                //delete all items of 'Tag List' list
                this.Log("Clearing Tag List", false, false);
                List<int> itemId = new List<int>();
                foreach (SPListItem item in web.Lists["Tag List"].GetItems(new SPQuery()))
                {
                    itemId.Add(item.ID);
                }
                //foreach (int id in itemId)
                //{
                //    web.Lists["Tag List"].Items.DeleteItemById(id);
                //}
                StringBuilder spDelete = new StringBuilder();
                spDelete.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch>");
                foreach (int id in itemId)
                {
                    spDelete.Append("<Method>");
                    spDelete.Append("<SetList Scope=\"Request\">"+web.Lists["Tag List"].ID+"</SetList>");
                    spDelete.Append("<SetVar Name=\"ID\">"+id.ToString()+"</SetVar>");
                    spDelete.Append("<SetVar Name=\"Cmd\">Delete</SetVar>");
                    spDelete.Append("</Method>");
                }
                spDelete.Append("</Batch>");
                web.ProcessBatchData(spDelete.ToString());

                //new updated entries in 'Tag List'                
                this.Log("Populating Tag List with present content", false, false);
                foreach (KeyValuePair<string, TagDetails> kvp in tagsDictionary)
                {
                    if (kvp.Value.Weight > 0)
                    {
                        SPListItem item = web.Lists["Tag List"].Items.Add();
                        item["Title"] = kvp.Key;
                        item["Count"] = kvp.Value.Weight;
                        item["Latest Content"] = kvp.Value.TimeStamp;
                        item.Update();
                    }
                }

                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
        }
        #endregion        
    }

    #region Helper Classes
    public class Tags
    {
        public string Title = string.Empty;
        public DateTime Timestamp = DateTime.MinValue;
        //public int Count = int.MinValue;

        public Tags(string title, DateTime timestamp)//, int count)
        {
            this.Title = title;
            this.Timestamp = timestamp;
            //this.Count = count;
        }
    }

    public class TagDetails
    {
        public DateTime TimeStamp = DateTime.MinValue;
        public int Weight = int.MinValue;

        public TagDetails(DateTime timestamp, int weight)
        {
            this.TimeStamp = timestamp;
            this.Weight = weight;
        }
    }
    #endregion
}
