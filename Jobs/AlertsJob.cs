using System;
using System.Collections.Generic;
using System.Text;

using System.Net.Mail;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Xml.Serialization;
using System.IO;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Xml;
using System.Web;
using System.Net;
using System.Drawing;
using System.Reflection;
using Indigo.Athena.Alerts;

namespace Indigo.Athena.Jobs
{
    public enum AlertScheduleType { Daily, Weekly };
    public enum AlertJobType { Content, Events, DVD };
    public class AlertsJob : Job
    {
        public AlertsJob() { }      // IMP: Default Constructor for Serialization

        public AlertsJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Alert, spWeb)
        {
        }

        #region Public Members
        [Persisted]
        public string SenderAddress;
        [Persisted]
        public DateTime LastAlertDate = DateTime.Today.AddMonths(-1);
        [Persisted]
        public string Subject;       // "New/Modified items alert"
        [Persisted]
        public AlertJobType AlertType;
        [Persisted]
        public AlertScheduleType AlertSchedule;
        #endregion

        # region Public Properties
        public static string AlertId
        {
            get;
            set;
        }
        #endregion

        # region Private Members
        private string SmtpServer;  // "pyxis.pune.indigoarchitects.com"  
        private DateTime currentTime;
        private int count = 0;
        # endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            SPSite spSite = null;
            SPWeb spWeb = null;
            try
            {
                currentTime = DateTime.Now;
                AlertId = targetInstanceId.ToString();
                this.Log("Job started...", false, true);
                this.Status = "Running";
                string userPrefList = "User Preferences";
                // 1. Get Package List from Remote site
                spSite = new SPSite(this.WebUrl);
                spWeb = spSite.OpenWeb();
                _LoadConfig();
                switch (AlertType)
                {
                    case AlertJobType.Content:
                        ContentAlertGenerator _contentGenerator = new ContentAlertGenerator(spWeb);
                        _contentGenerator.RequiresPersonalizationInfo(true);
                        SPListItem[] alertItems = _contentGenerator.GetAlertData(this.LastAlertDate);
                        SPListItemCollection userPreferences = spWeb.Lists[userPrefList].Items;
                        foreach (SPListItem userPreference in userPreferences)
                        {
                            /// --- Get Alert Items for this user ---
                            try
                            {
                                MailItem[] _contentItems = _contentGenerator.ApplyPersonalization(userPreference, AlertSchedule);
                                MailItem curContentItem = _contentGenerator.GenerateMailBody(_contentItems[0]);
                                if (curContentItem != null)
                                {
                                    SendMail(curContentItem);
                                    count++;
                                }
                            }
                            catch (Exception e)
                            {
                                continue;
                            }

                        }
                        break;
                    case AlertJobType.Events:
                        EventAlertGenerator _eventGenerator = new EventAlertGenerator(spWeb);
                        _eventGenerator.RequiresPersonalizationInfo(false);
                        alertItems = _eventGenerator.GetAlertData(this.LastAlertDate);
                        userPreferences = spWeb.Lists[userPrefList].Items;
                        foreach (SPListItem userPreference in userPreferences)
                        {
                            /// --- Get Alert Items for this user ---
                            MailItem[] _eventItems = _eventGenerator.ApplyPersonalization(userPreference, AlertSchedule);
                            foreach (MailItem curMailItem in _eventItems)
                            {
                                MailItem eventMailItem = _eventGenerator.GenerateMailBody(curMailItem);
                                if (eventMailItem != null)
                                {
                                    SendMail(eventMailItem);
                                    count++;
                                }
                            }
                        }
                        break;
                    case AlertJobType.DVD:
                        break;
                }

                this.LastAlertDate = DateTime.Now;
                this.Update();
                this.Log("Job Successfully completed...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                _WriteToAlertLogList();
                if (spWeb != null) spWeb.Dispose();
                if (spSite != null) spSite.Dispose();
            }
        }

       #endregion


        #region Helper Functions
        private void _WriteToAlertLogList()
        {
            try
            {
                SPList list = _GetList("Alert Log");
                if (list != null)
                {
                    SPListItem item = list.Items.Add();
                    item["Status"] = this.Status;
                    item["Alert Type"] = string.Format("{0}({1})", this.AlertSchedule, this.AlertType);
                    item["UserCount"] = count;
                    item["Timestamp"] = this.currentTime;
                    item["Title"] = this.Name;
                    item.Update();
                    item["Alert Id"] = AlertId;
                    item.Update();
                }
            }
            catch (Exception ex)
            {
                Utilities.Log(string.Format("Error in creating entry in Alert Log list.Error Details:{1}", ex));
            }
        }

        private SPList _GetList(string listName)
        {
            try
            {
                if (new SPSite(this.WebUrl).OpenWeb() != null)
                    return new SPSite(this.WebUrl).OpenWeb().Lists[listName];
            }
            catch (Exception)
            {;}
            return null;
        }
        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load MailTemplate
            this.SmtpServer = configMgr.Settings["SMTPServer"];
            if (string.IsNullOrEmpty(SmtpServer)) throw new Exception("Configuration Error: 'SMTPServer' can not be empty.");

        }

        private void SendMail(MailItem curItem)
        {
            SPUser user = null;
            try
            {
                user = curItem.User;
                MailMessage mail = new MailMessage();
                AlternateView viewPlainText = AlternateView.CreateAlternateViewFromString("This mail was generated in HTML format.", null, "text/plain");
                mail.From = new MailAddress(this.SenderAddress);
                mail.Subject = this.Subject;
                mail.To.Add(user.Email);
                mail.AlternateViews.Add(viewPlainText);
                mail.AlternateViews.Add(curItem.Body);
                SmtpClient mailClient = new SmtpClient(SmtpServer);
                mailClient.Send(mail);
                try
                {
                    foreach (SPListItem _evntItem in curItem.Data)
                    {
                        _evntItem["IsAlertSent"] = 1;
                        _evntItem.Update();
                    }
                }
                catch { };
            }
            catch (Exception ex)
            {
                this.Log("Error in sending mail for user: " + user.Name + "  Error: ", ex, false, true);
                Utilities.Log("Exception in SendMail method: ", ex);
            }
        }

        #endregion

    }
}
