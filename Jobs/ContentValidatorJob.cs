using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using Indigo.Athena.Synchronization;
using System.Net;
using System.Collections.Specialized;

namespace Indigo.Athena.Jobs
{
    public class ContentValidatorJob : Job
    {
        private SPWeb web;
        private SPSite site;
        public ContentValidatorJob() { }      // IMP: Default Constructor for Serialization

        public ContentValidatorJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.ContentValidator, spWeb)
        {
        }

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Log("Job started...", false, true);
                this.Status = "Validating";
                // Get a ref to SPWeb
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();

                //Read DVD
                SPList list = web.Lists["DVD"];
                SPListItemCollection itemCollection = list.Items;
                int count = 0, totalItem = itemCollection.Count;
                foreach (SPListItem item in itemCollection)
                {
                    this.Status = string.Format("Validating ({0}/{1})", ++count, totalItem);
                    string path = GetDVDLink(item);

                    //Check WebResponse to verify path
                    try
                    {
                        if (CheckWebResponse(path))
                        {
                            item["Status"] = "Available";
                            item["Comments"] = "";
                            item.Update();
                        }
                        else
                        {
                            item["Status"] = "Not Available";
                            item["Comments"] = "DVD is not present at the specified path.";
                            item.Update();
                            this.Log(string.Format("{0} : Not present at the specified path.", item.Title), false, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        item["Status"] = "Not Available";
                        item["Comments"] = ex.Message;
                        item.Update();
                        this.Log(string.Format("Dvd : {0},Exception : {1}", item.Title, ex.Message), false, true);
                    }
                }

                Dictionary<string, int> categoryCount = new Dictionary<string, int>();
                SPListItemCollection categories = SPListLib.GetAllItems("Categories", string.Empty, web);
                foreach (SPListItem category in categories)
                {
                    string spValue = (new SPFieldLookupValue(category.ID, GetValue(category, "LinkTitle"))).ToString();
                    categoryCount.Add(spValue, 0);
                }

                //Webcast File Check
                string[] listCollection = { "Webcasts", "Podcasts", "Other Content", "Hands on Labs" };
                foreach (string listName in listCollection)
                {
                    SPList curList = web.Lists[listName];
                    SPListItemCollection curItemCollection = curList.Items;
                    int curCount = 0;
                    for (int i = curItemCollection.Count - 1; i >= 0; i--)
                    {
                        SPListItem item = curItemCollection[i];
                        this.Status = string.Format("Validating {0} ({1}/{2})", listName, ++curCount, curItemCollection.Count);
                        if (item.Attachments.Count == 1 && listName != "Webcasts")
                        {
                            try { categoryCount[GetValue(item, "ContentCategory")] = categoryCount[GetValue(item, "ContentCategory")] + 1; }
                            catch { }
                            continue;
                        }
                        if (listName == "Webcasts")
                        {
                            bool valid = false;
                            foreach (string fileName in item.Attachments)
                            {
                                SPFile file = item.ParentList.ParentWeb.GetFile(
                                  item.Attachments.UrlPrefix + fileName);
                                if (_VerifyExtension(file.Name))
                                    valid = true;
                            }
                            if (valid)
                            {
                                try { categoryCount[GetValue(item, "ContentCategory")] = categoryCount[GetValue(item, "ContentCategory")] + 1; }
                                catch { }
                                continue;
                            }
                        }
                        this.Log(string.Format("Deleting Item {0} from {1} ", item["Title"], listName), false, false);
                        item.Delete();
                        web.Update();
                    }
                }

                foreach (SPListItem category in categories)
                {
                    string spValue = (new SPFieldLookupValue(category.ID, GetValue(category, "LinkTitle"))).ToString();
                    if (categoryCount[spValue] == 0 || spValue.ToLower().Contains("unknown"))
                        category["Enabled"] = false;
                    else
                        category["Enabled"] = true;
                    category.Update();
                }

                this.Log("Job Completed Successfully...", true, true);
                this.Status = "Completed";                
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }
        #endregion

        #region HelperFunction

        private bool _VerifyExtension(string p)
        {
            string[] validExtension = { "wmv" };
            string[] fileSplit = p.Split('.');
            foreach (string match in validExtension)
                if (fileSplit[fileSplit.Length - 1].ToLower() == match)
                    return true;
            return false;
        }

        private bool CheckWebResponse(string path)
        {
            try
            {
                WebRequest request = HttpWebRequest.Create(path);
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;

                // This may throw a WebException:
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                        return true;
                    else
                        return false;
                }
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }

        private string GetDVDLink(SPListItem dvdItem)
        {
            string folderName = GetFieldValue(dvdItem, "Folder");
            string startPage = GetFieldValue(dvdItem, "Start Page");
            ConfigManager configManager = new ConfigManager(web);
            string key = string.Format("Lists.{0}.MediaBaseUrl", dvdItem.ParentList.Title);
            string baseUrl = configManager.Settings[key];
            string siteUrl = configManager.Settings["WebUrl"];

            baseUrl = (baseUrl == null) ? string.Empty : baseUrl;
            if (baseUrl == string.Empty || startPage == string.Empty || folderName == string.Empty)
               return string.Empty;
            
            if (baseUrl.StartsWith("/"))
            {
                return siteUrl.TrimEnd('/') + baseUrl.TrimEnd('/') + "/" + folderName.Trim('/') + "/" + startPage.TrimStart('/');
            }
            else
            {
                return baseUrl.TrimEnd('/') + "/" + folderName.Trim('/') + "/" + startPage.TrimStart('/');
            }
        }

        internal static string GetFieldValue(Microsoft.SharePoint.SPListItem dvdItem, string p)
        {
            try
            {
                return dvdItem[p].ToString();
            }
            catch (Exception)
            {
                throw new Exception(p + " value cannot be retrieved ");
            }
        }

        private string GetValue(SPListItem item, string field)
        {
            try { return item[field].ToString(); }
            catch { return string.Empty; }
        }

        #endregion
    }
}
