using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Indigo.Athena.Jobs
{
    public class JobCriteria
    {
        public Guid? Id;
        public string Title;
        public string Name;
        public string Status;
    }

    public class JobManager
    {
        private SPWeb _spWeb;
        private SPJobDefinitionCollection _spJobDefinitions;

        public JobManager(SPWeb webContext)
        {
            _spWeb = webContext;
            _spJobDefinitions = _spWeb.Site.WebApplication.JobDefinitions;
        }


        public Job New(string jobName, JobType type)
        {
            // 1. If JobDefinition exists (Key=jobName), return jobName
            foreach (SPJobDefinition jobDef in _spJobDefinitions)
            {
                if (jobName == jobDef.Name)
                {
                    Job job = Cast(jobDef);
                    return job;
                }
            }

            // 2. Create Job with Context
            switch (type)
            {
                case JobType.Aggregate: return new ExtractorJob(jobName, _spWeb);
                case JobType.Alert: return new AlertsJob(jobName, _spWeb);
                case JobType.ContentValidator: return new ContentValidatorJob(jobName, _spWeb);
                case JobType.Download: return new PackageDownloaderJob(jobName, _spWeb);
                case JobType.Export: return new ContentExporterJob(jobName, _spWeb);
                case JobType.Import: return new ContentImporterJob(jobName, _spWeb);
                case JobType.Purge: return new PurgeJob(jobName, _spWeb);
                case JobType.Recall: return new ContentRecallJob(jobName, _spWeb);
                case JobType.InitialImport: return new InitialContentImporterJob(jobName, _spWeb);
                case JobType.BlogAggregate: return new BlogAggregateJob(jobName, _spWeb);
                case JobType.UsageUpload: return new UsageUploadJob(jobName, _spWeb);
                case JobType.UsageImport: return new UsageImportJob(jobName, _spWeb);
                case JobType.Updater: return new UpdaterJob(jobName, _spWeb);
                case JobType.ContentUpdater: return new ContentUpdater(jobName, _spWeb);
                case JobType.ReportGenerator: return new ReportGeneratorJob(jobName, _spWeb);
                case JobType.UpdateContentStatus: return new UpdateContentStatusJob(jobName, _spWeb);
                case JobType.TagListUpdater: return new TagListUpdaterJob(jobName, _spWeb);
                case JobType.UserProfileUploadJob: return new UserProfileUploadJob(jobName, _spWeb);
                default: return new AlertsJob(jobName, _spWeb);
            }
        }

        private Job Cast(SPJobDefinition jobDefinition)
        {
            if (jobDefinition.Properties["Jobtype"] == null) return null;

            JobType type = (JobType)jobDefinition.Properties["Jobtype"];
            switch (type)
            {
                case JobType.Aggregate: return ((ExtractorJob)jobDefinition) as Job;
                case JobType.Alert: return ((AlertsJob)jobDefinition) as Job;
                case JobType.ContentValidator: return ((ContentValidatorJob)jobDefinition) as Job;
                case JobType.Download: return ((PackageDownloaderJob)jobDefinition) as Job;
                case JobType.Export: return ((ContentExporterJob)jobDefinition) as Job;
                case JobType.Import: return ((ContentImporterJob)jobDefinition) as Job;
                case JobType.Purge: return ((PurgeJob)jobDefinition) as Job;
                case JobType.Recall: return ((ContentRecallJob)jobDefinition) as Job;
                case JobType.InitialImport: return ((InitialContentImporterJob)jobDefinition) as Job;
                case JobType.BlogAggregate: return ((BlogAggregateJob)jobDefinition) as Job;
                case JobType.UsageUpload: return ((UsageUploadJob)jobDefinition) as Job;
                case JobType.UsageImport: return ((UsageImportJob)jobDefinition) as Job;
                case JobType.Updater: return ((UpdaterJob)jobDefinition) as Job;
                case JobType.ContentUpdater: return ((ContentUpdater)jobDefinition) as Job;
                case JobType.ReportGenerator: return ((ReportGeneratorJob)jobDefinition) as Job;
                case JobType.UpdateContentStatus: return ((UpdateContentStatusJob)jobDefinition) as Job;
                case JobType.TagListUpdater: return ((TagListUpdaterJob)jobDefinition) as Job;
                case JobType.UserProfileUploadJob: return ((UserProfileUploadJob)jobDefinition) as Job;
                default: return null;
            }
        }

        public Job[] Find(JobCriteria criteria)
        {
            List<Job> jobs = new List<Job>();
            foreach (SPJobDefinition jobDef in _spJobDefinitions)
            {
                if (criteria.Id != null && criteria.Id != jobDef.Id) continue;
                if (criteria.Title != null && criteria.Title != jobDef.Title) continue;
                if (criteria.Name != null && criteria.Name != jobDef.Name) continue;
                if (criteria.Status != null && criteria.Status != (string)jobDef.Properties["JobStatus"]) continue;

                Job job = Cast(jobDef);
                if (job != null) jobs.Add(job);
            }
            return jobs.ToArray();
        }


        internal const string JOBS_LIST_NAME = "Jobs";
        internal const string JOB_DESCRIPTION = "Job Description";
        internal const string JOB_TYPE = "Job Type";
        internal const string JOB_STATUS = "Job Status";
        internal const string JOB_ID = "Job Id";
        internal const string JOB_TITLE = "Title";
        internal const string JOB_SCHEDULE = "Job Schedule";

        private SPListItem _FindJobitem(SPList spList, Guid jobId)
        {
            foreach (SPListItem jobItem in spList.Items)
            {
                if ((string)jobItem[JOB_ID] == jobId.ToString())
                    return jobItem;
            }
            return null;
        }


        /// <summary>
        /// Saves the Job. 
        /// </summary>
        //TODO: Need to synchronize the Job List and SP job def.If saved both should be saved.
        public void Save(Job job)
        {
            // Initialize List Item
            SPListItem _listItem = _FindJobitem(_spWeb.Lists[JOBS_LIST_NAME], job.Id);
            if (_listItem == null)
                _listItem = _spWeb.Lists[JOBS_LIST_NAME].Items.Add();

            // 1. Update Job Definition
            job.Update(true);

            // 2. Update List Item
            _listItem[JOB_ID] = job.Id;
            _listItem[JOB_TITLE] = job.Title;
            _listItem[JOB_SCHEDULE] = job.Schedule.ToString();
            _listItem[JOB_DESCRIPTION] = job.Description;
            _listItem[JOB_TYPE] = job.Type;
            _listItem[JOB_STATUS] = job.Status;
            _listItem.Update();

            job.Status = "Created";
        }

        public Job Clone(Job job)
        {
            // 1. Initialize List Item
            Job clone = this.New(job.Title + "(clone)", job.Type);
            // 2. Set common properties  
            clone.Title = job.Title + " (clone)";
            clone.WebUrl = job.WebUrl;
            clone.Description = job.Description;
            clone.Schedule = new SPOneTimeSchedule(DateTime.Now);
            clone.LogFile = job.LogFile;
            clone.TempFolder = job.TempFolder;
            clone.Status = job.Status;

            // 3. Set custom properties
            switch (job.Type)
            {
                case JobType.Aggregate:
                    ((ExtractorJob)clone).SourceUrl = ((ExtractorJob)job).SourceUrl;
                    ((ExtractorJob)clone).SourceName = ((ExtractorJob)job).SourceName;
                    break;

                case JobType.Alert:
                    ((AlertsJob)clone).SenderAddress = ((AlertsJob)job).SenderAddress;
                    ((AlertsJob)clone).Subject = ((AlertsJob)job).Subject;
                    ((AlertsJob)clone).LastAlertDate = ((AlertsJob)job).LastAlertDate;
                    ((AlertsJob)clone).AlertType = ((AlertsJob)job).AlertType;
                    ((AlertsJob)clone).AlertSchedule = ((AlertsJob)job).AlertSchedule;
                    break;

                case JobType.Download:
                    ((PackageDownloaderJob)clone).LastSyncDate = ((PackageDownloaderJob)job).LastSyncDate;
                    break;

                case JobType.Export:
                    ((ContentExporterJob)clone).Partner = ((ContentExporterJob)job).Partner;
                    ((ContentExporterJob)clone).ExportFromDate = ((ContentExporterJob)job).ExportFromDate;
                    ((ContentExporterJob)clone).ProcessorArchitecture = ((ContentExporterJob)job).ProcessorArchitecture;
                    break;

                case JobType.Import:
                case JobType.Updater:
                case JobType.ContentValidator:
                case JobType.BlogAggregate:
                case JobType.ContentUpdater:
                case JobType.TagListUpdater:
                case JobType.UserProfileUploadJob:
                break;
                case JobType.InitialImport:
                    ((InitialContentImporterJob)clone).ImportFolder = ((InitialContentImporterJob)job).ImportFolder;
                    break;
                
                case JobType.Purge:
                    break;

                case JobType.Recall:
                    ((ContentRecallJob)clone).LastSyncDate = ((ContentRecallJob)job).LastSyncDate;
                    break;

                case JobType.UsageImport: break;
                case JobType.UsageUpload:
                    ((UsageUploadJob)clone).ExportFromDate = ((UsageUploadJob)job).ExportFromDate;
                    break;

                case JobType.ReportGenerator:
                    ((ReportGeneratorJob)clone).ReceiverAddress = ((ReportGeneratorJob)job).ReceiverAddress;
                    ((ReportGeneratorJob)clone).SenderAddress = ((ReportGeneratorJob)job).SenderAddress;
                    break;

                case JobType.UpdateContentStatus:
                default: break;
            }
            clone.Update(true);
            return clone;
        }

        public void Delete(Guid jobId)
        {
            try
            {
                // 1. Remove List Item
                foreach (SPItem jobItem in _spWeb.Lists[JOBS_LIST_NAME].Items)
                {
                    if ((string)jobItem[JOB_ID] == jobId.ToString())
                    {
                        jobItem.Delete();
                        break;
                    }
                }

                // 2. Remove Job Definition
                SPJobDefinition jobDefinition = _spJobDefinitions[jobId];
                jobDefinition.Delete();
                jobDefinition.Unprovision();
            }
            catch { }
        }
    }
}
