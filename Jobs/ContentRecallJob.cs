using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.IO;

namespace Indigo.Athena.Jobs
{
    public class ContentRecallJob : Job
    {
        public ContentRecallJob() { }      // IMP: Default Constructor for Serialization

        public ContentRecallJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Recall, spWeb)
        {
        }

        #region Properties
        [Persisted]
        public DateTime LastSyncDate = DateTime.Now.AddMonths(-1);
        private string Partner = "";
        private string ServiceUrl = string.Empty;
        #endregion

        public override void Execute(Guid targetInstanceId)
        {
            SPSite spSite = null;
            SPWeb spWeb = null;
            try
            {
                this.Status = "Recalling";
                this.Log("Job started...", false, true);
                _LoadConfig();

                DateTime jobStartTime = DateTime.Now;

                // 1. Get Recall Item List from Remote site
                spSite = new SPSite(this.WebUrl);
                spWeb = spSite.OpenWeb();
                RecallManager recManager = new RecallManager(spWeb);

                RecallCriteria criteria = new RecallCriteria();
                criteria.MinRecallDate = this.LastSyncDate;
                criteria.Partner = this.Partner;
                RecallItem[] recallItems = recManager.Find(this.ServiceUrl, criteria);

                // 2. Save Recall Items to local Site
                foreach (RecallItem r in recallItems)
                {
                    r.Status = RecallStatus.Downloaded.ToString();
                    r.Save();                    
                }
                this.Log("Recall item list recieved and saved to local site...Deletion of content started...", false, true);

                // 3. Foreach Recall item, delete data
                criteria = new RecallCriteria();
                criteria.MinRecallDate = this.LastSyncDate;
                recallItems = recManager.Find(criteria);

                // -- Step 2: Delete items from site
                int count = 0;
                foreach (RecallItem recallItem in recallItems)
                {
                    this.Status = string.Format("Recalling ({0}/{1})", ++count, recallItems.Length);
                    if (recallItem.Status == RecallStatus.Deleted.ToString())
                        continue;
                    try
                    {
                        if(recManager.Exists(recallItem.ListId, recallItem.ItemId))
                            recManager.Delete(recallItem.ListId, recallItem.ItemId);
                        else
                            this.Log(string.Format("Recall item '{0}': {1}", recallItem.Title, "Item not found"), false, false);
                        recallItem.Status = RecallStatus.Deleted.ToString();
                    }
                    catch (Exception ex)
                    {
                        this.Log(string.Format("Error in deleting item '{0}': {1}", recallItem.Title, ex.Message), false,false);
                        recallItem.Status = RecallStatus.Error.ToString();
                    }

                    // - Update Status -
                    recallItem.Save();
                }

                this.LastSyncDate = jobStartTime;
                this.Update();
                this.Log("Recall Completed Successfully...Beginning Cleanup...", false, true);
                PerformCleanUp(spSite);
                this.Log("Cleanup Completed...Job Completed Successfully...", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true, true);
                this.Status = "Failed";
            }
            finally
            {
                if (spWeb != null) spWeb.Dispose();
                if (spSite != null) spSite.Dispose();
            }
        }




        #region Helper Functions

        private void PerformCleanUp(SPSite site)
        {
            String dirName = DirFromSiteId(site.ID);
            string[] files = Directory.GetFiles(dirName);

            Hashtable ht = new Hashtable();
            //GetAllFiles in EBS Folder
            foreach (string file in files)
            {
                FileInfo newFile = new FileInfo(file);
                ht.Add(newFile.Name, newFile);
            }
            foreach (SPExternalBinaryId blobid in site.ExternalBinaryIds)
            {
                byte[] idBytes = new byte[16];
                blobid.Bytes.CopyTo(idBytes, 0);
                Guid fileGuid = new Guid(idBytes);
                String fileName = fileGuid.ToString();
                if (ht.Contains(fileName))
                {
                    ht.Remove(fileName);
                }
            }
            foreach (FileInfo file in ht.Values)
            {
                file.Delete();
            }
        }

        private void _LoadConfig()
        {
            ConfigManager configMgr = new ConfigManager((new SPSite(this.WebUrl)).OpenWeb());

            // 1. Load ServiceUrl
            this.ServiceUrl = configMgr.Settings["ServiceUrl"];
            if (string.IsNullOrEmpty(ServiceUrl)) throw new Exception("Configuration Error: 'ServiceUrl' can not be empty.");
            
            //2. Load latest partner.
            _SetPartner();
        }

        private void _SetPartner()
        {
            //Load the partner using partner list.
            SPWeb web = (new SPSite(this.WebUrl)).OpenWeb();
            try
            {
                this.Partner = web.Lists["Partners"].Items[0]["LinkTitle"].ToString();
                if (string.IsNullOrEmpty(this.Partner))
                    throw new Exception("Partner name cannot be empty.");
            }
            catch (Exception ex) { throw new Exception("Configuration Error: " + ex.Message); }
            finally { if (web !=null) web.Dispose(); }
        }

        private string DirFromSiteId(Guid guid)
        {
            string EBSfolder = _GetRegistryLocalMachine(@"SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\12.0\WSS", "BlobDataPath");
            return Path.Combine(EBSfolder, guid.ToString());
        }

        private string _GetRegistryLocalMachine(string key, string name)
        {
            Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, true);
            if (registryKey != null)
                return (string)registryKey.GetValue(name);
            else
                Utilities.Log("EBS Folder Not registered");
            return string.Empty;
        }
        #endregion
    }
}
