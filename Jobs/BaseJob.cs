using System;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.IO;
using System.Text;
using System.Net.Mail;

namespace Indigo.Athena.Jobs
{
    public class Job : SPJobDefinition
    {

        // IMP: Default Constructor for Serialization
        public Job() : base() { }

        public Job(string jobName, JobType type, SPWeb spWeb)
            : base(jobName, spWeb.Site.WebApplication, null, SPJobLockType.Job)
        {
            base.Properties["JobType"] = type;
        }

        public string WebUrl
        {
            get { return (string)base.Properties["SiteUrl"]; }
            set { base.Properties["SiteUrl"] = value; }
        }

        public string LogFile
        {
            get { return (string)base.Properties["LogFile"]; }
            set { base.Properties["LogFile"] = value; }
        }
        public string TempFolder
        {
            get
            {
                string tempFolder = (string)base.Properties["TempFolder"];
                return (string.IsNullOrEmpty(tempFolder)) ? System.IO.Path.GetTempPath() : tempFolder;
            }
            set { base.Properties["TempFolder"] = value; }
        }

        public string Description
        {
            get { return (string)base.Properties["Description"]; }
            set { base.Properties["Description"] = value; }
        }

        internal static JobType Parse(string jobtype)
        {
            if (Enum.IsDefined(typeof(JobType), jobtype))
                return (JobType)Enum.Parse(typeof(JobType), jobtype, true);
            else
                return JobType.Other;
        }

        public JobType Type { get { return (JobType)base.Properties["JobType"]; } }

        public new string Status
        {
            get { return (string)base.Properties["JobStatus"]; }
            set
            {
                base.Properties["JobStatus"] = value;
                _UpdateStatus();
            }
        }

        private void _UpdateStatus()
        {
            try
            {
                SPSite site = null;
                SPWeb web = null;
                try
                {
                    site = new SPSite(this.WebUrl);
                    web = site.OpenWeb();
                    SPList spList = web.Lists["Jobs"];
                    foreach (SPListItem item in spList.Items)
                    {
                        if ((string)item["Job Id"] == this.Id.ToString())
                        {
                            if (_HasField(spList, "Job Status")) item["Job Status"] = this.Status;
                            if (_HasField(spList, "Timestamp")) item["Timestamp"] = DateTime.Now;
                            item.Update();
                        }
                    }
                }
                catch (Exception ex) { Utilities.Log("Exception in UpdateStatus: ", ex); }
                finally
                {
                    if (web != null) web.Dispose();
                    if (site != null) site.Dispose();
                }
            }
            catch (Exception ex) { this.Log("Error in _UpdateStatus.", ex, true, false); }
        }

        private bool _HasField(SPList list, string fieldName)
        {
            try { return (list.Fields[fieldName] != null); }
            catch { return false; }
        }

        #region Job logging
        public void Log(string message, bool isOver, bool InEventvwr)
        {
            if (InEventvwr)
                Utilities.Log("Job : " + this.Title + message);
            message = "\r\n" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss") + "\t" + message;
            if (isOver)
                message += "\r\n---------------------------------------";
            _UpdateLogAttachment(message);
        }

        public void Log(string message, Exception ex, bool isOver, bool InEventvwr)
        {
            if (InEventvwr)
                Utilities.Log("Job : " + this.Title + message, ex);
            message = "\r\n" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss") + "\t" + message;
            while (ex != null)
            {
                message += "\r\n" + ex.Message + ex.StackTrace;
                ex = ex.InnerException;
            }
            if (isOver)
                message += "\r\n---------------------------------------";
            _UpdateLogAttachment(message);
        }

        private void _UpdateLogAttachment(string message)
        {
            try
            {
                SPSite site = new SPSite(this.WebUrl);
                SPWeb web = site.OpenWeb();
                SPListItem JobListItem = null;
                ASCIIEncoding encoding = new ASCIIEncoding();

                SPListItemCollection items = web.Lists["Jobs"].Items;
                foreach (SPListItem item in items)
                {
                    if ((string)item["Job Id"] == this.Id.ToString())
                    {
                        JobListItem = item;
                        break;
                    }
                }
                if (JobListItem == null)
                    return;

                try
                {
                    JobListItem.Attachments.AddNow("Log.txt", encoding.GetBytes(this.Title));
                    JobListItem["Log"] = string.Format(@"<a target=""_blank"" href=""{0}"">View log</a>", JobListItem.Attachments.UrlPrefix + "Log.txt");
                    JobListItem.Update();
                }
                catch (Exception ex) { Utilities.Log("Exception in UpdateLogAttachment method: ", ex); }

                string path = JobListItem.Attachments.UrlPrefix + "Log.txt";
                SPFile file = (SPFile)web.GetObject(path);
                string data = encoding.GetString(file.OpenBinary());
                data += message;

                Stream stream = file.OpenBinaryStream();
                stream.Write(encoding.GetBytes(data), 0, (encoding.GetBytes(data)).Length);
                file.SaveBinary(stream);
                stream.Close();
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
            catch (Exception ex) { Utilities.Log("Exception when trying to Update Log Attachment with message: " + message, ex); }
        }
        #endregion
    }

    public enum JobType { Aggregate, Export, Download, Import, Recall, Purge, Alert, InitialImport, ContentValidator, BlogAggregate, UsageUpload, ReportGenerator, Updater, ContentUpdater, UpdateContentStatus, UsageImport, Other, TagListUpdater, UserProfileUploadJob };

}
