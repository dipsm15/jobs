using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using System.Collections.Specialized;

namespace Indigo.Athena.Jobs
{
    class UpdateContentStatusJob : Job
    {
        public UpdateContentStatusJob() { }      // IMP: Default Constructor for Serialization

        public UpdateContentStatusJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.UpdateContentStatus, spWeb)
        {
        }

        #region Properties
        SPSite site = null;
        SPWeb web = null;
        private const string CONTENT_STATUS_LIST = "Content Status";
        #endregion

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Log("Job Started", false, true);
                this.Status = "Started";
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();

                Dictionary<string, Dictionary<string, DateTime>> dictionaryContentQAReports = new Dictionary<string, Dictionary<string, DateTime>>();
                SPListItemCollection partners = web.Lists["Partners"].Items;
                List<string> partnerList = new List<string>();

                //Clean up
                this.Log("Deleting Content Status items.", false, false);
                this.Status = "Cleaning Content Status List";
                _BulkDelete(CONTENT_STATUS_LIST);
                this.Log("Cleaning Recycle Bin.", false, false);
                this.Status = "Cleaning Recycle Bin";
                _CleanRecycleBin(new string[] { CONTENT_STATUS_LIST });

                //Recreate PartnerReport List
                this.Log("Re populating Content Status List.", false, false);
                this.Status = "Populating Content Status List";
                _RegenerateListContent();

                //Create the dictionary containing partner name and content guid's
                //Create the partner list
                this.Log("Generating Dictionary and verifying columns.", false, false);
                this.Status = "Generating Dictionary and verifying columns";
                foreach (SPListItem partner in partners)
                {
                    string partnerName = _GetValue(partner, "LinkTitle");
                    if (partnerName.ToLower() == "default" || string.IsNullOrEmpty(partnerName))
                        continue;
                    dictionaryContentQAReports.Add(partnerName, _ContentQAReportToList(partnerName));
                    partnerList.Add(partnerName);
                    //Verify if all partner columns exist, if not create
                    _VerifyColumns(partnerName);
                }

                //Update the Content Status Report Items
                this.Log("Updating Partner Content Status.", false, false);
                this.Status = "Updating Content Status List";
                web.AllowUnsafeUpdates = true;
                SPListItemCollection contentStatusReportItems = web.Lists[CONTENT_STATUS_LIST].Items;
                foreach (SPListItem contentStatusReportItem in contentStatusReportItems)
                {
                    string guid = Convert.ToString(contentStatusReportItem["ContentGuid"]);
                    foreach (string partner in partnerList)
                    {
                        if (dictionaryContentQAReports[partner].ContainsKey(guid))
                        {
                            contentStatusReportItem[partner] = true;
                            contentStatusReportItem[string.Format("DateAdded({0})", partner)] = dictionaryContentQAReports[partner][guid];
                        }
                        else
                            contentStatusReportItem[partner] = false;
                    }
                    contentStatusReportItem.Update();
                }
                web.AllowUnsafeUpdates = false;
                this.Log("Job Completed Successfully.", true, true);
                this.Status = "Completed";
            }
            catch (Exception ex)
            {
                this.Status = "Failed";
                this.Log("Error in Job.", ex, true, true);
            }
            finally
            {
                if (web != null)
                {
                    web.AllowUnsafeUpdates = false;
                    web.Dispose();
                }
                if (site != null)
                    site.Dispose();
            }
        }
        #endregion

        #region Helper Function
        //Finds the item that was added recently(latest Timestamp) in the list, based on partner name 
        private SPListItem _GetReportItem(string partner)
        {
            NameValueCollection criteria = new NameValueCollection();
            criteria.Add("Partner", partner);
            SPListItemCollection items = SPListLib.GetItems("Content Reports", string.Empty, criteria, web);
            SPListItem item = null;
            DateTime dateTime = DateTime.MinValue;
            for (int i = 0; i < items.Count; i++)
            {
                try
                {
                    DateTime current = DateTime.Parse(Convert.ToString(items[i]["Timestamp"]));
                    if (current > dateTime && _AttachmentExists(items[i]))
                    {
                        dateTime = current;
                        item = items[i];
                    };
                }
                catch
                { }
            }
            return item;
        }
        //Gives a list of string of guids present
        private Dictionary<string, DateTime> _ContentQAReportToList(string partner)
        {
            SPListItem contentReportItem = _GetReportItem(partner);
            Dictionary<string, DateTime> report = new Dictionary<string, DateTime>();
            if (contentReportItem == null)
                return report;
            ASCIIEncoding encoding = new ASCIIEncoding();
            //Get file
            SPFile file = (SPFile)(web.GetFile(contentReportItem.Attachments.UrlPrefix + contentReportItem.Attachments[0]));
            string doc = encoding.GetString(file.OpenBinary());
            string[] elements = doc.Split(System.Environment.NewLine.ToCharArray());

            //Get column index of "GUID"
            string[] headings = elements[0].Split(',');
            int guidCsvIndex = -1, createdOn = -1;
            for (int i = 0; i < headings.Length; i++)
            {
                if (headings[i].ToLower() == "guid")
                    guidCsvIndex = i;
                if (headings[i].ToLower() == "created on")
                    createdOn = i;
            }

            //Generate guid list
            foreach (string element in elements)
            {
                if ((element.ToLower().StartsWith("webcast") || element.ToLower().StartsWith("podcast") || element.ToLower().StartsWith("hands on labs") || element.ToLower().StartsWith("other content")))
                    report.Add(Convert.ToString(element.Split(',')[guidCsvIndex]), DateTime.Parse(element.Split(',')[createdOn]));
            }
            return report;
        }
        //Checks if partner(Infi, Satyam etc) column's exists, and if not then adds one to PartnerReport
        private void _VerifyColumns(string partner)
        {
            SPList contentStatusReport = web.Lists[CONTENT_STATUS_LIST];
            if (!contentStatusReport.Fields.ContainsField(partner))
                _AddColumn(partner, SPFieldType.Boolean, Convert.ToString(0));
            if (!contentStatusReport.Fields.ContainsField(string.Format("DateAdded({0})", partner)))
                _AddColumn(string.Format("DateAdded({0})", partner), SPFieldType.DateTime, string.Empty);
        }
        private void _AddColumn(string columnName, SPFieldType type, string defaultValue)
        {
            web.AllowUnsafeUpdates = true;
            SPList contentStatusReport = web.Lists[CONTENT_STATUS_LIST];

            contentStatusReport.Fields.Add(columnName, type, false);
            contentStatusReport.Update();
            contentStatusReport.Fields[columnName].DefaultValue = defaultValue;
            contentStatusReport.Fields[columnName].Update();
            SPView view = contentStatusReport.Views["All Items"];
            view.ViewFields.Add(contentStatusReport.Fields[columnName]);
            view.Update();
            this.Log("Column added : " + columnName, false, false);

            web.AllowUnsafeUpdates = false;
        }
        //Gets value of the specified key of the item
        private string _GetValue(SPListItem item, string key)
        {
            try
            {
                if (!key.Equals("LinkTitle", StringComparison.InvariantCultureIgnoreCase) && !key.Equals("_Author", StringComparison.InvariantCultureIgnoreCase) && (item.Fields[key].Type == SPFieldType.Lookup || item.Fields[key].Type == SPFieldType.User))
                    return _GetFieldLookupValue(item, key);
                else
                    return item[key].ToString();
            }
            catch { return string.Empty; }
        }
        private string _GetFieldLookupValue(SPListItem item, string key)
        {
            try
            {
                SPFieldLookupValue lookupVal = new SPFieldLookupValue(item[key].ToString());
                return lookupVal.LookupValue;
            }
            catch { return string.Empty; }
        }
        //Re-populate List
        private void _RegenerateListContent()
        {
            web.AllowUnsafeUpdates = true;
            SPListItemCollection itemsPartnerReport = web.Lists[CONTENT_STATUS_LIST].Items;
            string[] lists = new string[] { "Webcasts", "Podcasts", "Hands on Labs", "Other Content" };
            foreach (string list in lists)
            {
                SPListItemCollection itemsList = web.Lists[list].Items;
                this.Log("Total " + list + " at " + web.Url + " : " + itemsList.Count, false, false);
                foreach (SPListItem item in itemsList)
                {
                    try
                    {
                        SPListItem newItemPartnerReport = itemsPartnerReport.Add();
                        newItemPartnerReport["Title"] = _GetValue(item, "LinkTitle");
                        newItemPartnerReport["ListType"] = list;

                        if (!string.IsNullOrEmpty(Convert.ToString(item["DateContentCreated"])))
                            newItemPartnerReport["DateContentCreated"] = DateTime.Parse(_GetValue(item, "DateContentCreated"));

                        if (item.ParentList.Title.ToLower() == "webcasts")
                            newItemPartnerReport["IsInteractive"] = Convert.ToBoolean(item["IsInteractive"]);

                        newItemPartnerReport["ContentGuid"] = item.UniqueId.ToString();
                        newItemPartnerReport["ContentCategory"] = Convert.ToString(item["ContentCategory"]);
                        newItemPartnerReport.Update();
                    }
                    catch (Exception ex)
                    { this.Log("Error updating item : " + item.Title, ex, false, false); }
                }
            }
            web.AllowUnsafeUpdates = false;
        }
        //Delete List Content Complete
        private void _BulkDelete(String listName)
        {
            web.AllowUnsafeUpdates = true;
            SPListItemCollection collection = web.Lists[listName].Items;
            this.Log("Entries present in " + listName + " : " + collection.Count, false, false);
            if (collection.Count == 0)
                return;
            StringBuilder sbDelete = new StringBuilder();
            sbDelete.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><Batch>");
            foreach (SPListItem item in collection)
            {
                sbDelete.Append("<Method>");
                sbDelete.Append("<SetList Scope=\"Request\">" + item.ParentList.ID + "</SetList>");
                sbDelete.Append("<SetVar Name=\"ID\">" + Convert.ToString(item.ID) + "</SetVar>");
                sbDelete.Append("<SetVar Name=\"Cmd\">Delete</SetVar>");
                sbDelete.Append("</Method>");
            }
            sbDelete.Append("</Batch>");
            web.ProcessBatchData(sbDelete.ToString());
            web.AllowUnsafeUpdates = false;
        }
        //Recycle bin cleanup
        private void _CleanRecycleBin(string[] listnames)
        {
            web.AllowUnsafeUpdates = true;
            List<string> dirName = new List<string>();
            foreach (string listname in listnames)
                dirName.Add("mcp/lists/" + listname.ToLower());

            SPRecycleBinItemCollection collection = web.RecycleBin;
            List<Guid> guid = new List<Guid>();
            for (int i = collection.Count - 1; i >= 0; i--)
                if (dirName.Contains(collection[i].DirName.ToLower()))
                    guid.Add(collection[i].ID);

            this.Log("Entries to be deleted : " + guid.Count, false, false);
            web.RecycleBin.Delete(guid.ToArray());
            web.AllowUnsafeUpdates = false;
        }
        //Verify attachment exists
        private Boolean _AttachmentExists(SPListItem item)
        {
            try
            {
                SPFile file = (SPFile)web.GetFile(item.Attachments.UrlPrefix + item.Attachments[0]);
                return true;
            }
            catch
            { return false; }
        }
        #endregion
    }
}
