using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Deployment;
using System.IO;
using Indigo.Athena.Synchronization;
using System.Net;
using System.Collections.Specialized;
using System.Drawing;
using DexterLib;
using System.Drawing.Drawing2D;

namespace Indigo.Athena.Jobs
{
    public class ContentUpdater : Job
    {
        private SPWeb web;
        private SPSite site;
        public ContentUpdater() { }      // IMP: Default Constructor for Serialization

        public ContentUpdater(string jobName, SPWeb spWeb)
            : base(jobName, JobType.ContentUpdater, spWeb)
        {
        }

        #region Execute
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                this.Status = "Started Updating";
                this.Log("Job started...", false, true);
                site = new SPSite(this.WebUrl);
                web = site.OpenWeb();

                string[] listName = new string[] { "Webcasts", "Podcasts", "Hands on Labs", "Other Content" };
                foreach (string list in listName)
                {
                    SPListItemCollection collection = web.Lists[list].Items;
                    int currentCount = 0;
                    foreach (SPListItem item in collection)
                    {
                        this.Status = string.Format("Updating {0} ({1}/{2})", list, ++currentCount, collection.Count);
                        string file = string.Empty;
                        foreach (string attachment in item.Attachments)
                        {
                            if (!attachment.ToLower().Contains("thumbnail") && !attachment.ToLower().Contains("screenshot"))
                            {
                                file = item.Attachments.UrlPrefix + attachment;
                                break;
                            }
                        }
                        if (!string.IsNullOrEmpty(file))
                        {
                            _UpdateMetaData(item, file);
                            GenerateThumbnail(item, file);
                        }
                    }
                }

                this.Status = "Completed";
                this.Log("Content Updater Job completed successfully", false, true);
            }
            catch (Exception ex)
            {
                this.Log("Content Updater Job failed with following error " + ex.ToString(), true, true);
            }
            finally
            {
                if (web != null) web.Dispose();
                if (site != null) site.Dispose();
            }
        }
        #endregion


        #region HelperFunctions
        private void SaveField(SPListItem item, string key, object value)
        {
            try
            {
                long _value = long.MinValue;
                _value = string.IsNullOrEmpty(Convert.ToString(item[key])) ? 0 : long.Parse(item[key].ToString());
                if (_value != (long)value)
                {
                    item[key] = value;
                    item.Update();
                }
            }
            catch
            { }
        }

        private void _UpdateMetaData(SPListItem item, string url)
        {
            try
            {
                string filePath = SPListLib.GetBlobFile(url);
                if (File.Exists(filePath))
                {
                    SaveField(item, "MediaSize", new FileInfo(filePath).Length);
                    if (url.ToLower().EndsWith("wmv") || url.ToLower().EndsWith("wma") || url.ToLower().EndsWith("mp3"))
                    {
                        MediaDetClass md = new MediaDetClass();
                        md.Filename = filePath;
                        md.CurrentStream = 0;
                        SaveField(item, "MediaDuration", (long)md.StreamLength);
                    }
                }
            }
            catch (Exception e)
            {
                this.Log(string.Format("Error updating meta data of : {0}", Convert.ToString(item["LinkTitle"])), e, false, false);
            }
        }

        public void GenerateThumbnail(SPListItem item, string url)
        {
            try
            {
                if (!url.ToLower().EndsWith("wmv"))
                    return;
                string filePath = SPListLib.GetBlobFile(url);
                if (File.Exists(filePath))
                {
                    string bmpFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".bmp");
                    string screenshot = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".jpg");
                    string thumbnail = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".jpg");
                    MediaDetClass md = new MediaDetClass();
                    md.Filename = filePath;
                    md.CurrentStream = 0;
                    if (item.Attachments.Count != 3)
                    {
                        this.Log("Creating Thumbnails for " + item["Title"].ToString(), false, true);
                        md.WriteBitmapBits(2, 800, 600, bmpFile);
                        Image img = Image.FromFile(bmpFile);
                        img.Save(screenshot, System.Drawing.Imaging.ImageFormat.Jpeg);
                        FileStream screenshotStream = File.OpenRead(screenshot);
                        byte[] screenshotByte = new byte[screenshotStream.Length];
                        screenshotStream.Read(screenshotByte, 0, (int)screenshotStream.Length);
                        screenshotStream.Close();
                        try
                        {
                            item.Attachments.DeleteNow("Screenshot");
                        }
                        catch { }
                        item.Attachments.AddNow("Screenshot", screenshotByte);

                        Size size = img.Size;
                        int required_width = 112;
                        double percentage = (double)required_width / (double)size.Width;
                        Image resizedImage = Resize(img, percentage);
                        resizedImage.Save(thumbnail, System.Drawing.Imaging.ImageFormat.Jpeg);
                        FileStream fStream = File.OpenRead(thumbnail);
                        byte[] thumbnailByte = new byte[fStream.Length];
                        fStream.Read(thumbnailByte, 0, (int)fStream.Length);
                        fStream.Close();
                        try
                        {
                            item.Attachments.DeleteNow("Thumbnail");
                        }
                        catch { }
                        item.Attachments.AddNow("Thumbnail", thumbnailByte);
                        this.Log("Thumbnails created succesfully for " + item["Title"].ToString(), false, true);
                    }
                }
                else
                {
                    throw new FileNotFoundException(filePath + " not found");
                }
            }
            catch (Exception ex)
            {
                this.Log(item["Title"].ToString() + " threw an error while creating Thumbnail. " + ex.Message, false, false);
            }
        }

        public Image Resize(Image img, double percentage)
        {
            //get the height and width of the image
            int originalW = img.Width;
            int originalH = img.Height;

            //get the new size based on the percentage change
            int resizedW = (int)(originalW * percentage);
            int resizedH = (int)(originalH * percentage);

            //create a new Bitmap the size of the new image
            Bitmap bmp = new Bitmap(resizedW, resizedH);
            //create a new graphic from the Bitmap
            Graphics graphic = Graphics.FromImage((Image)bmp);
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //draw the newly resized image
            graphic.DrawImage(img, 0, 0, resizedW, resizedH);
            //dispose and free up the resources
            graphic.Dispose();
            //return the image
            return (Image)bmp;
        }
        #endregion


    }
}
