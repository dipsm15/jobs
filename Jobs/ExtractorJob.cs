using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Indigo.Athena.Acquisition;

namespace Indigo.Athena.Jobs
{
    public class ExtractorJob : Job
    {
        public ExtractorJob() : base() { }      // IMP: Default Constructor for Serialization
        public ExtractorJob(string jobName, SPWeb spWeb)
            : base(jobName, JobType.Aggregate, spWeb)
        {
        }

        #region Properties
        [Persisted]
        public string SourceUrl = string.Empty;
        [Persisted]
        public string SourceName = string.Empty;
        private string HandlerType = "Indigo.Athena.Acquisition.Extractors.RSSExtractor";
        private string HandlerAssemply = "Indigo.Athena, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cc04cc67034428b7";
        #endregion

        public override void Execute(Guid targetInstanceId)
        {
            SPSite site = null;
            SPWeb web = null;
            try
            {
                this.Status = "Extracting";
                this.Log("Job started...", false,true);
                string assemblyName = this.HandlerAssemply;                 // "Indigo.Athena, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cc04cc67034428b7"
                string type = this.HandlerType;                             // "Indigo.Athena.Acquisition.Extractors.RSSExtractor";
                string sourceUrl = this.SourceUrl;                          // "http://athena/media/athenacontent.xml"; "http://channel9.msdn.com/Feeds/RSS/";
                string sourceName = this.SourceName;
                //Source type taken care in RssExtractor if its a Webcast,podcast,Hands on Labs,Whitepapers.
                MediaType sourceType = MediaType.Unknown;
                string siteUrl = this.WebUrl;                               // "http://Eridanus/Athena";

                System.Reflection.Assembly assembly = System.Reflection.Assembly.Load(assemblyName);
                IContentExtractor extractor = (IContentExtractor)Activator.CreateInstance(assembly.GetType(type));

                extractor.Initalize(sourceUrl, sourceType, null);
                ContentInfo[] allExtractedItems = extractor.GetMetadata();
                
                // Get a ref to SPWeb
                site = new SPSite(siteUrl);
                web = site.OpenWeb();

                if (string.IsNullOrEmpty(sourceName)) sourceName = web.CurrentUser.Name;
                foreach (ContentInfo c in allExtractedItems)
                {
                    c.LocalPath = this.TempFolder;
                    c.Source = sourceName;
                }

                int itemCount = 0;
                foreach (ContentInfo c in allExtractedItems)
                {
                    this.UpdateProgress((int)(100 * (++itemCount / allExtractedItems.Length)));
                    this.Status = string.Format("Extracting ({0}/{1})", itemCount, allExtractedItems.Length);
                    // Imports only Labs, PodCasts, Webcasts (Videos), Whitepapers (Articles)
                    if (c.MediaType != MediaType.HandsOnLabs && c.MediaType != MediaType.Podcast
                        && c.MediaType != MediaType.Webcast && c.MediaType != MediaType.Whitepapers)
                        continue;
                    try
                    {
                        this.Log(string.Format("Extracting ({0}/{1}) {2}...", itemCount, allExtractedItems.Length, c.Title), false, false);
                        if (c.IsNew(web))
                            c.Save(web);
                    }
                    catch (Exception e)
                    {
                        this.Log(string.Format("Error in {0}", c.Title), e, false,true);
                    }                    
                }
                this.Status = "Completed";
                this.Log("Job Completed Successfully...", true,true);
            }
            catch (Exception ex)
            {
                this.Log("Error in Job...", ex, true,true);
                this.Status = "Failed";
            }
            finally
            {
                // Release resources
                if (site != null) site.Dispose();
                if (web != null) web.Dispose();
            }
        }
    }
}
